<?php

namespace App\Exceptions;

use Exception;

class DataNotFoundException extends Exception
{
    protected $message;
    protected $statusCode;

    public function __construct($message = "Data not found", $statusCode = 404)
    {
        $this->message = $message;
        $this->statusCode = $statusCode;
        parent::__construct($message, $statusCode);
    }

    public function render($request)
    {
        return response()->default(
            400,
            false, 
            $this->message,
            null
        )->setStatusCode(400);
    }
}