<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\Builder as Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;

class AppServiceProvider extends ServiceProvider {
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        Response::macro('default', function (int $code, bool $success, string $message, array|null|object|string $data) {
            return Response::make(
                [
                    'code' => $code,
                    'success' => $success,
                    'message' => $message,
                    'data' => $data,
                ]
            );
        });
/*
        // Define the 'whereLike' macro
        Builder::macro('whereLike', function (string $searchTerms) {
            return $this->where(function (Builder $query) use ($searchTerms) {
                foreach (explode('&', $searchTerms) as $searchTerm) {
                    dump($searchTerm);
                    $attr=explode('~', $searchTerm);
                    /*
                    $query->when(
                        // Check if the attribute is not an expression and contains a dot (indicating a related model)
                       /* ! ($attribute instanceof \Illuminate\Contracts\Database\Query\Expression) &&
                        str_contains((string) $attribute, '.'),
                        function (Builder $query) use ($attribute, $searchTerm) {
                            // Split the attribute into a relation and related attribute
                            [$relation, $relatedAttribute] = explode('.', (string) $attribute);

                            // Perform a 'LIKE' search on the related model's attribute
                            $query->orWhereHas($relation, function (Builder $query) use ($relatedAttribute, $searchTerm) {
                                $query->where($relatedAttribute, 'LIKE', "%{$searchTerm}%");
                            });

                            // if need more deep nesting then commonet above code and 
                            // use below (which is not recommend)
                            // Split the attribute into a relation and related attribute
                            // $attrs = explode('.', (string) $attribute);
                            // $relatedAttribute = array_pop($attrs);
                            // $relation = implode('.', $attrs);

                            // Perform a 'LIKE' search on the related model's attribute
                            // $query->orWhereRelation($relation, $relatedAttribute, 'LIKE', "%{$searchTerm}%");
                        },
                        
                    );
                    
                    dump($attr[0], $attr[1]);
                    $query->orWhere($attr[0], 'LIKE', "%{$attr[1]}%");
                }
                //dd($searchTerms);
                $query->ddRawSql();
            });
        });*/

        Builder::macro('whereLike', function (string $searchTerms) {
            foreach (explode('&', $searchTerms) as $searchTerm) {
                $attr=explode('~', $searchTerm);
        
                if (count($attr) != 2) {
                    continue; // Skip if the sort term is not properly formatted
                }
                
                $column = $attr[0];
                $value = $attr[1];

                if($column == 'status'){
                    $this->where($column, '=', $value);
                }
                else{
                    $this->where($column, 'LIKE', "%{$value}%");
                }
                //$i++;
            }
            
            //dd($this->ddRawSql());
            return $this;
        });

        Builder::macro('sortBy', function (string|null $sortTerms, array $allowedColumns = []) {
            // Add default order by
            $sortTerms??$this->orderBy('created_date', 'desc');
        
            if (isset($sortTerms)) {
                foreach (explode('&', $sortTerms) as $sortTerm) {
                    $attr = explode('~', $sortTerm);
        
                    // Skip if the sort term is not properly formatted
                    if (count($attr) != 2) {
                        continue;
                    }
        
                    $column = $attr[0];
                    $direction = strtolower($attr[1]) == 'asc' ? 'asc' : 'desc';

                    // Validate if the column is in the allowed list
                    if (!in_array($column, $allowedColumns)) {
                        throw new \InvalidArgumentException("Invalid sort column: {$column}");
                    }
        
                    $this->orderBy($column, $direction);
                }
            }
            //dd($this);
            return $this;
        });
        
    }
}
