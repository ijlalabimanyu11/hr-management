<?php

// app/Providers/QueryServiceProvider.php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\QueryBuilder\GenericQueryService;

class QueryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(GenericQueryService::class, function ($app) {
            return new GenericQueryService($app->make('db'));
        });
    }
}