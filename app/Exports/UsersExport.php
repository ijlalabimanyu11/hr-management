<?php

namespace App\Exports;

use App\Models\User;
use App\Models\Employee;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data=User::get();
        foreach($data as $k=>$datas)
        {
            //$data[$k]["employee_id"]=Employee::employee_name($datas->employee_id);
            //$data[$k]["created_by"]=Employee::login_user($datas->created_by);
            //unset($datas->created_at,$datas->modified_at);
        }
        return $data;
    }
    public function headings(): array
    {
        return [
            "ID",
            "employee",
            "Asset Name",
            "Purchase Date",
            "Supported Date",
            "Amount",
            "Description",
            "Created By"
        ];
    }
}
