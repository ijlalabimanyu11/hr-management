<?php

// app/Traits/HasQueryService.php
namespace App\Traits;

use App\Services\QueryBuilder\GenericQueryService;
use Illuminate\Database\Eloquent\Model;

trait HasQueryService
{
    protected GenericQueryService $queryService;

    protected function getPaging(
        $data, $isPaging=true
    ) {
        $this->queryService = new GenericQueryService();
        
        return $this->queryService->applyFiltersAndPagination($data, $isPaging);
    }
}