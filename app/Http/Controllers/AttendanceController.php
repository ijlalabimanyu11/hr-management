<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Attendance;
use App\Models\Employee;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\GlobalTypeDetail;
use App\Traits\HasQueryService;

class AttendanceController extends Controller
{
    use HasQueryService;

    public function attendance(Request $request)
    {
        $creds = $request->validate([
            'employee_id' => 'required',
        ]);
        $employee = Employee::find($request->employee_id);
        if(!$employee) {
            return response()->default(
                404,
                false,
                "Employee not found!",
                null
            )->setStatusCode(404);
        }

        $attendance;
        if($request->action == 'IN') {
            $cekIn = Attendance::where('employee_id', $request->employee_id)
            ->where('status', 'IN')
            ->exists();
            if($cekIn) {
                return response()->default(
                    400,
                    false,
                    "This employee has been Clock In Attendance",
                    null
                )->setStatusCode(400);
            }

            $attendance = Attendance::create([
                'employee_id' => $request->employee_id,
                'date' => now()->format('Y-m-d'),
                'start_time' => now(),
                'status' => 'IN',
                'created_by' => $request->user()->username
            ]);
        } else if ($request->action == 'OUT') {
            $creds = $request->validate([
                'activity' => 'required',
                'notes' => 'required',
            ]);

            $cekOut = Attendance::where('employee_id', $request->employee_id)
            ->where('status', 'OUT')
            ->exists();
            if($cekOut) {
                return response()->default(
                    400,
                    false,
                    "This employee has been Clock Out Attendance",
                    null
                )->setStatusCode(400);
            }

            $attendance = Attendance::where('employee_id', $request->employee_id)
            ->where('status', 'IN')
            ->first();
            if(!$attendance) {
                return response()->default(
                    400,
                    false,
                    "There's no Clock In Attendance data in this employee",
                    null
                )->setStatusCode(400);
            }
            $attendance->activity = $request->activity;
            $attendance->notes = $request->notes;
            $attendance->end_time = now();
            $attendance->status = null;
            $attendance->modified_by = $request->user()->username;
            $attendance->update();
        } else {
            return response()->default(
                400,
                false,
                "There's no Clock In/Out Attendance data in this employee",
                null
            )->setStatusCode(400);
        }

        return response()->default(
            200,
            true,
            "Success Clock ". $request->action=='IN' ? 'IN' : 'OUT',
            $attendance
        )->setStatusCode(200);
    }

    public function showAttendance($employee_id)
    {
        $attendance = Attendance::where('employee_id', $employee_id)
        ->whereNotIn('status', ['Waiting Approval', 'APPROVED', 'REJECTED'])
        ->orderBy('created_date', 'desc')
        ->first();

        return response()->default(
            200,
            true,
            "Success get attendance employee",
            $attendance
        )->setStatusCode(200);
    }

    public function index() {
        try {
            $subQuery = DB::table(Attendance::getTableName())
        ->leftJoin(Employee::getTableName(), Attendance::getTableName().'.employee_id', '=', Employee::getTableName().'.id')
        ->select(
            Attendance::getTableName().'.id',
            Employee::getTableName().'.employee_code',
            Employee::getTableName().'.name',
            Attendance::getTableName().'.date',
            Attendance::getTableName().'.start_time',
            Attendance::getTableName().'.end_time',
            Attendance::getTableName().'.activity',
            Attendance::getTableName().'.notes',
            Attendance::getTableName().'.correction_cause',
            Attendance::getTableName().'.status',
            Attendance::getTableName().'.created_date',
            Attendance::getTableName().'.created_by',
            Attendance::getTableName().'.modified_date',
            Attendance::getTableName().'.modified_by'
        );

            // Call getPaging, which internally calls applyFiltersAndPagination
            $data = $this->getPaging($subQuery);
            return response()->default(200, true, 'Success get data Attendance', $data);
        } catch (\InvalidArgumentException $e) {
            // Catch specific InvalidArgumentException thrown by sortBy macro
            return response()->default(400, false, $e->getMessage(), null);
        } 
        catch (\Exception $e) {
            // Catch general exceptions and return a 500 response
            return response()->default(500, false, 'An unexpected error occurred.', null);
        }
    }

    public function show($id) 
    {
        $timesheet = Attendance::find($id);
        if(!$timesheet) {
            return response()->default(
                404,
                false,
                "Timesheet correction not found!",
                null
            )->setStatusCode(404);
        }

        $employee = Employee::find($timesheet->employee_id);
        $correction_cause_id = 
            $timesheet->correction_cause != null ?
            GlobalTypeDetail::where('value', $timesheet->correction_cause)->first() :
            null;

        $response = [
            'id' => $timesheet->id,
            'employee_id' => $employee->id,
            'employee_code' => $employee->employee_code,
            'employee_name' => $employee->name,
            'date' => $timesheet->date,
            'start_time' => $timesheet->start_time,
            'end_time' => $timesheet->end_time,
            'activity' => $timesheet->activity,
            'notes' => $timesheet->notes,
            'correction_cause_id' => $correction_cause_id !=null? $correction_cause_id->id :null,
            'correction_cause' => $timesheet->correction_cause,
            'status' => $timesheet->status,
            'created_date' => $timesheet->created_date,
            'created_by' => $timesheet->created_by,
            'modified_date' => $timesheet->modified_date,
            'modified_by' => $timesheet->modified_by
        ];

        return response()->default(
            200,
            true,
            "Success get detail timesheet correction",
            $response
        )->setStatusCode(200);
    }

    public function store(Request $request)
    {
        $creds = $request->validate([
            'employee_id' => 'required',
            'start_time' => ['required', 'date', function ($attribute, $value, $fail) {
                // Ensure start_time is not older than 7 days from today
                if (Carbon::parse($value)->lt(now()->subDays(7))) {
                    $fail('Start time must not be earlier than 7 days ago.');
                }
                // Ensure start_time is not a future date
                if (Carbon::parse($value)->gt(now())) {
                    $fail('Start time cannot be a future date.');
                }
            }],
            'end_time' => 'required|date|after:start_time',
            'activity' => 'required',
            'notes' => 'required',
            'correction_cause' => 'required',
        ]);

        $employee = Employee::find($request->employee_id);
        if(!$employee) {
            return response()->default(
                404,
                false,
                "Employee not found!",
                null
            )->setStatusCode(404);
        }

        $timesheet = Attendance::create([
            'employee_id' => $request->employee_id,
            'correction_cause' => $request->correction_cause,
            'date' => $request->date,
            'start_time' => $request->start_time,
            'end_time' => $request->end_time,
            'activity' => $request->activity,
            'notes' => $request->notes,
            'status' => "Waiting Approval",
            // APPROVAL
            'created_by' => $request->user()->username
        ]);

        return response()->default(
            201,
            true,
            "Timesheet correction has been created",
            $timesheet
        )->setStatusCode(201);

    }

    public function update(Request $request, $id)
    {
        $creds = $request->validate([
            'employee_id' => 'required',
            'start_time' => ['required', 'date', function ($attribute, $value, $fail) {
                // Ensure start_time is not older than 7 days from today
                if (Carbon::parse($value)->lt(now()->subDays(7))) {
                    $fail('Start time must not be earlier than 7 days ago.');
                }
                // Ensure start_time is not a future date
                if (Carbon::parse($value)->gt(now())) {
                    $fail('Start time cannot be a future date.');
                }
            }],
            'end_time' => 'required|date|after:start_time',
            'activity' => 'required',
            'notes' => 'required',
            'correction_cause' => 'required',
        ]);

        $timesheet = Attendance::find($id);
        if(!$timesheet) {
            return response()->default(
                404,
                false,
                "Timesheet correction not found!",
                null
            )->setStatusCode(404);
        }

        $employee = Employee::find($request->employee_id);
        if(!$employee) {
            return response()->default(
                404,
                false,
                "Employee not found!",
                null
            )->setStatusCode(404);
        }

        if($timesheet->status == 'APPROVED' || $timesheet->status == 'REJECTED') {
            return response()->default(
                400,
                false,
                "You cannot update data, because this timesheet correction has been ".$timesheet->status."!",
                null
            )->setStatusCode(400);
        }

        $timesheet->employee_id = $request->employee_id;
        $timesheet->date = $request->date;
        $timesheet->start_time = $request->start_time;
        $timesheet->end_time = $request->end_time;
        $timesheet->correction_cause = $request->correction_cause;
        $timesheet->activity = $request->activity;
        $timesheet->notes = $request->notes;
        $timesheet->modified_by = $request->user()->username;
        $timesheet->update();

        return response()->default(
            200,
            true,
            "Timesheet correction has been updated",
            $timesheet
        )->setStatusCode(200);

    }

    public function destroy($id)
    {
        $timesheet = Attendance::find($id);
        if(!$timesheet) {
            return response()->default(
                404,
                false,
                "Timesheet correction not found!",
                null
            )->setStatusCode(404);
        }

        if($timesheet->status == 'APPROVED' || $timesheet->status == 'REJECTED') {
            return response()->default(
                400,
                false,
                "You cannot delete data, because this timesheet correction has been ".$timesheet->status."!",
                null
            )->setStatusCode(400);
        }

        $timesheet->delete();

        return response()->default(
            200,
            true,
            "Timesheet correction has beed deleted",
            null
        )->setStatusCode(200);
    }
    
}
