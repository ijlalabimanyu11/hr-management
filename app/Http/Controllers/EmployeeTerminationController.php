<?php

namespace App\Http\Controllers;

use App\Models\EmployeeTermination;
use Illuminate\Http\Request;
use App\Models\Employee;
use Illuminate\Support\Facades\DB;
use Storage;
use App\Traits\HasQueryService;

class EmployeeTerminationController extends Controller
{
    use HasQueryService;
    /**
     * Display a listing of the resource.
     */
    public function index() {
        try {
            $subQuery = DB::table(EmployeeTermination::getTableName())
        ->leftJoin(Employee::getTableName(), EmployeeTermination::getTableName().'.employee_id', '=', Employee::getTableName().'.id')
        ->select(
            EmployeeTermination::getTableName().'.id',
            EmployeeTermination::getTableName().'.employee_id',
            Employee::getTableName().'.employee_code',
            Employee::getTableName().'.name',
            EmployeeTermination::getTableName().'.submission_date',
            EmployeeTermination::getTableName().'.approved_date',
            EmployeeTermination::getTableName().'.last_working_date',
            EmployeeTermination::getTableName().'.reason',
            EmployeeTermination::getTableName().'.attachment',
            EmployeeTermination::getTableName().'.status',
            EmployeeTermination::getTableName().'.created_date',
            EmployeeTermination::getTableName().'.created_by',
            EmployeeTermination::getTableName().'.modified_date',
            EmployeeTermination::getTableName().'.modified_by',
        );

            // Call getPaging, which internally calls applyFiltersAndPagination
            $data = $this->getPaging($subQuery);
            return response()->default(200, true, 'Get Employee Termination Header Success', $data);
        } catch (\InvalidArgumentException $e) {
            // Catch specific InvalidArgumentException thrown by sortBy macro
            return response()->default(400, false, $e->getMessage(), null);
        } 
        catch (\Exception $e) {
            // Catch general exceptions and return a 500 response
            return response()->default(500, false, 'An unexpected error occurred.', null);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $creds = $request->validate([
            'employee_id' => 'required',
            'submission_date' => 'required|date',
            'attachment' => 'required|file|mimes:PDF,pdf|max:2048'
        ]);

        $employee = Employee::find($request->employee_id);
        if(!$employee) {
            return response()->default(
                404,
                false,
                "Employee not found!",
                null
            )->setStatusCode(404);
        } 
        
        // UPLOAD DOCUMENT RESIGN
        $em_terminate = $request->file('attachment');
        $em_terminate_name = $employee->employee_code.'-'.round(microtime(true) * 1000).'.'.$em_terminate->getClientOriginalExtension();
        $em_terminate->storeAs('assets/employee/termination', $em_terminate_name);

        $EmployeeTermination = EmployeeTermination::create([
            'employee_id' => $request->employee_id,
            'submission_date' => $request->submission_date,
            'last_working_date' => $request->last_working_date,
            'reason' => $request->reason,
            'attachment' => $em_terminate_name,
            'status' => 'WAITING_APPROVAL',
            'created_by' => $request->user()->username,
            'created_date' => now()
        ]);

        return response()->default(
            201,
            true,
            "Employee termination has been created",
            $EmployeeTermination
        )->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $EmployeeTermination = EmployeeTermination::find($id);

        if(!$EmployeeTermination) {
            return response()->default(
                404,
                false,
                "Employee termination not found!",
                null
            )->setStatusCode(404);
        }

        $employee = Employee::find($EmployeeTermination->employee_id);

        $response = [
            'id' => $EmployeeTermination->id,
            'employee_id' => $employee->id,
            'employee_code' => $employee->employee_code,
            'employee_name' => $employee->name,
            'submission_date' => $EmployeeTermination->submission_date,
            'approved_date' => $EmployeeTermination->approved_date,
            'last_working_date' => $EmployeeTermination->last_working_date,
            'reason' => $EmployeeTermination->reason,
            'attachment' => $EmployeeTermination->attachment,
            'status' => $EmployeeTermination->status,
            'created_date' => $EmployeeTermination->created_date,
            'created_by' => $EmployeeTermination->created_by,
            'modified_date' => $EmployeeTermination->modified_date,
            'modified_by' => $EmployeeTermination->modified_by
        ];

        return response()->default(
            200,
            true,
            "Success get detail employee termination",
            $response
        )->setStatusCode(200);
       
    }

    public function downloadAttachment($id) {
        $EmployeeTermination = EmployeeTermination::find($id);
        //dd($EmployeeTermination->attachment);
        $filePath = storage_path("app/assets/employee/termination/{$EmployeeTermination->attachment}");
        
        if (file_exists($filePath)) {
            //return response()->download($filePath, $EmployeeTermination->attachment);
            return response()->file($filePath, [
                'Content-Disposition' => 'attachment; filename='.$EmployeeTermination->attachment
            ]);
        } else {
            return response()->default(
                404,
                false,
                "File not found!",
                null
            )->setStatusCode(404);   
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(EmployeeTermination $EmployeeTermination)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $creds = $request->validate([
            'employee_id' => 'required',
            'submission_date' => 'required|date',
            'attachment' => 'nullable|file|mimes:PDF,pdf|max:2048'
        ]);

        $employee = Employee::find($request->employee_id);
        if(!$employee) {
            return response()->default(
                404,
                false,
                "Employee not found!",
                null
            )->setStatusCode(404);
        }

        $EmployeeTermination = EmployeeTermination::find($id);
        if(!$EmployeeTermination) {
            return response()->default(
                404,
                false,
                "Employee termination not found!",
                null
            )->setStatusCode(404);
        }

        if($request->hasFile('attachment')) {
            Storage::delete('assets/employee/termination/'.$EmployeeTermination->attachment);
            $em_resign = $request->file('attachment');
            $em_resign_name = $employee->employee_code.'-'.round(microtime(true) * 1000).'.'.$em_resign->getClientOriginalExtension();
            $em_resign->storeAs('assets/employee/termination', $em_resign_name);
            $EmployeeTermination->attachment = $em_resign_name;
        }

        $EmployeeTermination->employee_id = $request->employee_id;
        $EmployeeTermination->submission_date = $request->submission_date;
        $EmployeeTermination->last_working_date = $request->last_working_date;
        $EmployeeTermination->reason = $request->reason;
        $EmployeeTermination->modified_by = $request->user()->username;
        $EmployeeTermination->modified_date = now();
        $EmployeeTermination->update();

        return response()->default(
            200,
            true,
            "Employee termination has been updated",
            $EmployeeTermination
        )->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $EmployeeTermination = EmployeeTermination::find($id);

        if(!$EmployeeTermination) {
            return response()->default(
                404,
                false,
                "Employee termination not found!",
                null
            )->setStatusCode(404);
        }

        Storage::delete('assets/employee/termination/'.$EmployeeTermination->attachment);
        $EmployeeTermination->delete();

        return response()->default(
            200,
            true,
            "Employee termination has been deleted",
            null
        )->setStatusCode(200);

    }
}
