<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\Exceptions\MissingAbilityException;
use App\Traits\HasQueryService;

//IMPORT EXCEL UTILS
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;

//IMPORT MODEL
use App\Models\Position;
use App\Models\Employee;
use App\Models\EmployeeDesignation;

class PositionController extends Controller {
    use HasQueryService;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        try {
            $subQuery = DB::table(Position::getTableName())
        ->select(
            Position::getTableName().'.id',
            Position::getTableName().'.code as position_code',
            Position::getTableName().'.name as position_name',
            Position::getTableName().'.description',
            Position::getTableName().'.status',
            Position::getTableName().'.created_date', 
            Position::getTableName().'.created_by', 
            Position::getTableName().'.modified_date', 
            Position::getTableName().'.modified_by'
        );

            // Call getPaging, which internally calls applyFiltersAndPagination
            $data = $this->getPaging($subQuery);
            return response()->default(200, true, 'Get Positions Success', $data);
        } catch (\InvalidArgumentException $e) {
            // Catch specific InvalidArgumentException thrown by sortBy macro
            return response()->default(400, false, $e->getMessage(), null);
        } 
        catch (\Exception $e) {
            // Catch general exceptions and return a 500 response
            return response()->default(500, false, 'An unexpected error occurred.', null);
        }
    }

    public function show($id) {
        $data = Position::query()->where(Position::getTableName().'.id', $id)
        ->select(
            Position::getTableName().'.id',
            Position::getTableName().'.code as positionCode',
            Position::getTableName().'.name as positionName',
            Position::getTableName().'.description',
            Position::getTableName().'.status',
            Position::getTableName().'.created_date', 
            Position::getTableName().'.created_by', 
            Position::getTableName().'.modified_date', 
            Position::getTableName().'.modified_by'
        )->first();

        return response()->default(
            200,
            true, 
            "Get Position Detail Success", 
            $data
        )->setStatusCode(200);
    }

    public function store(Request $request) {
        // Validate request data
        $validatedData = $request->validate([
            'positionCode' => 'required',
            'positionName' => 'required',
            'description' => 'nullable'
        ]);

        // Check if code & name unique
        $pos = Position::where('code', $validatedData['positionCode'])
        ->orWhere('name', $validatedData['positionName'])
        ->first();
        if ($pos) {
            return response()->default(
                400,
                false, 
                "Position Code & Name is exists!",
                null
            )->setStatusCode(400);
        }
    
        // Create the position
        $stored = Position::create([
            'code' => $validatedData['positionCode'],
            'name' => $validatedData['positionName'],
            'description' => $validatedData['description'],
            'status' => 'ACTIVE',
            'created_by' => $request->user()->username
        ]);
    
        // Return success response
        return response()->default(
            200,
            true, 
            "Position Created",
            $stored
        )->setStatusCode(200);
    }
    

    public function update(Request $request, $id) {
        // Find position by ID or return error response
        $data = Position::find($id);
    
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Position Not Found",
                null
            )->setStatusCode(400);
        }

        // Check if code & name unique
        $pos = Position::where(function ($query) use ($request) {
            $query->where('code', $request['positionCode'])
                  ->orWhere('name', $request['positionName']);
        })
        ->whereNot('id', $id)
        ->first();

        if ($pos) {
            return response()->default(
                400,
                false, 
                "Position Code & Name is exists!",
                null
            )->setStatusCode(400);
        }
    
        // Define the fields that can be updated
        $fields = [
            'positioncode' => 'code',
            'positionName' => 'name',
            'description' => 'description'
        ];
    
        // Prepare the update data
        $updateData = [];
    
        foreach ($fields as $requestField => $dbField) {
            if ($request->filled($requestField)) {
                $updateData[$dbField] = $request->input($requestField);
            }
        }
    
        // Always update modified_by field
        //$updateData['modified_by'] = $request->user()->username;
    
        // If there is data to update, perform the update
        if (!empty($updateData)) {
            $data->update($updateData);
        }
    
        // Return success response
        return response()->default(
            200,
            true, 
            "Position Updated",
            $data
        )->setStatusCode(200);
    }
    

    public function toggle(Request $request, $id) {
        // Find position by ID or return error response
        $data = Position::find($id);
    
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Position Not Found",
                null
            )->setStatusCode(400);
        }
    
        // Toggle the status using ternary operator
        $newStatus = $data->status === 'ACTIVE' ? 'INACTIVE' : 'ACTIVE';
    
        // Update position status and modified_by
        $data->update([
            'status' => $newStatus,
            'modified_by' => $request->user()->username
        ]);
    
        // Return success response with appropriate message
        return response()->default(
            200,
            true, 
            $newStatus === 'ACTIVE' ? "Position Activated!" : "Position Deactivated!",
            $data
        )->setStatusCode(200);
    }

    public function destroy($id) {
        $data = Position::find($id);

        $dsg = EmployeeDesignation::where('position', $id)->first();
        if ($dsg) {
            return response()->default(
                409,
                false, 
                "Position used in employee designation!",
                null
            )->setStatusCode(409);
        }

        $data->delete();
        
        return response()->default(
            200,
            true, 
            "Position Deleted",
            null
        )->setStatusCode(200);
    }

    public function export()
    {
        $name = 'users_' . date('Y-m-d i:h:s');
        //$data = Excel::download(new UsersExport(), $name . '.xlsx');
    
        return Excel::download(new UsersExport, 'invoices.xlsx');
        //return $data;
    }

    public function getLOV(){
        $items = Position::select(['id','name'])->where('status','ACTIVE')->get()->toArray();
        $data = collect();
        foreach($items as $item){
            $data->push(
                [
                    'key' => $item['id'],
                    'value' => $item['name']
                ]
            );
        }
        return response()->default(
            200,
            true, 
            "Get Position LOV Success",
            $data
        )->setStatusCode(200);
    }
}