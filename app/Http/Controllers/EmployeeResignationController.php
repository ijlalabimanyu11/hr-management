<?php

namespace App\Http\Controllers;

use App\Models\EmployeeResignation;
use Illuminate\Http\Request;
use App\Models\Employee;
use Illuminate\Support\Facades\DB;
use Storage;
use App\Traits\HasQueryService;

class EmployeeResignationController extends Controller
{
    use HasQueryService;
    /**
     * Display a listing of the resource.
     */
    public function index() {
        try {
            $subQuery = DB::table(EmployeeResignation::getTableName())
        ->leftJoin(Employee::getTableName(), EmployeeResignation::getTableName().'.employee_id', '=', Employee::getTableName().'.id')
        ->select(
            EmployeeResignation::getTableName().'.id',
            EmployeeResignation::getTableName().'.employee_id',
            Employee::getTableName().'.employee_code',
            Employee::getTableName().'.name',
            EmployeeResignation::getTableName().'.submission_date',
            EmployeeResignation::getTableName().'.approved_date',
            EmployeeResignation::getTableName().'.last_working_date',
            EmployeeResignation::getTableName().'.reason',
            EmployeeResignation::getTableName().'.attachment',
            EmployeeResignation::getTableName().'.status',
            EmployeeResignation::getTableName().'.created_date',
            EmployeeResignation::getTableName().'.created_by',
            EmployeeResignation::getTableName().'.modified_date',
            EmployeeResignation::getTableName().'.modified_by',
        );

            // Call getPaging, which internally calls applyFiltersAndPagination
            $data = $this->getPaging($subQuery);
            return response()->default(200, true, 'Get Employee Resignation Header Success', $data);
        } catch (\InvalidArgumentException $e) {
            // Catch specific InvalidArgumentException thrown by sortBy macro
            return response()->default(400, false, $e->getMessage(), null);
        } 
        catch (\Exception $e) {
            // Catch general exceptions and return a 500 response
            return response()->default(500, false, 'An unexpected error occurred.', null);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $creds = $request->validate([
            'employee_id' => 'required',
            'submission_date' => 'required|date',
            'attachment' => 'required|file|mimes:PDF,pdf|max:2048'
        ]);

        $employee = Employee::find($request->employee_id);
        if(!$employee) {
            return response()->default(
                404,
                false,
                "Employee not found!",
                null
            )->setStatusCode(404);
        } 

        // UPLOAD DOCUMENT RESIGN
        $em_resign = $request->file('attachment');
        $em_resign_name = $employee->employee_code.'-'.round(microtime(true) * 1000).'.'.$em_resign->getClientOriginalExtension();
        $em_resign->storeAs('assets/employee/resignation', $em_resign_name);

        $employeeResignation = EmployeeResignation::create([
            'employee_id' => $request->employee_id,
            'submission_date' => $request->submission_date,
            'last_working_date' => $request->last_working_date,
            'reason' => $request->reason,
            'attachment' => $em_resign_name,
            'status' => 'WAITING_APPROVAL',
            'created_by' => $request->user()->username,
            'created_date' => now()
        ]);

        return response()->default(
            201,
            true,
            "Employee resignation has been created",
            $employeeResignation
        )->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $employeeResignation = EmployeeResignation::find($id);

        if(!$employeeResignation) {
            return response()->default(
                404,
                false,
                "Employee resignation not found!",
                null
            )->setStatusCode(404);
        }

        $employee = Employee::find($employeeResignation->employee_id);

        $response = [
            'id' => $employeeResignation->id,
            'employee_id' => $employee->id,
            'employee_code' => $employee->employee_code,
            'employee_name' => $employee->name,
            'submission_date' => $employeeResignation->submission_date,
            'approved_date' => $employeeResignation->approved_date,
            'last_working_date' => $employeeResignation->last_working_date,
            'reason' => $employeeResignation->reason,
            'attachment' => $employeeResignation->attachment,
            'status' => $employeeResignation->status,
            'created_date' => $employeeResignation->created_date,
            'created_by' => $employeeResignation->created_by,
            'modified_date' => $employeeResignation->modified_date,
            'modified_by' => $employeeResignation->modified_by
        ];

        return response()->default(
            200,
            true,
            "Success get detail employee resignation",
            $response
        )->setStatusCode(200);
       
    }

    public function downloadAttachment($id) {
        $employeeResignation = EmployeeResignation::find($id);
        $filePath = storage_path("app/assets/employee/resignation/{$employeeResignation->attachment}");
        
        if (file_exists($filePath)) {
            //return response()->download($filePath, $employeeResignation->attachment);

            //testing not download
            //return response()->file($filePath);
            //or
            //return response()->file($filePath, $employeeResignation->attachment);

            return response()->file($filePath, [
                'Content-Disposition' => 'attachment; filename='.$employeeResignation->attachment
            ]);
        } else {
            return response()->default(
                404,
                false,
                "File not found!",
                null
            )->setStatusCode(404);   
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(EmployeeResignation $employeeResignation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $creds = $request->validate([
            'employee_id' => 'required',
            'submission_date' => 'required|date',
            'attachment' => 'nullable|file|mimes:PDF,pdf|max:2048'
        ]);

        $employee = Employee::find($request->employee_id);
        if(!$employee) {
            return response()->default(
                404,
                false,
                "Employee not found!",
                null
            )->setStatusCode(404);
        }

        $employeeResignation = EmployeeResignation::find($id);
        if(!$employeeResignation) {
            return response()->default(
                404,
                false,
                "Employee resignation not found!",
                null
            )->setStatusCode(404);
        }

        if($request->hasFile('attachment')) {
            Storage::delete('assets/employee/resignation/'.$employeeResignation->attachment);
            $em_resign = $request->file('attachment');
            $em_resign_name = $employee->employee_code.'-'.round(microtime(true) * 1000).'.'.$em_resign->getClientOriginalExtension();
            $em_resign->storeAs('assets/employee/resignation', $em_resign_name);
            $employeeResignation->attachment = $em_resign_name;
        }

        $employeeResignation->employee_id = $request->employee_id;
        $employeeResignation->submission_date = $request->submission_date;
        $employeeResignation->last_working_date = $request->last_working_date;
        $employeeResignation->reason = $request->reason;
        $employeeResignation->modified_by = $request->user()->username;
        $employeeResignation->modified_date = now();
        $employeeResignation->update();

        return response()->default(
            200,
            true,
            "Employee resignation has been updated",
            $employeeResignation
        )->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $employeeResignation = EmployeeResignation::find($id);

        if(!$employeeResignation) {
            return response()->default(
                404,
                false,
                "Employee resignation not found!",
                null
            )->setStatusCode(404);
        }

        Storage::delete('assets/employee/resignation/'.$employeeResignation->attachment);
        $employeeResignation->delete();

        return response()->default(
            200,
            true,
            "Employee resignation has been deleted",
            null
        )->setStatusCode(200);

    }
}
