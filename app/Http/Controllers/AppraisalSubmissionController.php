<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\Exceptions\MissingAbilityException;
use App\Traits\HasQueryService;

//IMPORT EXCEL UTILS
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;

//IMPORT MODEL
use App\Models\AppraisalPeriod;
use App\Models\AppraisalSubmission;
use App\Models\AppraisalSubmissionDetail;
use App\Models\Department;
use App\Models\Employee;
use App\Models\EmployeeDesignation;

class AppraisalSubmissionController extends Controller {
    use HasQueryService;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        try {
            $subQuery = DB::table(AppraisalSubmission::getTableName())
        ->leftJoin(Employee::getTableName(), AppraisalSubmission::getTableName().'.employee_id', '=', Employee::getTableName().'.id')
        ->leftJoin(Department::getTableName(), AppraisalSubmission::getTableName().'.department_id', '=', Department::getTableName().'.id')
        ->select(
            AppraisalSubmission::getTableName().'.id',
            //Employee::getTableName().'.code as employeeCode',
            Employee::getTableName().'.name as employeeName',
            Department::getTableName().'.name as departmentName',
            AppraisalSubmission::getTableName().'.final_score as finalScore',
            AppraisalSubmission::getTableName().'.description',
            AppraisalSubmission::getTableName().'.status',
            AppraisalSubmission::getTableName().'.created_date', 
            AppraisalSubmission::getTableName().'.created_by', 
            AppraisalSubmission::getTableName().'.modified_date', 
            AppraisalSubmission::getTableName().'.modified_by'
        );

            // Call getPaging, which internally calls applyFiltersAndPagination
            $data = $this->getPaging($subQuery);
            return response()->default(200, true, 'Get Appraisal Submission Success', $data);
        } catch (\InvalidArgumentException $e) {
            // Catch specific InvalidArgumentException thrown by sortBy macro
            return response()->default(400, false, $e->getMessage(), null);
        } 
        catch (\Exception $e) {
            dd($e);
            // Catch general exceptions and return a 500 response
            return response()->default(500, false, 'An unexpected error occurred.', null);
        }
    }

    public function show($id) {
        $appraisalSubmission = AppraisalSubmission::query()->where(AppraisalSubmission::getTableName().'.id', $id)
        ->leftJoin(Employee::getTableName(), AppraisalSubmission::getTableName().'.employee_id', '=', Employee::getTableName().'.id')
        ->leftJoin(Department::getTableName(), AppraisalSubmission::getTableName().'.department_id', '=', Department::getTableName().'.id')
        ->select(
            AppraisalSubmission::getTableName().'.id',
            Employee::getTableName().'.code as employeeCode',
            Employee::getTableName().'.name as employeeName',
            Department::getTableName().'.name as departmentName',
            AppraisalSubmission::getTableName().'.final_score as finalScore',
            AppraisalSubmission::getTableName().'.description',
            AppraisalSubmission::getTableName().'.status',
            AppraisalSubmission::getTableName().'.created_date', 
            AppraisalSubmission::getTableName().'.created_by', 
            AppraisalSubmission::getTableName().'.modified_date', 
            AppraisalSubmission::getTableName().'.modified_by'
        )->first();

        $appraisalDetail = $this->getAppraisalDetail($id);

        $data = [
            "appraisalSubmission" => $appraisalSubmission,
            "appraisalDetail" => [
                "currentPage" => $appraisalDetail['current_page'],
                "lastPage" => $appraisalDetail['last_page'],
                "pageSize" => $appraisalDetail['to'],
                "totalData" => $appraisalDetail['total'],
                "data" => $appraisalDetail['data'],
            ],
        ];

        return response()->default(
            200,
            true, 
            "Get Appraisal Submission Detail Success", 
            $data
        )->setStatusCode(200);
    }

    private function getAppraisalDetail($appraisalSubmissionid){
        $subQuery = DB::table(AppraisalSubmissionDetail::getTableName())
        ->leftJoin(AppraisalSubmission::getTableName(), AppraisalSubmissionDetail::getTableName().'.appraisal_submission_id', '=', Approval::getTableName().'.id')
        ->where(AppraisalSubmission::getTableName().'.approval_submissions_id', $appraisalSubmissionid)
        ->select(
            AppraisalSubmissionDetail::getTableName().'.id',
            AppraisalSubmissionDetail::getTableName().'.name',
            AppraisalSubmissionDetail::getTableName().'.description',
            AppraisalSubmissionDetail::getTableName().'.status',
            AppraisalSubmissionDetail::getTableName().'.created_date', 
            AppraisalSubmissionDetail::getTableName().'.created_by', 
            AppraisalSubmissionDetail::getTableName().'.modified_date', 
            AppraisalSubmissionDetail::getTableName().'.modified_by'
        );
        $subQuerySql = $subQuery->toSql();
        $subQueryBindings = $subQuery->getBindings();

        $data = Approval::query()->from(DB::raw("({$subQuerySql}) as sub"))->mergeBindings($subQuery);

        //dd($data);

        $size = request()->size ?? 10;
        
        $data = $data
            ->when(request()->search, fn($query, $search) => $query->whereLike($search))
            ->when(request()->sort, fn($query, $sort) => $query->sortBy($sort))
            ->paginate($size)
            ->toArray();
        
        return $data;
    }

    public function store(Request $request) {
        // Validate request data
        $validatedData = $request->validate([
            'employeeId' => 'required',
            'departmentId' => 'required',
            'appraisalPeriodId' => 'required',
            'description' => 'nullable',
            'appraisalDetail' => 'required|array',
            'appraisalDetail.*.indicatorId' => 'required',
            'appraisalDetail.*.achievement' => 'required',
            'appraisalDetail.*.remark' => 'nullable',
        ]);

        // Check if code & name unique
        $pos = AppraisalSubmission::where('department_id', $validatedData['departmentId'])
        ->where('year', $validatedData['year'])
        ->first();
        if ($pos) {
            return response()->default(
                400,
                false, 
                "Appraisal Submission is exists!",
                null
            )->setStatusCode(400);
        }
    
        // Create the appraisal period
        $stored = AppraisalSubmission::create([
            'employee_id' => $validatedData['employeeId'],
            'department_id' => $validatedData['departmentId'],
            'appraisal_period_id' => $validatedData['appraisalPeriodId'],
            'description' => $validatedData['description'],
            'created_by' => $request->user()->username
        ]);
    
        $appDtl = collect();

        foreach($validatedData['appraisalDetail'] as $dtl){
            $appDtl->push([
                'appraisal_submission_id' => $stored['id'],
                'indicator_id' => $dtl['indicatorId'],
                'weight' => $dtl['weight'],
                'target' => $dtl['target'],
                'achievement' => $dtl['achievement'],
                'remark' => $dtl['remark'],
                'is_deleted' => 'Y',
                'created_date' => now(),
                'created_by' => $request->user()->username
            ]);
        }

        //dd($orgDtl->all());
        $appDtlArr = $orgDtl->toArray();

        if(filled($appDtlArr)){
            $storedDtl = AppraisalSubmissionDetail::insert($appDtlArr);
            $stored->push(['appraisalDetail'=>$storedDtl]);
        }

        // Return success response
        return response()->default(
            200,
            true, 
            "Appraisal Submission Created",
            $stored
        )->setStatusCode(200);
    }
    

    public function update(Request $request, $id) {
        // Find appraisal period by ID or return error response
        $data = AppraisalSubmission::find($id);
    
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Appraisal Submission Not Found",
                null
            )->setStatusCode(400);
        }
    
        // Define the fields that can be updated
        $fields = [
            'description' => 'description'
        ];
    
        // Prepare the update data
        $updateData = [];
    
        foreach ($fields as $requestField => $dbField) {
            if ($request->filled($requestField)) {
                $updateData[$dbField] = $request->input($requestField);
            }
        }
    
        // Always update modified_by field
        $updateData['modified_by'] = $request->user()->username;
    
        // If there is data to update, perform the update
        if (!empty($updateData)) {
            $data->update($updateData);
        }
    
        // Return success response
        return response()->default(
            200,
            true, 
            "Appraisal Submission Updated",
            $data
        )->setStatusCode(200);
    }
    

    public function toggle(Request $request, $id) {
        // Find appraisal period by ID or return error response
        $data = AppraisalSubmission::find($id);
    
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Appraisal Submission Not Found",
                null
            )->setStatusCode(400);
        }
    
        // Toggle the status using ternary operator
        $newStatus = $data->status === 'ACTIVE' ? 'INACTIVE' : 'ACTIVE';
    
        // Update appraisal period status and modified_by
        $data->update([
            'status' => $newStatus,
            'modified_by' => $request->user()->username
        ]);
    
        // Return success response with appropriate message
        return response()->default(
            200,
            true, 
            $newStatus === 'ACTIVE' ? "Appraisal Submission Activated!" : "Appraisal Submission Deactivated!",
            $data
        )->setStatusCode(200);
    }

    public function destroy($id) {
        $data = $this->findData($id);

        $dsg = EmployeeDesignation::where('appraisal period', $id)->first();
        if ($emp) {
            return response()->default(
                409,
                false, 
                "Appraisal Submission used in appraisal submission!",
                null
            )->setStatusCode(409);
        }

        $data->delete();
        
        return response()->default(
            200,
            true, 
            "AppraisalSubmission Deleted",
            null
        )->setStatusCode(200);
    }

    public function export()
    {
        $name = 'users_' . date('Y-m-d i:h:s');
        //$data = Excel::download(new UsersExport(), $name . '.xlsx');
    
        return Excel::download(new UsersExport, 'invoices.xlsx');
        //return $data;
    }
}