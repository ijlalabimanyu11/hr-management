<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\Exceptions\MissingAbilityException;
use Illuminate\Support\Str;
use App\Traits\HasQueryService;

//IMPORT EXCEL UTILS
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;

//IMPORT MODEL
use App\Models\Approval;
use App\Models\ApprovalWorkflow;
use App\Models\ApprovalWorkflowCriteria;
use App\Models\ApprovalWorkflowStep;
use App\Models\ApprovalTransaction;
use App\Models\ApprovalTransactionHistory;
use App\Models\VwWorkflowCriteria;
use App\Models\VwWorkflowStep;

class ApprovalController extends Controller {
    use HasQueryService;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //APPROVAL CONFIG
    public function index() {
        try {
            $subQuery = DB::table(Approval::getTableName())
            ->select(
                Approval::getTableName().'.id',
                Approval::getTableName().'.code',
                Approval::getTableName().'.description',
                Approval::getTableName().'.status',
                Approval::getTableName().'.created_date', 
                Approval::getTableName().'.created_by', 
                Approval::getTableName().'.modified_date', 
                Approval::getTableName().'.modified_by'
            );

            // Call getPaging, which internally calls applyFiltersAndPagination
            $data = $this->getPaging($subQuery);
            return response()->default(200, true, 'Get Approval Config Success', $data);
        } catch (\InvalidArgumentException $e) {
            // Catch specific InvalidArgumentException thrown by sortBy macro
            return response()->default(400, false, $e->getMessage(), null);
        } 
        catch (\Exception $e) {
            // Catch general exceptions and return a 500 response
            return response()->default(500, false, 'An unexpected error occurred.', null);
        }
    }

    public function show($id) {
        $approval = Approval::query()->where(Approval::getTableName().'.id', $id)
        ->select(
            '*'
        )->first();

        $approvalWorkflow = $this->getWorkflowPaging($id, true);
        $approval->status = Str::ucfirst(Str::lower($approval->status));
        
        $approvalWorkflowStep = $this->getWorkflowStepPaging($approvalWorkflow['id']);
        
        $data = [
            "approval" => $approval,
            "defaultWorkflowStep" => $approvalWorkflowStep['data'],
        ];

        return response()->default(
            200,
            true, 
            "Get Approval Configuration Detail Success", 
            $data
        )->setStatusCode(200);
    }

    public function store(Request $request) {
        // Validate request data
        $validatedData = $request->validate([
            'code' => 'required',
            'description' => 'required|string|max:255',
            'defaultWorkflowStep' => 'required|array',
            'defaultWorkflowStep.*.step' => 'required|numeric',
            'defaultWorkflowStep.*.type' => 'required|numeric',
            'defaultWorkflowStep.*.lineManager' => 'nullable',
            'defaultWorkflowStep.*.position' => 'nullable',
            'defaultWorkflowStep.*.section' => 'nullable',
            'defaultWorkflowStep.*.department' => 'nullable'
        ]);

        // Check if code unique
        $code = Approval::where(function ($query) use ($validatedData) {
            $query->where('code', $validatedData['code']);
        })
        ->first();
        
        if ($code) {
            return response()->default(
                400,
                false, 
                "Approval Code is exists!",
                null
            )->setStatusCode(400);
        }
        //dd(config('app.timezone'),now());
        $header = Approval::create([
            'code' => $validatedData['code'],
            'description' => $validatedData['description'],
            'status' => 'ACTIVE',
            'created_date' => now(),
            'created_by' => $request->user()->username
        ]);

        $workflow = ApprovalWorkflow::create([
            'approval_id' => $header['id'],
            'name' => "DEFAULT",
            'status' => 'ACTIVE',
            'isDefault' => 'Y',
            'created_date' => now(),
            'created_by' => $request->user()->username
        ]);

        $step = collect();

        foreach ($validatedData['defaultWorkflowStep'] as $dtl) {
            $isLast = ($dtl === end($validatedData['defaultWorkflowStep']));
        
            $step->push([
                'approval_workflow_id' => $workflow['id'],
                'step' => $dtl['step'],
                'type' => $dtl['type'],
                'line_manager' => $dtl['lineManager'],
                'position_id' => $dtl['position'],
                'section_id' => $dtl['section'],
                'department_id' => $dtl['department'],
                'is_final' => $isLast ? "Y" : "N",
                'created_date' => now(),
                'created_by' => $request->user()->username
            ]);
        }

        $stepArr = $step->toArray();

        if(filled($stepArr)){
            $storedStep = ApprovalWorkflowStep::insert($stepArr);
            $header->push(['defaultWorkflowStep'=>$storedStep]);
        }
    
        // Return success response
        return response()->default(
            200,
            true, 
            "Approval Configuration Created",
            $header
        )->setStatusCode(200);
    }
    
    public function update(Request $request, $id) {
        // Find approval by ID or return error response
        $data = Approval::find($id);
    
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Approval Not Found",
                null
            )->setStatusCode(400);
        }
    
        // Check if code unique
        $exist = Approval::where(function ($query) use ($data) {
            $query->where('code', $data['code']);
        })
        ->whereNot('id', $id)
        ->first();
        
        if ($exist) {
            return response()->default(
                400,
                false, 
                "Approval Code is exists!",
                null
            )->setStatusCode(400);
        }

        // Define the fields that can be updated
        $fields = [
            'description' => 'description'
        ];
    
        // Prepare the update data
        $updateData = [];
    
        foreach ($fields as $requestField => $dbField) {
            if ($request->filled($requestField)) {
                $updateData[$dbField] = $request->input($requestField);
            }
        }
    
        // Always update modified_by field
        $updateData['modified_by'] = $request->user()->username;
    
        // If there is data to update, perform the update
        if (!empty($updateData)) {
            $data->update($updateData);
        }
    
        // Return success response
        return response()->default(
            200,
            true, 
            "Approval Configuration Updated",
            $data
        )->setStatusCode(200);
    }
    
    public function toggle(Request $request, $id) {
        // Find approval by ID or return error response
        $data = Approval::find($id);
    
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Approval Not Found",
                null
            )->setStatusCode(400);
        }

        $exist = ApprovalTransaction::where('approval_workflow_id', $id)
        ->where('approval_status', 'PENDING APPROVAL')->first();
        if ($exist) {
            return response()->default(
                400,
                false, 
                "There is pending transaction within this approval code!",
                null
            )->setStatusCode(400);
        }
    
        // Toggle the status using ternary operator
        $newStatus = $data->status === 'ACTIVE' ? 'INACTIVE' : 'ACTIVE';
    
        // Update approval status and modified_by
        $data->update([
            'status' => $newStatus,
            'modified_by' => $request->user()->username
        ]);
    
        // Return success response with appropriate message
        return response()->default(
            200,
            true, 
            $newStatus === 'ACTIVE' ? "Approval Activated!" : "Approval Deactivated!",
            $data
        )->setStatusCode(200);
    }

    public function destroy($id) {
        $data = Approval::find($id);
    
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Approval Not Found",
                null
            )->setStatusCode(400);
        }
        
        $exist = ApprovalTransaction::where('approval_workflow_id', $id)->first();
        if ($exist) {
            return response()->default(
                400,
                false, 
                "Approval used in transaction, cannot delete!",
                null
            )->setStatusCode(400);
        }

        $data->delete();
        
        return response()->default(
            200,
            true, 
            "Approval configuration deleted!",
            null
        )->setStatusCode(200);
    }

    public function export()
    {
        $name = 'users_' . date('Y-m-d i:h:s');
        //$data = Excel::download(new UsersExport(), $name . '.xlsx');
    
        return Excel::download(new UsersExport, 'invoices.xlsx');
        //return $data;
    }

    //WORKFLOW
    private function getWorkflowPaging($approvalId, $isDefault){
        // Extract 'appConfigId' from the request->search parameter
        if (!$isDefault) {
            $search = request()->search;
            // Split the search string into individual conditions
            $searchTerms = explode('&', $search);
            $filteredSearchTerms = [];

            foreach ($searchTerms as $term) {
                // Split each condition into key-value pairs
                [$key, $value] = explode('~', $term);

                // Check if the key is 'appConfigId'
                if ($key === 'appConfigId') {
                    $approvalId = $value; // Assign to $approvalId
                } else {
                    // Keep other search conditions
                    $filteredSearchTerms[] = $term;
                }
            }

            // Rebuild the search string without 'appConfigId'
            $search = implode('&', $filteredSearchTerms);

            // Replace the modified search back into the request
            $request = request();
            $request->merge(['search' => $search]);
        }

        // Build the subquery
        $subQuery = DB::table(ApprovalWorkflow::getTableName())
            ->leftJoin(Approval::getTableName(), ApprovalWorkflow::getTableName() . '.approval_id', '=', Approval::getTableName() . '.id')
            ->select(
                ApprovalWorkflow::getTableName() . '.id',
                ApprovalWorkflow::getTableName() . '.name',
                ApprovalWorkflow::getTableName() . '.description',
                ApprovalWorkflow::getTableName() . '.status',
                ApprovalWorkflow::getTableName() . '.approval_id as appConfigId',
                ApprovalWorkflow::getTableName() . '.created_date',
                ApprovalWorkflow::getTableName() . '.created_by',
                ApprovalWorkflow::getTableName() . '.modified_date',
                ApprovalWorkflow::getTableName() . '.modified_by'
            )
            ->when(isset($approvalId), fn($query) => $query->where(ApprovalWorkflow::getTableName() . '.approval_id', $approvalId))
            ->where(ApprovalWorkflow::getTableName() . '.isDefault', $isDefault ? 'Y' : 'N');

        $subQuerySql = $subQuery->toSql();
        $subQueryBindings = $subQuery->getBindings();

        $dataQuery = Approval::query()->from(DB::raw("({$subQuerySql}) as sub"))->mergeBindings($subQuery);
        
        if ($isDefault) {
            // Fetch the first record if $isDefault is true
            $data = $dataQuery->first();
        } else {
            // Apply additional filters and pagination for non-default case
            $size = request()->size ?? 10;

            $data = $dataQuery
                ->when($search, fn($query, $search) => $query->whereLike($search))
                ->when(request()->sort, fn($query, $sort) => $query->sortBy($sort))
                ->paginate($size)
                ->toArray();
        }
        
        return $data;
    }

    public function indexWorkflow() {
        $data = $this->getWorkflowPaging(null, false);
        
        //dd($data->ddRawSql());

        return response()->default(
            200,
            true, 
            "Get Workflow Success", 
            [
                "currentPage" => $data['current_page'],
                "lastPage" => $data['last_page'],
                "pageSize" => $data['to'],
                "totalData" => $data['total'],
                "data" => $data['data'],
            ]
        )->setStatusCode(200);
    }

    public function showWorkflow($id) {
        $data = ApprovalWorkflow::query()->where(ApprovalWorkflow::getTableName().'.id', $id)
        ->select(
            ApprovalWorkflow::getTableName().'.id',
            ApprovalWorkflow::getTableName().'.name',
            ApprovalWorkflow::getTableName().'.description',
            ApprovalWorkflow::getTableName().'.status',
            ApprovalWorkflow::getTableName().'.created_date', 
            ApprovalWorkflow::getTableName().'.created_by', 
            ApprovalWorkflow::getTableName().'.modified_date', 
            ApprovalWorkflow::getTableName().'.modified_by'
        )->first();
        
        $data->status = Str::ucfirst(Str::lower($data->status));

        $approvalWorkflowStep = $this->getWorkflowStepPaging($id);
        $approvalWorkflowCriteria = $this->getWorkflowCriteriaPaging($id);

        if($data){
            return response()->default(
                200,
                true, 
                "Get Approval Workflow Detail Success", 
                [
                    "approvalWorkflow" => $data,
                    "approvalWorkflowStep" => $approvalWorkflowStep['data'],
                    "approvalWorkflowCriteria" => $approvalWorkflowCriteria['data'],
                ]
            )->setStatusCode(200);
        }
        
        return response()->default(
            404,
            true,
            "Not Found", 
            null
        )->setStatusCode(404);
    }

    public function storeWorkflow(Request $request) {
        // Validate request data
        $validatedData = $request->validate([
            'appConfigId' => 'required',
            'name' => 'required',
            'description' => 'required|string|max:255',
            'workflowStep' => 'required|array',
            'workflowCriteria' => 'required|array',
            'workflowStep.*.step' => 'required|numeric',
            'workflowStep.*.type' => 'required|numeric',
            'workflowStep.*.lineManager' => 'nullable|nullable',
            'workflowStep.*.position' => 'nullable|nullable',
            'workflowStep.*.section' => 'nullable|nullable',
            'workflowStep.*.department' => 'nullable|nullable',
            'workflowCriteria.*.type' => 'required|numeric',
            'workflowCriteria.*.employee' => 'numeric|nullable',
            'workflowCriteria.*.position' => 'numeric|nullable',
            'workflowCriteria.*.section' => 'numeric|nullable',
            'workflowCriteria.*.department' => 'numeric|nullable'
        ]);

        // Check if code unique
        $code = ApprovalWorkflow::where(function ($query) use ($validatedData) {
            $query->where('approval_id', $validatedData['appConfigId']);
            $query->where('name', $validatedData['name']);
        })
        ->first();
        
        if ($code) {
            return response()->default(
                400,
                false, 
                "Approval Workflow with same Name is exists!",
                null
            )->setStatusCode(400);
        }

        $workflow = ApprovalWorkflow::create([
            'approval_id' => $validatedData['appConfigId'],
            'name' => $validatedData['name'],
            'description' => $validatedData['description'],
            'status' => 'ACTIVE',
            'isDefault' => 'N',
            'created_date' => now(),
            'created_by' => $request->user()->username
        ]);

        $step = collect();

        foreach ($validatedData['workflowStep'] as $dtl) {
            $isLast = ($dtl === end($validatedData['workflowStep']));
        
            $step->push([
                'approval_workflow_id' => $workflow['id'],
                'step' => $dtl['step'],
                'type' => $dtl['type'],
                'line_manager' => $dtl['lineManager'],
                'position_id' => $dtl['position'],
                'section_id' => $dtl['section'],
                'department_id' => $dtl['department'],
                'is_final' => $isLast ? "Y" : "N",
                'created_date' => now(),
                'created_by' => $request->user()->username
            ]);
        }

        $stepArr = $step->toArray();

        if(filled($stepArr)){
            $storedStep = ApprovalWorkflowStep::insert($stepArr);
            $workflow->push(['workflowStep'=>$storedStep]);
        }

        $crit = collect();

        foreach ($validatedData['workflowCriteria'] as $dtl) {
            $isLast = ($dtl === end($validatedData['workflowCriteria']));
        
            $crit->push([
                'approval_workflow_id' => $workflow['id'],
                'type' => $dtl['type'],
                'line_manager' => $dtl['employee'],
                'position' => $dtl['position'],
                'section_id' => $dtl['section'],
                'department_id' => $dtl['department'],
                'created_date' => now(),
                'created_by' => $request->user()->username
            ]);
        }

        $critArr = $crit->toArray();

        if(filled($critArr)){
            $storedCrit = ApprovalWorkflowCriteria::insert($critArr);
            $workflow->push(['workflowCriteria'=>$storedCrit]);
        }
    
        // Return success response
        return response()->default(
            200,
            true, 
            "Approval Workflow Created",
            $workflow
        )->setStatusCode(200);
    }

    public function updateWorkflow(Request $request, $id) {
        // Find approval by ID or return error response
        $data = ApprovalWorkflow::find($id);
    
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Approval Workflow Not Found",
                null
            )->setStatusCode(400);
        }
    
        // Check if code unique
        $exist = ApprovalWorkflow::where(function ($query) use ($data) {
            $query->where('name', $data['name']);
        })
        ->whereNot('id', $id)
        ->first();
        
        if ($exist) {
            return response()->default(
                400,
                false, 
                "Approval Worklofw Name is exists!",
                null
            )->setStatusCode(400);
        }

        // Define the fields that can be updated
        $fields = [
            'description' => 'description'
        ];
    
        // Prepare the update data
        $updateData = [];
    
        foreach ($fields as $requestField => $dbField) {
            if ($request->filled($requestField)) {
                $updateData[$dbField] = $request->input($requestField);
            }
        }
    
        // Always update modified_by field
        $updateData['modified_by'] = $request->user()->username;
    
        // If there is data to update, perform the update
        if (!empty($updateData)) {
            $data->update($updateData);
        }
    
        // Return success response
        return response()->default(
            200,
            true, 
            "Approval Workflow Updated",
            $data
        )->setStatusCode(200);
    }

    public function toggleWorkflow(Request $request, $id) {
        // Find approval by ID or return error response
        $data = ApprovalWorkflow::find($id);
    
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Approval Workflow Not Found",
                null
            )->setStatusCode(400);
        }

        $exist = ApprovalTransaction::where('approval_workflow_id', $id)
        ->where('approval_status', 'PENDING APPROVAL')->first();
        if ($exist) {
            return response()->default(
                400,
                false, 
                "There is pending transaction within this approval workflow!",
                null
            )->setStatusCode(400);
        }
    
        // Toggle the status using ternary operator
        $newStatus = $data->status === 'ACTIVE' ? 'INACTIVE' : 'ACTIVE';
    
        // Update approval status and modified_by
        $data->update([
            'status' => $newStatus,
            'modified_by' => $request->user()->username
        ]);
    
        // Return success response with appropriate message
        return response()->default(
            200,
            true, 
            $newStatus === 'ACTIVE' ? "Approval Workflow Activated!" : "Approval Workflow Deactivated!",
            $data
        )->setStatusCode(200);
    }

    public function destroyWorkflow($id) {
        $data = ApprovalWorkflow::find($id);
    
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Approval Workflow Not Found",
                null
            )->setStatusCode(400);
        }
        
        $exist = ApprovalTransaction::where('approval_workflow_id', $id)->first();
        if ($exist) {
            return response()->default(
                400,
                false, 
                "Approval Workflow used in transaction, cannot delete!",
                null
            )->setStatusCode(400);
        }

        $data->delete();
        
        return response()->default(
            200,
            true, 
            "Approval Workflow deleted!",
            null
        )->setStatusCode(200);
    }
    
    //WORKFLOW.CRITERIA
    private function getWorkflowCriteriaPaging($id){
        // Extract 'appConfigId' from the request->search parameter
        $search = request()->search;
        $approvalWorkflowId = null;

        if ($search) {
            // Split the search string into individual conditions
            $searchTerms = explode('&', $search);
            $filteredSearchTerms = [];

            foreach ($searchTerms as $term) {
                // Split each condition into key-value pairs
                [$key, $value] = explode('~', $term);

                // Check if the key is 'appConfigId'
                if ($key === 'appConfigWorkflowId') {
                    $approvalWorkflowId = $value; // Assign to $approvalId
                } else {
                    // Keep other search conditions
                    $filteredSearchTerms[] = $term;
                }
            }

            // Rebuild the search string without 'appConfigId'
            $search = implode('&', $filteredSearchTerms);

            // Replace the modified search back into the request
            $request = request();
            $request->merge(['search' => $search]);
        }
        else{
            $approvalWorkflowId = $id;
        }

        $subQuery = DB::table(VwWorkflowCriteria::getTableName())
            ->select(
                VwWorkflowCriteria::getTableName().'.id',
                VwWorkflowCriteria::getTableName().'.approval_workflow_id as appConfigWorkflowId',
                VwWorkflowCriteria::getTableName().'.criteria',
                VwWorkflowCriteria::getTableName().'.value_id',
                VwWorkflowCriteria::getTableName().'.value_name',
                VwWorkflowCriteria::getTableName().'.status',
                VwWorkflowCriteria::getTableName().'.created_date', 
                VwWorkflowCriteria::getTableName().'.created_by', 
                VwWorkflowCriteria::getTableName().'.modified_date', 
                VwWorkflowCriteria::getTableName().'.modified_by'
            )
        ->when(isset($approvalWorkflowId), fn($query) => $query->where(VwWorkflowCriteria::getTableName() . '.approval_workflow_id', $approvalWorkflowId));

        $subQuerySql = $subQuery->toSql();
        $subQueryBindings = $subQuery->getBindings();

        $data = VwWorkflowCriteria::query()->from(DB::raw("({$subQuerySql}) as sub"))->mergeBindings($subQuery);

        //dd($data);

        $size = request()->size ?? 10;
        
        $data = $data
            ->when(request()->search, fn($query, $search) => $query->whereLike($search))
            ->when(request()->sort, fn($query, $sort) => $query->sortBy($sort))
            ->paginate($size)
            ->toArray();
        
        return $data;
    }

    public function indexWorkflowCriteria() {
        $data = $this->getWorkflowCriteriaPaging();
        
        //dd($data->ddRawSql());

        return response()->default(
            200,
            true, 
            "Get Workflow Criteria Success", 
            [
                "currentPage" => $data['current_page'],
                "lastPage" => $data['last_page'],
                "pageSize" => $data['to'],
                "totalData" => $data['total'],
                "data" => $data['data'],
            ]
        )->setStatusCode(200);
    }

    public function showWorkflowCriteria($id) {
        $data = VwWorkflowCriteria::query()->where(VwWorkflowCriteria::getTableName().'.id', $id)
        ->select(
            VwWorkflowCriteria::getTableName().'.id',
            VwWorkflowCriteria::getTableName().'.approval_workflow_id as appConfigWorkflowId',
            VwWorkflowCriteria::getTableName().'.criteria',
            VwWorkflowCriteria::getTableName().'.value_id',
            VwWorkflowCriteria::getTableName().'.value_name',
            VwWorkflowCriteria::getTableName().'.created_date', 
            VwWorkflowCriteria::getTableName().'.created_by', 
            VwWorkflowCriteria::getTableName().'.modified_date', 
            VwWorkflowCriteria::getTableName().'.modified_by'
        )->first();
        
        if($data){
            return response()->default(
                200,
                true, 
                "Get Approval Workflow Criteria Detail Success", 
                $data
            )->setStatusCode(200);
        }
        
        return response()->default(
            404,
            true,
            "Not Found", 
            null
        )->setStatusCode(404);
    }

    public function storeWorkflowCriteria(Request $request) {
        // Validate request data
        $validatedData = $request->validate([
            'appWorkflowConfigId' => 'required',
            'type' => 'required|numeric',
            'employee' => 'numeric|nullable',
            'position' => 'numeric|nullable',
            'section' => 'numeric|nullable',
            'department' => 'numeric|nullable'
        ]);

        // Check if code unique
        $code = ApprovalWorkflowCriteria::where(function ($query) use ($validatedData) {
            $query->where('approval_workflow_id', $validatedData['appWorkflowConfigId']);
            $query->where('type', $validatedData['type']);
        })
        ->first();
        
        if ($code) {
            return response()->default(
                400,
                false, 
                "Approval Workflow Criteria with same Type is exists!",
                null
            )->setStatusCode(400);
        }

        $workflow = ApprovalWorkflowCriteria::create([
            'approval_workflow_id' => $validatedData['appWorkflowConfigId'],
            'type' => $validatedData['type'],
            'line_manager' => $validatedData['employee'],
            'position' => $validatedData['position'],
            'section_id' => $validatedData['section'],
            'department_id' => $validatedData['department'],
            'status' => 'ACTIVE',
            'created_date' => now(),
            'created_by' => $request->user()->username
        ]);
    
        // Return success response
        return response()->default(
            200,
            true, 
            "Approval Workflow Criteria Created",
            $workflow
        )->setStatusCode(200);
    }

    public function updateWorkflowCriteria(Request $request, $id) {
        // Find approval by ID or return error response
        $data = ApprovalWorkflowCriteria::find($id);
        
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Approval Workflow Criteria Not Found",
                null
            )->setStatusCode(400);
        }

        $code = ApprovalWorkflowCriteria::where(function ($query) use ($data, $request) {
            $query->where('approval_workflow_id', $data['approval_workflow_id']);
            $query->where('type', $request->input('type'));
        })
        ->whereNot('id', $id)
        ->first();
        
        if ($code) {
            return response()->default(
                400,
                false, 
                "Approval Workflow Criteria with same Type is exists!",
                null
            )->setStatusCode(400);
        }

        // Define the fields that can be updated
        $fields = [
            'type' => 'type',
            'employee' => 'line_manager',
            'position' => 'position',
            'section' => 'section_id',
            'department' => 'department_id'
        ];
    
        // Prepare the update data
        $updateData = [];
    
        foreach ($fields as $requestField => $dbField) {
            if ($request->filled($requestField)) {
                $updateData[$dbField] = $request->input($requestField);
            }
        }
    
        // Always update modified_by field
        $updateData['modified_date'] = now();
        $updateData['modified_by'] = $request->user()->username;
    
        // If there is data to update, perform the update
        if (!empty($updateData)) {
            $data->update($updateData);
        }
    
        // Return success response
        return response()->default(
            200,
            true, 
            "Approval Workflow Criteria Updated",
            $data
        )->setStatusCode(200);
    }

    public function toggleWorkflowCriteria(Request $request, $id) {
        // Find approval by ID or return error response
        $data = ApprovalWorkflowCriteria::find($id);
    
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Approval Workflow Not Found",
                null
            )->setStatusCode(400);
        }

        $exist = ApprovalTransaction::where('approval_workflow_id', $data['approval_workflow_id'])
        ->where('approval_status', 'PENDING APPROVAL')->first();
        if ($exist) {
            return response()->default(
                400,
                false, 
                "There is pending transaction within this approval workflow!",
                null
            )->setStatusCode(400);
        }
    
        // Toggle the status using ternary operator
        $newStatus = $data->status === 'ACTIVE' ? 'INACTIVE' : 'ACTIVE';
    
        // Update approval status and modified_by
        $data->update([
            'status' => $newStatus,
            'modified_by' => $request->user()->username
        ]);
    
        // Return success response with appropriate message
        return response()->default(
            200,
            true, 
            $newStatus === 'ACTIVE' ? "Approval Workflow Criteria Activated!" : "Approval Workflow Criteria Deactivated!",
            $data
        )->setStatusCode(200);
    }

    public function destroyWorkflowCriteria($id) {
        $data = ApprovalWorkflowCriteria::find($id);
    
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Approval Workflow Criteria Not Found",
                null
            )->setStatusCode(400);
        }
        
        $exist = ApprovalTransaction::where('approval_workflow_id', $data['approval_workflow_id'])->first();
        if ($exist) {
            return response()->default(
                400,
                false, 
                "Approval Workflow used in transaction, cannot delete!",
                null
            )->setStatusCode(400);
        }

        $data->delete();
        
        return response()->default(
            200,
            true, 
            "Approval Workflow Criteria deleted!",
            null
        )->setStatusCode(200);
    }
    
    //WORKFLOW.STEP
    private function getWorkflowStepPaging($id){
        // Extract 'appConfigId' from the request->search parameter
        $search = request()->search;
        $approvalWorkflowId = null;
        if ($search) {
            // Split the search string into individual conditions
            $searchTerms = explode('&', $search);
            $filteredSearchTerms = [];

            foreach ($searchTerms as $term) {
                // Split each condition into key-value pairs
                [$key, $value] = explode('~', $term);

                // Check if the key is 'appConfigId'
                if ($key === 'appConfigWorkflowId') {
                    $approvalWorkflowId = $value; // Assign to $approvalId
                } else {
                    // Keep other search conditions
                    $filteredSearchTerms[] = $term;
                }
            }

            // Rebuild the search string without 'appConfigId'
            $search = implode('&', $filteredSearchTerms);

            // Replace the modified search back into the request
            $request = request();
            $request->merge(['search' => $search]);
        }
        else{
            $approvalWorkflowId = $id;
        }

        $subQuery = DB::table(VwWorkflowStep::getTableName())
            ->select(
                VwWorkflowStep::getTableName().'.id',
                VwWorkflowStep::getTableName().'.approval_workflow_id as appConfigWorkflowId',
                VwWorkflowStep::getTableName().'.step',
                VwWorkflowStep::getTableName().'.is_final',
                VwWorkflowStep::getTableName().'.type_id',
                VwWorkflowStep::getTableName().'.type_name',
                VwWorkflowStep::getTableName().'.line_manager_id',
                VwWorkflowStep::getTableName().'.line_manager_name',
                VwWorkflowStep::getTableName().'.position_id',
                VwWorkflowStep::getTableName().'.position_name',
                VwWorkflowStep::getTableName().'.section_id',
                VwWorkflowStep::getTableName().'.section_name',
                VwWorkflowStep::getTableName().'.department_id',
                VwWorkflowStep::getTableName().'.department_name',
                VwWorkflowStep::getTableName().'.status', 
                VwWorkflowStep::getTableName().'.created_date', 
                VwWorkflowStep::getTableName().'.created_by', 
                VwWorkflowStep::getTableName().'.modified_date', 
                VwWorkflowStep::getTableName().'.modified_by'
            )
        ->when(isset($approvalWorkflowId), fn($query) => $query->where(VwWorkflowStep::getTableName() . '.approval_workflow_id', $approvalWorkflowId));

        $subQuerySql = $subQuery->toSql();
        $subQueryBindings = $subQuery->getBindings();

        $data = VwWorkflowStep::query()->from(DB::raw("({$subQuerySql}) as sub"))->mergeBindings($subQuery);

        //dd($data);

        $size = request()->size ?? 10;
        
        $data = $data
            ->when(request()->search, fn($query, $search) => $query->whereLike($search))
            ->when(request()->sort, fn($query, $sort) => $query->sortBy($sort))
            ->paginate($size)
            ->toArray();
        
        return $data;
    }

    public function indexWorkflowStep() {
        $data = $this->getWorkflowStepPaging();
        
        //dd($data->ddRawSql());
        
        return response()->default(
            200,
            true, 
            "Get Workflow Step Success", 
            [
                "currentPage" => $data['current_page'],
                "lastPage" => $data['last_page'],
                "pageSize" => $data['to'],
                "totalData" => $data['total'],
                "data" => $data['data'],
            ]
        )->setStatusCode(200);
    }

    public function showWorkflowStep($id) {
        $data = VwWorkflowStep::query()->where(VwWorkflowStep::getTableName().'.id', $id)
        ->select(
            VwWorkflowStep::getTableName().'.id',
            VwWorkflowStep::getTableName().'.approval_workflow_id as appConfigWorkflowId',
            VwWorkflowStep::getTableName().'.step',
            VwWorkflowStep::getTableName().'.is_final',
            VwWorkflowStep::getTableName().'.type_id',
            VwWorkflowStep::getTableName().'.type_name',
            VwWorkflowStep::getTableName().'.line_manager_id',
            VwWorkflowStep::getTableName().'.line_manager_name',
            VwWorkflowStep::getTableName().'.position_id',
            VwWorkflowStep::getTableName().'.position_name',
            VwWorkflowStep::getTableName().'.section_id',
            VwWorkflowStep::getTableName().'.section_name',
            VwWorkflowStep::getTableName().'.department_id',
            VwWorkflowStep::getTableName().'.department_name',
            VwWorkflowStep::getTableName().'.created_date', 
            VwWorkflowStep::getTableName().'.created_by', 
            VwWorkflowStep::getTableName().'.modified_date', 
            VwWorkflowStep::getTableName().'.modified_by'
        )->first();
        
        
        if($data){
            return response()->default(
                200,
                true, 
                "Get Approval Workflow Step Detail Success", 
                $data
            )->setStatusCode(200);
        }

        return response()->default(
            404,
            true,
            "Not Found", 
            null
        )->setStatusCode(404);
    }

    public function storeWorkflowStep(Request $request) {
        // Validate request data
        $validatedData = $request->validate([
            'appWorkflowConfigId' => 'required',
            'step' => 'required|numeric',
            'type' => 'required|numeric',
            'lineManager' => 'nullable|nullable',
            'position' => 'nullable|nullable',
            'section' => 'nullable|nullable',
            'department' => 'nullable|nullable',
        ]);

        // Check if code unique
        $code = ApprovalWorkflowCriteria::where(function ($query) use ($validatedData) {
            $query->where('approval_workflow_id', $validatedData['appWorkflowConfigId']);
            $query->where('type', $validatedData['type']);
        })
        ->first();
        
        if ($code) {
            return response()->default(
                400,
                false, 
                "Approval Workflow Step with same Type and Value is exists!",
                null
            )->setStatusCode(400);
        }

        //**UPDATE STEP

        $workflow = ApprovalWorkflowStep::create([
            'approval_workflow_id' => $validatedData['appWorkflowConfigId'],
            'step' => $validatedData['step'],
            'type' => $validatedData['type'],
            'line_manager' => $validatedData['lineManager'],
            'position_id' => $validatedData['position'],
            'section_id' => $validatedData['section'],
            'department_id' => $validatedData['department'],
            //'is_final' => $isLast ? "Y" : "N",
            'status' => 'ACTIVE',
            'created_date' => now(),
            'created_by' => $request->user()->username
        ]);

        //**UPDATE IS FINAL

        // Return success response
        return response()->default(
            200,
            true, 
            "Approval Workflow Step Created",
            $workflow
        )->setStatusCode(200);
    }

    public function updateWorkflowStep(Request $request, $id) {
        // Find approval by ID or return error response
        $data = ApprovalWorkflowStep::find($id);
        
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Approval Workflow Step Not Found",
                null
            )->setStatusCode(400);
        }

        $code = ApprovalWorkflowStep::where(function ($query) use ($data, $request) {
            $query->where('approval_workflow_id', $data['approval_workflow_id']);
            $query->where('type', $request->input('type'));
        })
        ->whereNot('id', $id)
        ->first();
        
        if ($code) {
            return response()->default(
                400,
                false, 
                "Approval Workflow Step with same Type and Value is exists!",
                null
            )->setStatusCode(400);
        }

        //**UPDATE STEP

        // Define the fields that can be updated
        $fields = [
            'step' => 'step',
            'type' => 'type',
            'lineManager' => 'line_manager',
            'position' => 'position',
            'section' => 'section_id',
            'department' => 'department_id'
        ];
    
        // Prepare the update data
        $updateData = [];
    
        foreach ($fields as $requestField => $dbField) {
            if ($request->filled($requestField)) {
                $updateData[$dbField] = $request->input($requestField);
            }
        }
    
        // Always update modified_by field
        $updateData['modified_date'] = now();
        $updateData['modified_by'] = $request->user()->username;
    
        // If there is data to update, perform the update
        if (!empty($updateData)) {
            $data->update($updateData);
        }
    
        //**UPDATE IS FINAL

        // Return success response
        return response()->default(
            200,
            true, 
            "Approval Workflow Step Updated",
            $data
        )->setStatusCode(200);
    }

    public function toggleWorkflowStep(Request $request, $id) {
        // Find approval by ID or return error response
        $data = ApprovalWorkflowStep::find($id);
    
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Approval Workflow Not Found",
                null
            )->setStatusCode(400);
        }

        $exist = ApprovalTransaction::where('approval_workflow_id', $data['approval_workflow_id'])
        ->where('approval_status', 'PENDING APPROVAL')->first();
        if ($exist) {
            return response()->default(
                400,
                false, 
                "There is pending transaction within this approval workflow!",
                null
            )->setStatusCode(400);
        }
    
        // Toggle the status using ternary operator
        $newStatus = $data->status === 'ACTIVE' ? 'INACTIVE' : 'ACTIVE';
    
        // Update approval status and modified_by
        $data->update([
            'status' => $newStatus,
            'modified_by' => $request->user()->username
        ]);
    
        // Return success response with appropriate message
        return response()->default(
            200,
            true, 
            $newStatus === 'ACTIVE' ? "Approval Workflow Step Activated!" : "Approval Workflow Step Deactivated!",
            $data
        )->setStatusCode(200);
    }

    public function destroyWorkflowStep($id) {
        $data = ApprovalWorkflowStep::find($id);
    
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Approval Workflow Step Not Found",
                null
            )->setStatusCode(400);
        }
        
        $exist = ApprovalTransaction::where('approval_workflow_id', $data['approval_workflow_id'])->first();
        if ($exist) {
            return response()->default(
                400,
                false, 
                "Approval Workflow used in transaction, cannot delete!",
                null
            )->setStatusCode(400);
        }

        $data->delete();
        
        return response()->default(
            200,
            true, 
            "Approval Workflow Step deleted!",
            null
        )->setStatusCode(200);
    }
}