<?php

namespace App\Http\Controllers;

use App\Models\JobApplications;
use App\Models\RecruitmentJobs;
use Illuminate\Http\Request;
use App\Models\File;
use Storage;
use App\Models\GlobalTypeDetail;
use Illuminate\Support\Facades\DB;
use App\Traits\HasQueryService;

class JobApplicationsController extends Controller
{
    use HasQueryService;

    public function download($id)
    {

        $job_applications = JobApplications::find($id);
        if(!$job_applications) {
            return response()->default(
                404,
                false,
                "Job application not found!",
                null
            )->setStatusCode(404);
        }

        $filePath = storage_path("app/assets/jobs/application/cv/{$job_applications->cv}");
        
        if (file_exists($filePath)) {
            return response()->download($filePath, $job_applications->cv);
        } else {
            return response()->default(
                404,
                false,
                "File not found!",
                null
            )->setStatusCode(404);   
        }
    }

    /**
     * Display a listing of the resource.
     */
    public function index() {
        try {
            $subQuery = DB::table(JobApplications::getTableName())
        ->leftJoin(RecruitmentJobs::getTableName(), JobApplications::getTableName().'.job_id', '=', RecruitmentJobs::getTableName().'.id')
        ->select(
            JobApplications::getTableName().'.id',
            RecruitmentJobs::getTableName().'.id as job_id',
            RecruitmentJobs::getTableName().'.job_title',
            JobApplications::getTableName().'.full_name',
            JobApplications::getTableName().'.date_of_birth',
            JobApplications::getTableName().'.email',
            JobApplications::getTableName().'.phone',
            JobApplications::getTableName().'.gender',
            JobApplications::getTableName().'.linkedin_url',
            JobApplications::getTableName().'.reason_to_apply',
            JobApplications::getTableName().'.rating',
            JobApplications::getTableName().'.status',
            JobApplications::getTableName().'.created_date',
            JobApplications::getTableName().'.created_by',
            JobApplications::getTableName().'.modified_date',
            JobApplications::getTableName().'.modified_by',
        );

            // Call getPaging, which internally calls applyFiltersAndPagination
            $data = $this->getPaging($subQuery);
            return response()->default(200, true, 'Get Job Application Success', $data);
        } catch (\InvalidArgumentException $e) {
            // Catch specific InvalidArgumentException thrown by sortBy macro
            return response()->default(400, false, $e->getMessage(), null);
        } 
        catch (\Exception $e) {
            // Catch general exceptions and return a 500 response
            return response()->default(500, false, 'An unexpected error occurred.', null);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $creds = $request->validate([
            'full_name' => 'required',
            'date_of_birth' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'gender' => 'required',
            'cv' => 'required|file|mimes:PDF,pdf|max:2048',
            'reason_to_apply' => 'required',
        ]);

        $job = RecruitmentJobs::find($request->job_id);
        if(!$job) {
            return response()->default(
                404,
                false,
                "Job not found!",
                null
            )->setStatusCode(404);
        }

        // //UPLOAD FILE WITH BASE64
        // $cv_file = $request->input('cv');
        // $decodedPdf = base64_decode($cv_file);
        // $cv_name = 'CV_'.$request->full_name.'_'.round(microtime(true) * 1000).'.pdf';
        // $path = 'assets/jobs/application/cv/'.$cv_name;
        // Storage::put($path, $decodedPdf);

        // UPLOAD FILE
        $app_cv = $request->file('cv');
        $app_cv_name = 'CV_'.$request->full_name.'_'.round(microtime(true) * 1000).'.pdf';
        $app_cv->storeAs('assets/jobs/application/cv/', $app_cv_name);


        $job_applications = JobApplications::create([
            'job_id' => $request->job_id,
            'full_name' => $request->full_name,
            'date_of_birth' => $request->date_of_birth,
            'email' => $request->email,
            'phone' => $request->phone,
            'gender' => $request->gender,
            'cv' => $app_cv_name,
            'linkedin_url' => $request->linkedin_url,
            'reason_to_apply' => $request->reason_to_apply,
            'rating' => $request->rating,
            'status' => 'Pending',
            // APPROVAL
            'created_by' => $request->user()->username
        ]);

        return response()->default(
            201,
            true,
            "Job application has been submitted",
            $job_applications
        )->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $job_applications = JobApplications::find($id);
        if(!$job_applications) {
            return respose()->default(
                404,
                false,
                "Job application not found!",
                null
            )->setStatusCode(404);
        }

        $job = RecruitmentJobs::find($job_applications->job_id);

        $response = [
            'id' => $job_applications->id,
            'job_id' => $job_applications->job_id,
            'job_title' => $job->job_title,
            'full_name' => $job_applications->full_name,
            'date_of_birth' => $job_applications->date_of_birth,
            'email' => $job_applications->email,
            'phone' => $job_applications->phone,
            'gender' => $job_applications->gender,
            'cv' => $job_applications->cv,
            'linkedin_url' => $job_applications->linkedin_url,
            'reason_to_apply' => $job_applications->reason_to_apply,
            'rating' => $job_applications->rating,
            'status' => $job_applications->status,
            'created_date' => $job_applications->created_date,
            'created_by' => $job_applications->created_by,
            'modified_date' => $job_applications->modified_date,
            'modified_by' => $job_applications->modified_by
        ];

        return response()->default(
            200,
            true,
            "Success get detail job application",
            $response
        )->setStatusCode(200);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(JobApplications $JobApplications)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $creds = $request->validate([
            'cv' => 'file|mimes:PDF,pdf|max:2048'
        ]);

        $job_applications = JobApplications::find($id);
        if(!$job_applications) {
            return response()->default(
                404,
                false,
                "Job application not found!",
                null
            )->setStatusCode(404);
        }

        if($request->hasFile('cv')) {
            //DELETE EXIST
            Storage::delete('assets/jobs/application/cv/'.$job_applications->cv);

            //UPLOAD FILE WITH BASE64
            // $cv_file = $request->input('cv');
            // $decodedPdf = base64_decode($cv_file);
            // $cv_name = 'CV_'.$request->full_name.'_'.round(microtime(true) * 1000).'.pdf';
            // $path = 'assets/jobs/application/cv/'.$cv_name;
            // Storage::put($path, $decodedPdf);

            // UPLOAD FILE
            $app_cv = $request->file('cv');
            $app_cv_name = 'CV_'.$job_applications->full_name.'_'.round(microtime(true) * 1000).'.pdf';
            $app_cv->storeAs('assets/jobs/application/cv/', $app_cv_name);
            
            //UPDATE NAME FILE
            $job_applications->cv = $app_cv_name;
        }

        // $job_applications->job_id = $request->job_id;
        // $job_applications->full_name = $request->full_name;
        // $job_applications->date_of_birth = $request->date_of_birth;
        // $job_applications->email = $request->email;
        // $job_applications->phone = $request->phone;
        // $job_applications->gender = $request->gender;
        // $job_applications->linkedin_url = $request->linkedin_url;
        // $job_applications->reason_to_apply = $request->reason_to_apply;
        
        $job_applications->rating = $request->rating;
        $job_applications->status = $request->status!=null? $request->status: $job_applications->status;
        $job_applications->modified_by = $request->user()->username;
        $job_applications->update();

        return response()->default(
            200,
            true,
            "Job application has been updated",
            $job_applications
        )->setStatusCode(200);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $job_applications = JobApplications::find($id);
        if(!$job_applications) {
            return response()->default(
                404,
                false,
                "Job application not found!",
                null
            )->setStatusCode(404);
        }

        Storage::delete('assets/jobs/application/cv/'.$job_applications->cv);
        $job_applications->delete();

        return response()->default(
            200,
            true,
            "Job application has been deleted",
            null
        )->setStatusCode(200);
    }
}
