<?php

namespace App\Http\Controllers;

use App\Models\EmployeeLeave;
use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\AttendanceLeave;
use Illuminate\Support\Facades\DB;
use App\Models\GlobalTypeDetail;
use DateTime;
use App\Traits\HasQueryService;

class EmployeeLeaveController extends Controller
{
    use HasQueryService;
    /**
     * Display a listing of the resource.
     */
    public function index() {
        try {
            $subQuery = DB::table(EmployeeLeave::getTableName())
        ->leftJoin(Employee::getTableName(), EmployeeLeave::getTableName().'.employee_id', '=', Employee::getTableName().'.id')
        ->select(
            EmployeeLeave::getTableName().'.id',
            Employee::getTableName().'.employee_code',
            Employee::getTableName().'.name',
            EmployeeLeave::getTableName().'.leave_type',
            EmployeeLeave::getTableName().'.amount',
            EmployeeLeave::getTableName().'.remaining',
            EmployeeLeave::getTableName().'.start_date',
            EmployeeLeave::getTableName().'.end_date',
            EmployeeLeave::getTableName().'.created_date',
            EmployeeLeave::getTableName().'.created_by',
            EmployeeLeave::getTableName().'.modified_date',
            EmployeeLeave::getTableName().'.modified_by'
        );

            // Call getPaging, which internally calls applyFiltersAndPagination
            $data = $this->getPaging($subQuery);
            return response()->default(200, true, 'Get Employee Leave Header Success', $data);
        } catch (\InvalidArgumentException $e) {
            // Catch specific InvalidArgumentException thrown by sortBy macro
            return response()->default(400, false, $e->getMessage(), null);
        } 
        catch (\Exception $e) {
            // Catch general exceptions and return a 500 response
            return response()->default(500, false, 'An unexpected error occurred.', null);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $creds = $request->validate([
            'employee_id' => 'required',
            'leave_type' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
        ]);

        $employee = Employee::find($request->employee_id);
        if(!$employee) {
            return response()->default(
                404,
                false,
                "Employee not found!",
                null
            )->setStatusCode(404);
        }

        $overlaps = AttendanceLeave::where('employee_id', $request->employee_id)
            ->where('is_debt', 1)
            ->where('leave_type', $request->leave_type)
            ->where(function ($query) use ($request) {
                $query->where(function ($q) use ($request) {
                    $q->where('start_date', '<=', $request->start_date)
                        ->where(function ($q) use ($request) {
                            $q->whereNull('end_date')
                            ->orWhere('end_date', '>=', $request->start_date);
                        });
                })->orWhere(function ($q) use ($request) {
                    $q->where('start_date', '<=', $request->end_date)
                        ->where(function ($q) use ($request) {
                            $q->whereNull('end_date')
                            ->orWhere('end_date', '>=', $request->end_date);
                        });
                })->orWhere(function ($q) use ($request) {
                    $q->where('start_date', '>=', $request->start_date)
                    ->where('end_date', '<=', $request->end_date);
                });
            })
            ->distinct(); 

        $realAmount = $request->amount;
        if($overlaps->exists()) {
            $totalOverlapDays = 0;
            foreach ($overlaps->get() as $overlap) {
                $overlapStart = new DateTime(max($overlap->start_date, $request->start_date));
                $overlapEnd = new DateTime(min($overlap->end_date ?? $request->end_date, $request->end_date));
                $interval = $overlapStart->diff($overlapEnd);
                $totalOverlapDays += $interval->days + 1; 
            }
    
            $realAmount = $request->amount - $totalOverlapDays;
        }
        
        $EmployeeLeave = EmployeeLeave::create([
            'employee_id' => $request->employee_id,
            'leave_type' => $request->leave_type,
            'amount' => $realAmount,
            'remaining' => $realAmount,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'created_by' => $request->user()->username
        ]);

        return response()->default(
            201,
            true,
            $overlaps->exists() ?
            $overlaps->get() :
            // "Employee leave has been created with a reduction in the amount of the previous debt" :
            "Employee leave has been created",
            $EmployeeLeave
        )->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $EmployeeLeave = EmployeeLeave::find($id);

        if($EmployeeLeave) {
            $employee = Employee::find($EmployeeLeave->employee_id);
            $leave_type_id = GlobalTypeDetail::where('value', $EmployeeLeave->leave_type)->first();
        
            $response = [
                'id' => $EmployeeLeave->id,
                'employee_id' => $employee->id,
                'employee_code' => $employee->employee_code,
                'employee_name' => $employee->name,
                'leave_type_id' => $leave_type_id->id,
                'leave_type' => $EmployeeLeave->leave_type,
                'amount' => $EmployeeLeave->amount,
                'remaining' => $EmployeeLeave->remaining,
                'start_date' => $EmployeeLeave->start_date,
                'end_date' => $EmployeeLeave->end_date,
                'created_date' => $EmployeeLeave->created_date,
                'created_by' => $EmployeeLeave->created_by,
                'modified_date' => $EmployeeLeave->modified_date,
                'modified_by' => $EmployeeLeave->modified_by
            ];

            return response()->default(
                200,
                true,
                "Success get detail employee leave",
                $response
            )->setStatusCode(200);
        } else {
            return response()->default(
                404,
                false,
                "Employee leave not found!",
                null
            )->setStatusCode(404);
        }

        
    }

    public function showByEmployeeId($employee_id)
    {
        $EmployeeLeave = EmployeeLeave::where('employee_id', $employee_id);

        if($EmployeeLeave->exists()) {
            return response()->default(
                200,
                true,
                "Success get detail employee leave by employee id",
                $EmployeeLeave->get()
            )->setStatusCode(200);
        } else {
            return response()->default(
                404,
                false,
                "This employee doesnt have employee leave data!",
                null
            )->setStatusCode(404);
        }

        
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(EmployeeLeave $EmployeeLeave)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $creds = $request->validate([
            'employee_id' => 'required',
            'leave_type' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
        ]);

        $EmployeeLeave = EmployeeLeave::find($id);
        if(!$EmployeeLeave) {
            return response()->default(
                404,
                false,
                "Employee leave not found!",
                null
            )->setStatusCode(404);
        }

        $employee = Employee::find($request->employee_id);
        if(!$employee) {
            return response()->default(
                404,
                false,
                "Employee not found!",
                null
            )->setStatusCode(404);
        }

        $EmployeeLeave->employee_id = $request->employee_id;
        $EmployeeLeave->leave_type = $request->leave_type;
        $EmployeeLeave->amount = $request->amount;
        $EmployeeLeave->start_date = $request->start_date;
        $EmployeeLeave->end_date = $request->end_date;
        $EmployeeLeave->modified_by = $request->user()->username;
        $EmployeeLeave->update();

        return response()->default(
            200,
            true,
            "Employee leave has beed updated",
            $EmployeeLeave
        )->setStatusCode(200);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $EmployeeLeave = EmployeeLeave::find($id);

        if($EmployeeLeave) {
            $EmployeeLeave->delete();

            return response()->default(
                200,
                true,
                "Employee leave has beed deleted",
                $EmployeeLeave
            )->setStatusCode(200);
        } else {
            return response()->default(
                404,
                false,
                "Employee leave not found!",
                null
            )->setStatusCode(404);
        }
    }

    public function toggle($id) {
        //
    }
}
