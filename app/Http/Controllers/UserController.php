<?php

namespace App\Http\Controllers;

use App\Exports\UsersExport;
use Illuminate\Support\Facades\DB;
use App\Traits\HasQueryService;

use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use App\Models\UserRoleAccess;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\Exceptions\MissingAbilityException;
use App\Http\Controllers\MenuController;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller {
    use HasQueryService;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        try {
            $subQuery = DB::table(User::getTableName())
                ->leftJoin(Employee::getTableName(), User::getTableName().'.employee_id', '=', Employee::getTableName().'.id')
                ->leftJoin(UserRole::getTableName(), User::getTableName().'.id', '=', UserRole::getTableName().'.user_id')
                ->leftJoin(Role::getTableName(), UserRole::getTableName().'.role_id', '=', Role::getTableName().'.id')
                ->select(
                    User::getTableName().'.id as user_id', 
                    User::getTableName().'.email', 
                    User::getTableName().'.username', 
                    User::getTableName().'.status', 
                    Employee::getTableName().".name", 
                    //Role::getTableName().".name as role",
                    Role::getTableName().".slug as role",
                    User::getTableName().'.created_date', 
                    User::getTableName().'.created_by', 
                    User::getTableName().'.modified_date', 
                    User::getTableName().'.modified_by'
                );

            // Call getPaging, which internally calls applyFiltersAndPagination
            $data = $this->getPaging($subQuery);
            return response()->default(200, true, 'Get Users Success', $data);
        } catch (\InvalidArgumentException $e) {
            // Catch specific InvalidArgumentException thrown by sortBy macro
            return response()->default(400, false, $e->getMessage(), null);
        } 
        catch (\Exception $e) {
            // Catch general exceptions and return a 500 response
            return response()->default(500, false, 'An unexpected error occurred.', null);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $creds = $request->validate([
            'employeeId' => 'nullable|integer',
            'email' => 'required|email',
            'username' => 'required',
            'role' => 'required',
            'isDefault' => 'required',
            'password' => 'nullable'
        ]);
        
        //dd(Role::where('slug', $creds['role'])->first());

        $user = User::where('username', $creds['username'])->first();
        if ($user) {
            return response()->default(
                409,
                false, 
                "Username already exists",
                null
            )->setStatusCode(409);
        }

        $user = User::where('employee_id', $creds['employeeId'])->first();
        if ($user) {
            return response()->default(
                409,
                false, 
                "Employee already have user",
                null
            )->setStatusCode(409);
        }

        $user = User::create([
            'email' => $creds['email'],
            'password' => $creds['isDefault']==true?Hash::make('default'):Hash::make($creds['password']),
            'username' => $creds['username'],
            'status' => 'ACTIVE',
            'employee_id'=>$creds['employeeId'],
            'created_by' => $request->user()->username,
            'modified_by' => $request->user()->username
            
        ]);

        //$defaultRoleSlug = config('hydra.default_user_role_slug', 'user');
        $user->roles()->attach(Role::where('slug', $creds['role'])->first(),['status' => "ACTIVE"]);

        return response()->default(
            200,
            true, 
            "User Created",
            $user
        )->setStatusCode(200);
    }

    /**
     * Authenticate an user and dispatch token.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request) {
        $creds = $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        $user = User::where(
            [
                'username' => $creds['username'],
                'status' => "ACTIVE",
            ]
        )->first();
        
        if (! $user || !Hash::check($request->password, $user->password)) {
            return response()->default(
                401,
                false, 
                "Username/Password Not Match!",
                null
            )->setStatusCode(401);
        }

        if (config('hydra.delete_previous_access_tokens_on_login', false)) {
            $user->tokens()->delete();
        }

        $roles = $user->roles->pluck('slug')->first();
        
        $plainTextToken = $user->createToken('hydra-api-token', [$roles])->plainTextToken;
        $sidebar = (new MenuController)->getSidebarJson('AE');

        return response()->default(
            200,
            true, 
            "Login Success", 
            [
                "id"=> $user->id,
                'username' => $user->name,
                'token' => $plainTextToken,
                'role' => $roles,
                "sidebar"=>$sidebar
            ]
        )->setStatusCode(200);
    }

    /**
     * Display the specified resource.
     *
     * @return \App\Models\User  $user
     */
    public function show($id) {
        $data = User::query()->where(User::getTableName().'.id', $id)
        ->leftJoin(Employee::getTableName(), User::getTableName().'.employee_id', '=', Employee::getTableName().'.id')
        ->leftJoin(UserRole::getTableName(), User::getTableName().'.id', '=', UserRole::getTableName().'.user_id')
        ->leftJoin(Role::getTableName(), UserRole::getTableName().'.role_id', '=', Role::getTableName().'.id')
        ->select(
            User::getTableName().'.id', 
            User::getTableName().'.email', 
            User::getTableName().'.username', 
            User::getTableName().'.status', 
            Employee::getTableName().".id as employee_id", 
            Employee::getTableName().".name as employee_name", 
            Role::getTableName().".id as role_id",
            Role::getTableName().".slug as role",
            User::getTableName().'.created_date', 
            User::getTableName().'.created_by', 
            User::getTableName().'.modified_date', 
            User::getTableName().'.modified_by', 
        )->first();
            
        //dd($user);
        return response()->default(
            200,
            true, 
            "Get User Success", 
            $data
        )->setStatusCode(200);
        //return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @return User
     *
     * @throws MissingAbilityException
     */
    public function update(Request $request, $id) {
        $data = User::find($id);

        if(!$data){
            return response()->default(
                400,
                false, 
                "User Not Found",
                null
            )->setStatusCode(400);
        }
        $data->email = $request->email ?? $data->email;
        $data->username = $request->username ?? $data->username;
        $data->modified_by = $request->user()->username;
        //dd($user);
        //$user->password = $request->password ? Hash::make($request->password) : $user->password;
        //$user->email_verified_at = $request->email_verified_at ?? $user->email_verified_at;

        //check if the logged in user is updating it's own record
        
        $loggedInUser = $request->user();
        if ($loggedInUser->id == $data->id) {
            return response()->default(
                409,
                false,
                "You cant update user that you logged in",
                $user
            )->setStatusCode(409);
        } elseif ($loggedInUser->tokenCan('admin') || $loggedInUser->tokenCan('super-admin')) {
            //$user = User::find($user->id);
            $data->update();
            $roleId = Role::where('slug', $request->role)->first()->id;
            $data->roles()->sync([$roleId=>['status' => "ACTIVE"]]);
        } else {
            throw new MissingAbilityException('Not Authorized');
        }
        return response()->default(
            200,
            true, 
            "User Updated",
            $data
        )->setStatusCode(200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return User
     *
     * @throws MissingAbilityException
     */
    public function toggle(Request $request, User $user) {
        $loggedInUser = $request->user();
        $status = null;
        if ($loggedInUser->id == $user->id) {
            return response()->default(
                409,
                false,
                "You cant change status of user that you logged in",
                $user
            )->setStatusCode(200);
        } elseif ($loggedInUser->tokenCan('admin') || $loggedInUser->tokenCan('super-admin')) {
            $user->status == 'ACTIVE' ? $status = 'INACTIVE': $status = 'ACTIVE';
            $user->status = $status;
            $user->update();
        } else {
            throw new MissingAbilityException('Not Authorized');
        }

        return response()->default(
            200,
            true, 
            $status=="ACTIVE"?"User Activated!":"User Deactivated!",
            $user
        )->setStatusCode(200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return User
     *
     * @throws MissingAbilityException
     */
    public function resetPassword(Request $request, User $user) {
        $creds = $request->validate([
            'isDefault' => 'required',
            'password' => 'nullable'
        ]);
        if(!$creds['isDefault']&&$creds['password']==null){
            return response()->default(
                401,
                false, 
                "Password cannot be null!",
                null
            )->setStatusCode(401);
        }

        $loggedInUser = $request->user();
        $status = null;
        if(Hash::check($creds['password'], $user->password)){
            return response()->default(
                409,
                false,
                "You cant reset password with same value of your current password",
                null
            )->setStatusCode(409);
        }
        elseif ($loggedInUser->id == $user->id) {
            return response()->default(
                409,
                false,
                "You cant reset password of user that you logged in",
                $user
            )->setStatusCode(409);
        } elseif ($loggedInUser->tokenCan('admin') || $loggedInUser->tokenCan('super-admin')) {
            $user->password = $creds['isDefault']==true?Hash::make('default'):Hash::make($creds['password']);
            $user->update();
        } else {
            throw new MissingAbilityException('Not Authorized');
        }
        $msg = $creds['isDefault']?'default':'***********';
        return response()->default(
            200,
            true, 
            'Password has been reset to: "'.$msg.'"',
            $user
        )->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user) {
        $adminRole = Role::where('slug', 'admin')->first();
        $userRoles = $user->roles;

        if ($userRoles->contains($adminRole)) {
            //the current user is admin, then if there is only one admin - don't delete
            $numberOfAdmins = Role::where('slug', 'admin')->first()->users()->count();
            if ($numberOfAdmins == 1) {
                return response()->default(
                    409,
                    false, 
                    "Create another admin before deleting this only admin user",
                    null
                )->setStatusCode(200);
            }
        }

        $user->delete();
        
        return response()->default(
            200,
            true, 
            "User Deleted",
            null
        )->setStatusCode(200);
    }

    /**
     * Return Auth user
     *
     * @return mixed
     */
    public function me(Request $request) {
        return $request->user();
    }

    public function logout(Request $request) {
        $request->user()->currentAccessToken()->delete();
        return response()->default(
            200,
            true, 
            "Log Out Success",
            null
        )->setStatusCode(200);
    }

    
    public function getGrantAccess(Request $request){
        $role = $request->user()->roles->pluck('slug')->first();
        
        $gas = UserRoleAccess::where([
            ['role_slug', '=', $role],
            ['status', '=', 'ACTIVE'],
        ])->get();
        
        $data = collect();
        foreach($gas as $ga){
            $data->push(
                [
                    'menu-key' => $ga->menu_key,
                    'privilages' => json_decode($ga->privilages_json)
                ]
            );
        }

        return response()->default(
            200,
            true, 
            "Get Grant Access Success", 
            $data
        )->setStatusCode(200);
    }
    public function export()
    {
        $name = 'users_' . date('Y-m-d i:h:s');
        //$data = Excel::download(new UsersExport(), $name . '.xlsx');
    
        return Excel::download(new UsersExport, 'invoices.xlsx');
        //return $data;
    }
}