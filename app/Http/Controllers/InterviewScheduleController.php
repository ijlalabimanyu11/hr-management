<?php

namespace App\Http\Controllers;

use App\Models\InterviewSchedule;
use Illuminate\Http\Request;
use App\Traits\HasQueryService;

class InterviewScheduleController extends Controller
{
    use HasQueryService;
    /**
     * Display a listing of the resource.
     */
    public function index() {
        try {
            $subQuery = DB::table(InterviewSchedule::getTableName())
        ->leftJoin(RecruitmentJobs::getTableName(), InterviewSchedule::getTableName().'.job_id', '=', RecruitmentJobs::getTableName().'.id')
        ->leftJoin(JobApplications::getTableName(), InterviewSchedule::getTableName().'.application_id', '=', JobApplications::getTableName().'.id')
        ->leftJoin(Employee::getTableName(), InterviewSchedule::getTableName().'.pic', '=', JobApplications::getTableName().'.id')
        ->select(
            JobApplications::getTableName().'.id',
            RecruitmentJobs::getTableName().'.id as job_id',
            RecruitmentJobs::getTableName().'.job_title',
            JobApplications::getTableName().'.id as application_id',
            JobApplications::getTableName().'.full_name as application_name',
            Employee::getTableName().'.id as pic_id',
            Employee::getTableName().'.name as pic_name',
            InterviewSchedule::getTableName().'.start_date',
            InterviewSchedule::getTableName().'.end_date',
            InterviewSchedule::getTableName().'.platform',
            InterviewSchedule::getTableName().'.link',
            InterviewSchedule::getTableName().'.description',
            InterviewSchedule::getTableName().'.status',
            InterviewSchedule::getTableName().'.created_date',
            InterviewSchedule::getTableName().'.created_by',
            InterviewSchedule::getTableName().'.modified_date',
            InterviewSchedule::getTableName().'.modified_by',
        );

            // Call getPaging, which internally calls applyFiltersAndPagination
            $data = $this->getPaging($subQuery);
            return response()->default(200, true, 'Get Interview Schedule Success', $data);
        } catch (\InvalidArgumentException $e) {
            // Catch specific InvalidArgumentException thrown by sortBy macro
            return response()->default(400, false, $e->getMessage(), null);
        } 
        catch (\Exception $e) {
            // Catch general exceptions and return a 500 response
            return response()->default(500, false, 'An unexpected error occurred.', null);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $creds = $request->validate([
            'job_id' => 'required',
            'application_id' => 'required',
            'pic' => 'required',
            'department' => 'required',
            'position' => 'required',
            'employee' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'platform' => 'required',
            'link' => 'required'
        ]);

        $job_data = RecruitmentJobs::find($request->job_id);
        $application_data = JobApplications::find($request->application_id);
        $pic_data = Employee::find($request->pic);
        $department_data = Department::find($request->department);
        $position_data = Position::find($request->position);
        $employee_data = Employee::find($request->employee);
        
        if(!$job_data || !$application_data || !$pic_data || !$department_data || !$position_data || !$employee_data) {
            return response()->default(
                404,
                false,
                "Data reference not found!",
                null
            )->setStatusCode(404);
        }

        $interviewSchedule = InterviewSchedule::create([
            'job_id' => $request->job_id,
            'application_id' => $request->application_id,
            'applier_name' => $application_data->full_name,
            'pic' => $request->pic,
            'department' => $request->department,
            'position' => $request->position,
            'employee' => $request->employee,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'platform' => $request->platform,
            'link' => $request->link,
            'description' => $request->description,
            'status' => 'Pending',
            'created_by' => $request->user()->username
        ]);

        return response()->default(
            201,
            true,
            "Interview schedule has been created",
            $interviewSchedule
        )->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $interview_schedule = InterviewSchedule::find($id);
        if(!$interview_schedule) {
            return response()->default(
                404,
                false,
                "Interview schedule not found!",
                null
            )->setStatusCode(404);
        }

        $job_data = RecruitmentJobs::find($interview_schedule->job_id);
        $application_data = JobApplications::find($interview_schedule->application_id);
        $pic_data = Employee::find($interview_schedule->pic);

        $response = [
            'id' => $interview_schedule->id,
            'job_id' => $job_data->id,
            'job_title' => $job_data->job_title,
            'application_id' => $application_data->id,
            'application_name' => $application_data->full_name,
            'pic_id' => $pic_data->id,
            'pic_name' => $pic_data->name,
            'start_date' => $interview_schedule->start_date,
            'end_date' => $interview_schedule->end_date,
            'platform' => $interview_schedule->platform,
            'link' => $interview_schedule->link,
            'description' => $interview_schedule->description,
            'status' => $interview_schedule->status,
            'created_date' => $interview_schedule->created_date,
            'created_by' => $interview_schedule->created_by,
            'modified_date' => $interview_schedule->modified_date,
            'modified_by' => $interview_schedule->modified_by
        ];

        return response()->default(
            200,
            true,
            "Success get detail interview schedule",
            $response
        )->setStatusCode(200);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(InterviewSchedule $interviewSchedule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $cred = $request->validate([
            'job_id' => 'required',
            'application_id' => 'required',
            'pic' => 'required',
            'department' => 'required',
            'position' => 'required',
            'employee' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'platform' => 'required',
            'link' => 'required'
        ]);

        $interviewSchedule = InterviewSchedule::find($id);
        if(!$interviewSchedule) {
            return response()->default(
                404,
                false,
                "Interview schedule not found!",
                null
            )->setStatusCode(404);
        }

        //update
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $interview_schedule = InterviewSchedule::find($id);
        if(!$interview_schedule) {
            return response()->default(
                404,
                false,
                "Interview schedule not found!",
                null
            )->setStatusCode(404);
        }

        if($interview_schedule->status == 'Done') {
            return response()->default(
                404,
                false,
                "You cannot delete this interview schedule data, because this status done",
                null
            )->setStatusCode(200);
        }
    }
}
