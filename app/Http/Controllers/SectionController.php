<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\Exceptions\MissingAbilityException;
use App\Traits\HasQueryService;

//IMPORT EXCEL UTILS
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;

//IMPORT MODEL
use App\Models\Section;
use App\Models\Employee;
use App\Models\EmployeeDesignation;

class SectionController extends Controller {
    use HasQueryService;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function index() {
        try {
            $subQuery = DB::table(Section::getTableName())
        ->leftJoin(Employee::getTableName(), Section::getTableName().'.head', '=', Employee::getTableName().'.id')
        ->select(
            Section::getTableName().'.id',
            Section::getTableName().'.code as section_code',
            Section::getTableName().'.name as section_name',
            Employee::getTableName().".name as section_head", 
            Section::getTableName().'.description',
            Section::getTableName().'.status',
            Section::getTableName().'.created_date', 
            Section::getTableName().'.created_by', 
            Section::getTableName().'.modified_date', 
            Section::getTableName().'.modified_by'
        );

            // Call getPaging, which internally calls applyFiltersAndPagination
            $data = $this->getPaging($subQuery);
            return response()->default(200, true, 'Get Sections Success', $data);
        } catch (\InvalidArgumentException $e) {
            // Catch specific InvalidArgumentException thrown by sortBy macro
            return response()->default(400, false, $e->getMessage(), null);
        } 
        catch (\Exception $e) {
            // Catch general exceptions and return a 500 response
            return response()->default(500, false, 'An unexpected error occurred.', null);
        }
    }

    public function show($id) {
        $data = Section::query()->where(Section::getTableName().'.id', $id)
        ->leftJoin(Employee::getTableName(), Section::getTableName().'.head', '=', Employee::getTableName().'.id')
        ->select(
            Section::getTableName().'.id',
            Section::getTableName().'.code as sectionCode',
            Section::getTableName().'.name as sectionName',
            Employee::getTableName().".id as employee_id", 
            Employee::getTableName().".name as sectionHead", 
            Section::getTableName().'.description',
            Section::getTableName().'.status',
            Section::getTableName().'.created_date', 
            Section::getTableName().'.created_by', 
            Section::getTableName().'.modified_date', 
            Section::getTableName().'.modified_by'
        )->first();

        return response()->default(
            200,
            true, 
            "Get Section Detail Success", 
            $data
        )->setStatusCode(200);
    }

    public function store(Request $request) {
        // Validate request data
        $validatedData = $request->validate([
            'sectionCode' => 'required',
            'sectionName' => 'required',
            'sectionHead' => 'nullable|integer',
            'description' => 'nullable'
        ]);
    
        // Check if the employee is already a section head
        $emp = !empty($validatedData['departmentHead']) 
        ? Department::where('head', $validatedData['departmentHead'])->first() 
        : null;

        if ($emp) {
            return response()->default(
                409,
                false, 
                "Employee is already a head of another section",
                null
            )->setStatusCode(409);
        }

        // Check if code & name unique
        $dept = Section::where('code', $validatedData['sectionCode'])
        ->orWhere('name', $validatedData['sectionName'])
        ->first();
        if ($dept) {
            return response()->default(
                400,
                false, 
                "Section Code & Name is exists!",
                null
            )->setStatusCode(400);
        }
    
        // Create the section
        $stored = Section::create([
            'code' => $validatedData['sectionCode'],
            'name' => $validatedData['sectionName'],
            'head' => $validatedData['sectionHead'],
            'description' => $validatedData['description'],
            'status' => 'ACTIVE',
            'created_by' => $request->user()->username
        ]);
    
        // Return success response
        return response()->default(
            200,
            true, 
            "Section Created",
            $stored
        )->setStatusCode(200);
    }
    

    public function update(Request $request, $id) {
        // Find section by ID or return error response
        $data = Section::find($id);
    
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Section Not Found",
                null
            )->setStatusCode(400);
        }

        // Check if the employee is already a section head
        $emp = !empty($request['sectionHead']) 
        ? Section::where('head', $request['sectionHead'])->whereNot('id', $id)->first() 
        : null;

        if ($emp) {
            return response()->default(
                409,
                false, 
                "Employee is already a head of another section",
                null
            )->setStatusCode(409);
        }

        // Check if code & name unique
        $sect = Section::where(function ($query) use ($request) {
            $query->where('code', $request['sectionCode'])
                  ->orWhere('name', $request['sectionName']);
        })
        ->whereNot('id', $id)
        ->first();
        
        if ($sect) {
            return response()->default(
                400,
                false, 
                "Section Code & Name is exists!",
                null
            )->setStatusCode(400);
        }
    
        // Define the fields that can be updated
        $fields = [
            'sectioncode' => 'code',
            'sectionName' => 'name',
            'sectionHead' => 'head',
            'description' => 'description'
        ];
    
        // Prepare the update data
        $updateData = [];
    
        foreach ($fields as $requestField => $dbField) {
            if ($request->filled($requestField)) {
                $updateData[$dbField] = $request->input($requestField);
            }
        }
    
        // Always update modified_by field
        //$updateData['modified_by'] = $request->user()->username;
    
        // If there is data to update, perform the update
        if (!empty($updateData)) {
            $data->update($updateData);
        }
    
        // Return success response
        return response()->default(
            200,
            true, 
            "Section Updated",
            $data
        )->setStatusCode(200);
    }
    

    public function toggle(Request $request, $id) {
        // Find section by ID or return error response
        $data = Section::find($id);
    
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Section Not Found",
                null
            )->setStatusCode(400);
        }
    
        // Toggle the status using ternary operator
        $newStatus = $data->status === 'ACTIVE' ? 'INACTIVE' : 'ACTIVE';
    
        // Update section status and modified_by
        $data->update([
            'status' => $newStatus,
            'modified_by' => $request->user()->username
        ]);
    
        // Return success response with appropriate message
        return response()->default(
            200,
            true, 
            $newStatus === 'ACTIVE' ? "Section Activated!" : "Section Deactivated!",
            $data
        )->setStatusCode(200);
    }

    public function destroy($id) {
        $data = Section::find($id);

        $dsg = EmployeeDesignation::where('section', $id)->first();
        if ($dsg) {
            return response()->default(
                409,
                false, 
                "Section used in employee designation!",
                null
            )->setStatusCode(409);
        }

        $data->delete();
        
        return response()->default(
            200,
            true, 
            "Section Deleted",
            null
        )->setStatusCode(200);
    }

    public function export()
    {
        $name = 'users_' . date('Y-m-d i:h:s');
        //$data = Excel::download(new UsersExport(), $name . '.xlsx');
    
        return Excel::download(new UsersExport, 'invoices.xlsx');
        //return $data;
    }

    public function getLOV(){
        $items = Section::select(['id','name'])->where('status','ACTIVE')->get()->toArray();
        $data = collect();
        foreach($items as $item){
            $data->push(
                [
                    'key' => $item['id'],
                    'value' => $item['name']
                ]
            );
        }
        return response()->default(
            200,
            true, 
            "Get Section LOV Success", 
            $data
        )->setStatusCode(200);
    }
}