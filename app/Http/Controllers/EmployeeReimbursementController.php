<?php

namespace App\Http\Controllers;

use App\Models\EmployeeReimbursement;
use Illuminate\Http\Request;
use App\Models\Employee;
use Illuminate\Support\Facades\DB;
use App\Models\GlobalTypeDetail;
use App\Traits\HasQueryService;

class EmployeeReimbursementController extends Controller
{
    use HasQueryService;
    /**
     * Display a listing of the resource.
     */
    public function index() {
        try {
            $subQuery = DB::table(EmployeeReimbursement::getTableName())
        ->leftJoin(Employee::getTableName(), EmployeeReimbursement::getTableName().'.employee_id', '=', Employee::getTableName().'.id')
        ->select(
            EmployeeReimbursement::getTableName().'.id',
            Employee::getTableName().'.employee_code',
            Employee::getTableName().'.name',
            EmployeeReimbursement::getTableName().'.reimbursement_type',
            EmployeeReimbursement::getTableName().'.amount',
            EmployeeReimbursement::getTableName().'.start_date',
            EmployeeReimbursement::getTableName().'.end_date',
            EmployeeReimbursement::getTableName().'.created_date',
            EmployeeReimbursement::getTableName().'.created_by',
            EmployeeReimbursement::getTableName().'.modified_date',
            EmployeeReimbursement::getTableName().'.modified_by'
        );

            // Call getPaging, which internally calls applyFiltersAndPagination
            $data = $this->getPaging($subQuery);
            return response()->default(200, true, 'Get Employee Reimbursement Header Success', $data);
        } catch (\InvalidArgumentException $e) {
            // Catch specific InvalidArgumentException thrown by sortBy macro
            return response()->default(400, false, $e->getMessage(), null);
        } 
        catch (\Exception $e) {
            // Catch general exceptions and return a 500 response
            return response()->default(500, false, 'An unexpected error occurred.', null);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $creds = $request->validate([
            'employee_id' => 'required',
            'reimbursement_type' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
        ]);

        $employee = Employee::find($request->employee_id);
        if(!$employee) {
            return response()->default(
                404,
                false,
                "Employee not found!",
                null
            )->setStatusCode(404);
        }

        $EmployeeReimbursement = EmployeeReimbursement::create([
            'employee_id' => $request->employee_id,
            'reimbursement_type' => $request->reimbursement_type,
            'amount' => $request->amount,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'created_by' => $request->user()->username
        ]);

        return response()->default(
            201,
            true,
            "Employee reimbursement has been created",
            $EmployeeReimbursement
        )->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $EmployeeReimbursement = EmployeeReimbursement::find($id);

        if($EmployeeReimbursement) {
            $employee = Employee::find($EmployeeReimbursement->employee_id);
            $reimbursement_type_id = GlobalTypeDetail::where('value', $EmployeeReimbursement->reimbursement_type)->first();
        
            $response = [
                'id' => $EmployeeReimbursement->id,
                'employee_id' => $employee->id,
                'employee_code' => $employee->employee_code,
                'employee_name' => $employee->name,
                'reimbursement_type_id' => $reimbursement_type_id->id,
                'reimbursement_type' => $EmployeeReimbursement->reimbursement_type,
                'amount' => $EmployeeReimbursement->amount,
                'start_date' => $EmployeeReimbursement->start_date,
                'end_date' => $EmployeeReimbursement->end_date,
                'created_date' => $EmployeeReimbursement->created_date,
                'created_by' => $EmployeeReimbursement->created_by,
                'modified_date' => $EmployeeReimbursement->modified_date,
                'modified_by' => $EmployeeReimbursement->modified_by
            ];

            return response()->default(
                200,
                true,
                "Success get detail employee reimbursement",
                $response
            )->setStatusCode(200);
        } else {
            return response()->default(
                404,
                false,
                "Employee reimbursement not found!",
                null
            )->setStatusCode(404);
        }

        
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(EmployeeReimbursement $EmployeeReimbursement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $creds = $request->validate([
            'employee_id' => 'required',
            'reimbursement_type' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
        ]);

        $EmployeeReimbursement = EmployeeReimbursement::find($id);
        if(!$EmployeeReimbursement) {
            return response()->default(
                404,
                false,
                "Employee reimbursement not found!",
                null
            )->setStatusCode(404);
        }

        $employee = Employee::find($request->employee_id);
        if(!$employee) {
            return response()->default(
                404,
                false,
                "Employee not found!",
                null
            )->setStatusCode(404);
        }

        $EmployeeReimbursement->employee_id = $request->employee_id;
        $EmployeeReimbursement->reimbursement_type = $request->reimbursement_type;
        $EmployeeReimbursement->amount = $request->amount;
        $EmployeeReimbursement->start_date = $request->start_date;
        $EmployeeReimbursement->end_date = $request->end_date;
        $EmployeeReimbursement->modified_by = $request->user()->username;
        $EmployeeReimbursement->update();

        return response()->default(
            200,
            true,
            "Employee reimbursement has beed updated",
            $EmployeeReimbursement
        )->setStatusCode(200);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $EmployeeReimbursement = EmployeeReimbursement::find($id);

        if($EmployeeReimbursement) {
            $EmployeeReimbursement->delete();

            return response()->default(
                200,
                true,
                "Employee reimbursement has beed deleted",
                $EmployeeReimbursement
            )->setStatusCode(200);
        } else {
            return response()->default(
                404,
                false,
                "Employee reimbursement not found!",
                null
            )->setStatusCode(404);
        }
    }

    public function toggle($id) {
        //
    }
}
