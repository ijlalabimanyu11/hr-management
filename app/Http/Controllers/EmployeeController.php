<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Models\Employee;
use App\Models\EmployeeCertification;
use App\Models\EmployeeDesignation;
use App\Models\GlobalTypeDetail;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\Models\File;
use Storage;
use App\Traits\HasQueryService;

class EmployeeController extends Controller {
    use HasQueryService;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index() {
    //     return Employee::all();
    // }

    /*
    public function store(Request $request) {
        $data = $request->validate([
            'name' => 'required',
            'slug' => 'required',
        ]);

        $existing = Role::where('slug', $data['slug'])->first();

        if (! $existing) {
            $role = Role::create([
                'name' => $data['name'],
                'slug' => $data['slug'],
            ]);

            return $role;
        }

        return response(['error' => 1, 'message' => 'role already exists'], 409);
    }

    public function show(Role $role) {
        return $role;
    }

    public function update(Request $request, ?Role $role = null) {
        if (! $role) {
            return response(['error' => 1, 'message' => 'role doesn\'t exist'], 404);
        }

        $role->name = $request->name ?? $role->name;

        if ($request->slug) {
            if ($role->slug != 'admin' && $role->slug != 'super-admin') {
                //don't allow changing the admin slug, because it will make the routes inaccessbile due to faile ability check
                $role->slug = $request->slug;
            }
        }

        $role->update();

        return $role;
    }
    */
    public function getEmployeeLOV(){
        $items = Employee::select(['id','name'])->where('status','ACTIVE')->get()->toArray();
        $data = collect();
        foreach($items as $item){
            $data->push(
                [
                    'key' => $item['id'],
                    'value' => $item['name']
                ]
            );
        }
        return response()->default(
            200,
            true, 
            "Get Employee LOV Success", 
            $data
        )->setStatusCode(200);
    }

    public function store(Request $request) {
        $creds = $request->validate([
            'employee_code' => 'required',
            'employee_picture' => 'required|image:jpeg,png,jpg|max:2048',
            'cv' => 'required|sometimes|file|mimes:pdf|max:2048'
        ]);

        $employee = Employee::where('employee_code', $creds['employee_code'])->first();
        if($employee) {
            return response()->default(
                409,
                false,
                "employee_code already exists",
                null
            )->setStatusCode(409);
        }

        // UPLOAD EMPLOYEE PICTURE
        $em_picture = $request->file('employee_picture');
        if($em_picture){
            $em_picture_name = 'PHOTO_' . $request->employee_code . '.' . $em_picture->getClientOriginalExtension();
            $em_picture->storeAs('assets/employee/photo', $em_picture_name);
        }
        
        //dd($em_picture);
        // UPLOAD EMPLOYEE CV
        $em_cv = $request->file('cv');
        if($em_cv){
            $em_cv_name = 'CV_' . $request->employee_code . '.' . $em_cv->getClientOriginalExtension();
            $em_cv->storeAs('assets/employee/cv', $em_cv_name);
        }
        $employee = Employee::create([
            'employee_code' => $request->employee_code,
            'national_identifier' => $request->national_identifier,
            'name' => $request->name,
            'email' => $request->email,
            'birth_date' => $request->birth_date,
            'gender' => $request->gender,
            'phone' => $request->phone,
            'address' => $request->address,
            'salary_type' => $request->salary_type,
            'basic_salary' => $request->basic_salary,

            'bank_name' => $request->bank_name,
            'branch_location' => $request->branch_location,
            'bank_account_name' => $request->bank_account_name,
            'bank_account_number' => $request->bank_account_number,
            'tax_payer_identifier' => $request->tax_payer_identifier,

            'employee_picture' => $em_picture?$em_picture_name:null,
            'cv' => $em_cv?$em_cv_name:null,

            'join_date' => $request->join_date,
            'status' => 'ACTIVE',
            'created_by' => $request->user()->username,
            'created_date' => now()
        ]);

        return response()->default(
            201,
            true,
            "Employee has been created",
            $employee
        )->setStatusCode(200);
    }

    public function storeCertification(Request $request) {
        $creds = $request->validate([
            'employee_id' => 'required',
            'certification' => 'required|file|mimes:PDF,pdf|max:10240',
            'name' => 'required',
            'issuing_organization' => 'required',
            'issuing_date' => 'required'
        ]);

        $employee = Employee::find($request->employee_id);
        if(!$employee) {
            return response()->default(
                404,
                false,
                "Employee id not found!",
                null
            )->setStatusCode(404);
        }

        // UPLOAD EMPLOYEE CERTIFICATION
        $em_certification = $request->file('certification');
        $em_certification_name = round(microtime(true) * 1000).'-'.str_replace(' ','-',$em_certification->getClientOriginalName());
        $em_certification->storeAs('assets/employee/certification', $em_certification_name);

        $employeeCertification = EmployeeCertification::create([
            'employee_id' => $request->employee_id,
            'name' => $request->name,
            'file' => $em_certification_name,
            'issuing_organization' => $request->issuing_organization,
            'issuing_date' => $request->issuing_date,
            'expired_date' => $request->expired_date,
            'notes' => $request->notes,
            'status' => 'ACTIVE',
            'created_by' => $request->user()->username,
            'created_date' => now()
        ]);

        return response()->default(
            201,
            true,
            "Certification has been uploaded",
            $employeeCertification
        )->setStatusCode(200);
    }

    public function storeCertificationByFile(Request $request) {
        $creds = $request->validate([
            'employee_id' => 'required',
            'file_name' => 'required',
            'certification' => 'required|file|mimes:PDF,pdf|max:10240',
            'name' => 'required',
            'issuing_organization' => 'required',
            'issuing_date' => 'required'
        ]);

        $employee = Employee::find($request->employee_id);
        if(!$employee) {
            return response()->default(
                404,
                false,
                "Employee id not found!",
                null
            )->setStatusCode(404);
        }

        if($request->id != null) {
            $existFile = EmployeeCertification::where('employee_id', $request->employee_id)
            ->where('file', $request->file_name)
            ->first();

            if(!$existFile) {
                // UPLOAD EMPLOYEE CERTIFICATION
                $em_certification = $request->file('certification');
                $em_certification_name = round(microtime(true) * 1000).'-'.str_replace(' ','-',$em_certification->getClientOriginalName());
                $em_certification->storeAs('assets/employee/certification', $em_certification_name);
            }

            $employeeCertification = EmployeeCertification::find($request->id);
            if(!$employeeCertification) {
                return response()->default(
                    404,
                    false,
                    "Certification not found!",
                    null
                )->setStatusCode(404);
            }
            $employeeCertification->name = $request->name;
            $employeeCertification->issuing_organization = $request->issuing_organization;
            $employeeCertification->issuing_date = $request->issuing_date;
            $employeeCertification->expired_date = $request->expired_date;
            $employeeCertification->notes = $request->notes;
            if(!$existFile) {
                Storage::delete('assets/employee/certification/'.$employeeCertification->file);
                $employeeCertification->file = $em_certification_name;
            }
            // $employee->status = 'ACTIVE',
            $employeeCertification->modified_by = $request->user()->username;
            $employeeCertification->modified_date = now();
            $employeeCertification->update();
        } else {
            // UPLOAD EMPLOYEE CERTIFICATION
            $em_certification = $request->file('certification');
            $em_certification_name = round(microtime(true) * 1000).'-'.str_replace(' ','-',$em_certification->getClientOriginalName());
            $em_certification->storeAs('assets/employee/certification', $em_certification_name);
            $employeeCertification = EmployeeCertification::create([
                'employee_id' => $request->employee_id,
                'name' => $request->name,
                'file' => $em_certification_name,
                'issuing_organization' => $request->issuing_organization,
                'issuing_date' => $request->issuing_date,
                'expired_date' => $request->expired_date,
                'notes' => $request->notes,
                'status' => 'ACTIVE',
                'created_by' => $request->user()->username,
                'created_date' => now()
            ]);
        }

        return response()->default(
            201,
            true,
            $request->id != null ? "Certification has been updated" : "Certification has been created",
            $employeeCertification
        )->setStatusCode(200);
    }

    public function deleteCertification($id) {
        $employeeCertification = EmployeeCertification::find($id);
        if(!$employeeCertification) {
            return response()->default(
                404,
                false,
                "Certification not found!",
                null
            )->setStatusCode(404);
        }

        // DELETE FILE AND DATA
        Storage::delete('assets/employee/certification/'.$employeeCertification->file);
        $employeeCertification->delete();

        return response()->default(
            201,
            true,
            "Certification has been deleted",
            null
        )->setStatusCode(200);
    }

    public function updateCertification(Request $request, $id) {
        $creds = $request->validate([
            'certification' => 'nullable|file|mimes:PDF,pdf|max:2048',
            'name' => 'required',
            'issuing_organization' => 'required',
            'issuing_date' => 'required'
        ]);

        $employeeCertification = EmployeeCertification::find($id);

        if(!$employeeCertification) {
            return response()->default(
                404,
                false,
                "Certification not found!",
                null
            )->setStatusCode(404);
        }

        if($request->hasFile('certification')) {
            Storage::delete('assets/employee/certification/'.$employeeCertification->file);
            $em_certification = $request->file('certification');
            $em_certification_name = round(microtime(true) * 1000).'-'.str_replace(' ','-',$em_certification->getClientOriginalName());
            $em_certification->storeAs('assets/employee/certification', $em_certification_name);

            $employeeCertification->file = $em_certification_name;
        }

        $employeeCertification->name = $request->name;
        $employeeCertification->issuing_organization = $request->issuing_organization;
        $employeeCertification->issuing_date = $request->issuing_date;
        $employeeCertification->expired_date = $request->expired_date;
        $employeeCertification->notes = $request->notes;
        
        // $employee->status = 'ACTIVE',
        $employeeCertification->modified_by = $request->user()->username;
        $employeeCertification->modified_date = now();
        $employeeCertification->update();

        return response()->default(
            200,
            true,
            "Certification has been updated",
            $employeeCertification
        )->setStatusCode(200);

    }

    public function download($id, string $type)
    {
        $employee = Employee::find($id);
        if(!$employee) {
            return response()->default(
                404,
                false,
                "Employee id not found!",
                null
            )->setStatusCode(404);
        }

        if($type=='photo') {
            $filePath = storage_path("app/assets/employee/photo/{$employee->employee_picture}");
        } else if($type=='cv') {
            $filePath = storage_path("app/assets/employee/cv/{$employee->cv}");
        } else {
            return response()->default(
                404,
                false,
                "Url download path not found!",
                null
            )->setStatusCode(404);
        }
        
        if (file_exists($filePath)) {
            //return response()->download($filePath, $type=='photo' ? $employee->employee_picture : $employee->cv);
            $flname = $type=='photo'?$employee->employee_picture : $employee->cv;
            return response()->file($filePath, [
                'Content-Disposition' => 'attachment; filename='.$flname
            ]);
        } else {
            return response()->default(
                404,
                false,
                "File not found!",
                null
            )->setStatusCode(404);   
        }
    }

    public function downloadCertification($id)
    {
        $employeeCertification = EmployeeCertification::find($id);

        $filePath = storage_path("app/assets/employee/certification/{$employeeCertification->file}");
        
        if (file_exists($filePath)) {
            //return response()->download($filePath, $employeeCertification->file);
            return response()->file($filePath, [
                'Content-Disposition' => 'attachment; filename='.$employeeCertification->file
            ]);
        } else {
            return response()->default(
                404,
                false,
                "File not found!",
                null
            )->setStatusCode(404);   
        }
    }

    public function update(Request $request, $id) {
        $creds = $request->validate([
            'employee_code' => 'required',
        ]);

        $employee = Employee::find($id);

        if(!$employee) {
            return response()->default(
                404,
                false,
                "Employee not found!",
                null
            )->setStatusCode(404);
        }

        if(Employee::where('id', '!=', $id)
            ->where(function (Builder $query) use ($creds){
                $query->where('employee_code', $creds['employee_code']);
            })
            ->first()
        ) {
            return response()->default(
                400,
                false,
                "Employee code already exists!",
                null
            )->setStatusCode(400);        
        }

        if($request->hasFile('employee_picture')) {
            $request->validate([
                'employee_picture' => 'required|image:jpeg,png,jpg|max:2048'
            ]);
            Storage::delete('assets/employee/photo/'.$employee->employee_picture);
            $em_picture = $request->file('employee_picture');
            $em_picture_name = 'PHOTO_' . $request->employee_code . '.' . $em_picture->getClientOriginalExtension();
            $em_picture->storeAs('assets/employee/photo', $em_picture_name);

            $employee->employee_picture = $em_picture_name;
        }

        if($request->hasFile('cv')) {
            $request->validate([
                'cv' => 'required|file:pdf|max:2048'
            ]);
            Storage::delete('assets/employee/cv/'.$employee->cv);
            $em_cv = $request->file('cv');
            $em_cv_name = 'CV_' . $request->employee_code . '.' . $em_cv->getClientOriginalExtension();
            $em_cv->storeAs('assets/employee/cv', $em_cv_name);

            $employee->cv = $em_cv_name;
        }

        if($employee->employee_code!=$request->employee_code) {
            $name_picture = 'PHOTO_'.$request->employee_code.'.'.(pathinfo($employee->employee_picture, PATHINFO_EXTENSION));
            $name_cv = 'CV_'.$request->employee_code.'.'.(pathinfo($employee->cv, PATHINFO_EXTENSION));
            Storage::move('assets/employee/photo/'.$employee->employee_picture, 'assets/employee/photo/'.$name_picture);
            Storage::move('assets/employee/cv/'.$employee->cv, 'assets/employee/cv/'.$name_cv);
            $employee->employee_picture = $name_picture;
            $employee->cv = $name_cv;
        }

        $employee->employee_code = $request->employee_code;
        $employee->national_identifier = $request->national_identifier;
        $employee->name = $request->name;
        $employee->email = $request->email;
        $employee->birth_date = $request->birth_date;
        $employee->gender = $request->gender;
        $employee->phone = $request->phone;
        $employee->address = $request->address;
        $employee->salary_type = $request->salary_type;
        $employee->basic_salary = $request->basic_salary;

        $employee->bank_name = $request->bank_name;
        $employee->branch_location = $request->branch_location;
        $employee->bank_account_name = $request->bank_account_name;
        $employee->bank_account_number = $request->bank_account_number;
        $employee->tax_payer_identifier = $request->tax_payer_identifier;

        $employee->join_date = $request->join_date;
        // $employee->status = 'ACTIVE',
        $employee->modified_by = $request->user()->username;
        $employee->modified_date = now();
        $employee->update();

        return response()->default(
            200,
            true,
            "Employee has been updated",
            $employee
        )->setStatusCode(200);

    }

    public function index() {
        try {
            $subQuery = DB::table(Employee::getTableName())
            ->select(
                Employee::getTableName().'.id',
                Employee::getTableName().'.employee_code',
                Employee::getTableName().'.national_identifier',
                Employee::getTableName().'.name',
                Employee::getTableName().'.email',
                Employee::getTableName().'.birth_date',
                Employee::getTableName().'.gender',
                Employee::getTableName().'.phone',
                Employee::getTableName().'.address',
                Employee::getTableName().'.salary_type',
                Employee::getTableName().'.basic_salary',
                Employee::getTableName().'.bank_name',
                Employee::getTableName().'.branch_location',
                Employee::getTableName().'.bank_account_name',
                Employee::getTableName().'.bank_account_number',
                Employee::getTableName().'.tax_payer_identifier',
                Employee::getTableName().'.employee_picture',
                Employee::getTableName().'.cv',
                Employee::getTableName().'.join_date',
                Employee::getTableName().'.status',
                Employee::getTableName().'.created_date', 
                Employee::getTableName().'.created_by', 
                Employee::getTableName().'.modified_date', 
                Employee::getTableName().'.modified_by'
            );

            // Call getPaging, which internally calls applyFiltersAndPagination
            $data = $this->getPaging($subQuery);
            return response()->default(200, true, 'Get Employees Header Success', $data);
        } catch (\InvalidArgumentException $e) {
            // Catch specific InvalidArgumentException thrown by sortBy macro
            return response()->default(400, false, $e->getMessage(), null);
        } 
        catch (\Exception $e) {
            // Catch general exceptions and return a 500 response
            return response()->default(500, false, 'An unexpected error occurred.', null);
        }
    }

    public function indexCertification(int $employee_id) {
        try {
            $subQuery = DB::table(EmployeeCertification::getTableName())
            ->select(
                '*'
            )
            ->where('employee_id', $employee_id);

            // Call getPaging, which internally calls applyFiltersAndPagination
            $data = $this->getPaging($subQuery);
            return response()->default(200, true, 'Get Employee Certification Success', $data);
        } catch (\InvalidArgumentException $e) {
            // Catch specific InvalidArgumentException thrown by sortBy macro
            return response()->default(400, false, $e->getMessage(), null);
        } 
        catch (\Exception $e) {
            // Catch general exceptions and return a 500 response
            return response()->default(500, false, 'An unexpected error occurred.', null);
        }
    }

    public function show($id) {
        $employee = Employee::find($id);

        if(!$employee) {
            return response()->default(
                404,
                false,
                "Employee not found!",
                null
            )->setStatusCode(404);
        }

        // GLOBAL TYPE ID
        $salary_type_id = GlobalTypeDetail::where('value', $employee->salary_type)->first();
        $basic_salary_id = GlobalTypeDetail::where('value', $employee->basic_salary)->first();
        $bank_name_id = GlobalTypeDetail::where('value', $employee->bank_name)->first();
       
        $response = [
            'id' => $employee->id,
            'employee_code' => $employee->employee_code,
            'national_identifier' => $employee->national_identifier,
            'name' => $employee->name,
            'email' => $employee->email,
            'birth_date' => $employee->birth_date,
            'gender' => $employee->gender,
            'phone' => $employee->phone,
            'address' => $employee->address,
            'salary_type_id' => $salary_type_id->id,
            'salary_type' => $employee->salary_type,
            'basic_salary_id' => $basic_salary_id->id,
            'basic_salary' => $employee->basic_salary,
            'bank_name_id' => $bank_name_id->id,
            'bank_name' => $employee->bank_name,
            'branch_location' => $employee->branch_location,
            'bank_account_name' => $employee->bank_account_name,
            'bank_account_number' => $employee->bank_account_number,
            'tax_payer_identifier' => $employee->tax_payer_identifier,
            'employee_picture' => $employee->employee_picture,
            'cv' => $employee->cv,
            'join_date' => $employee->join_date,
            'status' => $employee->status,
            'created_date' => $employee->created_date,
            'created_by' => $employee->created_by,
            'modified_date' => $employee->modified_date,
            'modified_by' => $employee->modified_by
        ];

        return response()->default(
            200,
            true, 
            "Success get detail employee", 
            $response
        )->setStatusCode(200);

    }

    public function showCertification($id) {
        $employee = EmployeeCertification::find($id);

        return response()->default(
            200,
            true, 
            "Success get detail employee certification", 
            $employee
        )->setStatusCode(200);

    }

    public function toggle($id) {
        $employee = Employee::find($id);

        $activeDesignation = EmployeeDesignation::where('employee_id', $id)
            ->where('status', 'ACTIVE')
            ->first();

        if ($activeDesignation) {
            return response()->default(
                400,
                false,
                "This employee has an active designation",
                null
            )->setStatusCode(400);
        }

        $employee->status = $employee->status=='ACTIVE' ? 'INACTIVE' : 'ACTIVE';
        $employee->update();

        return response()->default(
            200,
            true,
            $employee->status=='ACTIVE' ? "Employee Activated!" : "Employee Deactivated!",
            $employee
        )->setStatusCode(200);
    }

    public function destroy($id) {
        $activeDesignation = EmployeeDesignation::where('employee_id', $id)
            ->where('status', 'ACTIVE')
            ->first();

        if ($activeDesignation) {
            return response()->default(
                400,
                false,
                "This employee has an active designation",
                null
            )->setStatusCode(400);
        }

        $employee = Employee::find($id);
        if(!$employee) {
            return response()->default(
                404,
                false,
                "Employee not found!",
                null
            )->setStatusCode(404);
        }

        $employee->delete();

        return response()->default(
            200,
            true,
            "Employee has been deleted",
            $employee
        )->setStatusCode(200);
    }

}
