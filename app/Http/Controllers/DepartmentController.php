<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\Exceptions\MissingAbilityException;
use App\Traits\HasQueryService;

//IMPORT EXCEL UTILS
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;

//IMPORT MODEL
use App\Models\Department;
use App\Models\Employee;
use App\Models\EmployeeDesignation;

class DepartmentController extends Controller {
    use HasQueryService;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        try {
            $subQuery = DB::table(Department::getTableName())
        ->leftJoin(Employee::getTableName(), Department::getTableName().'.head', '=', Employee::getTableName().'.id')
        ->select(
            Department::getTableName().'.id',
            Department::getTableName().'.code as department_code',
            Department::getTableName().'.name as department_name',
            Employee::getTableName().".name as department_head", 
            Department::getTableName().'.description',
            Department::getTableName().'.status',
            Department::getTableName().'.created_date', 
            Department::getTableName().'.created_by', 
            Department::getTableName().'.modified_date', 
            Department::getTableName().'.modified_by'
        );


            // Call getPaging, which internally calls applyFiltersAndPagination
            $data = $this->getPaging($subQuery);
            return response()->default(200, true, 'Get Departments Success', $data);
        } catch (\InvalidArgumentException $e) {
            // Catch specific InvalidArgumentException thrown by sortBy macro
            return response()->default(400, false, $e->getMessage(), null);
        } 
        catch (\Exception $e) {
            // Catch general exceptions and return a 500 response
            return response()->default(500, false, 'An unexpected error occurred.', null);
        }
    }

    public function show($id) {
        $data = Department::query()->where(Department::getTableName().'.id', $id)
        ->leftJoin(Employee::getTableName(), Department::getTableName().'.head', '=', Employee::getTableName().'.id')
        ->select(
            Department::getTableName().'.id',
            Department::getTableName().'.code as departmentCode',
            Department::getTableName().'.name as departmentName',
            Employee::getTableName().".id as employee_id", 
            Employee::getTableName().".name as departmentHead", 
            Employee::getTableName().".name as employee_name", 
            Department::getTableName().'.description',
            Department::getTableName().'.status',
            Department::getTableName().'.created_date', 
            Department::getTableName().'.created_by', 
            Department::getTableName().'.modified_date', 
            Department::getTableName().'.modified_by'
        )->first();

        return response()->default(
            200,
            true, 
            "Get Department Detail Success", 
            $data
        )->setStatusCode(200);
    }

    public function store(Request $request) {
        // Validate request data
        $validatedData = $request->validate([
            'departmentCode' => 'required',
            'departmentName' => 'required',
            'departmentHead' => 'nullable|integer',
            'description' => 'nullable'
        ]);
        
        //dd(isset($validatedData['departmentHead']));

        // Check if the employee is already a department head
        $emp = !empty($validatedData['departmentHead']) 
        ? Department::where('head', $validatedData['departmentHead'])->first() 
        : null;
        
        if ($emp) {
            return response()->default(
                409,
                false, 
                "Employee is already a head of another department",
                null
            )->setStatusCode(409);
        }

        // Check if the code & name unique
        $dept = Department::where('code', $validatedData['departmentCode'])
        ->orWhere('name', $validatedData['departmentName'])
        ->first();
        if ($dept) {
            return response()->default(
                400,
                false, 
                "Department Code & Name is exists!",
                null
            )->setStatusCode(400);
        }
    
        // Create the department
        $stored = Department::create([
            'code' => $validatedData['departmentCode'],
            'name' => $validatedData['departmentName'],
            'head' => $validatedData['departmentHead'],
            'description' => $validatedData['description'],
            'status' => 'ACTIVE',
            'created_by' => $request->user()->username
        ]);
    
        // Return success response
        return response()->default(
            200,
            true, 
            "Department Created",
            $stored
        )->setStatusCode(200);
    }
    

    public function update(Request $request, $id) {
        // Find department by ID or return error response
        $data = Department::find($id);
    
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Department Not Found",
                null
            )->setStatusCode(400);
        }
    
        // Check if the employee is already a section head
        $emp = !empty($request['departmentHead']) 
        ? Department::where('head', $request['departmentHead'])->whereNot('id', $id)->first() 
        : null;

        if ($emp) {
            return response()->default(
                409,
                false, 
                "Employee is already a head of another section",
                null
            )->setStatusCode(409);
        }

        // Check if code & name unique
        $dept = Department::where(function ($query) use ($data) {
            $query->where('code', $data['code'])
                  ->orWhere('name', $data['name']);
        })
        ->whereNot('id', $id)
        ->first();
        
        if ($dept) {
            return response()->default(
                400,
                false, 
                "Department Code & Name is exists!",
                null
            )->setStatusCode(400);
        }

        // Define the fields that can be updated
        $fields = [
            'departmentcode' => 'code',
            'departmentName' => 'name',
            'departmentHead' => 'head',
            'description' => 'description'
        ];
    
        // Prepare the update data
        $updateData = [];
    
        foreach ($fields as $requestField => $dbField) {
            if ($request->filled($requestField)) {
                $updateData[$dbField] = $request->input($requestField);
            }
        }
    
        // Always update modified_by field
        //$updateData['modified_by'] = $request->user()->username;
    
        // If there is data to update, perform the update
        if (!empty($updateData)) {
            $data->update($updateData);
        }
    
        // Return success response
        return response()->default(
            200,
            true, 
            "Department Updated",
            $data
        )->setStatusCode(200);
    }
    

    public function toggle(Request $request, $id) {
        // Find department by ID or return error response
        $data = Department::find($id);
    
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Department Not Found",
                null
            )->setStatusCode(400);
        }
    
        // Toggle the status using ternary operator
        $newStatus = $data->status === 'ACTIVE' ? 'INACTIVE' : 'ACTIVE';
    
        // Update department status and modified_by
        $data->update([
            'status' => $newStatus,
            'modified_by' => $request->user()->username
        ]);
    
        // Return success response with appropriate message
        return response()->default(
            200,
            true, 
            $newStatus === 'ACTIVE' ? "Department Activated!" : "Department Deactivated!",
            $data
        )->setStatusCode(200);
    }

    public function destroy($id) {
        $data = Section::find($id);

        // Check if the employee is already a section head
        $dsg = EmployeeDesignation::where('department', $id)->first();
        if ($dsg) {
            return response()->default(
                409,
                false, 
                "Department used in employee designation!",
                null
            )->setStatusCode(409);
        }

        $data->delete();
        
        return response()->default(
            200,
            true, 
            "Department Deleted",
            null
        )->setStatusCode(200);
    }

    public function export()
    {
        $name = 'users_' . date('Y-m-d i:h:s');
        //$data = Excel::download(new UsersExport(), $name . '.xlsx');
    
        return Excel::download(new UsersExport, 'invoices.xlsx');
        //return $data;
    }

    public function getLOV(){
        $items = Department::select(['id','name'])->where('status','ACTIVE')->get()->toArray();
        $data = collect();
        foreach($items as $item){
            $data->push(
                [
                    'key' => $item['id'],
                    'value' => $item['name']
                ]
            );
        }
        return response()->default(
            200,
            true, 
            "Get Department LOV Success", 
            $data
        )->setStatusCode(200);
    }
}