<?php

namespace App\Http\Controllers;

use App\Models\EmployeeDesignation;
use Illuminate\Http\Request;
use App\Models\Employee;
use Illuminate\Support\Facades\DB;

use App\Models\GlobalTypeDetail;
use App\Models\Department;
use App\Models\Section;
use App\Models\Position;
use App\Traits\HasQueryService;


class EmployeeDesignationController extends Controller
{
    use HasQueryService;
    /**
     * Display a listing of the resource.
     */
    public function index() {
        try {
            $subQuery = DB::table(EmployeeDesignation::getTableName())
        ->leftJoin(Employee::getTableName().' as employee_main', EmployeeDesignation::getTableName().'.employee_id', '=', 'employee_main.id')
        ->leftJoin(Department::getTableName().' as department', EmployeeDesignation::getTableName().'.department', '=', 'department.id')
        ->leftJoin(Section::getTableName().' as section', EmployeeDesignation::getTableName().'.section', '=', 'section.id')
        ->leftJoin(Position::getTableName().' as position', EmployeeDesignation::getTableName().'.position', '=', 'position.id')
        ->leftJoin(Employee::getTableName().' as manager1', EmployeeDesignation::getTableName().'.line_manager1', '=', 'manager1.id')
        ->leftJoin(Employee::getTableName().' as manager2', EmployeeDesignation::getTableName().'.line_manager2', '=', 'manager2.id')
        ->select(
            EmployeeDesignation::getTableName().'.id',
            EmployeeDesignation::getTableName().'.employee_id',
            'employee_main.employee_code',
            'employee_main.name',
            EmployeeDesignation::getTableName().'.department',
            'department.code as department_code',
            'department.name as department_name',
            EmployeeDesignation::getTableName().'.section',
            'section.code as section_code',
            'section.name as section_name',
            EmployeeDesignation::getTableName().'.position',
            'position.code as position_code',
            'position.name as position_name',
            EmployeeDesignation::getTableName().'.line_manager1',
            'manager1.employee_code as line_manager1_code',
            'manager1.name as line_manager1_name',
            EmployeeDesignation::getTableName().'.line_manager2',
            'manager2.employee_code as line_manager2_code',
            'manager2.name as line_manager2_name',
            EmployeeDesignation::getTableName().'.start_date',
            EmployeeDesignation::getTableName().'.end_date',
            EmployeeDesignation::getTableName().'.notes',
            EmployeeDesignation::getTableName().'.status',
            EmployeeDesignation::getTableName().'.created_date',
            EmployeeDesignation::getTableName().'.created_by',
            EmployeeDesignation::getTableName().'.modified_date',
            EmployeeDesignation::getTableName().'.modified_by'
        );

            // Call getPaging, which internally calls applyFiltersAndPagination
            $data = $this->getPaging($subQuery);
            return response()->default(200, true, 'Get Employee Designation Success', $data);
        } catch (\InvalidArgumentException $e) {
            // Catch specific InvalidArgumentException thrown by sortBy macro
            return response()->default(400, false, $e->getMessage(), null);
        } 
        catch (\Exception $e) {
            // Catch general exceptions and return a 500 response
            return response()->default(500, false, 'An unexpected error occurred.', null);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $creds = $request->validate([
            'employee_id' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'nullable|date|after:start_date',
        ]);

        $employee = Employee::find($request->employee_id);
        if(!$employee) {
            return response()->default(
                404,
                false,
                "Employee not found!",
                null
            )->setStatusCode(404);
        }

        // Check for overlapping date range
        $overlap = EmployeeDesignation::where('employee_id', $request->employee_id)
        ->where('status', 'ACTIVE')
        ->where(function ($query) use ($request) {
            $query->where(function ($q) use ($request) {
                $q->where('start_date', '<=', $request->start_date)
                    ->where(function ($q) use ($request) {
                        $q->whereNull('end_date')
                            ->orWhere('end_date', '>=', $request->start_date);
                    });
            })->orWhere(function ($q) use ($request) {
                $q->where('start_date', '<=', $request->end_date)
                    ->where(function ($q) use ($request) {
                        $q->whereNull('end_date')
                            ->orWhere('end_date', '>=', $request->end_date);
                    });
            });
        })
        ->exists();

        if ($overlap) {
            return response()->default(
                422,
                false,
                "The given dates overlap with an existing active designation!",
                null
            )->setStatusCode(422);
        }

        $employeeDesignation = EmployeeDesignation::create([
            'employee_id' => $request->employee_id,
            'department' => $request->department,
            'section' => $request->section,
            'position' => $request->position,
            'line_manager1' => $request->line_manager1,
            'line_manager2' => $request->line_manager2,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'notes' => $request->notes,
            'status' => 'ACTIVE',
            'created_by' => $request->user()->username
        ]);

        return response()->default(
            201,
            true,
            "Employee designation has been created",
            $employeeDesignation
        )->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $employeeDesignation = EmployeeDesignation::find($id);

        if($employeeDesignation) {
            $employee = Employee::find($employeeDesignation->employee_id);
            $line_manager1_master = Employee::find($employeeDesignation->line_manager1);
            $line_manager2_master = Employee::find($employeeDesignation->line_manager2);
            $department_master = Department::find($employeeDesignation->department);
            $section_master = Section::find($employeeDesignation->section);
            $position_master = Position::find($employeeDesignation->position);
            $response = [
                'id' => $employeeDesignation->id,
                'employee_id' => $employee->id,
                'employee_code' => $employee->employee_code,
                'employee_name' => $employee->name,
                'department_id' => $department_master->id,
                'department_code' => $department_master->code,
                'department_name' => $department_master->name,
                'section_id' => $section_master->id,
                'section_code' => $section_master->code,
                'section_name' => $section_master->name,
                'position_id' => $position_master->id,
                'position_code' => $position_master->code,
                'position_name' => $position_master->name,
                'line_manager1_id' => $line_manager1_master->id,
                'line_manager1_code' => $line_manager1_master->employee_code,
                'line_manager1_name' => $line_manager1_master->name,
                'line_manager2_id' => $line_manager2_master->id,
                'line_manager2_code' => $line_manager2_master->employee_code,
                'line_manager2_name' => $line_manager2_master->name,
                'start_date' => $employeeDesignation->start_date,
                'end_date' => $employeeDesignation->end_date,
                'notes' => $employeeDesignation->notes,
                'created_date' => $employeeDesignation->created_date,
                'created_by' => $employeeDesignation->created_by,
                'modified_date' => $employeeDesignation->modified_date,
                'modified_by' => $employeeDesignation->modified_by
            ];

            return response()->default(
                200,
                true,
                "Success get detail employee designation",
                $response
            )->setStatusCode(200);
        } else {
            return response()->default(
                404,
                false,
                "Employee designation not found!",
                null
            )->setStatusCode(404);
        }

        
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(EmployeeDesignation $employeeDesignation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $creds = $request->validate([
            'employee_id' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'nullable|date|after:start_date',
        ]);

        $employeeDesignation = EmployeeDesignation::find($id);
        if(!$employeeDesignation) {
            return response()->default(
                404,
                false,
                "Employee designation not found!",
                null
            )->setStatusCode(404);
        }

        $employee = Employee::find($request->employee_id);
        if(!$employee) {
            return response()->default(
                404,
                false,
                "Employee not found!",
                null
            )->setStatusCode(404);
        }

        // Check for overlapping date range
        $overlap = EmployeeDesignation::where('employee_id', $request->employee_id)
        ->where('id', '!=', $id)
        ->where('status', 'ACTIVE')
        ->where(function ($query) use ($request) {
            $query->where(function ($q) use ($request) {
                $q->where('start_date', '<=', $request->start_date)
                    ->where(function ($q) use ($request) {
                        $q->whereNull('end_date')
                            ->orWhere('end_date', '>=', $request->start_date);
                    });
            })->orWhere(function ($q) use ($request) {
                $q->where('start_date', '<=', $request->end_date)
                    ->where(function ($q) use ($request) {
                        $q->whereNull('end_date')
                            ->orWhere('end_date', '>=', $request->end_date);
                    });
            });
        })
        ->exists();

        if ($overlap) {
            return response()->default(
                400,
                false,
                "The given dates overlap with an existing active designation!",
                null
            )->setStatusCode(400);
        }

        $employeeDesignation->employee_id = $request->employee_id;
        $employeeDesignation->department = $request->department;
        $employeeDesignation->section = $request->section;
        $employeeDesignation->position = $request->position;
        $employeeDesignation->line_manager1 = $request->line_manager1;
        $employeeDesignation->line_manager2 = $request->line_manager2;
        $employeeDesignation->start_date = $request->start_date;
        $employeeDesignation->end_date = $request->end_date;
        $employeeDesignation->notes = $request->notes;
        $employeeDesignation->modified_by = $request->user()->username;
        $employeeDesignation->update();

        return response()->default(
            200,
            true,
            "Employee designation has beed updated",
            $employeeDesignation
        )->setStatusCode(200);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $employeeDesignation = EmployeeDesignation::find($id);

        if($employeeDesignation) {
            $employeeDesignation->delete();

            return response()->default(
                200,
                true,
                "Employee designation has beed deleted",
                $employeeDesignation
            )->setStatusCode(200);
        } else {
            return response()->default(
                404,
                false,
                "Employee designation not found!",
                null
            )->setStatusCode(404);
        }
    }

    public function toggle($id) {
        $employeeDesignation = EmployeeDesignation::find($id);

        $employeeDesignation->status = $employeeDesignation->statis=='ACTIVE' ? 'INACTIVE' : 'ACTIVE';
        $employeeDesignation->update();

        return response()->default(
            200,
            true,
            $employeeDesignation->status=='ACTIVE' ? "Employee designation actived!" : "Employee designation deactived!",
            $employeeDesignation
        )->setStatusCode(200);
    }
}
