<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\Exceptions\MissingAbilityException;

//IMPORT EXCEL UTILS
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;

//IMPORT MODEL
use App\Models\Indicator;
use App\Models\Employee;
use App\Models\AppraisalPeriodDetail;
use App\Models\AppraisalPeriod;
use App\Models\AppraisalSubmissionDetail;
use App\Models\AppraisalSubmission;
use App\Traits\HasQueryService;

class IndicatorController extends Controller {
    use HasQueryService;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        try {
            $subQuery = DB::table(Indicator::getTableName())
        ->select(
            Indicator::getTableName().'.id',
            Indicator::getTableName().'.name',
            Indicator::getTableName().'.description',
            Indicator::getTableName().'.status',
            Indicator::getTableName().'.created_date', 
            Indicator::getTableName().'.created_by', 
            Indicator::getTableName().'.modified_date', 
            Indicator::getTableName().'.modified_by'
        );

            // Call getPaging, which internally calls applyFiltersAndPagination
            $data = $this->getPaging($subQuery);
            return response()->default(200, true, 'Get Indicator Success', $data);
        } catch (\InvalidArgumentException $e) {
            // Catch specific InvalidArgumentException thrown by sortBy macro
            return response()->default(400, false, $e->getMessage(), null);
        } 
        catch (\Exception $e) {
            // Catch general exceptions and return a 500 response
            return response()->default(500, false, 'An unexpected error occurred.', null);
        }
    }

    public function show($id) {
        $data = Indicator::query()->where(Indicator::getTableName().'.id', $id)
        ->select(
            Indicator::getTableName().'.id',
            Indicator::getTableName().'.name',
            Indicator::getTableName().'.description',
            Indicator::getTableName().'.status',
            Indicator::getTableName().'.created_date', 
            Indicator::getTableName().'.created_by', 
            Indicator::getTableName().'.modified_date', 
            Indicator::getTableName().'.modified_by'
        )->first();

        if (!$data) {
            return response()->default(
                400,
                false, 
                "Data Not Found",
                null
            )->setStatusCode(400);
        }

        return response()->default(
            200,
            true, 
            "Get Indicator Detail Success", 
            $data
        )->setStatusCode(200);
    }

    public function store(Request $request) {
        // Validate request data
        $validatedData = $request->validate([
            'name' => 'required',
            'description' => 'nullable'
        ]);

        // Check if code & name unique
        $pos = Indicator::where('name', $validatedData['name'])
        ->first();
        if ($pos) {
            return response()->default(
                400,
                false, 
                "Indicator Name is exists!",
                null
            )->setStatusCode(400);
        }
    
        // Create the indicator
        $stored = Indicator::create([
            'name' => $validatedData['name'],
            'description' => $validatedData['description'],
            'status' => 'ACTIVE',
            'created_by' => $request->user()->username
        ]);
    
        // Return success response
        return response()->default(
            200,
            true, 
            "Indicator Created",
            $stored
        )->setStatusCode(200);
    }
    
    public function update(Request $request, $id) {
        // Find indicator by ID or return error response
        $data = Indicator::find($id);
    
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Indicator Not Found",
                null
            )->setStatusCode(400);
        }

        // Check if code & name unique
        $pos = Indicator::where(function ($query) use ($data) {
            $query->where('name', $data['name']);
        })
        ->whereNot('id', $id)
        ->first();
        
        if ($pos) {
            return response()->default(
                400,
                false, 
                "Indicator Name is exists!",
                null
            )->setStatusCode(400);
        }
    
        // Define the fields that can be updated
        $fields = [
            'name' => 'name',
            'description' => 'description'
        ];
    
        // Prepare the update data
        $updateData = [];
    
        foreach ($fields as $requestField => $dbField) {
            if ($request->filled($requestField)) {
                $updateData[$dbField] = $request->input($requestField);
            }
        }
    
        // Always update modified_by field
        $updateData['modified_by'] = $request->user()->username;
    
        // If there is data to update, perform the update
        if (!empty($updateData)) {
            $data->update($updateData);
        }
    
        // Return success response
        return response()->default(
            200,
            true, 
            "Indicator Updated",
            $data
        )->setStatusCode(200);
    }
    
    public function toggle(Request $request, $id) {
        // Find indicator by ID or return error response
        $data = Indicator::find($id);
    
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Indicator Not Found",
                null
            )->setStatusCode(400);
        }
    
        // Toggle the status using ternary operator
        $newStatus = $data->status === 'ACTIVE' ? 'INACTIVE' : 'ACTIVE';
    
        // Update indicator status and modified_by
        $data->update([
            'status' => $newStatus,
            'modified_by' => $request->user()->username
        ]);
    
        // Return success response with appropriate message
        return response()->default(
            200,
            true, 
            $newStatus === 'ACTIVE' ? "Indicator Activated!" : "Indicator Deactivated!",
            $data
        )->setStatusCode(200);
    }

    public function destroy($id) {
        $data =  Indicator::find($id);
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Data Not Found",
                null
            )->setStatusCode(400);
        }

        if ($data['status'] == 'ACTIVE') {
            return response()->default(
                400,
                false, 
                "Can't delete data with status Active!",
                null
            )->setStatusCode(400);
        }

        $aprp = AppraisalPeriodDetail::join(AppraisalPeriod::getTableName(), AppraisalPeriodDetail::getTableName().'.appraisal_period_id', '=', AppraisalPeriod::getTableName().'.id')
        ->where('indicator_id', $id)
        ->where(AppraisalPeriod::getTableName().'.is_deleted', "N")
        ->first();

        $aprs = AppraisalSubmissionDetail::join(AppraisalSubmission::getTableName(), AppraisalSubmissionDetail::getTableName().'.appraisal_submission_id', '=', AppraisalSubmission::getTableName().'.id')
        ->where('indicator_id', $id)
        ->where(AppraisalSubmission::getTableName().'.is_deleted', "N")
        ->first();

        if ($aprp || $aprs) {
            return response()->default(
                409,
                false, 
                "Indicator used in appraisal period/submission!",
                null
            )->setStatusCode(409);
        }

        $data->delete();
        
        return response()->default(
            200,
            true, 
            "Indicator Deleted",
            null
        )->setStatusCode(200);
    }

    public function export()
    {
        $name = 'users_' . date('Y-m-d i:h:s');
        //$data = Excel::download(new UsersExport(), $name . '.xlsx');
    
        return Excel::download(new UsersExport, 'invoices.xlsx');
        //return $data;
    }
}