<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\Exceptions\MissingAbilityException;
use App\Traits\HasQueryService;

//IMPORT EXCEL UTILS
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;

//IMPORT MODEL
use App\Models\AppraisalPeriod;
use App\Models\AppraisalPeriodDetail;
use App\Models\AppraisalSubmission;
use App\Models\AppraisalSubmissionDetail;
use App\Models\Department;

class AppraisalPeriodController extends Controller {
    use HasQueryService;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        try {
            $subQuery = DB::table(AppraisalPeriod::getTableName())
        ->leftJoin(Department::getTableName(), AppraisalPeriod::getTableName().'.department_id', '=', Department::getTableName().'.id')
        ->select(
            AppraisalPeriod::getTableName().'.id',
            Department::getTableName().'.name as departmentName',
            AppraisalPeriod::getTableName().'.year',
            AppraisalPeriod::getTableName().'.description',
            AppraisalPeriod::getTableName().'.created_date', 
            AppraisalPeriod::getTableName().'.created_by', 
            AppraisalPeriod::getTableName().'.modified_date', 
            AppraisalPeriod::getTableName().'.modified_by'
        );

            // Call getPaging, which internally calls applyFiltersAndPagination
            $data = $this->getPaging($subQuery);
            return response()->default(200, true, 'Get Appraisal Period Success', $data);
        } catch (\InvalidArgumentException $e) {
            // Catch specific InvalidArgumentException thrown by sortBy macro
            return response()->default(400, false, $e->getMessage(), null);
        } 
        catch (\Exception $e) {
            // Catch general exceptions and return a 500 response
            return response()->default(500, false, 'An unexpected error occurred.', null);
        }
    }

    public function show($id) {
        $data = AppraisalPeriod::query()->where(AppraisalPeriod::getTableName().'.id', $id)
        ->leftJoin(Department::getTableName(), AppraisalPeriod::getTableName().'.department_id', '=', Department::getTableName().'.id')
        ->select(
            AppraisalPeriod::getTableName().'.id',
            Department::getTableName().'.id as departmentId',
            Department::getTableName().'.name as departmentName',
            AppraisalPeriod::getTableName().'.year',
            AppraisalPeriod::getTableName().'.description',
            AppraisalPeriod::getTableName().'.created_date', 
            AppraisalPeriod::getTableName().'.created_by', 
            AppraisalPeriod::getTableName().'.modified_date', 
            AppraisalPeriod::getTableName().'.modified_by'
        )->first();

        return response()->default(
            200,
            true, 
            "Get Appraisal Period Detail Success", 
            $data
        )->setStatusCode(200);
    }

    public function store(Request $request) {
        // Validate request data
        $validatedData = $request->validate([
            'departmentId' => 'required',
            'year' => 'required',
            'description' => 'nullable'
        ]);

        // Check if code & name unique
        $pos = AppraisalPeriod::where('department_id', $validatedData['departmentId'])
        ->where('year', $validatedData['year'])
        ->first();
        if ($pos) {
            return response()->default(
                400,
                false, 
                "Appraisal Period is exists!",
                null
            )->setStatusCode(400);
        }
    
        // Create the appraisal period
        $stored = AppraisalPeriod::create([
            'department_id' => $validatedData['departmentId'],
            'year' => $validatedData['year'],
            'created_by' => $request->user()->username
        ]);
    
        // Return success response
        return response()->default(
            200,
            true, 
            "Appraisal Period Created",
            $stored
        )->setStatusCode(200);
    }
    
    public function update(Request $request, $id) {
        // Find appraisal period by ID or return error response
        $data = AppraisalPeriod::find($id);
    
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Appraisal Period Not Found",
                null
            )->setStatusCode(400);
        }
    
        // Define the fields that can be updated
        $fields = [
            'description' => 'description'
        ];
    
        // Prepare the update data
        $updateData = [];
    
        foreach ($fields as $requestField => $dbField) {
            if ($request->filled($requestField)) {
                $updateData[$dbField] = $request->input($requestField);
            }
        }
    
        // Always update modified_by field
        $updateData['modified_by'] = $request->user()->username;
    
        // If there is data to update, perform the update
        if (!empty($updateData)) {
            $data->update($updateData);
        }
    
        // Return success response
        return response()->default(
            200,
            true, 
            "Appraisal Period Updated",
            $data
        )->setStatusCode(200);
    }
    
    public function destroy($id) {
        $data = AppraisalPeriod::find($id);

        $app = AppraisalSubmission::where('appraisal_period_id', $id)->first();
        if ($app) {
            return response()->default(
                409,
                false, 
                "Appraisal Period used in appraisal submission!",
                null
            )->setStatusCode(409);
        }

        $data->delete();
        
        return response()->default(
            200,
            true, 
            "Appraisal Period Deleted",
            null
        )->setStatusCode(200);
    }

    public function showDetail($id) {
        $data = AppraisalPeriodDetail::query()->where(AppraisalPeriod::getTableName().'.id', $id)
        ->join(Department::getTableName(), AppraisalPeriod::getTableName().'.department_id', '=', Department::getTableName().'.id')
        ->select(
            AppraisalPeriod::getTableName().'.id',
            Department::getTableName().'.id as departmentId',
            Department::getTableName().'.name as departmentName',
            AppraisalPeriod::getTableName().'.year',
            AppraisalPeriod::getTableName().'.description',
            AppraisalPeriod::getTableName().'.created_date', 
            AppraisalPeriod::getTableName().'.created_by', 
            AppraisalPeriod::getTableName().'.modified_date', 
            AppraisalPeriod::getTableName().'.modified_by'
        )->first();

        return response()->default(
            200,
            true, 
            "Get AppraisalPeriod Detail Success", 
            $data
        )->setStatusCode(200);
    }

    public function storeDetail(Request $request, $appraisalPeriodId) {
        // Validate request data
        $validatedData = $request->validate([
            'indicatorId' => 'required|numeric',
            'weight' => 'required|decimal:0',
            'target' => 'required|decimal:0|lte:weight',
            'description' => 'nullable'
        ]);

        // Check if indicator exists
        $dtl = AppraisalPeriodDetail::where('indicator_id', $validatedData['indicatorId'])
        ->where('appraisal_period_id', $appraisalPeriodId)
        ->first();
        if ($dtl) {
            return response()->default(
                400,
                false, 
                "Indicator is already exists!",
                null
            )->setStatusCode(400);
        }

        $summedData = DB::table(AppraisalPeriodDetail::getTableName())
        ->select(
            DB::raw('SUM(weight) as weight')
        )
        ->groupBy('appraisal_period_id')
        ->where('appraisal_period_id', $appraisalPeriodId)
        ->first();
        
        $weight = isset($summedData->weight)?$summedData->weight:0;
        
        $finalWeight=$weight+$validatedData['weight'];
        
        if($finalWeight>=100){
            return response()->default(
                400,
                false, 
                "Total weight cannot be greater than 100!",
                null
            )->setStatusCode(400);
        }
    
        // Create the appraisal period detail
        $stored = AppraisalPeriodDetail::create([
            'appraisal_period_id' => $appraisalPeriodId,
            'indicator_id' => $validatedData['indicatorId'],
            'weight' => $validatedData['weight'],
            'target' => $validatedData['target'],
            'description' => $validatedData['description'],
            'created_by' => $request->user()->username
        ]);
    
        return response()->default(
            200,
            true, 
            "Appraisal Period Detail Created",
            $stored
        )->setStatusCode(200);
    }
    
    public function updateDetail(Request $request, $id) {
        // Find appraisal period by ID or return error response
        $data = AppraisalPeriodDetail::find($id);
    
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Appraisal Period Detail Not Found",
                null
            )->setStatusCode(400);
        }
    
        $validatedData = $request->validate([
            'indicatorId' => 'required|numeric',
            'weight' => 'required|decimal:0',
            'target' => 'required|decimal:0|lte:weight',
            'description' => 'nullable'
        ]);

        $appraisalPeriodId = $data['appraisal_period_id'];

        // Check if indicator exists
        $dtl = AppraisalPeriodDetail::where('indicator_id', $validatedData['indicatorId'])
        ->where('appraisal_period_id', $appraisalPeriodId)
        ->whereNot('id', $id)
        ->first();
        if ($dtl) {
            return response()->default(
                400,
                false, 
                "Indicator is already exists!",
                null
            )->setStatusCode(400);
        }

        $summedData = DB::table(AppraisalPeriodDetail::getTableName())
        ->select(
            DB::raw('SUM(weight) as weight')
        )
        ->groupBy('appraisal_period_id')
        ->where('appraisal_period_id', $appraisalPeriodId)
        ->whereNot('id', $id)
        ->first();
        
        $weight = isset($summedData->weight)?$summedData->weight:0;
        
        $finalWeight=$weight+$validatedData['weight'];
        
        if($finalWeight>=100){
            return response()->default(
                400,
                false, 
                "Total weight cannot be greater than 100!",
                null
            )->setStatusCode(400);
        }

        // Define the fields that can be updated
        $fields = [
            'indicatorId' => 'indicator_id',
            'weight' => 'weight',
            'target' => 'target',
            'description' => 'description'
        ];
    
        // Prepare the update data
        $updateData = [];
    
        foreach ($fields as $requestField => $dbField) {
            if ($request->filled($requestField)) {
                $updateData[$dbField] = $request->input($requestField);
            }
        }
    
        // Always update modified_by field
        $updateData['modified_by'] = $request->user()->username;
    
        // If there is data to update, perform the update
        if (!empty($updateData)) {
            $data->update($updateData);
        }
    
        // Return success response
        return response()->default(
            200,
            true, 
            "Appraisal Period Detail Updated",
            $data
        )->setStatusCode(200);
    }
    
    public function destroyDetail($id) {
        $data = AppraisalPeriodDetail::find($id);

        $aprs = AppraisalSubmissionDetail::join(AppraisalSubmission::getTableName(), AppraisalSubmissionDetail::getTableName().'.appraisal_submission_id', '=', AppraisalSubmission::getTableName().'.id')
        ->where('appraisal_period_detail_id', $id)
        ->where(AppraisalSubmission::getTableName().'.is_deleted', "N")
        ->first();

        if ($aprs) {
            return response()->default(
                409,
                false, 
                "Appraisal Period Detail used in appraisal submission!",
                null
            )->setStatusCode(409);
        }

        $data->delete();
        
        return response()->default(
            200,
            true, 
            "Appraisal Period Detail Deleted",
            null
        )->setStatusCode(200);
    }

    public function export()
    {
        $name = 'users_' . date('Y-m-d i:h:s');
        //$data = Excel::download(new UsersExport(), $name . '.xlsx');
    
        return Excel::download(new UsersExport, 'invoices.xlsx');
        //return $data;
    }
}