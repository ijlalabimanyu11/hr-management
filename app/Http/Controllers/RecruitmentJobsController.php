<?php

namespace App\Http\Controllers;

use App\Models\RecruitmentJobs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Position;
use App\Models\JobApplications;
use App\Models\GlobalTypeDetail;

class RecruitmentJobsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // Build the query but don't execute it yet
        $subQuery = DB::table(RecruitmentJobs::getTableName())
            ->leftJoin(Position::getTableName(), RecruitmentJobs::getTableName().'.position', '=', Position::getTableName().'.id')
            ->select(
                RecruitmentJobs::getTableName().'.id',
                RecruitmentJobs::getTableName().'.job_title',
                RecruitmentJobs::getTableName().'.number_of_position',
                RecruitmentJobs::getTableName().'.skill_box',
                RecruitmentJobs::getTableName().'.location',
                RecruitmentJobs::getTableName().'.work_type',
                RecruitmentJobs::getTableName().'.position',
                Position::getTableName().'.code as position_code',
                Position::getTableName().'.name as position_name',
                RecruitmentJobs::getTableName().'.job_type',
                RecruitmentJobs::getTableName().'.salary_type',
                RecruitmentJobs::getTableName().'.job_description',
                RecruitmentJobs::getTableName().'.job_requirement',
                RecruitmentJobs::getTableName().'.publish_start_date',
                RecruitmentJobs::getTableName().'.publish_end_date',
                RecruitmentJobs::getTableName().'.created_date',
                RecruitmentJobs::getTableName().'.created_by',
                RecruitmentJobs::getTableName().'.modified_date',
                RecruitmentJobs::getTableName().'.modified_by',
                DB::raw('(SELECT JSON_ARRAYAGG(JSON_OBJECT(
                                "id", '.JobApplications::getTableName().'.id,
                                "job_id", '.RecruitmentJobs::getTableName().'.id,
                                "full_name", '.JobApplications::getTableName().'.full_name,
                                "date_of_birth", '.JobApplications::getTableName().'.date_of_birth,
                                "email", '.JobApplications::getTableName().'.email,
                                "phone", '.JobApplications::getTableName().'.phone,
                                "gender", '.JobApplications::getTableName().'.gender,
                                "cv", '.JobApplications::getTableName().'.cv,
                                "linkedin_url", '.JobApplications::getTableName().'.linkedin_url,
                                "reason_to_apply", '.JobApplications::getTableName().'.reason_to_apply,
                                "rating", '.JobApplications::getTableName().'.rating,
                                "status", '.JobApplications::getTableName().'.status,
                                "created_date", '.JobApplications::getTableName().'.created_date,
                                "created_by", '.JobApplications::getTableName().'.created_by,
                                "modified_date", '.JobApplications::getTableName().'.modified_date,
                                "modified_by", '.JobApplications::getTableName().'.modified_by
                            )) FROM '.JobApplications::getTableName().'
                            WHERE '.JobApplications::getTableName().'.job_id = '.RecruitmentJobs::getTableName().'.id) as applications')
            );

        // Get the raw SQL query and bindings
        $subQuerySql = $subQuery->toSql();
        $subQueryBindings = $subQuery->getBindings();

        // Build the final query with pagination, sorting, and searching
        $data = RecruitmentJobs::query()
            ->from(DB::raw("({$subQuerySql}) as sub"))
            ->mergeBindings($subQuery)
            ->when(request()->search, fn($query, $search) => $query->whereLike($search))
            ->when(request()->sort, fn($query, $sort) => $query->sortBy($sort));

        // Set the page size
        $size = request()->size ?? 10;

        // Paginate the data
        $data = $data->paginate($size)->toArray();

        // Decode applications from JSON to array
        foreach ($data['data'] as &$job) {
            $job['applications'] = json_decode($job['applications'], true) ?: [];
        }

        // Return the response
        return response()->default(
            200,
            true, 
            "Success get data header recruitment jobs", 
            [
                "currentPage" => $data['current_page'],
                "lastPage" => $data['last_page'],
                "pageSize" => $data['to'],
                "totalData" => $data['total'],
                "data" => $data['data'],
            ]
        )->setStatusCode(200);
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $creds = $request->validate([
            'job_title' => 'required',
            'number_of_position' => 'required',
            'skill_box' => 'required',
            'location' => 'required',
            'work_type' => 'required',
            'position' => 'required',
            'job_type' => 'required',
            'salary_range' => 'required',
            'salary_type' => 'required',
            'job_description' => 'required',
            'job_requirement' => 'required',
            'publish_on_career' => ['required', function ($attribute, $value, $fail) {
                if ($value==true && 'publish_start_date'==null) {
                    $fail('Publish start date is required, because value of publish on career is active.');
                }
            }],
            'publish_start_date' => 'date',
            'publish_end_date' => 'date|after:publish_start_date',
        ]);

        $position_master = Position::find($request->position);
        if(!$position_master) {
            return response()->default(
                404,
                false,
                "Position data not found!",
                null
            )->setStatusCode(404);
        }

        $jobs = RecruitmentJobs::create([
            'job_title' => $request->job_title,
            'number_of_position' => $request->number_of_position,
            'skill_box' => $request->skill_box,
            'location' => $request->location,
            'work_type' => $request->work_type,
            'position' => $request->position,
            'job_type' => $request->job_type,
            'salary_range' => $request->salary_range,
            'salary_type' => $request->salary_type,
            'job_description' => $request->job_description,
            'job_requirement' => $request->job_requirement,
            'publish_on_career' => $request->publish_on_career,
            'publish_start_date' => $request->publish_start_date,
            'publish_end_date' => $request->publish_end_date,
            'created_by' => $request->user()->username
        ]);

        return response()->default(
            201,
            true,
            "Recruitment jobs correction has been created",
            $jobs
        )->setStatusCode(201);

    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $recruitmentJobs = RecruitmentJobs::find($id);
        if(!$recruitmentJobs) {
            return response()->default(
                404,
                false,
                "Recruitment jobs not found!",
                null
            )->setStatusCode(404);
        }

        $position = Position::find($recruitmentJobs->position);
        $job_title_lov = GlobalTypeDetail::where('value', $recruitmentJobs->job_title)->first();
        $work_type_lov = GlobalTypeDetail::where('value', $recruitmentJobs->work_type)->first();
        $job_type_lov = GlobalTypeDetail::where('value', $recruitmentJobs->job_type)->first();
        $salary_type_lov = GlobalTypeDetail::where('value', $recruitmentJobs->salary_type)->first();
        $jobApplications = JobApplications::where('job_id', $recruitmentJobs->id)->get();

        $response = [
            'id' => $recruitmentJobs->id,
            'job_title_id' => $recruitmentJobs->job_title,
            'job_title_name' => $job_title_lov!=null? $job_title_lov->name: null,
            'number_of_position' => $recruitmentJobs->number_of_position,
            'skill_box' => $recruitmentJobs->skill_box,
            'location' => $recruitmentJobs->location,
            'work_type_id' => $recruitmentJobs->work_type,
            'work_type_name' => $work_type_lov!=null? $work_type_lov->name: null,
            'position_id' => $recruitmentJobs->position,
            'position_code' => $position->code,
            'position_name' => $position->name,
            'job_type_id' => $recruitmentJobs->job_type,
            'job_type_name' => $job_type_lov!=null? $job_type_lov->name: null,
            'salary_range' => $recruitmentJobs->salary_range,
            'salary_type_id' => $recruitmentJobs->salary_type,
            'salary_type_name' => $salary_type_lov!=null? $salary_type_lov->name: null,
            'job_description' => $recruitmentJobs->job_description,
            'job_requirement' => $recruitmentJobs->job_requirement,
            'publish_start_date' => $recruitmentJobs->publish_start_date,
            'pubish_end_date' => $recruitmentJobs->publish_end_date,
            'created_date' => $recruitmentJobs->created_date,
            'created_by' => $recruitmentJobs->created_by,
            'modified_date' => $recruitmentJobs->modified_date,
            'modified_by' => $recruitmentJobs->modified_by,
            'applications' => $jobApplications
        ];

        return response()->default(
            200,
            true,
            "Success get detail recruitment jobs",
            $response
        )->setStatusCode(200);

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(RecruitmentJobs $recruitmentJobs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $creds = $request->validate([
            'job_title' => 'required',
            'number_of_position' => 'required',
            'skill_box' => 'required',
            'location' => 'required',
            'work_type' => 'required',
            'position' => 'required',
            'job_type' => 'required',
            'salary_range' => 'required',
            'salary_type' => 'required',
            'job_description' => 'required',
            'job_requirement' => 'required',
            'publish_on_career' => ['required', function ($attribute, $value, $fail) {
                if ($value==true && 'publish_start_date'==null) {
                    $fail('Publish start date is required, because value of publish on career is active.');
                }
            }],
            'publish_start_date' => 'date',
            'publish_end_date' => 'date|after:publish_start_date',
        ]);

        $recruitment_job = RecruitmentJobs::find($id);
        if(!$recruitment_job) {
            return response()->default(
                404,
                false,
                "Recruitment job not found!",
                null
            )->setStatusCode(404);
        }

        $position_master = Position::find($request->position);
        if(!$position_master) {
            return response()->default(
                404,
                false,
                "Position data not found!",
                null
            )->setStatusCode(404);
        }

        $recruitment_job->job_title = $request->job_title;
        $recruitment_job->number_of_position = $request->number_of_position;
        $recruitment_job->skill_box = $request->skill_box;
        $recruitment_job->location = $request->location;
        $recruitment_job->work_type = $request->work_type;
        $recruitment_job->position = $request->position;
        $recruitment_job->job_type = $request->job_type;
        $recruitment_job->salary_range = $request->salary_range;
        $recruitment_job->salary_type = $request->salary_type;
        $recruitment_job->job_description = $request->job_description;
        $recruitment_job->job_requirement = $request->job_requirement;
        $recruitment_job->publish_on_career = $request->publish_on_career;
        $recruitment_job->publish_start_date = $request->publish_start_date;
        $recruitment_job->publish_end_date = $request->publish_end_date;
        $recruitment_job->modified_by = $request->user()->username;
        $recruitment_job->update();

        return response()->default(
            200,
            true,
            "Recruitment job has been updated",
            $recruitment_job
        )->setStatusCode(200);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $recruitment_job = RecruitmentJobs::find($id);
        if(!$recruitment_job) {
            return response()->default(
                404,
                false,
                "Recruitment job not found!",
                null
            )->setStatusCode(404);
        }

        $dataApplication = JobApplications::where('job_id', $id)->first();

        if($dataApplication) {
            return response()->default(
                400,
                false,
                "This job has an application data",
                null
            )->setStatusCode(400);
        } 

        $recruitment_job->delete();

        return response()->default(
            200,
            true,
            "Recruitment job has been deleted",
            null
        )->setStatusCode(200);
        
    }
}
