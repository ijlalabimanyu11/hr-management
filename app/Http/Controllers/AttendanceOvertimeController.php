<?php

namespace App\Http\Controllers;

use App\Models\AttendanceOvertime;
use Illuminate\Http\Request;
use App\Models\Employee;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\GlobalTypeDetail;
use App\Traits\HasQueryService;

class AttendanceOvertimeController extends Controller
{
    use HasQueryService;
    /**
     * Display a listing of the resource.
     */
    public function index() {
        try {
            $subQuery = DB::table(AttendanceOvertime::getTableName())
        ->leftJoin(Employee::getTableName(), AttendanceOvertime::getTableName().'.employee_id', '=', Employee::getTableName().'.id')
        ->select(
            AttendanceOvertime::getTableName().'.id',
            Employee::getTableName().'.employee_code',
            Employee::getTableName().'.name',
            AttendanceOvertime::getTableName().'.overtime_type',
            AttendanceOvertime::getTableName().'.cause',
            AttendanceOvertime::getTableName().'.start_date',
            AttendanceOvertime::getTableName().'.end_date',
            AttendanceOvertime::getTableName().'.notes',
            AttendanceOvertime::getTableName().'.status',
            AttendanceOvertime::getTableName().'.created_date',
            AttendanceOvertime::getTableName().'.created_by',
            AttendanceOvertime::getTableName().'.modified_date',
            AttendanceOvertime::getTableName().'.modified_by'
        );

            // Call getPaging, which internally calls applyFiltersAndPagination
            $data = $this->getPaging($subQuery);
            return response()->default(200, true, 'Success get data Attendance Overtime', $data);
        } catch (\InvalidArgumentException $e) {
            // Catch specific InvalidArgumentException thrown by sortBy macro
            return response()->default(400, false, $e->getMessage(), null);
        } 
        catch (\Exception $e) {
            // Catch general exceptions and return a 500 response
            return response()->default(500, false, 'An unexpected error occurred.', null);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $creds = $request->validate([
            'employee_id' => 'required',
            'overtime_type' => 'required',
            'cause' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
            'notes' => 'required'
        ]);

        $employee = Employee::find($request->employee_id);
        if(!$employee) {
            return response()->default(
                404,
                false,
                "Employee not found!",
                null
            )->setStatusCode(404);
        }

        $attendanceOvertime = AttendanceOvertime::create([
            'employee_id' => $request->employee_id,
            'overtime_type' => $request->overtime_type,
            'cause' => $request->cause,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'notes' => $request->notes,
            'status' => 'Waiting Approval',
            'created_by' => $request->user()->username
        ]);

        return response()->default(
            201,
            true,
            "Attendance overtime has been created",
            $attendanceOvertime
        )->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $attendanceOvertime = AttendanceOvertime::find($id);
        if(!$attendanceOvertime) {
            return response()->default(
                404,
                false,
                "Attendance overtime not found!",
                null
            )->setStatusCode(404);
        }

        $employee = Employee::find($attendanceOvertime->employee_id);
        $overtime_type_id = GlobalTypeDetail::where('value', $attendanceOvertime->overtime_type)->first();
        $cause_id = GlobalTypeDetail::where('value', $attendanceOvertime->cause)->first();

        $response = [
            'id' => $attendanceOvertime->id,
            'employee_id' => $employee->id,
            'employee_code' => $employee->employee_code,
            'employee_name' => $employee->name,
            'overtime_type_id' => $overtime_type_id->id,
            'overtime_type' => $attendanceOvertime->overtime_type,
            'cause_id' => $cause_id->id,
            'cause' => $attendanceOvertime->cause,
            'start_date' => $attendanceOvertime->start_date,
            'end_date' => $attendanceOvertime->end_date,
            'notes' => $attendanceOvertime->notes,
            'status' => $attendanceOvertime->status,
            'created_date' => $attendanceOvertime->created_date,
            'created_by' => $attendanceOvertime->created_by,
            'modified_date' => $attendanceOvertime->modified_date,
            'modified_by' => $attendanceOvertime->modified_by
        ];

        return response()->default(
            200,
            true,
            "Success get detail attendance overtime",
            $response
        )->setStatusCode(200);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(AttendanceOvertime $attendanceOvertime)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $creds = $request->validate([
            'employee_id' => 'required',
            'overtime_type' => 'required',
            'cause' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
            'notes' => 'required'
        ]);

        $attendanceOvertime = AttendanceOvertime::find($id);
        if(!$attendanceOvertime) {
            return response()->default(
                404,
                false,
                "Attendance overtime not found!",
                null
            )->setStatusCode(404);
        }

        $employee = Employee::find($request->employee_id);
        if(!$employee) {
            return response()->default(
                404,
                false,
                "Employee not found!",
                null
            )->setStatusCode(404);
        }

        if($attendanceOvertime->status == 'APPROVED' || $attendanceOvertime->status == 'REJECTED') {
            return response()->default(
                400,
                false,
                "You cannot update data, because this attendance overtime has been ".$attendanceOvertime->status."!",
                null
            )->setStatusCode(400);
        }

        $attendanceOvertime->employee_id = $request->employee_id;
        $attendanceOvertime->overtime_type = $request->overtime_type;
        $attendanceOvertime->cause = $request->cause;
        $attendanceOvertime->start_date = $request->start_date;
        $attendanceOvertime->end_date = $request->end_date;
        $attendanceOvertime->notes = $request->notes;
        $attendanceOvertime->modified_by = $request->user()->username;
        $attendanceOvertime->update();

        return response()->default(
            200,
            true,
            "Attendance overtime has beed updated",
            $attendanceOvertime
        )->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(AttendanceOvertime $attendanceOvertime)
    {
        //
    }

    public function toggle($id) {
        $attendanceOvertime = AttendanceOvertime::find($id);

        if($attendanceOvertime->status == 'APPROVED' || $attendanceOvertime->status == 'REJECTED' || $attendanceOvertime->status == 'INACTIVE') {
            return response()->default(
                400,
                false,
                "You cannot update data, because this attendance overtime has been ".$attendanceOvertime->status."!",
                null
            )->setStatusCode(400);
        }

        $attendanceOvertime->status = 'INACTIVE';
        $attendanceOvertime->update();

        return response()->default(
            200,
            true,
            "Attendance overtime has been canceled!",
            $attendanceOvertime
        )->setStatusCode(200);
    }
}
