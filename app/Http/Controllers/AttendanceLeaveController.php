<?php

namespace App\Http\Controllers;

use App\Models\AttendanceLeave;
use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\EmployeeLeave;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\GlobalTypeDetail;
use DateTime;
use App\Traits\HasQueryService;

class AttendanceLeaveController extends Controller
{
    use HasQueryService;
    /**
     * Display a listing of the resource.
     */
    public function index() {
        try {
            $subQuery = DB::table(AttendanceLeave::getTableName())
        ->leftJoin(Employee::getTableName(), AttendanceLeave::getTableName().'.employee_id', '=', Employee::getTableName().'.id')
        ->select(
            AttendanceLeave::getTableName().'.id',
            Employee::getTableName().'.employee_code',
            Employee::getTableName().'.name',
            AttendanceLeave::getTableName().'.leave_type',
            AttendanceLeave::getTableName().'.start_date',
            AttendanceLeave::getTableName().'.end_date',
            AttendanceLeave::getTableName().'.is_debt',
            AttendanceLeave::getTableName().'.reason',
            AttendanceLeave::getTableName().'.status',
            AttendanceLeave::getTableName().'.created_date',
            AttendanceLeave::getTableName().'.created_by',
            AttendanceLeave::getTableName().'.modified_date',
            AttendanceLeave::getTableName().'.modified_by'
        );

            // Call getPaging, which internally calls applyFiltersAndPagination
            $data = $this->getPaging($subQuery);
            return response()->default(200, true, 'Success get data Attendance Leave', $data);
        } catch (\InvalidArgumentException $e) {
            // Catch specific InvalidArgumentException thrown by sortBy macro
            return response()->default(400, false, $e->getMessage(), null);
        } 
        catch (\Exception $e) {
            // Catch general exceptions and return a 500 response
            return response()->default(500, false, 'An unexpected error occurred.', null);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $creds = $request->validate([
            'employee_id' => 'required',
            'leave_type' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
            'reason' => 'required'
        ]);

        $employee = Employee::find($request->employee_id);
        if(!$employee) {
            return response()->default(
                404,
                false,
                "Employee not found!",
                null
            )->setStatusCode(404);
        }

        // check any amount leave for selected employee
        $leaveData = EmployeeLeave::where('employee_id', $request->employee_id)
            ->where('leave_type', $request->leave_type)
            ->where(function ($query) use ($request) {
                $query->where(function ($q) use ($request) {
                    $q->where('start_date', '<=', $request->start_date)
                        ->where(function ($q) use ($request) {
                            $q->whereNull('end_date')
                                ->orWhere('end_date', '>=', $request->start_date);
                        });
                })->orWhere(function ($q) use ($request) {
                    $q->where('start_date', '<=', $request->end_date)
                        ->where(function ($q) use ($request) {
                            $q->whereNull('end_date')
                                ->orWhere('end_date', '>=', $request->end_date);
                        });
                });
            })
            ->first();

        $attendanceLeave = AttendanceLeave::create([
            'employee_id' => $request->employee_id,
            'leave_type' => $request->leave_type,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'reason' => $request->reason,
            'is_debt' => $leaveData ? 0 : 1,
            'status' => "WAITING APPROVAL",
            'created_by' => $request->user()->username
        ]);

        // update remaining
        if($leaveData) {
            $totalOverlapDays = 0;
            $overlapStart = new DateTime(max($leaveData->start_date, $request->start_date));
            $overlapEnd = new DateTime(min($leaveData->end_date ?? $request->end_date, $request->end_date));
            $interval = $overlapStart->diff($overlapEnd);
            $totalOverlapDays += $interval->days + 1; 
            $remaining = $leaveData->amount - $totalOverlapDays;

            $leaveData->remaining = $remaining;
            $leaveData->update();
        }

        return response()->default(
            201,
            true,
            "Attendance leave has been created",
            $attendanceLeave
        )->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $attendanceLeave = AttendanceLeave::find($id);
        if(!$attendanceLeave) {
            return response()->default(
                404,
                false,
                "Attendance leave not found!",
                null
            )->setStatusCode(404);
        }

        $employee = Employee::find($attendanceLeave->employee_id);
        $leave_type_id = GlobalTypeDetail::where('value', $attendanceLeave->leave_type)->first();

        $response = [
            'id' => $attendanceLeave->id,
            'employee_id' => $employee->id,
            'employee_code' => $employee->employee_code,
            'employee_name' => $employee->name,
            'leave_type_id' => $leave_type_id->id,
            'leave_type' => $attendanceLeave->leave_type,
            'start_date' => $attendanceLeave->start_date,
            'end_date' => $attendanceLeave->end_date,
            'is_debt' => $attendanceLeave->is_debt,
            'reason' => $attendanceLeave->reason,
            'status' => $attendanceLeave->status,
            'created_date' => $attendanceLeave->created_date,
            'created_by' => $attendanceLeave->created_by,
            'modified_date' => $attendanceLeave->modified_date,
            'modified_by' => $attendanceLeave->modified_by
        ];

        return response()->default(
            200,
            true,
            "Success get detail attendance leave",
            $response
        )->setStatusCode(200);

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(AttendanceLeave $attendanceLeave)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $creds = $request->validate([
            'employee_id' => 'required',
            'leave_type' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
            'reason' => 'required'
        ]);

        $attendanceLeave = AttendanceLeave::find($id);
        if(!$attendanceLeave) {
            return response()->default(
                404,
                false,
                "Attendance Leave not found!",
                null
            )->setStatusCode(404);
        }

        $employee = Employee::find($request->employee_id);
        if(!$employee) {
            return response()->default(
                404,
                false,
                "Employee not found!",
                null
            )->setStatusCode(404);
        }

        if($attendanceLeave->status == 'APPROVED' || $attendanceLeave->status == 'REJECTED') {
            return response()->default(
                400,
                false,
                "You cannot update data, because this attendance leave has been ".$attendanceLeave->status."!",
                null
            )->setStatusCode(400);
        }

        $attendanceLeave->employee_id = $request->employee_id;
        $attendanceLeave->leave_type = $request->leave_type;
        $attendanceLeave->start_date = $request->start_date;
        $attendanceLeave->end_date = $request->end_date;
        $attendanceLeave->reason = $request->reason;
        $attendanceLeave->modified_by = $request->user()->username;
        $attendanceLeave->update();

        return response()->default(
            200,
            true,
            "Attendance leave has beed updated",
            $attendanceLeave
        )->setStatusCode(200);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $attendanceLeave = AttendanceLeave::find($id);
        if(!$attendanceLeave) {
            return response()->default(
                404,
                false,
                "Attendance leave not found!",
                null
            )->setStatusCode(404);
        }

        if($attendanceLeave->status == 'APPROVED' || $attendanceLeave->status == 'REJECTED') {
            return response()->default(
                400,
                false,
                "You cannot delete data, because this attendance leave has been ".$attendanceLeave->status."!",
                null
            )->setStatusCode(400);
        }

        $attendanceLeave->delete();

        return response()->default(
            200,
            true,
            "Attendance leave has beed deleted",
            $attendanceLeave
        )->setStatusCode(200);
    }
}
