<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Models\Role;
use App\Models\Menu;
use App\Models\UserRoleAccess;
use App\Models\UserRole;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\Exceptions\MissingAbilityException;
use App\Http\Controllers\MenuController;
use App\Traits\HasQueryService;

class RoleAccessController extends Controller {
    use HasQueryService;

    public function index() {
        try {
            $subQuery = DB::table(UserRoleAccess::getTableName())
            ->leftJoin(Menu::getTableName(), UserRoleAccess::getTableName().'.menu_key', '=', Menu::getTableName().'.key')
            ->select(
                UserRoleAccess::getTableName().'.id',
                UserRoleAccess::getTableName().'.privilages_json',
                Menu::getTableName().'.name as menu_name',
                UserRoleAccess::getTableName().'.role_slug',
                UserRoleAccess::getTableName().'.status',
                UserRoleAccess::getTableName().'.created_date', 
                UserRoleAccess::getTableName().'.created_by', 
                UserRoleAccess::getTableName().'.modified_date', 
                UserRoleAccess::getTableName().'.modified_by',
            );

            // Call getPaging, which internally calls applyFiltersAndPagination
            $data = $this->getPaging($subQuery);

            $data['data'] = array_map(function($item) {
                $item['privilages_json'] = json_decode($item['privilages_json']);
                return $item;
            }, $data['data']);

            return response()->default(200, true, 'Get Role Access Success', $data);
        } catch (\InvalidArgumentException $e) {
            // Catch specific InvalidArgumentException thrown by sortBy macro
            return response()->default(400, false, $e->getMessage(), null);
        } 
        catch (\Exception $e) {
            // Catch general exceptions and return a 500 response
            return response()->default(500, false, 'An unexpected error occurred.', null);
        }
    }

    public function show($id) {
        $this->findData($id);

        $data = UserRoleAccess::query()->where(UserRoleAccess::getTableName().'.id', $id)
        ->select(
            '*' 
        )->first();
        $data['privilages_json'] = json_decode($data['privilages_json']);
        //dd($user);
        return response()->default(
            200,
            true, 
            "Get User Success", 
            $data
        )->setStatusCode(200);
        //return $data;
    }

    public function store($role_slug, Request $request) {
        $validatedData = $request->validate([
            'privilages_json' => 'required|array',
            'menu_key' => 'required|string'
        ]);

        if (UserRoleAccess::where('role_slug', $role_slug)->where('menu_key', $validatedData['menu_key'])->first()) {
            return response()->default(
                400,
                false, 
                "Role Access with same menu key is exists!",
                null
            )->setStatusCode(400);
        }

        if (!Role::where('slug', $role_slug)->first()) {
            return response()->default(
                400,
                false, 
                "Role Slug not recognized!",
                null
            )->setStatusCode(400);
        }

        if (!Menu::where('key', $validatedData['menu_key'])->first()) {
            return response()->default(
                400,
                false, 
                "Menu Key not recognized!",
                null
            )->setStatusCode(400);
        }

        $data = UserRoleAccess::create([
            'role_slug' => $role_slug,
            'privilages_json' => json_encode($validatedData['privilages_json']),
            'menu_key' =>  $validatedData['menu_key'],
            'status' => 'ACTIVE',
            'created_by' => $request->user()->username,
            'modified_by' => $request->user()->username
        ]);

        return response()->default(
            200,
            true, 
            "Role Access Created",
            $data
        )->setStatusCode(200);
    }

    public function update(Request $request, $id) {
        $data = $this->findData($id);

        $validatedData = $request->validate([
            'privilages_json' => 'required|array',
            'menu_key' => 'required|string'
        ]);
        //dd($data);
        $data->privilages_json = $request->privilages_json ?? json_encode($data->privilages_json);
        $data->menu_key = $request->menu_key ?? $data->menu_key;
        $data->modified_by = $request->user()->username;
        
        $data->update();

        return response()->default(
            200,
            true, 
            "Role Access Updated",
            $data
        )->setStatusCode(200);
    }

    public function toggle(Request $request, $id) {
        $data = $this->findData($id);

        $data->status == 'ACTIVE' ? $status = 'INACTIVE': $status = 'ACTIVE';
        $data->status = $status;
        $data->update();

        return response()->default(
            200,
            true, 
            $status=="ACTIVE"?"Role Access Activated!":"Role Access Deactivated!",
            $data
        )->setStatusCode(200);
    }

    public function destroy($id) {
        $data = $this->findData($id);

        $data->delete();
        
        return response()->default(
            200,
            true, 
            "Role Access Deleted",
            null
        )->setStatusCode(200);
    }

    private function findData($id) {
        $data = UserRoleAccess::find($id);
    
        if (!$data) {
            throw new \App\Exceptions\DataNotFoundException('Role Access Not Found', 400);
        }
    
        return $data;
    }

}