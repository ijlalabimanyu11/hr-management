<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use App\Models\Employee;
use App\Models\GlobalType;
use App\Models\GlobalTypeDetail;
use Illuminate\Database\Eloquent\Builder;
use App\Traits\HasQueryService;

class LOVController extends Controller {
    use HasQueryService;
    //CRUD FUNCTION
    public function index() {
        /*$data = GlobalType::query()
            ->select(
                "*"
            );
        //dd($data->ddRawSql());
        $size = request()->size ?? 10;

        $data = $data
            ->when(request()->search, fn($query, $search) => $query->whereLike($search))
            ->when(request()->sort, fn($query, $sort) => $query->sortBy($sort))
            ->paginate($size)
            ->toArray();

        return response()->default(
            200,
            true, 
            "Get Data Header Success", 
            [
                "currentPage" => $data['current_page'],
                "lastPage" => $data['last_page'],
                "pageSize" => $data['to'],
                "totalData" => $data['total'],
                "data" => $data['data'],
            ]
        )->setStatusCode(200);*/
        $data = GlobalType::select(['key', 'value', 'is_props'])->where('status','ACTIVE')->get()->toArray();
        //$data = collect();
        /*foreach($items as $item){
            $data->push(
                [
                    'key' => $item['id'],
                    'value' => $item['key']
                ]
            );
        }*/
        return response()->default(
            200,
            true, 
            "Get Global Type Header Success", 
            $data
        )->setStatusCode(200);
    }

    public function detail() {
        try {
            $subQuery = DB::table(GlobalTypeDetail::getTableName())
        ->leftJoin(GlobalType::getTableName(), GlobalTypeDetail::getTableName().'.global_type_id', '=', GlobalType::getTableName().'.id')
        ->select(
            GlobalTypeDetail::getTableName().'.id as id', 
            GlobalType::getTableName().'.key as name_slug', 
            GlobalTypeDetail::getTableName().'.key', 
            GlobalTypeDetail::getTableName().'.value', 
            GlobalTypeDetail::getTableName().'.status', 
            GlobalTypeDetail::getTableName().'.created_date', 
            GlobalTypeDetail::getTableName().'.created_by', 
            GlobalTypeDetail::getTableName().'.modified_date', 
            GlobalTypeDetail::getTableName().'.modified_by'
        );

            // Call getPaging, which internally calls applyFiltersAndPagination
            $data = $this->getPaging($subQuery);
            
            $data['data'] = array_map(function($item) {
                unset($item['name_slug']);
                return $item;
            }, $data['data']);

            return response()->default(200, true, 'Get Data Detail Success', $data);
        } catch (\InvalidArgumentException $e) {
            // Catch specific InvalidArgumentException thrown by sortBy macro
            return response()->default(400, false, $e->getMessage(), null);
        } 
        catch (\Exception $e) {
            // Catch general exceptions and return a 500 response
            return response()->default(500, false, 'An unexpected error occurred.', null);
        }
    }

    public function show($id) {
        $this->findData($id);

        $data = GlobalTypeDetail::query()->where(GlobalTypeDetail::getTableName().'.id', $id)
        ->select(
            '*' 
        )->first();
        
        //dd($user);
        return response()->default(
            200,
            true, 
            "Get Detail Data Success", 
            $data
        )->setStatusCode(200);
        //return $data;
    }

    public function store(Request $request) {
        $validatedData = $request->validate([
            'name_slug' => 'required|string',
            'key' => 'required|string',
            'value' => 'required|string',
            'is_props' => 'required|boolean'
        ]);

        $parent = $this->findParentName($validatedData['name_slug'],$validatedData['is_props']);

        if (GlobalTypeDetail::where('key', $validatedData['key'])->orWhere('value', $validatedData['value'])->first()) {
            return response()->default(
                400,
                false, 
                "Key/Value already exist!",
                null
            )->setStatusCode(400);
        }

        $data = GlobalTypeDetail::create([
            'global_type_id' =>  $parent->id,
            'key' =>  $validatedData['key'],
            'value' =>  $validatedData['value'],
            'status' => 'ACTIVE',
            'created_by' => $request->user()->username,
            'modified_by' => $request->user()->username
        ]);

        return response()->default(
            200,
            true, 
            $validatedData['is_props']?"System Configuration Created":"Data Configuration Created",
            $data
        )->setStatusCode(200);
    }

    public function update(Request $request, $id) {
        $data = $this->findData($id);

        $validatedData = $request->validate([
            'name_slug' => 'required|string',
            'key' => 'required|string',
            'value' => 'required|string',
            'is_props' => 'required|boolean'
        ]);
        //dd($data);

        if (GlobalTypeDetail::
            where('id', '!=', $id)
            ->where(function (Builder $query) use ($validatedData) {
                $query->where('key', $validatedData['key'])
                      ->orWhere('value', $validatedData['value']);
            })
            ->first()
            ) {
            return response()->default(
                400,
                false, 
                "Key/Value already exist!",
                null
            )->setStatusCode(400);
        }
        
        $data->key = $request->key ?? $data->key;
        $data->value = $request->value ?? $data->value;
        $data->modified_by = $request->user()->username;
        
        $data->update();

        return response()->default(
            200,
            true, 
            $validatedData['is_props']?"System Configuration Updated":"Data Configuration Updated",
            $data
        )->setStatusCode(200);
    }

    public function toggle(Request $request, $id) {
        $data = $this->findData($id);

        $parent = GlobalType::find($data->global_type_id);
        $msg = $parent->is_props?"System Configuration":"Data Configuration";

        $data->status == 'ACTIVE' ? $status = 'INACTIVE': $status = 'ACTIVE';
        $data->status = $status;
        $data->update();

        return response()->default(
            200,
            true, 
            $status=="ACTIVE"? $msg." Activated!" : $msg." Deactivated!",
            $data
        )->setStatusCode(200);
    }

    public function destroy($id) {
        $data = $this->findData($id);

        $data->delete();
        
        return response()->default(
            200,
            true, 
            "Data Deleted",
            null
        )->setStatusCode(200);
    }

    //GENERAL FUNCTION
    public function getGlobalTypeDetail($name_slug) {
        $parent = $this->findParentName($name_slug, false);

        $data = GlobalTypeDetail::select(
                "key",
                "value"
            )
            ->where('status','ACTIVE')
            ->where('global_type_id', $parent->id)
            ->get();
        
        return response()->default(
            200,
            true, 
            "Get Global Type ". $name_slug ." Success",
            $data
        )->setStatusCode(200);
    }

    public function getPropsDetail($name_slug) {
        $parent = $this->findParentName($name_slug, true);

        $data = GlobalTypeDetail::select(
                "key",
                "value"
            )
            ->where('status','ACTIVE')
            ->where('global_type_id', $parent->id)
            ->get();
        
        return response()->default(
            200,
            true, 
            "Get Props ". $name_slug ." Success",
            $data
        )->setStatusCode(200);
    }
    /*
    public function getRole() {
        $data = GlobalType::where('key', 'ROLE');
        dd($data);
        $data = collect();
        $data->push(
            [
                'key' => "super-admin",
                'value' => "Super Admin"
            ],
            [
                'key' => "admin",
                'value' => "Admin"
            ],
            [
                'key' => "user",
                'value' => "User"
            ],
            [
                'key' => "general",
                'value' => "General"
            ]
        );

        return response()->default(
            200,
            true, 
            "Get Role Success",
            $data->toArray()
        )->setStatusCode(200);
    }*/

    //HELPER
    private function findData($id) {
        $data = GlobalTypeDetail::find($id);

        if (!$data) {
            throw new \App\Exceptions\DataNotFoundException('Data Not Found', 400);
        }

        $parent = GlobalType::find($data->global_type_id);
        $msg = $parent->is_props?"System Configuration":"Data Configuration";
    
        if (!$parent) {
            throw new \App\Exceptions\DataNotFoundException($msg.' Not Found', 400);
        }
    
        return $data;
    }

    private function findParentName($key, $is_props) {
        $data = null;
        $msg = null;
        if($is_props){
            $data = GlobalType::select("id", "is_props")->where('key', $key)->where('is_props',1)->first();
            $msg = "System Configuration";
        }else{
            $data = GlobalType::select("id", "is_props")->where('key', $key)->where('is_props',0)->first();
            $msg = "Data Configuration";
        }

        if (!$data) {
            throw new \App\Exceptions\DataNotFoundException($msg.' Type Not Found', 400);
        }
    
        return $data;
    }
}
