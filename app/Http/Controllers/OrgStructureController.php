<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\Exceptions\MissingAbilityException;
use App\Traits\HasQueryService;

//IMPORT EXCEL UTILS
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;

//IMPORT MODEL
use App\Models\OrgStructureDetail;
use App\Models\VwOrgStructureDetail;
use App\Models\Department;
use App\Models\Section;
use App\Models\Position;
use App\Models\Employee;
use App\Models\EmployeeDesignation;

class OrgStructureController extends Controller {
    use HasQueryService;

    public function index()
    {
        try {
            $subQuery = DB::table(VwOrgStructureDetail::getTableName())
                ->select(
                    VwOrgStructureDetail::getTableName() . '.id',
                    VwOrgStructureDetail::getTableName() . '.item_name as position',
                    DB::raw("CASE 
                        WHEN " . VwOrgStructureDetail::getTableName() . ".item_parent_name IS NULL 
                        THEN 'Company' 
                        ELSE " . VwOrgStructureDetail::getTableName() . ".item_parent_name 
                    END as parent_position"),
                    VwOrgStructureDetail::getTableName() . '.employee_name',
                    VwOrgStructureDetail::getTableName() . '.section_name',
                    VwOrgStructureDetail::getTableName() . '.department_name',
                    VwOrgStructureDetail::getTableName() . '.created_date',
                    VwOrgStructureDetail::getTableName() . '.created_by',
                    VwOrgStructureDetail::getTableName() . '.modified_date',
                    VwOrgStructureDetail::getTableName() . '.modified_by'
                );

            // Call getPaging, which internally calls applyFiltersAndPagination
            $data = $this->getPaging($subQuery);
            return response()->default(200, true, 'Get Organization Structure Paging Success', $data);
        } catch (\InvalidArgumentException $e) {
            // Catch specific InvalidArgumentException thrown by sortBy macro
            return response()->default(400, false, $e->getMessage(), null);
        } catch (\Exception $e) {
            // Catch general exceptions and return a 500 response
            return response()->default(500, false, 'An unexpected error occurred.', null);
        }
    }

    // public function index() {
    //     $subQuery = DB::table(VwOrgStructureDetail::getTableName())
    //         ->select(
    //             VwOrgStructureDetail::getTableName() . '.id',
    //             VwOrgStructureDetail::getTableName() . '.item_name as position',
    //             //VwOrgStructureDetail::getTableName() . '.item_parent_name as parent_position',
    //             DB::raw("CASE 
    //                 WHEN " . VwOrgStructureDetail::getTableName() . ".item_parent_name IS NULL 
    //                 THEN 'Company' 
    //                 ELSE " . VwOrgStructureDetail::getTableName() . ".item_parent_name 
    //              END as parent_position"),
    //             VwOrgStructureDetail::getTableName() . '.employee_name',
    //             VwOrgStructureDetail::getTableName() . '.section_name',
    //             VwOrgStructureDetail::getTableName() . '.department_name',
    //             VwOrgStructureDetail::getTableName() . '.created_date',
    //             VwOrgStructureDetail::getTableName() . '.created_by',
    //             VwOrgStructureDetail::getTableName() . '.modified_date',
    //             VwOrgStructureDetail::getTableName() . '.modified_by'
    //         );

    //     $subQuerySql = $subQuery->toSql();
    //     $subQueryBindings = $subQuery->getBindings();

    //     $data = VwOrgStructureDetail::query()->from(DB::raw("({$subQuerySql}) as sub"))->mergeBindings($subQuery);

    //     // Define the allowed columns based on your query select list
    //     $allowedColumns = [
    //         'id',
    //         'position',
    //         'parent_position',
    //         'employee_name',
    //         'section_name',
    //         'department_name',
    //         'created_date',
    //         'created_by',
    //         'modified_date',
    //         'modified_by'
    //     ];

    //     // Pagination
    //     $size = request()->size ?? 10;

    //     $data = $data
    //         ->when(request()->search, fn($query, $search) => $query->whereLike($search))
    //         ->sortBy(request()->sort ?? null, $allowedColumns)
    //         ->paginate($size)
    //         ->toArray();


    //     return response()->default(
    //         200,
    //         true, 
    //         "Get Organization Structure Success", 
    //         [
    //             "currentPage" => $data['current_page'],
    //             "lastPage" => $data['last_page'],
    //             "pageSize" => $data['to'],
    //             "totalData" => $data['total'],
    //             "data" => $data['data'],
    //         ]
    //     )->setStatusCode(200);
    // }

    public function show($id) {
        $show = OrgStructureDetail::find($id);
        
        if (!$show) {
            return response()->default(
                400,
                false, 
                "Data not found",
                null
            )->setStatusCode(400);
        }

        $orgStructure = VwOrgStructureDetail::query()->where(VwOrgStructureDetail::getTableName().'.id', $id)
        ->select(
            VwOrgStructureDetail::getTableName().'.id',
            VwOrgStructureDetail::getTableName().'.item_id as position_id',
            VwOrgStructureDetail::getTableName().'.item_name as position',
            VwOrgStructureDetail::getTableName().'.item_parent_id as parent_position_id',
            //VwOrgStructureDetail::getTableName().'.item_parent_name as parent_position',
            DB::raw("CASE 
                    WHEN " . VwOrgStructureDetail::getTableName() . ".item_parent_name IS NULL 
                    THEN 'Company' 
                    ELSE " . VwOrgStructureDetail::getTableName() . ".item_parent_name 
                 END as parent_position"),
            VwOrgStructureDetail::getTableName().'.employee_name',
            VwOrgStructureDetail::getTableName().'.section_name',
            VwOrgStructureDetail::getTableName().'.department_name',
            VwOrgStructureDetail::getTableName().'.created_date', 
            VwOrgStructureDetail::getTableName().'.created_by', 
            VwOrgStructureDetail::getTableName().'.modified_date', 
            VwOrgStructureDetail::getTableName().'.modified_by'
        )->first();

        return response()->default(
            200,
            true, 
            "Get Organization Structure Detail Success", 
            $orgStructure
        )->setStatusCode(200);
    }

    private function getEmp($type, $id)
    {
        $validTypes = ['DEPARTMENT', 'SECTION', 'POSITION'];
        if (!in_array($type, $validTypes)) {
            return null;
        }

        $subQuery = DB::table(EmployeeDesignation::getTableName())
        ->leftJoin(Employee::getTableName().' as employee_main', EmployeeDesignation::getTableName().'.employee_id', '=', 'employee_main.id')
        ->leftJoin(Department::getTableName().' as department', EmployeeDesignation::getTableName().'.department', '=', 'department.id')
        ->leftJoin(Section::getTableName().' as section', EmployeeDesignation::getTableName().'.section', '=', 'section.id')
        ->leftJoin(Position::getTableName().' as position', EmployeeDesignation::getTableName().'.position', '=', 'position.id')
        ->leftJoin(Employee::getTableName().' as manager1', EmployeeDesignation::getTableName().'.line_manager1', '=', 'manager1.id')
        ->leftJoin(Employee::getTableName().' as manager2', EmployeeDesignation::getTableName().'.line_manager2', '=', 'manager2.id')
        ->select(
            EmployeeDesignation::getTableName().'.id',
            EmployeeDesignation::getTableName().'.employee_id',
            'employee_main.employee_code',
            'employee_main.name',
            EmployeeDesignation::getTableName().'.department',
            'department.code as department_code',
            'department.name as department_name',
            EmployeeDesignation::getTableName().'.section',
            'section.code as section_code',
            'section.name as section_name',
            EmployeeDesignation::getTableName().'.position',
            'position.code as position_code',
            'position.name as position_name',
            EmployeeDesignation::getTableName().'.line_manager1',
            'manager1.employee_code as line_manager1_code',
            'manager1.name as line_manager1_name',
            EmployeeDesignation::getTableName().'.line_manager2',
            'manager2.employee_code as line_manager2_code',
            'manager2.name as line_manager2_name',
            EmployeeDesignation::getTableName().'.start_date',
            EmployeeDesignation::getTableName().'.end_date',
            EmployeeDesignation::getTableName().'.notes',
            EmployeeDesignation::getTableName().'.status',
            EmployeeDesignation::getTableName().'.created_date',
            EmployeeDesignation::getTableName().'.created_by',
            EmployeeDesignation::getTableName().'.modified_date',
            EmployeeDesignation::getTableName().'.modified_by'
        );

        switch($type) {
            case 'DEPARTMENT':
                $subQuery->where(EmployeeDesignation::getTableName().'.department', '=', $id);
                break;
            case 'SECTION':
                $subQuery->where(EmployeeDesignation::getTableName().'.section', '=', $id);
                break;
            case 'POSITION':
                $subQuery->where(EmployeeDesignation::getTableName().'.position', '=', $id);
                break;
            default:
                // Do nothing
                break;
        }

        $subQuerySql = $subQuery->toSql();
        $subQueryBindings = $subQuery->getBindings();

        $data = EmployeeDesignation::query()->from(DB::raw("({$subQuerySql}) as sub"))->mergeBindings($subQuery);

        $size = request()->size ?? 10;
        
        $data = $data
            ->when(request()->search, fn($query, $search) => $query->whereLike($search))
            ->when(request()->sort, fn($query, $sort) => $query->sortBy($sort))
            ->paginate($size)
            ->toArray();

        return $data;
    }

    public function showChart()
    {
        $subQuery = DB::table(VwOrgStructureDetail::getTableName())
            ->select(
                VwOrgStructureDetail::getTableName().'.id',
                VwOrgStructureDetail::getTableName().'.item_name',
                VwOrgStructureDetail::getTableName().'.item_parent_name',
                VwOrgStructureDetail::getTableName().'.employee_name',
                VwOrgStructureDetail::getTableName().'.section_name',
                VwOrgStructureDetail::getTableName().'.department_name',
                VwOrgStructureDetail::getTableName().'.created_date', 
                VwOrgStructureDetail::getTableName().'.created_by', 
                VwOrgStructureDetail::getTableName().'.modified_date', 
                VwOrgStructureDetail::getTableName().'.modified_by'
            );

        $subQuerySql = $subQuery->toSql();
        $subQueryBindings = $subQuery->getBindings();

        $data = VwOrgStructureDetail::query()->from(DB::raw("({$subQuerySql}) as sub"))->mergeBindings($subQuery);
        
        $data = $data->get()->toArray();
        
        // Step 1: Create a map of nodes by item_name
        $nodes = [];
        foreach ($data as $item) {
            $nodes[$item['item_name']] = [
                'label' => $item['employee_name'],//employeeName
                'position' => $item['item_name'],//positionName
                'department' => $item['section_name'].$item['department_name'],//departmentName
                'children' => []
            ];
        }

        // Step 2: Build the hierarchy using item_parent_name
        $topLevelNodes = [];
        foreach ($data as $item) {
            $name = $item['item_name'];
            $parentName = $item['item_parent_name'];

            if ($parentName && isset($nodes[$parentName])) {
                // Add as child to the parent node
                $nodes[$parentName]['children'][] = &$nodes[$name];
            } else {
                // This is a top-level node (no parent in the data)
                $topLevelNodes[] = &$nodes[$name];
            }
        }

        // Step 3: Create the hardcoded root node "Company" and add top-level nodes as its children
        $rootNode = [
            'label' => 'Company',
            'children' => $topLevelNodes
        ];

        return response()->default(
            200,
            true, 
            "Get Organization Structure Chart Success", 
            $rootNode
        )->setStatusCode(200);
    }

    public function store(Request $request) {
        // Validate request data
        $validatedData = $request->validate([
            'position' => 'required|numeric',
            'parentPosition' => 'required|numeric',
        ]);

        // Check if item exists in organization structure
        $pos = OrgStructureDetail::where('item', $validatedData['position'])
        ->first();
        if ($pos) {
            return response()->default(
                400,
                false, 
                "Position already is exists in organization structure!",
                null
            )->setStatusCode(400);
        }

        // Check if item exists   
        $itemId = $validatedData['position'];

        $item = Position::find($itemId);
        if (!$item) {
            return response()->default(
                400,
                false,
                "Position not found!",
                null
            )->setStatusCode(400);
        }
        
        // Create the organization structure
        $stored = OrgStructureDetail::create([
            'item' => $validatedData['position'],
            'item_parent' => $validatedData['parentPosition']==0?null:$validatedData['parentPosition'],
            'created_date' => now(),
            'created_by' => $request->user()->username
        ]);
        
        // Return success response
        return response()->default(
            200,
            true, 
            "Position added to organization structure!",
            $stored
        )->setStatusCode(200);
    }
    
    public function update(Request $request, $id) {
        // Find org structure by ID or return error response
        $data = OrgStructureDetail::find($id);
        
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Position not found in organization structure",
                null
            )->setStatusCode(400);
        }

        $validatedData = $request->validate([
            'parentPosition' => 'numeric|nullable',
        ]);
        
        // Define the fields that can be updated
        $fields = [
            'parentPosition' => 'item_parent'
        ];
    
        // Prepare the update data
        $updateData = [];
    
        foreach ($fields as $requestField => $dbField) {
            if ($request->filled($requestField)) {
                $updateData[$dbField] = $request->input($requestField);
            }
        }
        $updateData['modified_by'] = $request->user()->username;
        $updateData['modified_date'] = now();
        
        // If there is data to update, perform the update
        if (!empty($updateData)) {
            $data->update($updateData);
        }
    
        $data = VwOrgStructureDetail::query()->where(VwOrgStructureDetail::getTableName().'.id', $id)
        ->select(
            VwOrgStructureDetail::getTableName().'.id',
            VwOrgStructureDetail::getTableName().'.item_id as position_id',
            VwOrgStructureDetail::getTableName().'.item_name as position',
            VwOrgStructureDetail::getTableName().'.item_parent_id as parent_position_id',
            VwOrgStructureDetail::getTableName().'.item_parent_name as parent_position',
            VwOrgStructureDetail::getTableName().'.employee_name',
            VwOrgStructureDetail::getTableName().'.section_name',
            VwOrgStructureDetail::getTableName().'.department_name',
            VwOrgStructureDetail::getTableName().'.created_date', 
            VwOrgStructureDetail::getTableName().'.created_by', 
            VwOrgStructureDetail::getTableName().'.modified_date', 
            VwOrgStructureDetail::getTableName().'.modified_by'
        )->first();
        // Return success response
        return response()->default(
            200,
            true, 
            "Item updated in organization structure",
            $data
        )->setStatusCode(200);
    }

    public function destroy($id) {
        $data = OrgStructureDetail::find($id);
        
        $child = VwOrgStructureDetail::where('item_parent_id', $data['item_parent'])->first();
        
        if ($child) {
            return response()->default(
                409,
                false, 
                "Position have children in organization structure!",
                null
            )->setStatusCode(409);
        }

        $data->delete();
        
        return response()->default(
            200,
            true, 
            "Position deleted from organization structure",
            null
        )->setStatusCode(200);
    }

    public function export()
    {
        $name = 'users_' . date('Y-m-d i:h:s');
        //$data = Excel::download(new UsersExport(), $name . '.xlsx');
    
        return Excel::download(new UsersExport, 'invoices.xlsx');
        //return $data;
    }

    /*
    public function updateOld(Request $request, $id) {
        // Find org structure by ID or return error response
        $data = OrgStructure::find($id);
        
        if (!$data) {
            return response()->default(
                400,
                false, 
                "Organization Structure Not Found",
                null
            )->setStatusCode(400);
        }

        $validatedData = $request->validate([
            'code' => 'required|string|max:255',
            'name' => 'required|string|max:255',
            'effectiveDate' => 'required|date',
            'description' => 'nullable|string',
            'isDraft' => 'required|boolean',
            'orgStructureDetail' => 'required|array',
            'orgStructureDetail.*.itemType' => 'required|string',
            'orgStructureDetail.*.item' => 'required',
            'orgStructureDetail.*.itemParent' => 'nullable',
        ]);

        // Check if code unique
        $code = OrgStructure::where(function ($query) use ($validatedData) {
            $query->where('code', $validatedData['code']);
        })
        ->whereNot('id', $id)
        ->first();
        
        if ($code) {
            return response()->default(
                400,
                false, 
                "Organization Structure Code is exists!",
                null
            )->setStatusCode(400);
        }
        
        if (!$validatedData['isDraft']) {
            $updateExistingStatus = OrgStructure::where('status','ACTIVE');
            if (!$updateExistingStatus) {
                // return response()->default(
                //     500,
                //     false, 
                //     "Error OrgStructure Not Found",
                //     null
                // )->setStatusCode(400);
            }
            $updateExistingStatus->update(['status'=>'INACTIVE']);
        }
    
        

        // Define the fields that can be updated
        $fields = [
            'code' => 'code',
            'name' => 'name',
            'effectiveDate' => 'effective_date',
            'description' => 'description'
        ];
    
        // Prepare the update data
        $updateData = [];
    
        foreach ($fields as $requestField => $dbField) {
            if ($request->filled($requestField)) {
                $updateData[$dbField] = $request->input($requestField);
            }
        }
    
        $updateData['status'] = $validatedData['isDraft']?'DRAFT':'ACTIVE';
        $updateData['modified_by'] = $request->user()->username;
    
        // If there is data to update, perform the update
        if (!empty($updateData)) {
            $data->update($updateData);
        }
    
        // Fetch existing details
        $existingDetails = OrgStructureDetail::where('org_structure_id', $id)->get();

        // Prepare incoming details for comparison
        $incomingDetails = collect($validatedData['orgStructureDetail']);

        // IDs of existing details for tracking deletions
        $existingDetailIds = $existingDetails->pluck('id')->toArray();

        // IDs of incoming details for tracking which ones to update or create
        $incomingDetailIds = [];

        // Loop through incoming details to update or create
        foreach ($incomingDetails as $dtl) {
            $detail = OrgStructureDetail::updateOrCreate(
                [
                    'org_structure_id' => $id,
                    'item_type' => $dtl['itemType'],
                    'item' => $dtl['item'],
                    'item_parent' => $dtl['itemParent'],
                    'modified_by' => $request->user()->username,
                    'modified_date' => now(),
                ]
            );

            // Track incoming detail IDs to prevent deletion of updated/created ones
            $incomingDetailIds[] = $detail->id;
        }

        // Find details that exist in the database but are not in the incoming payload (for deletion)
        $detailsToDelete = array_diff($existingDetailIds, $incomingDetailIds);

        // Delete details that are no longer in the incoming payload
        OrgStructureDetail::whereIn('id', $detailsToDelete)->delete();

        // Return success response
        return response()->default(
            200,
            true, 
            "Organization Structure Updated",
            $data
        )->setStatusCode(200);
    }
    */
}