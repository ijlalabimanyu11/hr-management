<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class MenuController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = Menu::query();
        $size = request()->size==null?10:request()->size;

        $data = request()->search!=null?
        $data->whereLike(request()->search)->paginate($size)->toArray()
        :
        $data->paginate($size)->toArray();

        //->toJson();

        //return Menu::all();

        return response()->default(
            200,
            true, 
            "Get Menu Success", 
            $data
        )->setStatusCode(200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data = $request->validate([
            'name' => 'required',
            'order' => 'required',
            'title' => 'required',
            'key' => 'required',
            'path' => 'nullable',
            'icon' => 'nullable',
            'is_group' => 'nullable',
            'parent' => 'nullable|integer',
        ]);

        $existing = Menu::where('name', $data['name'])->first();

        if (! $existing) {
            $Menu = Menu::create([
                'name' => $data['name'],
                'order' => $data['order'],
                'title' => $data['title'],
                'key' => $data['key'],
                'path' => $data['path'],
                'is_group' => $data['is_group'],
                'parent' => $data['parent'],
            ]);

            return $Menu;
        }

        return response(['error' => 1, 'message' => 'Menu already exists'], 409);
    }

    /**
     * Display the specified resource.
     *
     * @return \App\Models\Menu $Menu
     */
    public function show(Menu $Menu) {
        return $Menu;
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|Menu
     */
    public function update(Request $request, ?Menu $Menu = null) {
        if (! $Menu) {
            return response(['error' => 1, 'message' => 'Menu doesn\'t exist'], 404);
        }

        $Menu->update();

        return $Menu;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $Menu) {
        $Menu->delete();

        return response(['error' => 0, 'message' => 'Menu has been deleted']);
    }

    public function getSidebar(Request $request) {
        /*$data = $request->validate([
            'userLevel' => 'required',
        ]);*/
        
        //$menus = Menu::where('parent', null)->orderBy('order')->get();
        //$menus = Menu::all();
        $menus = Menu::where('status', 'ACTIVE')->get();

        $headerMenus = $menus->filter(function (Menu $value, int $key) {
            return $value['parent'] == null;
        })->sortBy('order')->all();

        $sidebar = collect();
        foreach($headerMenus as $menu){
            $submenus = $this->getChildren($menu->id, $menus);
            $sidebar->push(
                [
                    'key' => $menu->key,
                    'label' => $menu->name,
                    'type' => "group",
                    'children' => $submenus
                ]
            );
        }

        return response()->default(
            200,
            true, 
            "Get Sidebar Success", 
            $sidebar->all()
        )->setStatusCode(200);
        //return $sidebar->all();
    }

    public function getSidebarJson($userLevel) {
        /*$data = $request->validate([
            'userLevel' => 'required',
        ]);*/
        
        //$menus = Menu::where('parent', null)->orderBy('order')->get();
        //$menus = Menu::all();
        $menus = Menu::where('status', 'ACTIVE')->get();

        $headerMenus = $menus->filter(function (Menu $value, int $key) {
            return $value['parent'] == null;
        })->sortBy('order')->all();

        $sidebar = collect();
        foreach($headerMenus as $menu){
            $submenus = $this->getChildren($menu->id, $menus);
            $sidebar->push(
                [
                    'key' => $menu->key,
                    'label' => $menu->name,
                    'type' => "group",
                    'children' => $submenus
                ]
            );
        }

        /*return response()->default(
            true, 
            "Get Sidebar Success", 
            $sidebar->all()
        )->setStatusCode(200);*/
        return $sidebar->all();
    }

    private function getChildren($parentId, $allMenus){
        $childMenus = $allMenus->filter(function (Menu $value, int $key) use($parentId) {
            return $value['parent'] == $parentId;
        })->sortBy('order')->all();

        if(count($childMenus)>0){
            $subMenus = collect();
            foreach($childMenus as $menu){
                $childItem = $this->getChildren($menu->id, $allMenus);
                $subMenus->push(
                    [
                        'key' => $menu->key,
                        'label' => $menu->name,
                        'path' => $menu->path,
                        'icon' => $menu->icon,
                        'children' => $childItem
                    ],
                );
            }
            return $subMenus->all();
        }
        return null;
    }

    public function getMenuLOV(){
        $items = Menu::select(['key','name'])->where('status','ACTIVE')->get()->toArray();
        $data = collect();
        foreach($items as $item){
            $data->push(
                [
                    'key' => $item['key'],
                    'value' => $item['name']
                ]
            );
        }
        return response()->default(
            200,
            true, 
            "Get Menu LOV Success", 
            $data
        )->setStatusCode(200);
    }
}
