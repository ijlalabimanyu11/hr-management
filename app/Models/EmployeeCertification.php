<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\StaticTableName;

class EmployeeCertification extends Model
{
    use HasFactory, StaticTableName;
    protected $table = 'employee_certification';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'modified_date';
    public $timestamps = false;

    protected $fillable = [
        'employee_id',
        'name',
        'file',
        'issuing_organization',
        'issuing_date',
        'expired_date',
        'notes',
        'status',
        'created_by',
        'created_date'
    ];

    protected $casts = [
        'issuing_date' => 'datetime:Y-m-d',
        'expired_date' => 'datetime:Y-m-d',
        'created_date' => 'datetime:Y-m-d H:i:s',
        'modified_date' => 'datetime:Y-m-d H:i:s',
    ];

}
