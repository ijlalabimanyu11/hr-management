<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\StaticTableName;

class EmployeeTermination extends Model
{
    use HasFactory, StaticTableName;
    protected $table = 'employee_termination';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'modified_date';
    public $timestamps = false;

    protected $fillable = [
        'employee_id',
        'submission_date',
        'approved_date',
        'last_working_date',
        'reason',
        'attachment',
        'created_by',
    ];

    protected $cast = [
        'submission_date' => 'datetime:Y-m-d',
        'approved_date' => 'datetime:Y-m-d',
        'last_working_date' => 'datetime:Y-m-d',
        'created_date' => 'datetime:Y-m-d H:i:s',
        'modified_date' => 'datetime:Y-m-d H:i:s'
    ];
}
