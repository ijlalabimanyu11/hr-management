<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\StaticTableName;

class Employee extends Model {
    use HasFactory, StaticTableName;
    protected $table = 'employees';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'modified_date';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'employee_code',
        'national_identifier',
        'name',
        'email',
        'birth_date',
        'gender',
        'phone',
        'address',
        'salary_type',
        'basic_salary',

        'bank_name',
        'branch_location',
        'bank_account_name',
        'bank_account_number',
        'tax_payer_identifier',

        'employee_picture',
        'cv',

        'join_date',
        'status',
        'created_by',
        'created_date'
    ];

    protected $casts = [
        'birth_date' => 'datetime:Y-m-d',
        'join_date' => 'datetime:Y-m-d',
        'created_date' => 'datetime:Y-m-d H:i:s',
        'modified_date' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        
    ];
}
