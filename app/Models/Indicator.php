<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\StaticTableName;

class Indicator extends Model {
    use HasFactory, StaticTableName;
    protected $table = 'indicators';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'modified_date';
    
    protected $fillable = [
        'name',
        'description',
        'status',
        'created_by',
        'modified_by'
    ];
    protected $casts = [
        'created_date' => 'datetime:Y-m-d H:i:s',
        'modified_date' => 'datetime:Y-m-d H:i:s',
    ];
}
