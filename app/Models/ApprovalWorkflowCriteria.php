<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\StaticTableName;

class ApprovalWorkflowCriteria extends Model {
    use HasFactory, StaticTableName;
    protected $table = 'approval_workflow_criterias';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'modified_date';
    
    protected $fillable = [
        'approval_workflow_id',
        'type',
        'line_manager',
        'position',
        'department_id',
        'section_id',
        'status',
        'created_by',
        'modified_by'
    ];
    protected $casts = [
        'created_date' => 'datetime:Y-m-d H:i:s',
        'modified_date' => 'datetime:Y-m-d H:i:s',
    ];
}
