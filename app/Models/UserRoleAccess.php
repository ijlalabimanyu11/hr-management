<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\StaticTableName;

class UserRoleAccess extends Model {
    use HasFactory,StaticTableName;
    protected $table = 'user_roles_access';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'modified_date';

    protected $fillable = [
        'role_slug',
        'privilages_json',
        'menu_key',
        'status',
        'created_by',
        'modified_by'
    ];

    protected $casts = [
        'created_date' => 'datetime:Y-m-d H:i:s',
        'modified_date' => 'datetime:Y-m-d H:i:s',
    ];
}
