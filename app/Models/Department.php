<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\StaticTableName;

class Department extends Model {
    use HasFactory, StaticTableName;
    protected $table = 'departments';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = null;//'modified_date';
    
    protected $fillable = [
        'code',
        'name',
        'head',
        'description',
        'status',
        'created_by',
        'modified_by'
    ];
    protected $casts = [
        'created_date' => 'datetime:Y-m-d H:i:s',
        'modified_date' => 'datetime:Y-m-d H:i:s',
    ];
    protected static function boot()
    {
        parent::boot();

        static::updating(function ($model) {
            $model->modified_date = now(); // Manually set modified_at
            $model->modified_by = auth()->user()->username ?? 'system'; // Store the modifier
        });
    }
}
