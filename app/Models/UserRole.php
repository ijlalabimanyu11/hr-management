<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\StaticTableName;

class UserRole extends Model {
    use HasFactory,StaticTableName;
    protected $table = 'user_roles';
    protected $fillable = [
        'user_id',
        'role_id',
        'status'
    ];
    protected $casts = [
        'created_date' => 'datetime:Y-m-d H:i:s',
        'modified_date' => 'datetime:Y-m-d H:i:s',
    ];
}
