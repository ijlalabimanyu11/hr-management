<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\StaticTableName;

class AppraisalSubmission extends Model {
    use HasFactory, StaticTableName;
    protected $table = 'appraisal_submissions';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'modified_date';
    
    protected $fillable = [
        'appraisal_period_id',
        'employee_id',
        'department_id',
        'final_score',
        'status',
        'description',
        'is_deleted',
        'created_by',
        'modified_by'
    ];
    protected $casts = [
        'created_date' => 'datetime:Y-m-d H:i:s',
        'modified_date' => 'datetime:Y-m-d H:i:s',
    ];
}
