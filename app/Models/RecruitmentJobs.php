<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\StaticTableName;

class RecruitmentJobs extends Model
{
    use HasFactory, StaticTableName;
    protected $table = 'recruitment_jobs';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'modified_date';
    public $timestamps = false;

    protected $fillable = [
        'job_title',
        'number_of_position',
        'skill_box',
        'location',
        'work_type',
        'position',
        'job_type',
        'salary_range',
        'salary_type',
        'job_description',
        'job_requirement',
        'publish_on_career',
        'publish_start_date',
        'publish_end_date',
        'created_by'
    ];

    protected $cast = [
        'publish_start_date' => 'datetime:Y-m-d',
        'publish_end_date' => 'datetime:Y-m-d',
        'created_date' => 'datetime:Y-m-d H:i:s',
        'modified_date' => 'datetime:Y-m-d H:i:s'
    ];
}
