<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\StaticTableName;

class InterviewSchedule extends Model
{
    use HasFactory, StaticTableName;
    protected $table = 'interview_schedule';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'modified_date';
    public $timestamps = false;

    protected $fillable = [
        'job_id',
        'application_id',
        'applier_name',
        'pic',
        'department',
        'position',
        'employee',
        'start_date',
        'end_date',
        'platform',
        'link',
        'description',
        'feedback',
        'status',
        'created_by'
    ];

    protected $cast = [
        'start_date' => 'datetime:Y-m-d H:i:s',
        'end_date' => 'datetime:Y-m-d H:i:s',
        'created_date' => 'datetime:Y-m-d H:i:s',
        'modified_date' => 'datetime:Y-m-d H:i:s'
    ];
}
