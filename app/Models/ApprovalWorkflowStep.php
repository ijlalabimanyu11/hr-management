<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\StaticTableName;

class ApprovalWorkflowStep extends Model {
    use HasFactory, StaticTableName;
    protected $table = 'approval_workflow_steps';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'modified_date';
    
    protected $fillable = [
        'approval_workflow_id',
        'step',
        'type',
        'line_manager',
        'department_id',
        'section_id',
        'position',
        'is_final',
        'status',
        'created_by',
        'modified_by'
    ];
    protected $casts = [
        'created_date' => 'datetime:Y-m-d H:i:s',
        'modified_date' => 'datetime:Y-m-d H:i:s',
    ];
}
