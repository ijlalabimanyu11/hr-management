<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\StaticTableName;

class AppraisalSubmissionDetail extends Model {
    use HasFactory, StaticTableName;
    protected $table = 'appraisal_submission_details';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'modified_date';
    
    protected $fillable = [
        'appraisal_submission_id',
        'appraisal_period_detail_id',
        'indicator_id',
        'weight',
        'target',
        'achievement',
        'score',
        'remark',
        'is_deleted',
        'created_by',
        'modified_by'
    ];
    protected $casts = [
        'created_date' => 'datetime:Y-m-d H:i:s',
        'modified_date' => 'datetime:Y-m-d H:i:s',
    ];
}
