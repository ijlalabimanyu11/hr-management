<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\StaticTableName;

class GlobalTypeDetail extends Model {
    use HasFactory, StaticTableName;
    protected $table = 'global_type_detail';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'modified_date';
    
    protected $fillable = [
        'global_type_id',
        'key',
        'value',
        'status',
        'created_by',
        'modified_by'
    ];
    protected $casts = [
        'created_date' => 'datetime:Y-m-d H:i:s',
        'modified_date' => 'datetime:Y-m-d H:i:s',
    ];
}
