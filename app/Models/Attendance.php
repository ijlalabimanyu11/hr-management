<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\StaticTableName;

class Attendance extends Model
{
    use HasFactory, StaticTableName;
    protected $table = 'attendance';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'modified_date';
    public $timestamps = false;

    protected $fillable = [
        'employee_id',
        'date',
        'start_time',
        'end_time',
        'activity',
        'notes',
        'correction_cause',
        'status',
        'created_by'
    ];

    protected $cast = [
        'date' => 'datetime:Y-m-d',
        'start_time' => 'datetime:Y-m-d H:i:s',
        'end_time' => 'datetime:Y-m-d H:i:s',
        'created_date' => 'datetime:Y-m-d H:i:s',
        'modified_date' => 'datetime:Y-m-d H:i:s'
    ];
}
