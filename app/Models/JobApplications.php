<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\StaticTableName;

class JobApplications extends Model
{
    use HasFactory, StaticTableName;
    protected $table = 'job_applications';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'modified_date';
    public $timestamps = false;

    protected $fillable = [
        'job_id',
        'full_name',
        'date_of_birth',
        'email',
        'phone',
        'gender',
        'cv',
        'linkedin_url',
        'reason_to_apply',
        'rating',
        'status',
        'created_by'
    ];

    protected $cast = [
        'date_of_birth' => 'datetime:Y-m-d',
        'created_date' => 'datetime:Y-m-d H:i:s',
        'modified_date' => 'datetime:Y-m-d H:i:s'
    ];
}
