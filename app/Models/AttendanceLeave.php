<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\StaticTableName;

class AttendanceLeave extends Model
{
    use HasFactory, StaticTableName;
    protected $table = 'attendance_leave';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'modified_date';
    public $timestamps = false;

    protected $fillable = [
        'employee_id',
        'leave_type',
        'start_date',
        'end_date',
        'reason',
        'is_debt',
        'status',
        'created_by'
    ];

    protected $cast = [
        'start_date' => 'datetime:Y-m-d',
        'end_date' => 'datetime:Y-m-d',
        'created_date' => 'datetime:Y-m-d H:i:s',
        'modified_date' => 'datetime:Y-m-d H:i:s'
    ];
}
