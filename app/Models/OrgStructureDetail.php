<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\StaticTableName;

class OrgStructureDetail extends Model {
    use HasFactory, StaticTableName;
    protected $table = 'org_structure_details';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'modified_date';
    public $timestamps = false;
    protected $fillable = [
        'org_structure_id',
        'item_type',
        'item',
        'item_parent',
        'created_date',
        'created_by',
        'modified_date',
        'modified_by'
    ];
    protected $casts = [
        'created_date' => 'datetime:Y-m-d H:i:s',
        'modified_date' => 'datetime:Y-m-d H:i:s',
    ];
}
