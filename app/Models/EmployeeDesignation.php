<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\StaticTableName;

class EmployeeDesignation extends Model
{
    use HasFactory, StaticTableName;
    protected $table = 'employee_designation';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'modified_date';
    public $timestamps = false;

    protected $fillable = [
        'employee_id',
        'department',
        'section',
        'position',
        'line_manager1',
        'line_manager2',
        'start_date',
        'end_date',
        'notes',
        'status',
        'created_by'
    ];

    protected $cast = [
        'start_date' => 'datetime:Y-m-d',
        'end_date' => 'datetime:Y-m-d',
        'created_date' => 'datetime:Y-m-d H:i:s',
        'modified_date' => 'datetime:Y-m-d H:i:s'
    ];


}
