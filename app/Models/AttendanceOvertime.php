<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\StaticTableName;

class AttendanceOvertime extends Model
{
    use HasFactory, StaticTableName;
    protected $table = 'attendance_overtime';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'modified_date';
    public $timestamps = false;

    protected $fillable = [
        'employee_id',
        'overtime_type',
        'cause',
        'start_date',
        'end_date',
        'notes',
        'status',
        'created_by'
    ];

    protected $cast = [
        'start_date' => 'datetime:Y-m-d H:i:s',
        'end_date' => 'datetime:Y-m-d H:i:s',
        'created_date' => 'datetime:Y-m-d H:i:s',
        'modified_date' => 'datetime:Y-m-d H:i:s'
    ];
}
