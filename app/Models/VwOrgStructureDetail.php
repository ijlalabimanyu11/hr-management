<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\StaticTableName;

class VwOrgStructureDetail extends Model {
    use HasFactory, StaticTableName;
    protected $table = 'vw_org_structure_details';
}
