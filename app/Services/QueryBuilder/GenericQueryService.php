<?php

namespace App\Services\QueryBuilder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;

class GenericQueryService
{   
    private function getAliases($data)
    {
        $rawSql = $data->from->getValue($data->getGrammar());
        $aliases = [];
        
        // Get all columns in order
        preg_match('/select\s+(.*?)\s+from/is', $rawSql, $selectMatch);
        if (!empty($selectMatch[1])) {
            $columns = $selectMatch[1];
            
            // Get regular columns and aliases
            preg_match_all('/`[^`]*\.`([^`]+)`(?:\s+as\s+`([^`]+)`)?/', $columns, $matches);
            foreach ($matches[1] as $i => $column) {
                $aliases[] = !empty($matches[2][$i]) ? $matches[2][$i] : $column;
            }

            // Get CASE statement alias
            preg_match('/CASE[\s\S]*?END\s+as\s+([^\s,]+)/i', $columns, $caseMatch);
            if (!empty($caseMatch[1])) {
                $aliases[] = trim($caseMatch[1], '`');
            }
        }
        
        return array_filter($aliases);
    }

    public function applyFiltersAndPagination($subQuery, $isPaging)
    {
        $subQuerySql = $subQuery->toSql();
        $subQueryBindings = $subQuery->getBindings();

        // Use DB::table with raw subquery to make it generic
        $data = DB::table(DB::raw("({$subQuerySql}) as sub"))
            ->mergeBindings($subQuery);

        // Use a generic model to convert it to Eloquent query builder
        $data = (new class extends Model {
            protected $table = 'sub'; // Just a placeholder table
            public $timestamps = false;
        })->newQuery()->from(DB::raw("({$subQuerySql}) as sub"))
            ->mergeBindings($subQuery);

        $size = request()->size ?? 10;;
        //dd($data, $this->getAliases($data));
        $data = $data
            ->when(request()->search, fn($query, $search) => $query->whereLike($search))
            ->sortBy(request()->sort ?? null, $this->getAliases($data))
            ->paginate($size)
            ->toArray();
        if($isPaging){
            return [
                "currentPage" => $data['current_page'],
                    "lastPage" => $data['last_page'],
                    "pageSize" => $data['to'],
                    "totalData" => $data['total'],
                    "data" => $data['data'],
            ];
        }
        return $data;
    }
}

