<?php

use App\Http\Controllers\HydraController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\RoleAccessController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserRoleController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\LOVController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\EmployeeDesignationController;
use App\Http\Controllers\EmployeeResignationController;
use App\Http\Controllers\ApprovalController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\SectionController;
use App\Http\Controllers\EmployeeTerminationController;
use App\Http\Controllers\EmployeeLeaveController;
use App\Http\Controllers\EmployeeReimbursementController;
use App\Http\Controllers\AttendanceController;
use App\Http\Controllers\AttendanceLeaveController;
use App\Http\Controllers\AttendanceOvertimeController;
use App\Http\Controllers\RecruitmentJobsController;
use App\Http\Controllers\JobApplicationsController;
use App\Http\Controllers\InterviewScheduleController;
use App\Http\Controllers\PositionController;
use App\Http\Controllers\OrgStructureController;
use App\Http\Controllers\IndicatorController;
use App\Http\Controllers\AppraisalPeriodController;
use App\Http\Controllers\AppraisalSubmissionController;
use App\Http\Controllers\TesController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

//use the middleware 'hydra.log' with any request to get the detailed headers, request parameters and response logged in logs/laravel.log

Route::get('hydra', [HydraController::class, 'hydra']);
Route::get('hydra/version', [HydraController::class, 'version']);

//Route::apiResource('users', UserController::class)->except(['edit', 'create', 'store', 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin']);
Route::apiResource('users', UserController::class)->except(['edit', 'create', 'store', 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin']);
Route::post('users', [UserController::class, 'store'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::put('users/{user}', [UserController::class, 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('users/{user}', [UserController::class, 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::patch('users/{user}', [UserController::class, 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('me', [UserController::class, 'me'])->middleware('auth:sanctum');
Route::get('export/', [UserController::class, 'export'])->middleware('auth:sanctum');
Route::get('grant-access', [UserController::class, 'getGrantAccess'])->middleware('auth:sanctum');
Route::post('login', [UserController::class, 'login']);
Route::post('logout', [UserController::class, 'logout'])->middleware('auth:sanctum');;
Route::post('users/{user}/toggle', [UserController::class, 'toggle'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('users/{user}/reset-password', [UserController::class, 'resetPassword'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);

Route::apiResource('roles', RoleController::class)->except(['create', 'edit'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::apiResource('users.roles', UserRoleController::class)->except(['create', 'edit', 'show', 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin']);

Route::apiResource('menus', MenuController::class)->except(['create', 'edit', 'show', 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);

Route::get('sidebar', [MenuController::class, 'getSidebar'])->middleware('auth:sanctum');

//LOV FROM CONTROLLER
Route::get('lov/employee', [EmployeeController::class, 'getEmployeeLOV'])->middleware('auth:sanctum');
Route::get('lov/department', [DepartmentController::class, 'getLOV'])->middleware('auth:sanctum');
Route::get('lov/section', [SectionController::class, 'getLOV'])->middleware('auth:sanctum');
Route::get('lov/position', [PositionController::class, 'getLOV'])->middleware('auth:sanctum');
Route::get('lov/menu', [MenuController::class, 'getMenuLOV'])->middleware('auth:sanctum');

//GLOBAL TYPE & PROPS
Route::get('lov/{name_slug}', [LOVController::class, 'getGlobalTypeDetail'])->middleware('auth:sanctum');
Route::get('props/{name_slug}', [LOVController::class, 'getPropsDetail'])->middleware('auth:sanctum');

Route::get('data-system-config', [LOVController::class, 'index'])->middleware(['auth:sanctum', 'ability:super-admin']);
Route::get('data-system-config/detail', [LOVController::class, 'detail'])->middleware(['auth:sanctum', 'ability:super-admin']);
Route::get('data-system-config/{id}', [LOVController::class, 'show'])->middleware(['auth:sanctum', 'ability:super-admin']);
Route::post('data-system-config', [LOVController::class, 'store'])->middleware(['auth:sanctum', 'ability:super-admin']);
Route::put('data-system-config/{id}', [LOVController::class, 'update'])->middleware(['auth:sanctum', 'ability:super-admin']);
Route::post('data-system-config/{id}/toggle', [LOVController::class, 'toggle'])->middleware(['auth:sanctum', 'ability:super-admin']);
Route::delete('data-system-config/{id}', [LOVController::class, 'destroy'])->middleware(['auth:sanctum', 'ability:super-admin']);

//ROLE ACCESS
Route::apiResource('roles-access', RoleAccessController::class)->except(['store'])->middleware(['auth:sanctum', 'ability:super-admin']);
Route::post('roles-access/{role_slug}', [RoleAccessController::class, 'store'])->middleware(['auth:sanctum', 'ability:super-admin']);
Route::post('roles-access/{roles_access}/toggle', [RoleAccessController::class, 'toggle'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);

//EMPLOYEE
Route::post('employee/create', [EmployeeController::class, 'store'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('employee/update/{id}', [EmployeeController::class, 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('employee/view-paging', [EmployeeController::class, 'index'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('employee/view-detail/{id}', [EmployeeController::class, 'show'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('employee/toggle/{id}', [EmployeeController::class, 'toggle'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('/employee/download/{id}/{type}', [EmployeeController::class, 'download'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::delete('/employee/delete/{id}', [EmployeeController::class, 'destroy'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
//EMPLOYEE CERTIFICATION
Route::post('employee/create-certification', [EmployeeController::class, 'storeCertification'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('employee/update-certification/{id}', [EmployeeController::class, 'updateCertification'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('employee/store-certification', [EmployeeController::class, 'storeCertificationByFile'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('employee/view-paging-certification/{employee_id}', [EmployeeController::class, 'indexCertification'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('employee/view-detail-certification/{id}', [EmployeeController::class, 'showCertification'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('/employee/download-certification/{id}', [EmployeeController::class, 'downloadCertification'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::delete('/employee/delete-certification/{id}', [EmployeeController::class, 'deleteCertification'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
//EMPLOYEE DESIGNATION
Route::post('employee-designation/create', [EmployeeDesignationController::class, 'store'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::put('employee-designation/update/{id}', [EmployeeDesignationController::class, 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('employee-designation/view-paging', [EmployeeDesignationController::class, 'index'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('employee-designation/view-detail/{id}', [EmployeeDesignationController::class, 'show'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::delete('employee-designation/delete/{id}', [EmployeeDesignationController::class, 'destroy'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
//EMPLOYEE RESIGNATION
Route::post('employee-resignation/create', [EmployeeResignationController::class, 'store'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('employee-resignation/update/{id}', [EmployeeResignationController::class, 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('employee-resignation/view-paging', [EmployeeResignationController::class, 'index'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('employee-resignation/view-detail/{id}', [EmployeeResignationController::class, 'show'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('/employee-resignation/download-attachment/{id}', [EmployeeResignationController::class, 'downloadAttachment'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::delete('employee-resignation/delete/{id}', [EmployeeResignationController::class, 'destroy'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
//EMPLOYEE TERMINATION
Route::post('employee-termination/create', [EmployeeTerminationController::class, 'store'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('employee-termination/update/{id}', [EmployeeTerminationController::class, 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('employee-termination/view-paging', [EmployeeTerminationController::class, 'index'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('employee-termination/view-detail/{id}', [EmployeeTerminationController::class, 'show'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('/employee-termination/download-attachment/{id}', [EmployeeTerminationController::class, 'downloadAttachment'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::delete('employee-termination/delete/{id}', [EmployeeTerminationController::class, 'destroy'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
//EMPLOYEE SET LEAVE QUOTA 
Route::post('employee-leave/create', [EmployeeLeaveController::class, 'store'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::put('employee-leave/update/{id}', [EmployeeLeaveController::class, 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('employee-leave/view-paging', [EmployeeLeaveController::class, 'index'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('employee-leave/view-detail/{id}', [EmployeeLeaveController::class, 'show'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::delete('employee-leave/delete/{id}', [EmployeeLeaveController::class, 'destroy'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('employee-leave/view-by-employee/{employee_id}', [EmployeeLeaveController::class, 'showByEmployeeId'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
//EMPLOYEE SET REIMBURSEMENT QUOTA
Route::post('employee-reimbursement/create', [EmployeeReimbursementController::class, 'store'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::put('employee-reimbursement/update/{id}', [EmployeeReimbursementController::class, 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('employee-reimbursement/view-paging', [EmployeeReimbursementController::class, 'index'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('employee-reimbursement/view-detail/{id}', [EmployeeReimbursementController::class, 'show'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::delete('employee-reimbursement/delete/{id}', [EmployeeReimbursementController::class, 'destroy'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);

//ATTENDANCE
//IN,OUT
Route::post('attendance', [AttendanceController::class, 'attendance'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('attendance/{employee_id}', [AttendanceController::class, 'showAttendance'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
//TIMESHEET CORRECTION
Route::post('timesheet/create', [AttendanceController::class, 'store'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::put('timesheet/update/{id}', [AttendanceController::class, 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('timesheet/view-paging', [AttendanceController::class, 'index'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('timesheet/view-detail/{id}', [AttendanceController::class, 'show'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::delete('timesheet/delete/{id}', [AttendanceController::class, 'destroy'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
//LEAVE
Route::post('attendance-leave/create', [AttendanceLeaveController::class, 'store'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::put('attendance-leave/update/{id}', [AttendanceLeaveController::class, 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('attendance-leave/view-paging', [AttendanceLeaveController::class, 'index'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('attendance-leave/view-detail/{id}', [AttendanceLeaveController::class, 'show'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::delete('attendance-leave/delete/{id}', [AttendanceLeaveController::class, 'destroy'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
//OVERTIME
Route::post('attendance-overtime/create', [AttendanceOvertimeController::class, 'store'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::put('attendance-overtime/update/{id}', [AttendanceOvertimeController::class, 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('attendance-overtime/view-paging', [AttendanceOvertimeController::class, 'index'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('attendance-overtime/view-detail/{id}', [AttendanceOvertimeController::class, 'show'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::put('attendance-overtime/inactive/{id}', [AttendanceOvertimeController::class, 'toggle'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);

//RECRUITMENT
//RECRUITMENT JOBS
Route::post('recruitment-job/create', [RecruitmentJobsController::class, 'store'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::put('recruitment-job/update/{id}', [RecruitmentJobsController::class, 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('recruitment-job/view-paging', [RecruitmentJobsController::class, 'index'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('recruitment-job/view-detail/{id}', [RecruitmentJobsController::class, 'show'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::delete('recruitment-job/delete/{id}', [RecruitmentJobsController::class, 'destroy'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
//JOB APPLICATIONS
Route::post('job-application/create', [JobApplicationsController::class, 'store'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('job-application/update/{id}', [JobApplicationsController::class, 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('job-application/view-paging', [JobApplicationsController::class, 'index'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('job-application/view-detail/{id}', [JobApplicationsController::class, 'show'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::delete('job-application/delete/{id}', [JobApplicationsController::class, 'destroy'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('job-application/download-attachment/{id}', [JobApplicationsController::class, 'download'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
//INTERVIEW SCHEDULE
Route::post('interview-schedule/create', [RecruitmentJobsController::class, 'store'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::put('interview-schedule/update/{id}', [RecruitmentJobsController::class, 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('interview-schedule/view-paging', [RecruitmentJobsController::class, 'index'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('interview-schedule/view-detail/{id}', [RecruitmentJobsController::class, 'show'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::delete('interview-schedule/delete/{id}', [RecruitmentJobsController::class, 'destroy'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);

//MENU
Route::get('menu/lov', [MenuController::class, 'getMenuLOV'])->middleware('auth:sanctum');

//APPROVAL WORKFLOW CRITERIA
Route::get('approval-config-workflow/criteria', [ApprovalController::class, 'indexWorkflowCriteria'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('approval-config-workflow/criteria/{id}', [ApprovalController::class, 'showWorkflowCriteria'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('approval-config-workflow/criteria', [ApprovalController::class, 'storeWorkflowCriteria'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::put('approval-config-workflow/criteria/{id}', [ApprovalController::class, 'updateWorkflowCriteria'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('approval-config-workflow/criteria/{id}/toggle', [ApprovalController::class, 'toggleWorkflowCriteria'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::delete('approval-config-workflow/criteria/{id}', [ApprovalController::class, 'destroyWorkflowCriteria'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);

//APPROVAL WORKFLOW STEP
Route::get('approval-config-workflow/step', [ApprovalController::class, 'indexWorkflowStep'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('approval-config-workflow/step/{id}', [ApprovalController::class, 'showWorkflowStep'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('approval-config-workflow/step', [ApprovalController::class, 'storeWorkflowStep'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::put('approval-config-workflow/step/{id}', [ApprovalController::class, 'updateWorkflowStep'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('approval-config-workflow/step/{id}/toggle', [ApprovalController::class, 'toggleWorkflowStep'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::delete('approval-config-workflow/step/{id}', [ApprovalController::class, 'destroyWorkflowStep'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);

//APPROVAL WORKFLOW
Route::get('approval-config-workflow', [ApprovalController::class, 'indexWorkflow'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('approval-config-workflow/{id}', [ApprovalController::class, 'showWorkflow'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('approval-config-workflow', [ApprovalController::class, 'storeWorkflow'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::put('approval-config-workflow/{id}', [ApprovalController::class, 'updateWorkflow'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('approval-config-workflow/{id}/toggle', [ApprovalController::class, 'toggleWorkflow'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::delete('approval-config-workflow/{id}', [ApprovalController::class, 'destroyWorkflow'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);

//APPROVAL CONFIG
Route::get('approval-config', [ApprovalController::class, 'index'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('approval-config/{id}', [ApprovalController::class, 'show'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('approval-config/export', [ApprovalController::class, 'export'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('approval-config', [ApprovalController::class, 'store'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::put('approval-config/{id}', [ApprovalController::class, 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('approval-config/{id}/toggle', [ApprovalController::class, 'toggle'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::delete('approval-config/{id}', [ApprovalController::class, 'destroy'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);

//DEPARTMENT
Route::get('department', [DepartmentController::class, 'index'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('department/{id}', [DepartmentController::class, 'show'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('department', [DepartmentController::class, 'store'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::put('department/{id}', [DepartmentController::class, 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('department/{id}/toggle', [DepartmentController::class, 'toggle'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::delete('department/{id}', [DepartmentController::class, 'destroy'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);

//SECTION
Route::get('section', [SectionController::class, 'index'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('section/{id}', [SectionController::class, 'show'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('section', [SectionController::class, 'store'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::put('section/{id}', [SectionController::class, 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('section/{id}/toggle', [SectionController::class, 'toggle'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::delete('section/{id}', [SectionController::class, 'destroy'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);

//POSITION
Route::get('position', [PositionController::class, 'index'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('position/{id}', [PositionController::class, 'show'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('position', [PositionController::class, 'store'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::put('position/{id}', [PositionController::class, 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('position/{id}/toggle', [PositionController::class, 'toggle'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::delete('position/{id}', [PositionController::class, 'destroy'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);

//ORG STRUCTURE
Route::get('org-structure', [OrgStructureController::class, 'index'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('org-structure/chart', [OrgStructureController::class, 'showChart'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('org-structure/{id}', [OrgStructureController::class, 'show'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('org-structure', [OrgStructureController::class, 'store'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::put('org-structure/{id}', [OrgStructureController::class, 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('org-structure/{id}/toggle', [OrgStructureController::class, 'toggle'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::delete('org-structure/{id}', [OrgStructureController::class, 'destroy'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);

//APPRAISAL INDICATOR
Route::get('appraisal-indicator', [IndicatorController::class, 'index'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('appraisal-indicator/{id}', [IndicatorController::class, 'show'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('appraisal-indicator', [IndicatorController::class, 'store'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::put('appraisal-indicator/{id}', [IndicatorController::class, 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('appraisal-indicator/{id}/toggle', [IndicatorController::class, 'toggle'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::delete('appraisal-indicator/{id}', [IndicatorController::class, 'destroy'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);

//APPRAISAL PERIOD
Route::get('appraisal-period', [AppraisalPeriodController::class, 'index'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('appraisal-period/{id}', [AppraisalPeriodController::class, 'show'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('appraisal-period', [AppraisalPeriodController::class, 'store'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::put('appraisal-period/{id}', [AppraisalPeriodController::class, 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::delete('appraisal-period/{id}', [AppraisalPeriodController::class, 'destroy'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);

Route::get('appraisal-period-detail/{id}', [AppraisalPeriodController::class, 'showDetail'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('appraisal-period-detail/{appraisalPeriodId}', [AppraisalPeriodController::class, 'storeDetail'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::put('appraisal-period-detail/{id}', [AppraisalPeriodController::class, 'updateDetail'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::delete('appraisal-period-detail/{id}', [AppraisalPeriodController::class, 'destroyDetail'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);

//APPRAISAL SUBMISSION
Route::get('appraisal-submission', [AppraisalSubmissionController::class, 'index'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('appraisal-submission/{id}', [AppraisalSubmissionController::class, 'show'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::post('appraisal-submission', [AppraisalSubmissionController::class, 'store'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::put('appraisal-submission/{id}', [AppraisalSubmissionController::class, 'update'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::delete('appraisal-submission/{id}', [AppraisalSubmissionController::class, 'destroy'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);

Route::post('tes/create', [TesController::class, 'store'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);
Route::get('tes/download/{id}', [TesController::class, 'downloadAttachment'])->middleware(['auth:sanctum', 'ability:admin,super-admin,user']);