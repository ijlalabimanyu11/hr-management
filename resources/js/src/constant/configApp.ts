import axios from 'axios';

export const apiKey = import.meta.env.VITE_API_URL;

export const fetchCsrfToken = async () => {
    await axios
        .get(`${import.meta.env.VITE_API_URL_2}/sanctum/csrf-cookie`, {
            withCredentials: true,
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded',
            },
        })
        .then((response) => {})
        .catch((error) => {
            console.error('Error fetching CSRF cookie:', error);
        });
};
