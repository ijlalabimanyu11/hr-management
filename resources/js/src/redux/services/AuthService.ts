import axios, { AxiosResponse, AxiosError } from 'axios';
import { tokenHeader } from '../../utils/tokenHeader';
import { apiKey } from '../../constant/configApp';

interface AuthHttpService {
    login: (url: string, body: any) => Promise<any>;
    logout: (url: string) => Promise<any>;
    createData: (url: string, body: any) => Promise<any>;
    getAll: (url: string) => Promise<any>;
}

const login = async (url: string, body: any): Promise<any> => {
    const response = await axios.post(apiKey + url, body, {
        headers: { accept: 'application/json' },
        withCredentials: true,
    });
    return response.data;
};

const createData = async (url: string, body: any): Promise<any> => {
    try {
        const response = await axios.post(apiKey + url, body, {
            headers: { accept: 'application/json' },
            withCredentials: true,
        });
        return response.data;
    } catch (error: any) {
        throw error;
    }
};
const logout = async (url: string): Promise<any> => {
    try {
        const response: AxiosResponse = await axios.post(apiKey + url, {
            headers: tokenHeader(),
        });
        localStorage.clear();
        window.sessionStorage.clear();
        return response.data;
    } catch (error) {
        if (axios.isAxiosError(error)) {
            throw error as AxiosError;
        } else {
            throw error;
        }
    }
};

const getAll = async (url: string): Promise<any> => {
    const response = await axios.get(apiKey + url, {
        headers: tokenHeader(),
        withCredentials: true,
    });
    return response.data;
};

const authHttpService: AuthHttpService = {
    login,
    logout,
    createData,
    getAll
};

export default authHttpService;
