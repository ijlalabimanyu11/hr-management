import axios, { AxiosResponse, AxiosError } from 'axios';
import { tokenHeader } from '../../utils/tokenHeader';
import FileSaver from 'file-saver';
import { apiKey } from '../../constant/configApp';

interface EmployeeHttpService {
    getAll: (url: string) => Promise<any>;
    getPagination: (url: string) => Promise<any>;
    getDetail: (url: string) => Promise<any>;
    createData: (url: string, body: any) => Promise<any>;
    updateData: (url: string, data: any) => Promise<any>;
    deleteData: (url: string) => Promise<any>;
    approvalData: (url: string, body: any) => Promise<any>;
    downloadData: (url: string) => Promise<any>;
}

const getAll = async (url: string): Promise<any> => {
    try {
        const response: AxiosResponse = await axios.get(apiKey + url, {
            headers: tokenHeader(),
        });
        return response.data;
    } catch (error) {
        if (axios.isAxiosError(error)) {
            throw error as AxiosError;
        } else {
            throw error;
        }
    }
};

const getPagination = async (url: string): Promise<any> => {
    try {
        const response: AxiosResponse<any> = await axios.get(apiKey + url, {
            headers: tokenHeader(),
        });
        return response.data;
    } catch (error) {
        if (axios.isAxiosError(error)) {
            throw error as AxiosError;
        } else {
            throw error;
        }
    }
};

const getDetail = async (url: string): Promise<any> => {
    try {
        const response: AxiosResponse = await axios.get(apiKey + url, {
            headers: tokenHeader(),
        });
        return response.data;
    } catch (error) {
        if (axios.isAxiosError(error)) {
            throw error as AxiosError;
        } else {
            throw error;
        }
    }
};

const createData = async (url: string, body: any): Promise<any> => {
    try {
        const response: AxiosResponse = await axios.post(apiKey + url, body, {
            headers: tokenHeader(),
        });
        return response.data;
    } catch (error) {
        if (axios.isAxiosError(error)) {
            throw error as AxiosError;
        } else {
            throw error;
        }
    }
};

const updateData = async (url: string, data: any): Promise<any> => {
    try {
        const response: AxiosResponse = await axios.post(apiKey + url, data, {
            headers: tokenHeader(),
        });
        return response.data;
    } catch (error) {
        if (axios.isAxiosError(error)) {
            throw error as AxiosError;
        } else {
            throw error;
        }
    }
};

const deleteData = async (url: string): Promise<any> => {
    try {
        const response: AxiosResponse = await axios.delete(apiKey + url, {
            headers: tokenHeader(),
        });
        return response.data;
    } catch (error) {
        if (axios.isAxiosError(error)) {
            throw error as AxiosError;
        } else {
            throw error;
        }
    }
};

const approvalData = async (url: string, body: any): Promise<any> => {
    try {
        const response: AxiosResponse = await axios.post(apiKey + url, body, {
            headers: tokenHeader(),
        });
        return response.data;
    } catch (error) {
        if (axios.isAxiosError(error)) {
            throw error as AxiosError;
        } else {
            throw error;
        }
    }
};

const downloadData = async (url: string): Promise<void> => {
    try {
        const response: AxiosResponse<Blob> = await axios.get(apiKey + url, {
            headers: tokenHeader(),
            responseType: 'blob',
        });

        const contentDisposition = response.headers['content-disposition'];
        const filename = contentDisposition
            ? contentDisposition
                  .split(';')
                  .find((n) => n.includes('filename='))
                  ?.replace('filename=', '')
                  .trim() || 'download'
            : 'download';

        const blob = response.data;
        // Download the file
        FileSaver.saveAs(blob, filename);
    } catch (error) {
        if (axios.isAxiosError(error)) {
            // Handle Axios error
            throw error as AxiosError;
        } else {
            // Handle non-Axios error
            throw error;
        }
    }
};

const employeeHttpService: EmployeeHttpService = {
    getAll,
    getPagination,
    getDetail,
    createData,
    updateData,
    deleteData,
    approvalData,
    downloadData,
};

export default employeeHttpService;
