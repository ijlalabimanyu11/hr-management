import axios, { AxiosResponse, AxiosError } from 'axios';
import { apiKey } from '../../constant/configApp';
import { tokenHeader } from '../../utils/tokenHeader';
import FileSaver from 'file-saver';


interface SystemSetupHttpService {
    getAll: (url: string) => Promise<any>;
    getPagination: (url: string) => Promise<any>;
    getDetail: (url: string) => Promise<any>;
    createData: (url: string, body: any) => Promise<any>;
    updateData: (url: string, body: any) => Promise<any>;
    activeOrInactiveData: (url: string) => Promise<any>;
    deleteData: (url: string) => Promise<any>;
    approvalData: (url: string, body: any) => Promise<any>;
    downloadData: (url: string) => Promise<any>;
}

const getAll = async (url: string): Promise<any> => {
    const response = await axios.get(apiKey + url, {
        headers: tokenHeader(),
        withCredentials: true,
    });
    return response.data;
};

const getPagination = async (url: string): Promise<any> => {
    const response = await axios.get(apiKey + url, {
        headers: tokenHeader(),
        withCredentials: true,
    });
    return response.data;
};

const getDetail = async (url: string): Promise<any> => {
    const response = await axios.get(apiKey + url, {
        headers: tokenHeader(),
        withCredentials: true,
    });
    return response.data;
};

const createData = async (url: string, body: any): Promise<any> => {
    const response = await axios.post(apiKey + url, body, {
        headers: tokenHeader(),
        withCredentials: true,
    });
    return response.data;
};

const updateData = async (url: string, body: any): Promise<any> => {
    const response = await axios.put(apiKey + url, body, {
        headers: tokenHeader(),
        withCredentials: true,
    });
    return response.data;
};

const activeOrInactiveData = async (url: string): Promise<any> => {
    const response = await axios.post(apiKey + url, {}, {
        headers: tokenHeader(),
        withCredentials: true,
    });
    return response.data;
};

const deleteData = async (url: string): Promise<any> => {
    const response = await axios.delete(apiKey + url, {
        headers: tokenHeader(),
        withCredentials: true,
    });
    return response.data;
};

const approvalData = async (url: string, body: any): Promise<any> => {
    const response = await axios.post(apiKey + url, body, {
        headers: tokenHeader(),
        withCredentials: true,
    });
    return response.data;
};

const downloadData = async (url: string): Promise<void> => {
    const response: AxiosResponse<Blob> = await axios.get(apiKey + url, {
        headers: tokenHeader(),
        responseType: 'blob',
    });

    const contentDisposition = response.headers['content-disposition'];
    const filename = contentDisposition
        ? contentDisposition
              .split(';')
              .find((n: any) => n.includes('filename='))
              ?.replace('filename=', '')
              .trim() || 'download'
        : 'download';

    const blob = response.data;
    // Download the file
    FileSaver.saveAs(blob, filename);
};

const systemSetupHttpService: SystemSetupHttpService = {
    getAll,
    getPagination,
    getDetail,
    createData,
    updateData,
    deleteData,
    approvalData,
    downloadData,
    activeOrInactiveData
}

export default systemSetupHttpService
