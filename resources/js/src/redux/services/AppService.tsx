import axios, { AxiosResponse, AxiosError } from 'axios';
import { apiKey } from '../../constant/configApp';
import { tokenHeader } from '../../utils/tokenHeader';
import FileSaver from 'file-saver';
import { RcFile } from 'antd/es/upload/interface';

interface AppHttpService {
    getAll: (url: string) => Promise<any>;
    getPagination: (url: string) => Promise<any>;
    getDetail: (url: string) => Promise<any>;
    createData: (url: string, body: any) => Promise<any>;
    createDataMulti: (url: string, body: any) => Promise<any>;
    updateData: (url: string, body: any) => Promise<any>;
    activeOrInactiveData: (url: string) => Promise<any>;
    deleteData: (url: string) => Promise<any>;
    approvalData: (url: string, body: any) => Promise<any>;
    downloadData: (url: string, filename: string) => Promise<any>;
    getFileData: (url: string, filename: string) => Promise<any>;
    getFileDataPicture: (url: string, filename: string) => Promise<any>;
}

const getAll = async (url: string): Promise<any> => {
    const response = await axios.get(apiKey + url, {
        headers: tokenHeader(),
        withCredentials: true,
    });
    return response.data;
};

const getPagination = async (url: string): Promise<any> => {
    const response = await axios.get(apiKey + url, {
        headers: tokenHeader(),
        withCredentials: true,
    });
    return response.data;
};

const getDetail = async (url: string): Promise<any> => {
    const response = await axios.get(apiKey + url, {
        headers: tokenHeader(),
        withCredentials: true,
    });
    return response.data;
};

const createData = async (url: string, body: any): Promise<any> => {
    const response = await axios.post(apiKey + url, body, {
        headers: tokenHeader(),
        withCredentials: true,
    });
    return response.data;
};

const createDataMulti = async (url: string, body: any): Promise<any> => {
    const response = await axios.post(apiKey + url, body, {
        headers: {
            ...tokenHeader(),
            'Content-Type': 'multipart/form-data',
        },
        withCredentials: true,
    });
    return response.data;
};

const updateData = async (url: string, body: any): Promise<any> => {
    const response = await axios.put(apiKey + url, body, {
        headers: tokenHeader(),
        withCredentials: true,
    });
    return response.data;
};

const activeOrInactiveData = async (url: string): Promise<any> => {
    const response = await axios.post(
        apiKey + url,
        {},
        {
            headers: tokenHeader(),
            withCredentials: true,
        }
    );
    return response.data;
};

const deleteData = async (url: string): Promise<any> => {
    const response = await axios.delete(apiKey + url, {
        headers: tokenHeader(),
        withCredentials: true,
    });
    return response.data;
};

const approvalData = async (url: string, body: any): Promise<any> => {
    const response = await axios.post(apiKey + url, body, {
        headers: tokenHeader(),
        withCredentials: true,
    });
    return response.data;
};

const downloadData = async (url: string, filename: string): Promise<void> => {
    const response: AxiosResponse<Blob> = await axios.get(apiKey + url, {
        headers: tokenHeader(),
        responseType: 'blob',
    });

    // const contentDisposition = response.headers['Content-Disposition'];
    // const filename = contentDisposition
    //     ? contentDisposition
    //           .split(';')
    //           .find((n: any) => n.includes('filename='))
    //           ?.replace('filename=', '')
    //           .trim() || 'download'
    //     : 'download';

    const blob = response.data;

    // Download the file
    FileSaver.saveAs(blob, filename);
};

const getFileData = async (url: string, filename: string): Promise<any> => {
    const response = await axios.get(apiKey + url, {
        headers: tokenHeader(),
        responseType: 'blob',
    });

    const blob = response.data;

    // Convert Blob to File
    const file = new File([blob], filename, { type: blob.type });

    return file;
};

const getFileDataPicture = async (url: string, filename: string): Promise<any> => {
    const response = await axios.get(apiKey + url, {
        headers: tokenHeader(),
        responseType: 'blob',
    });

    const blob = response.data;

    // Convert Blob to File
    const file = new File([blob], filename, { type: blob.type });

    const data = {
        uid: "1",
        status: 'done',
        url: URL.createObjectURL(file),
        name: filename,
    }

    return data;
};

const appHttpService: AppHttpService = {
    getAll,
    getPagination,
    getDetail,
    createData,
    createDataMulti,
    updateData,
    deleteData,
    approvalData,
    downloadData,
    activeOrInactiveData,
    getFileData,
    getFileDataPicture,
};

export default appHttpService;
