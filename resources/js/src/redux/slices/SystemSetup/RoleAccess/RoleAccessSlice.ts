import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import systemSetupHttpService from '../../../services/SystemSetupService';
import { notificationSuccess } from '../../../../components/Notification';

export interface roleAccessBody {
    menu_key: string;
    privilages_json: string[];
}

interface RoleAccessState {
    loading: boolean;
    message: string;
    data: any | null;
    dataDetail: any | null;
    dataRoleAccess: any | null;
}

const initialState: RoleAccessState = {
    loading: false,
    message: '',
    data: null,
    dataDetail: null,
    dataRoleAccess: null,
};

export const createRoleAccess = createAsyncThunk<any, { body: roleAccessBody; role: any }, { rejectValue: string }>('system-setup/role-access/create', async ({ body, role }, { rejectWithValue }) => {
    try {
        const url = `/roles-access/${role}`;
        const response = await systemSetupHttpService.createData(url, body);
        const data = response.data;
        notificationSuccess('success', 'Role Access Successfully Created', '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const updateRoleAccess = createAsyncThunk<any, { body: roleAccessBody; id: any }, { rejectValue: string }>('system-setup/role-access/update', async ({ body, id }, { rejectWithValue }) => {
    try {
        const url = `/roles-access/${id}`;
        const response = await systemSetupHttpService.updateData(url, body);
        const data = response.data;
        notificationSuccess('success', 'Role Access Successfully Updated', '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getRoleAccessPaginate = createAsyncThunk<any, { type: any; search: any; page: any; pageSize: any; sort: any }, { rejectValue: string }>(
    'system-setup/role-access/view',
    async ({ type, search, page, pageSize, sort }, { rejectWithValue }) => {
        try {
            const searchParams = search === undefined ? '' : search;
            const sortParams = sort === undefined || sort === '' ? 'created_date~desc' : sort;
            const url = `/roles-access?search=role_slug~${type}%26${searchParams}&page=${page}&size=${pageSize}&sort=${sortParams}`;
            const response = await systemSetupHttpService.getPagination(url);
            const data = response.data;
            return data;
        } catch (e: any) {
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const activeOrInactiveRoleAccess = createAsyncThunk<any, { id: any; status: any }, { rejectValue: string }>('system-setup/role-access/toggle', async ({ id, status }, { rejectWithValue }) => {
    try {
        const url = `/roles-access/${id}/toggle`;
        const response = await systemSetupHttpService.activeOrInactiveData(url);
        const data = response.data;
        notificationSuccess('success', `Role Access Successfully ${status === 'ACTIVE' ? 'Deactivate' : 'Activate'}`, '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const deleteRoleAccess = createAsyncThunk<any, { id: any }, { rejectValue: string }>('system-setup/role-access/delete', async ({ id }, { rejectWithValue }) => {
    try {
        const url = `/roles-access/${id}`;
        const response = await systemSetupHttpService.deleteData(url);
        const data = response.data;
        notificationSuccess('success', `Role Access Successfully Deleted`, '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getDetailRoleAccess = createAsyncThunk<any, { id: any }, { rejectValue: string }>('system-setup/role-access/detail', async (id, { rejectWithValue }) => {
    try {
        const url = `/roles-access/${id}`;
        const response = await systemSetupHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.message as string);
    }
});

const roleAccessSlice = createSlice({
    name: 'roleAccess',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            // Create Role Access
            .addCase(createRoleAccess.pending, (state) => {
                state.loading = true;
            })
            .addCase(createRoleAccess.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataRoleAccess = action.payload;
            })
            .addCase(createRoleAccess.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataRoleAccess = action.payload;
            })

            // Update Role Access
            .addCase(updateRoleAccess.pending, (state) => {
                state.loading = true;
            })
            .addCase(updateRoleAccess.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataRoleAccess = action.payload;
            })
            .addCase(updateRoleAccess.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataRoleAccess = action.payload;
            })

            // View Role Access
            .addCase(getRoleAccessPaginate.pending, (state) => {
                state.loading = true;
            })
            .addCase(getRoleAccessPaginate.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })
            .addCase(getRoleAccessPaginate.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })

            // Active/Inactive Role Access
            .addCase(activeOrInactiveRoleAccess.pending, (state) => {
                state.loading = true;
            })
            .addCase(activeOrInactiveRoleAccess.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataRoleAccess = action.payload;
            })
            .addCase(activeOrInactiveRoleAccess.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataRoleAccess = action.payload;
            })

            // Delete Role Access
            .addCase(deleteRoleAccess.pending, (state) => {
                state.loading = true;
            })
            .addCase(deleteRoleAccess.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataRoleAccess = action.payload;
            })
            .addCase(deleteRoleAccess.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataRoleAccess = action.payload;
            })

            // Get Detail Role Access
            .addCase(getDetailRoleAccess.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDetailRoleAccess.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            })
            .addCase(getDetailRoleAccess.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            });
    },
});

export default roleAccessSlice.reducer;
