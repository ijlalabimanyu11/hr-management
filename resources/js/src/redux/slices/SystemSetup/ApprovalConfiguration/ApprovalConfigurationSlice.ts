import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import systemSetupHttpService from '../../../services/SystemSetupService';
import { notificationSuccess, notificationSuccessWithBack } from '../../../../components/Notification';
import { useNavigate } from 'react-router-dom';

export interface defaultWorkflowStepBody {
    step: string;
    type: string;
    lineManager: number;
    position: number;
    section: number;
    department: number;
}

export interface approvalConfigurationHeaderBody {
    code?: string;
    description?: string;
    defaultWorkflowStep?: defaultWorkflowStepBody[];
}

interface approvalConfigurationState {
    loading: boolean;
    message: string;
    dataPagingHeader: any | null;
    dataPagingWorkflowCriteria: any | null;
    dataPagingWorkflowStep: any | null;
    dataDetailHeader: any | null;
    dataDetailWorkflow: any | null;
    dataDetailWorkflowCriteria: any | null;
    dataDetailWorkflowStep: any | null;
    dataApprovalConfigurationDetail: any | null;
    dataApprovalConfigurationHeader: any | null;
}

const initialState: approvalConfigurationState = {
    loading: false,
    message: '',
    dataPagingHeader: null,
    dataPagingWorkflowCriteria: null,
    dataPagingWorkflowStep: null,
    dataDetailHeader: null,
    dataDetailWorkflow: null,
    dataDetailWorkflowCriteria: null,
    dataDetailWorkflowStep: null,
    dataApprovalConfigurationDetail: null,
    dataApprovalConfigurationHeader: null,
};

export const createApprovalConfigurationHeader = createAsyncThunk<any, { body: approvalConfigurationHeaderBody, navigate: any }, { rejectValue: string }>(
    'system-setup/approval-configuration-header/create',
    async ({ body, navigate }, { rejectWithValue }) => {
        try {
            const url = `/approval-config`;
            const response = await systemSetupHttpService.createData(url, body);
            const data = response.data;
            notificationSuccessWithBack('success', 'Approval Configuration Successfully Created', '', navigate);
            return data;
        } catch (e: any) {
            notificationSuccess('error', e.response.data.message as string, '');
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const updateApprovalConfigurationHeader = createAsyncThunk<any, { body: approvalConfigurationHeaderBody; id: any, navigate: any }, { rejectValue: string }>(
    'system-setup/approval-configuration-header/update',
    async ({ body, id, navigate }, { rejectWithValue }) => {
        try {
            const url = `/approval-config/${id}`;
            const response = await systemSetupHttpService.updateData(url, body);
            const data = response.data;
            notificationSuccessWithBack('success', 'Approval Configuration Successfully Updated', '', navigate);
            return data;
        } catch (e: any) {
            notificationSuccess('error', e.response.data.message as string, '');
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const getApprovalConfigurationHeaderPaginate = createAsyncThunk<any, { search: any; page: any; pageSize: any; sort: any }, { rejectValue: string }>(
    'system-setup/approval-configuration-header/view',
    async ({ search, page, pageSize, sort }, { rejectWithValue }) => {
        try {
            const searchParams = search === undefined ? '' : search;
            const sortParams = sort === undefined || sort === '' ? 'created_date~desc' : sort;
            const url = `/approval-config?search=${searchParams}&page=${page}&size=${pageSize}&sort=${sortParams}`;
            const response = await systemSetupHttpService.getPagination(url);
            const data = response.data;
            return data;
        } catch (e: any) {
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const activeOrInactiveApprovalConfigurationHeader = createAsyncThunk<any, { id: any; status: any }, { rejectValue: string }>(
    'system-setup/approval-configuration-header/toggle',
    async ({ id, status }, { rejectWithValue }) => {
        try {
            const url = `/approval-config/${id}/toggle`;
            const response = await systemSetupHttpService.activeOrInactiveData(url);
            const data = response.data;
            notificationSuccess('success', `Approval Configuration Successfully ${status === 'ACTIVE' ? 'Deactivate' : 'Activate'}`, '');
            return data;
        } catch (e: any) {
            notificationSuccess('error', e.response.data.message as string, '');
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const getDetailApprovalConfigurationHeader = createAsyncThunk<any, { id: any }, { rejectValue: string }>(
    'system-setup/approval-configuration-header/detail',
    async (id, { rejectWithValue }) => {
        try {
            const url = `/approval-config/${id}`;
            const response = await systemSetupHttpService.getAll(url);
            const data = response.data;
            return data;
        } catch (e: any) {
            return rejectWithValue(e.response.message as string);
        }
    }
);

export const deleteApprovalConfiguration = createAsyncThunk<any, { id: any }, { rejectValue: string }>('system-setup/approval-configuration-header/delete', async ({ id }, { rejectWithValue }) => {
    try {
        const url = `/approval-config/${id}`;
        const response = await systemSetupHttpService.deleteData(url);
        const data = response.data;
        notificationSuccess('success', `Approval Configuration Successfully Deleted`, '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

const approvalConfigurationSlice = createSlice({
    name: 'approvalConfiguration',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            // Create Approval Configuration Header
            .addCase(createApprovalConfigurationHeader.pending, (state) => {
                state.loading = true;
            })
            .addCase(createApprovalConfigurationHeader.fulfilled, (state, action) => {
                state.loading = false;
                state.dataApprovalConfigurationHeader = action.payload;
            })
            .addCase(createApprovalConfigurationHeader.rejected, (state, action) => {
                state.loading = false;
                state.dataApprovalConfigurationHeader = action.payload;
            })

            // Update Approval Configuration Header
            .addCase(updateApprovalConfigurationHeader.pending, (state) => {
                state.loading = true;
            })
            .addCase(updateApprovalConfigurationHeader.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataApprovalConfigurationHeader = action.payload;
            })
            .addCase(updateApprovalConfigurationHeader.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataApprovalConfigurationHeader = action.payload;
            })

            // View Approval Configuration Header
            .addCase(getApprovalConfigurationHeaderPaginate.pending, (state) => {
                state.loading = true;
            })
            .addCase(getApprovalConfigurationHeaderPaginate.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataPagingHeader = action.payload;
            })
            .addCase(getApprovalConfigurationHeaderPaginate.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataPagingHeader = action.payload;
            })

            // Get Detail Approval Configuration Header
            .addCase(getDetailApprovalConfigurationHeader.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDetailApprovalConfigurationHeader.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetailHeader = action.payload;
            })
            .addCase(getDetailApprovalConfigurationHeader.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetailHeader = action.payload;
            })

            // Delete Approval Configuration
            .addCase(deleteApprovalConfiguration.pending, (state) => {
                state.loading = true;
            })
            .addCase(deleteApprovalConfiguration.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataPagingHeader = action.payload;
            })
            .addCase(deleteApprovalConfiguration.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataPagingHeader = action.payload;
            });
    },
});

export default approvalConfigurationSlice.reducer;
