import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import systemSetupHttpService from '../../../services/SystemSetupService';
import { notificationSuccess } from '../../../../components/Notification';

export interface departmentBody {
    departmentCode: string;
    departmentName: string;
    departmentHead: number;
    description: string;
}

interface departmentState {
    loading: boolean;
    message: string;
    data: any | null;
    dataDetail: any | null;
    dataDepartment: any | null;
}

const initialState: departmentState = {
    loading: false,
    message: '',
    data: null,
    dataDetail: null,
    dataDepartment: null,
};

export const createDepartment = createAsyncThunk<any, { body: departmentBody }, { rejectValue: string }>('system-setup/department/create', async ({ body }, { rejectWithValue }) => {
    try {
        const url = `/department`;
        const response = await systemSetupHttpService.createData(url, body);
        const data = response.data;
        notificationSuccess('success', 'Department Successfully Created', '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const updateDepartment = createAsyncThunk<any, { body: departmentBody; id: any }, { rejectValue: string }>('system-setup/department/update', async ({ body, id }, { rejectWithValue }) => {
    try {
        const url = `/department/${id}`;
        const response = await systemSetupHttpService.updateData(url, body);
        const data = response.data;
        notificationSuccess('success', 'Department Successfully Updated', '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getDepartmentPaginate = createAsyncThunk<any, { search: any; page: any; pageSize: any; sort: any }, { rejectValue: string }>(
    'system-setup/department/view',
    async ({ search, page, pageSize, sort }, { rejectWithValue }) => {
        try {
            const searchParams = search === undefined ? '' : search;
            const sortParams = sort === undefined || sort === '' ? 'created_date~desc' : sort;
            const url = `/department?search=${searchParams}&page=${page}&size=${pageSize}&sort=${sortParams}`;
            const response = await systemSetupHttpService.getPagination(url);
            const data = response.data;
            return data;
        } catch (e: any) {
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const activeOrInactiveDepartment = createAsyncThunk<any, { id: any; status: any }, { rejectValue: string }>('system-setup/department/toggle', async ({ id, status }, { rejectWithValue }) => {
    try {
        const url = `/department/${id}/toggle`;
        const response = await systemSetupHttpService.activeOrInactiveData(url);
        const data = response.data;
        notificationSuccess('success', `Department Successfully ${status === 'ACTIVE' ? 'Deactivate' : 'Activate'}`, '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const deleteDepartment = createAsyncThunk<any, { id: any }, { rejectValue: string }>('system-setup/department/delete', async ({ id }, { rejectWithValue }) => {
    try {
        const url = `/department/${id}`;
        const response = await systemSetupHttpService.deleteData(url);
        const data = response.data;
        notificationSuccess('success', `Department Successfully Deleted`, '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getDetailDepartment = createAsyncThunk<any, { id: any }, { rejectValue: string }>('system-setup/department/detail', async (id, { rejectWithValue }) => {
    try {
        const url = `/department/${id}`;
        const response = await systemSetupHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.message as string);
    }
});

const departmentSlice = createSlice({
    name: 'department',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            // Create Department
            .addCase(createDepartment.pending, (state) => {
                state.loading = true;
            })
            .addCase(createDepartment.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDepartment = action.payload;
            })
            .addCase(createDepartment.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDepartment = action.payload;
            })

            // Update Department
            .addCase(updateDepartment.pending, (state) => {
                state.loading = true;
            })
            .addCase(updateDepartment.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDepartment = action.payload;
            })
            .addCase(updateDepartment.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDepartment = action.payload;
            })

            // View Department
            .addCase(getDepartmentPaginate.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDepartmentPaginate.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })
            .addCase(getDepartmentPaginate.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })

            // Delete Department
            .addCase(deleteDepartment.pending, (state) => {
                state.loading = true;
            })
            .addCase(deleteDepartment.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDepartment = action.payload;
            })
            .addCase(deleteDepartment.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDepartment = action.payload;
            })

            // Get Detail Department
            .addCase(getDetailDepartment.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDetailDepartment.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            })
            .addCase(getDetailDepartment.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            });
    },
});

export default departmentSlice.reducer;
