import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import systemSetupHttpService from '../../../services/SystemSetupService';
import { notificationSuccess } from '../../../../components/Notification';

export interface dataConfigurationBody {
    name_slug: string;
    key: string;
    value: string;
    is_props: boolean;
}

interface DataConfigurationState {
    loading: boolean;
    message: string;
    dataHeader: any | null;
    dataPaging: any | null;
    dataDetail: any | null;
    dataConfig: any | null;
}

const initialState: DataConfigurationState = {
    loading: false,
    message: '',
    dataHeader: null,
    dataPaging: null,
    dataDetail: null,
    dataConfig: null,
};

export const getDataConfigurationHeader = createAsyncThunk<any, void, { rejectValue: string }>('system-setup/data-configuration/header', async (_, { rejectWithValue }) => {
    try {
        const url = '/data-system-config';
        const response = await systemSetupHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getDataConfigurationPaginate = createAsyncThunk<any, { type: any; search: any; page: any; pageSize: any; sort: any }, { rejectValue: string }>(
    'system-setup/data-configuration/view',
    async ({ type, search, page, pageSize, sort }, { rejectWithValue }) => {
        try {
            const searchParams = search === undefined ? '' : search;
            const sortParams = sort === undefined || sort === '' ? 'created_date~desc' : sort;
            const url = `/data-system-config/detail?size=${pageSize}&search=name_slug~${type}%26${searchParams}&page=${page}&sort=${sortParams}`;
            const response = await systemSetupHttpService.getPagination(url);
            const data = response.data;
            return data;
        } catch (e: any) {
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const createDataConfiguration = createAsyncThunk<any, { body: dataConfigurationBody }, { rejectValue: string }>(
    'system-setup/data-configuration/create',
    async ({ body }, { rejectWithValue }) => {
        try {
            const url = `/data-system-config`;
            const response = await systemSetupHttpService.createData(url, body);
            const data = response.data;
            notificationSuccess('success', 'Data Configuration Successfully Created', '');
            return data;
        } catch (e: any) {
            notificationSuccess('error', e.response.data.message as string, '');
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const updateDataConfiguration = createAsyncThunk<any, { body: dataConfigurationBody; id: any }, { rejectValue: string }>(
    'system-setup/data-configuration/update',
    async ({ body, id }, { rejectWithValue }) => {
        try {
            const url = `/data-system-config/${id}`;
            const response = await systemSetupHttpService.updateData(url, body);
            const data = response.data;
            notificationSuccess('success', 'Data Configuration Successfully Updated', '');
            return data;
        } catch (e: any) {
            notificationSuccess('error', e.response.data.message as string, '');
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const getDetailDataConfiguration = createAsyncThunk<any, { id: any }, { rejectValue: string }>('system-setup/data-configuration/detail', async (id, { rejectWithValue }) => {
    try {
        const url = `/data-system-config/${id}`;
        const response = await systemSetupHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.message as string);
    }
});

export const activeOrInactiveDataConfiguration = createAsyncThunk<any, { id: any; status: any }, { rejectValue: string }>(
    'system-setup/data-configuration/toggle',
    async ({ id, status }, { rejectWithValue }) => {
        try {
            const url = `/data-system-config/${id}/toggle`;
            const response = await systemSetupHttpService.activeOrInactiveData(url);
            const data = response.data;
            notificationSuccess('success', `Data Configuration Successfully ${status === 'ACTIVE' ? 'Deactivate' : 'Activate'}`, '');
            return data;
        } catch (e: any) {
            notificationSuccess('error', e.response.data.message as string, '');
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const deleteDataConfiguration = createAsyncThunk<any, { id: any }, { rejectValue: string }>('system-setup/data-configuration/delete', async ({ id }, { rejectWithValue }) => {
    try {
        const url = `/data-system-config/${id}`;
        const response = await systemSetupHttpService.deleteData(url);
        const data = response.data;
        notificationSuccess('success', `Data Configuration Successfully Deleted`, '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

const dataConfigurationSlice = createSlice({
    name: 'dataConfiguration',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            // Get Data Header
            .addCase(getDataConfigurationHeader.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDataConfigurationHeader.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataHeader = action.payload;
            })
            .addCase(getDataConfigurationHeader.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataHeader = action.payload;
            })

            // View Data Configuration
            .addCase(getDataConfigurationPaginate.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDataConfigurationPaginate.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataPaging = action.payload;
            })
            .addCase(getDataConfigurationPaginate.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataPaging = action.payload;
            })

            // Get Detail Data Configuration
            .addCase(getDetailDataConfiguration.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDetailDataConfiguration.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            })
            .addCase(getDetailDataConfiguration.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            })

            // Create Data Configuration
            .addCase(createDataConfiguration.pending, (state) => {
                state.loading = true;
            })
            .addCase(createDataConfiguration.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataConfig = action.payload;
            })
            .addCase(createDataConfiguration.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataConfig = action.payload;
            })

            // Update Data Configuration
            .addCase(updateDataConfiguration.pending, (state) => {
                state.loading = true;
            })
            .addCase(updateDataConfiguration.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataConfig = action.payload;
            })
            .addCase(updateDataConfiguration.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataConfig = action.payload;
            })

            // Active/Inactive Data Configuration
            .addCase(activeOrInactiveDataConfiguration.pending, (state) => {
                state.loading = true;
            })
            .addCase(activeOrInactiveDataConfiguration.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataConfig = action.payload;
            })
            .addCase(activeOrInactiveDataConfiguration.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataConfig = action.payload;
            })

            // Delete Data Configuration
            .addCase(deleteDataConfiguration.pending, (state) => {
                state.loading = true;
            })
            .addCase(deleteDataConfiguration.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataConfig = action.payload;
            })
            .addCase(deleteDataConfiguration.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataConfig = action.payload;
            });
    },
});

export default dataConfigurationSlice.reducer;
