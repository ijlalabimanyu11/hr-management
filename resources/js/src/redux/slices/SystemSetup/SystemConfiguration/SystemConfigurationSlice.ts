import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import systemSetupHttpService from '../../../services/SystemSetupService';
import { notificationSuccess } from '../../../../components/Notification';

export interface systemConfigurationBody {
    name_slug: string;
    key: string;
    value: string;
    is_props: boolean;
}

interface SystemConfigurationState {
    loading: boolean;
    message: string;
    dataHeader: any | null;
    dataPaging: any | null;
    dataDetail: any | null;
    dataSystem: any | null;
}

const initialState: SystemConfigurationState = {
    loading: false,
    message: '',
    dataHeader: null,
    dataPaging: null,
    dataDetail: null,
    dataSystem: null,
};

export const getSystemConfigurationHeader = createAsyncThunk<any, void, { rejectValue: string }>('system-setup/system-configuration/header', async (_, { rejectWithValue }) => {
    try {
        const url = '/data-system-config';
        const response = await systemSetupHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getSystemConfigurationPaginate = createAsyncThunk<any, { type: any; search: any; page: any; pageSize: any; sort: any }, { rejectValue: string }>(
    'system-setup/system-configuration/view',
    async ({ type, search, page, pageSize, sort }, { rejectWithValue }) => {
        try {
            const searchParams = search === undefined ? '' : search;
            const sortParams = sort === undefined || sort === '' ? 'created_date~desc' : sort;
            const url = `/data-system-config/detail?size=${pageSize}&search=name_slug~${type}%26${searchParams}&page=${page}&sort=${sortParams}`;
            const response = await systemSetupHttpService.getPagination(url);
            const data = response.data;
            return data;
        } catch (e: any) {
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const createSystemConfiguration = createAsyncThunk<any, { body: systemConfigurationBody }, { rejectValue: string }>(
    'system-setup/system-configuration/create',
    async ({ body }, { rejectWithValue }) => {
        try {
            const url = `/data-system-config`;
            const response = await systemSetupHttpService.createData(url, body);
            const data = response.data;
            notificationSuccess('success', 'System Configuration Successfully Created', '');
            return data;
        } catch (e: any) {
            notificationSuccess('error', e.response.data.message as string, '');
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const updateSystemConfiguration = createAsyncThunk<any, { body: systemConfigurationBody; id: any }, { rejectValue: string }>(
    'system-setup/system-configuration/update',
    async ({ body, id }, { rejectWithValue }) => {
        try {
            const url = `/data-system-config/${id}`;
            const response = await systemSetupHttpService.updateData(url, body);
            const data = response.data;
            notificationSuccess('success', 'System Configuration Successfully Updated', '');
            return data;
        } catch (e: any) {
            notificationSuccess('error', e.response.data.message as string, '');
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const getDetailSystemConfiguration = createAsyncThunk<any, { id: any }, { rejectValue: string }>('system-setup/system-configuration/detail', async (id, { rejectWithValue }) => {
    try {
        const url = `/data-system-config/${id}`;
        const response = await systemSetupHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.message as string);
    }
});

export const activeOrInactiveSystemConfiguration = createAsyncThunk<any, { id: any; status: any }, { rejectValue: string }>(
    'system-setup/system-configuration/toggle',
    async ({ id, status }, { rejectWithValue }) => {
        try {
            const url = `/data-system-config/${id}/toggle`;
            const response = await systemSetupHttpService.activeOrInactiveData(url);
            const data = response.data;
            notificationSuccess('success', `System Configuration Successfully ${status === 'ACTIVE' ? 'Deactivate' : 'Activate'}`, '');
            return data;
        } catch (e: any) {
            notificationSuccess('error', e.response.data.message as string, '');
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const deleteSystemConfiguration = createAsyncThunk<any, { id: any }, { rejectValue: string }>('system-setup/system-configuration/delete', async ({ id }, { rejectWithValue }) => {
    try {
        const url = `/data-system-config/${id}`;
        const response = await systemSetupHttpService.deleteData(url);
        const data = response.data;
        notificationSuccess('success', `System Configuration Successfully Deleted`, '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

const systemConfigurationSlice = createSlice({
    name: 'system-configuration',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            // Get Data Header
            .addCase(getSystemConfigurationHeader.pending, (state) => {
                state.loading = true;
            })
            .addCase(getSystemConfigurationHeader.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataHeader = action.payload;
            })
            .addCase(getSystemConfigurationHeader.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataHeader = action.payload;
            })

            // View System Configuration
            .addCase(getSystemConfigurationPaginate.pending, (state) => {
                state.loading = true;
            })
            .addCase(getSystemConfigurationPaginate.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataPaging = action.payload;
            })
            .addCase(getSystemConfigurationPaginate.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataPaging = action.payload;
            })

            // Get Detail System Configuration
            .addCase(getDetailSystemConfiguration.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDetailSystemConfiguration.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            })
            .addCase(getDetailSystemConfiguration.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            })

            // Create System Configuration
            .addCase(createSystemConfiguration.pending, (state) => {
                state.loading = true;
            })
            .addCase(createSystemConfiguration.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSystem = action.payload;
            })
            .addCase(createSystemConfiguration.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSystem = action.payload;
            })

            // Update System Configuration
            .addCase(updateSystemConfiguration.pending, (state) => {
                state.loading = true;
            })
            .addCase(updateSystemConfiguration.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSystem = action.payload;
            })
            .addCase(updateSystemConfiguration.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSystem = action.payload;
            })

             // Active/Inactive System Configuration
             .addCase(activeOrInactiveSystemConfiguration.pending, (state) => {
                state.loading = true;
            })
            .addCase(activeOrInactiveSystemConfiguration.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSystem = action.payload;
            })
            .addCase(activeOrInactiveSystemConfiguration.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSystem = action.payload;
            })

             // Delete System Configuration
             .addCase(deleteSystemConfiguration.pending, (state) => {
                state.loading = true;
            })
            .addCase(deleteSystemConfiguration.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSystem = action.payload;
            })
            .addCase(deleteSystemConfiguration.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSystem = action.payload;
            });
    },
});

export default systemConfigurationSlice.reducer;
