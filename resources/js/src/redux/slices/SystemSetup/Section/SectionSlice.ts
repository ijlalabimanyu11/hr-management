import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import systemSetupHttpService from '../../../services/SystemSetupService';
import { notificationSuccess } from '../../../../components/Notification';

export interface sectionBody {
    sectionCode: string;
    sectionName: string;
    sectionHead: number;
    description: string;
}

interface sectionState {
    loading: boolean;
    message: string;
    data: any | null;
    dataDetail: any | null;
    dataSection: any | null;
}

const initialState: sectionState = {
    loading: false,
    message: '',
    data: null,
    dataDetail: null,
    dataSection: null,
};

export const createSection = createAsyncThunk<any, { body: sectionBody }, { rejectValue: string }>('system-setup/section/create', async ({ body }, { rejectWithValue }) => {
    try {
        const url = `/section`;
        const response = await systemSetupHttpService.createData(url, body);
        const data = response.data;
        notificationSuccess('success', 'Section Successfully Created', '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const updateSection = createAsyncThunk<any, { body: sectionBody; id: any }, { rejectValue: string }>('system-setup/section/update', async ({ body, id }, { rejectWithValue }) => {
    try {
        const url = `/section/${id}`;
        const response = await systemSetupHttpService.updateData(url, body);
        const data = response.data;
        notificationSuccess('success', 'Section Successfully Updated', '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getSectionPaginate = createAsyncThunk<any, { search: any; page: any; pageSize: any; sort: any }, { rejectValue: string }>(
    'system-setup/section/view',
    async ({ search, page, pageSize, sort }, { rejectWithValue }) => {
        try {
            const searchParams = search === undefined ? '' : search;
            const sortParams = sort === undefined || sort === '' ? 'created_date~desc' : sort;
            const url = `/section?search=${searchParams}&page=${page}&size=${pageSize}&sort=${sortParams}`;
            const response = await systemSetupHttpService.getPagination(url);
            const data = response.data;
            return data;
        } catch (e: any) {
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const activeOrInactiveSection = createAsyncThunk<any, { id: any; status: any }, { rejectValue: string }>('system-setup/section/toggle', async ({ id, status }, { rejectWithValue }) => {
    try {
        const url = `/section/${id}/toggle`;
        const response = await systemSetupHttpService.activeOrInactiveData(url);
        const data = response.data;
        notificationSuccess('success', `Section Successfully ${status === 'ACTIVE' ? 'Deactivate' : 'Activate'}`, '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const deleteSection = createAsyncThunk<any, { id: any }, { rejectValue: string }>('system-setup/section/delete', async ({ id }, { rejectWithValue }) => {
    try {
        const url = `/section/${id}`;
        const response = await systemSetupHttpService.deleteData(url);
        const data = response.data;
        notificationSuccess('success', `Section Successfully Deleted`, '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getDetailSection = createAsyncThunk<any, { id: any }, { rejectValue: string }>('system-setup/section/detail', async (id, { rejectWithValue }) => {
    try {
        const url = `/section/${id}`;
        const response = await systemSetupHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.message as string);
    }
});

const sectionSlice = createSlice({
    name: 'section',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            // Create Section
            .addCase(createSection.pending, (state) => {
                state.loading = true;
            })
            .addCase(createSection.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSection = action.payload;
            })
            .addCase(createSection.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSection = action.payload;
            })

            // Update Section
            .addCase(updateSection.pending, (state) => {
                state.loading = true;
            })
            .addCase(updateSection.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSection = action.payload;
            })
            .addCase(updateSection.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSection = action.payload;
            })

            // View Section
            .addCase(getSectionPaginate.pending, (state) => {
                state.loading = true;
            })
            .addCase(getSectionPaginate.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })
            .addCase(getSectionPaginate.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })

            // Delete Section
            .addCase(deleteSection.pending, (state) => {
                state.loading = true;
            })
            .addCase(deleteSection.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSection = action.payload;
            })
            .addCase(deleteSection.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSection = action.payload;
            })

            // Get Detail Section
            .addCase(getDetailSection.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDetailSection.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            })
            .addCase(getDetailSection.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            });
    },
});

export default sectionSlice.reducer;
