import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import systemSetupHttpService from '../../../services/SystemSetupService';
import { notificationSuccess } from '../../../../components/Notification';

export interface organizationStructureBody {
    position: string;
    parentPosition: number;
}

interface organizationStructureState {
    loading: boolean;
    message: string;
    data: any | null;
    dataDetail: any | null;
    dataOrganizationStructure: any | null;
    dataChart: any | null;
}

const initialState: organizationStructureState = {
    loading: false,
    message: '',
    data: null,
    dataDetail: null,
    dataOrganizationStructure: null,
    dataChart: null,
};

export const createOrganizationStructure = createAsyncThunk<any, { body: organizationStructureBody }, { rejectValue: string }>(
    'system-setup/organization-structure/create',
    async ({ body }, { rejectWithValue }) => {
        try {
            const url = `/org-structure`;
            const response = await systemSetupHttpService.createData(url, body);
            const data = response.data;
            notificationSuccess('success', 'Organization Structure Successfully Created', '');
            return data;
        } catch (e: any) {
            notificationSuccess('error', e.response.data.message as string, '');
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const updateOrganizationStructure = createAsyncThunk<any, { body: organizationStructureBody; id: any }, { rejectValue: string }>(
    'system-setup/organization-structure/update',
    async ({ body, id }, { rejectWithValue }) => {
        try {
            const url = `/org-structure/${id}`;
            const response = await systemSetupHttpService.updateData(url, body);
            const data = response.data;
            notificationSuccess('success', 'Organization Structure Successfully Updated', '');
            return data;
        } catch (e: any) {
            notificationSuccess('error', e.response.data.message as string, '');
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const getOrganizationStructurePaginate = createAsyncThunk<any, { search: any; page: any; pageSize: any; sort: any }, { rejectValue: string }>(
    'system-setup/organization-structure/view',
    async ({ search, page, pageSize, sort }, { rejectWithValue }) => {
        try {
            const searchParams = search === undefined ? '' : search;
            const sortParams = sort === undefined || sort === '' ? 'created_date~desc' : sort;
            const url = `/org-structure?search=${searchParams}&page=${page}&size=${pageSize}&sort=${sortParams}`;
            const response = await systemSetupHttpService.getPagination(url);
            const data = response.data;
            return data;
        } catch (e: any) {
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const deleteOrganizationStructure = createAsyncThunk<any, { id: any }, { rejectValue: string }>('system-setup/organization-structure/delete', async ({ id }, { rejectWithValue }) => {
    try {
        const url = `/org-structure/${id}`;
        const response = await systemSetupHttpService.deleteData(url);
        const data = response.data;
        notificationSuccess('success', `Organization Structure Successfully Deleted`, '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getDetailOrganizationStructure = createAsyncThunk<any, { id: any }, { rejectValue: string }>('system-setup/organization-structure/detail', async (id, { rejectWithValue }) => {
    try {
        const url = `/org-structure/${id}`;
        const response = await systemSetupHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.message as string);
    }
});

export const getChartOrganizationStructure = createAsyncThunk<any, void, { rejectValue: string }>('system-setup/organization-structure/chart', async (_, { rejectWithValue }) => {
    try {
        const url = `/org-structure/chart`;
        const response = await systemSetupHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.message as string);
    }
});

const organizationStructureSlice = createSlice({
    name: 'organization-structure',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            // Create Organization Structure
            .addCase(createOrganizationStructure.pending, (state) => {
                state.loading = true;
            })
            .addCase(createOrganizationStructure.fulfilled, (state, action) => {
                state.loading = false;
                state.dataOrganizationStructure = action.payload;
            })
            .addCase(createOrganizationStructure.rejected, (state, action) => {
                state.loading = false;
                state.dataOrganizationStructure = action.payload as string;
            })

            // Update Organization Structure
            .addCase(updateOrganizationStructure.pending, (state) => {
                state.loading = true;
            })
            .addCase(updateOrganizationStructure.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataOrganizationStructure = action.payload;
            })
            .addCase(updateOrganizationStructure.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataOrganizationStructure = action.payload;
            })

            // View Organization Structure
            .addCase(getOrganizationStructurePaginate.pending, (state) => {
                state.loading = true;
            })
            .addCase(getOrganizationStructurePaginate.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })
            .addCase(getOrganizationStructurePaginate.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })

            // Delete Organization Structure
            .addCase(deleteOrganizationStructure.pending, (state) => {
                state.loading = true;
            })
            .addCase(deleteOrganizationStructure.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataOrganizationStructure = action.payload;
            })
            .addCase(deleteOrganizationStructure.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataOrganizationStructure = action.payload;
            })

            // Get Detail Organization Structure
            .addCase(getDetailOrganizationStructure.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDetailOrganizationStructure.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            })
            .addCase(getDetailOrganizationStructure.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            })

            // Get Chart Organization Structure
            .addCase(getChartOrganizationStructure.pending, (state) => {
                state.loading = true;
            })
            .addCase(getChartOrganizationStructure.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataChart = action.payload;
            })
            .addCase(getChartOrganizationStructure.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataChart = action.payload;
            });
    },
});

export default organizationStructureSlice.reducer;
