import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import systemSetupHttpService from '../../../services/SystemSetupService';
import { notificationSuccess } from '../../../../components/Notification';

export interface userData {
    id: number;
    name: string;
    email: string;
    username: string;
    status: string;
}
export interface userListResponse {
    currentPage: number;
    lastPage: number;
    pageSize: number;
    totalData: number;
    data: userData[];
}

export interface userBody {
    employeeId?: number;
    email?: string;
    username?: string;
    role?: string;
    isDefault?: boolean;
    password?: string;
}

interface UserState {
    loading: boolean;
    message: string;
    data: any | null;
    dataDetail: any | null;
    dataUser: any | null;
}

const initialState: UserState = {
    loading: false,
    message: '',
    data: null,
    dataDetail: null,
    dataUser: null,
};

export const getUserPaginate = createAsyncThunk<userListResponse, { search: any; page: any; pageSize: any; sort: any }, { rejectValue: string }>(
    'system-setup/user/view',
    async ({ search, page, pageSize, sort }, { rejectWithValue }) => {
        try {
            const searchParams = search === undefined ? '' : search;
            const sortParams = sort === undefined || sort === '' ? 'created_date~desc' : sort;
            const url = `/users?size=${pageSize}&search=${searchParams}&page=${page}&sort=${sortParams}`;
            const response = await systemSetupHttpService.getPagination(url);
            const data = response.data;
            return data;
        } catch (e: any) {
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const createUser = createAsyncThunk<any, { body: userBody }, { rejectValue: string }>('system-setup/user/create', async ({ body }, { rejectWithValue }) => {
    try {
        const url = '/users';
        const response = await systemSetupHttpService.createData(url, body);
        const data = response.data;
        notificationSuccess('success', 'User Successfully Created', '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const updateUser = createAsyncThunk<any, { body: userBody; id: any }, { rejectValue: string }>('system-setup/user/update', async ({ body, id }, { rejectWithValue }) => {
    try {
        const url = `/users/${id}`;
        const response = await systemSetupHttpService.updateData(url, body);
        const data = response.data;
        notificationSuccess('success', 'User Successfully Updated', '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const resetPasswordUser = createAsyncThunk<any, { body: userBody; id: any }, { rejectValue: string }>('system-setup/user/reset-password', async ({ body, id }, { rejectWithValue }) => {
    try {
        const url = `/users/${id}/reset-password`;
        const response = await systemSetupHttpService.createData(url, body);
        const data = response.data;
        notificationSuccess('success', 'User Successfully Reset Password', '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const activeOrInactiveUser = createAsyncThunk<any, { id: any; status: any }, { rejectValue: string }>('system-setup/user/active-inactive', async ({ id, status }, { rejectWithValue }) => {
    try {
        const url = `/users/${id}/toggle`;
        const response = await systemSetupHttpService.activeOrInactiveData(url);
        const data = response.data;
        notificationSuccess('success', `User Successfully ${status === 'ACTIVE' ? 'Deactivate' : 'Activate'}`, '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const deleteUser = createAsyncThunk<any, { id: any }, { rejectValue: string }>('system-setup/user/delete', async ({ id }, { rejectWithValue }) => {
    try {
        const url = `/users/${id}`;
        const response = await systemSetupHttpService.deleteData(url);
        const data = response.data;
        notificationSuccess('success', `User Successfully Deleted`, '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getDetailUser = createAsyncThunk<any, { id: any }, { rejectValue: string }>('system-setup/user/detail', async (id, { rejectWithValue }) => {
    try {
        const url = `/users/${id}`;
        const response = await systemSetupHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.message as string);
    }
});

const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            // Create User
            .addCase(createUser.pending, (state) => {
                state.loading = true;
            })
            .addCase(createUser.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataUser = action.payload;
            })
            .addCase(createUser.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataUser = action.payload;
            })

            // Update User
            .addCase(updateUser.pending, (state) => {
                state.loading = true;
            })
            .addCase(updateUser.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataUser = action.payload;
            })
            .addCase(updateUser.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataUser = action.payload;
            })

            // Reset Password User
            .addCase(resetPasswordUser.pending, (state) => {
                state.loading = true;
            })
            .addCase(resetPasswordUser.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataUser = action.payload;
            })
            .addCase(resetPasswordUser.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataUser = action.payload;
            })

            // Active/Inactive User
            .addCase(activeOrInactiveUser.pending, (state) => {
                state.loading = true;
            })
            .addCase(activeOrInactiveUser.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataUser = action.payload;
            })
            .addCase(activeOrInactiveUser.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataUser = action.payload;
            })

            // Delete User
            .addCase(deleteUser.pending, (state) => {
                state.loading = true;
            })
            .addCase(deleteUser.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataUser = action.payload;
            })
            .addCase(deleteUser.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataUser = action.payload;
            })

            // View User
            .addCase(getUserPaginate.pending, (state) => {
                state.loading = true;
            })
            .addCase(getUserPaginate.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })
            .addCase(getUserPaginate.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })

            // Get Detail
            .addCase(getDetailUser.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDetailUser.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            })
            .addCase(getDetailUser.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            });
    },
});

export default userSlice.reducer;
