import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import systemSetupHttpService from '../../../services/SystemSetupService';
import { notificationSuccess } from '../../../../components/Notification';

export interface positionBody {
    positionCode: string;
    positionName: string;
    description: string;
}

interface positionState {
    loading: boolean;
    message: string;
    data: any | null;
    dataDetail: any | null;
    dataPosition: any | null;
}

const initialState: positionState = {
    loading: false,
    message: '',
    data: null,
    dataDetail: null,
    dataPosition: null,
};

export const createPosition = createAsyncThunk<any, { body: positionBody }, { rejectValue: string }>('system-setup/position/create', async ({ body }, { rejectWithValue }) => {
    try {
        const url = `/position`;
        const response = await systemSetupHttpService.createData(url, body);
        const data = response.data;
        notificationSuccess('success', 'Position Successfully Created', '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const updatePosition = createAsyncThunk<any, { body: positionBody; id: any }, { rejectValue: string }>('system-setup/position/update', async ({ body, id }, { rejectWithValue }) => {
    try {
        const url = `/position/${id}`;
        const response = await systemSetupHttpService.updateData(url, body);
        const data = response.data;
        notificationSuccess('success', 'Position Successfully Updated', '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getPositionPaginate = createAsyncThunk<any, { search: any; page: any; pageSize: any; sort: any }, { rejectValue: string }>(
    'system-setup/position/view',
    async ({ search, page, pageSize, sort }, { rejectWithValue }) => {
        try {
            const searchParams = search === undefined ? '' : search;
            const sortParams = sort === undefined || sort === '' ? 'created_date~desc' : sort;
            const url = `/position?search=${searchParams}&page=${page}&size=${pageSize}&sort=${sortParams}`;
            const response = await systemSetupHttpService.getPagination(url);
            const data = response.data;
            return data;
        } catch (e: any) {
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const activeOrInactivePosition = createAsyncThunk<any, { id: any; status: any }, { rejectValue: string }>('system-setup/position/toggle', async ({ id, status }, { rejectWithValue }) => {
    try {
        const url = `/position/${id}/toggle`;
        const response = await systemSetupHttpService.activeOrInactiveData(url);
        const data = response.data;
        notificationSuccess('success', `Position Successfully ${status === 'ACTIVE' ? 'Deactivate' : 'Activate'}`, '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const deletePosition = createAsyncThunk<any, { id: any }, { rejectValue: string }>('system-setup/position/delete', async ({ id }, { rejectWithValue }) => {
    try {
        const url = `/position/${id}`;
        const response = await systemSetupHttpService.deleteData(url);
        const data = response.data;
        notificationSuccess('success', `Position Successfully Deleted`, '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getDetailPosition = createAsyncThunk<any, { id: any }, { rejectValue: string }>('system-setup/position/detail', async (id, { rejectWithValue }) => {
    try {
        const url = `/position/${id}`;
        const response = await systemSetupHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.message as string);
    }
});

const positionSlice = createSlice({
    name: 'position',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            // Create Position
            .addCase(createPosition.pending, (state) => {
                state.loading = true;
            })
            .addCase(createPosition.fulfilled, (state, action) => {
                state.loading = false;
                state.dataPosition = action.payload;
            })
            .addCase(createPosition.rejected, (state, action) => {
                state.loading = false;
                state.dataPosition = action.payload as string;
            })

            // Update Position
            .addCase(updatePosition.pending, (state) => {
                state.loading = true;
            })
            .addCase(updatePosition.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataPosition = action.payload;
            })
            .addCase(updatePosition.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataPosition = action.payload;
            })

             // View Position
             .addCase(getPositionPaginate.pending, (state) => {
                state.loading = true;
            })
            .addCase(getPositionPaginate.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })
            .addCase(getPositionPaginate.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })

            // Delete Position
            .addCase(deletePosition.pending, (state) => {
                state.loading = true;
            })
            .addCase(deletePosition.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataPosition = action.payload;
            })
            .addCase(deletePosition.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataPosition = action.payload;
            })

            // Get Detail Position
            .addCase(getDetailPosition.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDetailPosition.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            })
            .addCase(getDetailPosition.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            });
    },
});

export default positionSlice.reducer
