import { notificationSuccess } from '../../components/Notification';
import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import globalHttpService from '../services/GlobalService';

interface GlobalState {
    loading: boolean;
    message: string;
    data: any | null;
    dataRole: any | null;
    dataEmployee: any | null;
    dataDefaultPassword: any | null;
    dataPrivileges: any | null;
    dataMenu: any | null;
    dataDepartment: any | null;
    dataSection: any | null;
    dataSalary: any | null;
    dataBank: any | null;
    dataLeave: any | null;
    dataReimbursement: any | null;
    dataBasicSalary: any | null;
    dataPosition: any | null;
}

const initialState: GlobalState = {
    loading: false,
    message: '',
    data: null,
    dataRole: null,
    dataEmployee: null,
    dataDefaultPassword: null,
    dataPrivileges: null,
    dataMenu: null,
    dataDepartment: null,
    dataSection: null,
    dataSalary: null,
    dataBank: null,
    dataLeave: null,
    dataReimbursement: null,
    dataBasicSalary: null,
    dataPosition: null
};

export const getRoleType = createAsyncThunk<any, void, { rejectValue: string }>('global/role-type', async (_, { rejectWithValue }) => {
    try {
        const url = '/lov/role';
        const response = await globalHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getDefaultPassword = createAsyncThunk<any, void, { rejectValue: string }>('global/default-password', async (_, { rejectWithValue }) => {
    try {
        const url = '/lov/props/system_config';
        const response = await globalHttpService.getAll(url);
        const data = response.data;
        return data?.filter((item: any) => item.key === 'default-password');
    } catch (e: any) {
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getEmployee = createAsyncThunk<any, void, { rejectValue: string }>('global/employee', async (_, { rejectWithValue }) => {
    try {
        const url = '/lov/employee';
        const response = await globalHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getPrivileges = createAsyncThunk<any, void, { rejectValue: string }>('global/privileges', async (_, { rejectWithValue }) => {
    try {
        const url = '/lov/privilages';
        const response = await globalHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getMenu = createAsyncThunk<any, void, { rejectValue: string }>('global/menu', async (_, { rejectWithValue }) => {
    try {
        const url = '/lov/menu';
        const response = await globalHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getDepartment = createAsyncThunk<any, void, { rejectValue: string }>('global/department', async (_, { rejectWithValue }) => {
    try {
        const url = '/lov/department';
        const response = await globalHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getSection = createAsyncThunk<any, void, { rejectValue: string }>('global/section', async (_, { rejectWithValue }) => {
    try {
        const url = '/lov/section';
        const response = await globalHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getSalary = createAsyncThunk<any, void, { rejectValue: string }>('global/salary', async (_, { rejectWithValue }) => {
    try {
        const url = '/lov/salary-type';
        const response = await globalHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getBank = createAsyncThunk<any, void, { rejectValue: string }>('global/bank', async (_, { rejectWithValue }) => {
    try {
        const url = '/lov/bank-name';
        const response = await globalHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getLeave = createAsyncThunk<any, void, { rejectValue: string }>('global/leave', async (_, { rejectWithValue }) => {
    try {
        const url = '/lov/leave-type';
        const response = await globalHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getReimbursement = createAsyncThunk<any, void, { rejectValue: string }>('global/reimbursement', async (_, { rejectWithValue }) => {
    try {
        const url = '/lov/reimbursement-type';
        const response = await globalHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getBasicSalary = createAsyncThunk<any, void, { rejectValue: string }>('global/basic-salary', async (_, { rejectWithValue }) => {
    try {
        const url = '/lov/basic-salary';
        const response = await globalHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getPosition = createAsyncThunk<any, void, { rejectValue: string }>('global/position', async (_, { rejectWithValue }) => {
    try {
        const url = '/lov/position';
        const response = await globalHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.data.message as string);
    }
});

const globalSlice = createSlice({
    name: 'global',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            // Get Role Type
            .addCase(getRoleType.pending, (state) => {
                state.loading = true;
            })
            .addCase(getRoleType.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataRole = action.payload;
            })
            .addCase(getRoleType.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataRole = action.payload;
            })

            // Get Default Password
            .addCase(getDefaultPassword.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDefaultPassword.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDefaultPassword = action.payload;
            })
            .addCase(getDefaultPassword.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDefaultPassword = action.payload;
            })

            // Get Employee
            .addCase(getEmployee.pending, (state) => {
                state.loading = true;
            })
            .addCase(getEmployee.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataEmployee = action.payload;
            })
            .addCase(getEmployee.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataEmployee = action.payload;
            })

            // Get Privileges
            .addCase(getPrivileges.pending, (state) => {
                state.loading = true;
            })
            .addCase(getPrivileges.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataPrivileges = action.payload;
            })
            .addCase(getPrivileges.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataPrivileges = action.payload;
            })

            // Get Menu
            .addCase(getMenu.pending, (state) => {
                state.loading = true;
            })
            .addCase(getMenu.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataMenu = action.payload;
            })
            .addCase(getMenu.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataMenu = action.payload;
            })

            // Get Department
            .addCase(getDepartment.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDepartment.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDepartment = action.payload;
            })
            .addCase(getDepartment.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDepartment = action.payload;
            })

            // Get Section
            .addCase(getSection.pending, (state) => {
                state.loading = true;
            })
            .addCase(getSection.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSection = action.payload;
            })
            .addCase(getSection.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSection = action.payload;
            })

            // Get Salary Type
            .addCase(getSalary.pending, (state) => {
                state.loading = true;
            })
            .addCase(getSalary.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSalary = action.payload;
            })
            .addCase(getSalary.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSalary = action.payload;
            })

            // Get Bank Name
            .addCase(getBank.pending, (state) => {
                state.loading = true;
            })
            .addCase(getBank.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataBank = action.payload;
            })
            .addCase(getBank.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataBank = action.payload;
            })

            // Get Leave Type
            .addCase(getLeave.pending, (state) => {
                state.loading = true;
            })
            .addCase(getLeave.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataLeave = action.payload;
            })
            .addCase(getLeave.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataLeave = action.payload;
            })

            // Get Reimbursement Type
            .addCase(getReimbursement.pending, (state) => {
                state.loading = true;
            })
            .addCase(getReimbursement.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataReimbursement = action.payload;
            })
            .addCase(getReimbursement.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataReimbursement = action.payload;
            })

            // Get Basic Salary
            .addCase(getBasicSalary.pending, (state) => {
                state.loading = true;
            })
            .addCase(getBasicSalary.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataBasicSalary = action.payload;
            })
            .addCase(getBasicSalary.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataBasicSalary = action.payload;
            })

             // Get Position
             .addCase(getPosition.pending, (state) => {
                state.loading = true;
            })
            .addCase(getPosition.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataPosition = action.payload;
            })
            .addCase(getPosition.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataPosition = action.payload;
            });
    },
});

export default globalSlice.reducer;
