import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { notificationSuccess } from '../../../../../components/Notification';
import appHttpService from '../../../../services/AppService';

export interface designationBody {
    employee_id: number;
    department: string;
    section: string;
    position: string;
    line_manager1: string;
    line_manager2: string;
    start_date: string;
    end_date: string;
    notes: string;
}

interface designationState {
    loading: boolean;
    message: string;
    data: any | null;
    dataDetail: any | null;
    dataDesignation: any | null;
}

const initialState: designationState = {
    loading: false,
    message: '',
    data: null,
    dataDetail: null,
    dataDesignation: null,
};

export const createDesignation = createAsyncThunk<any, { body: designationBody }, { rejectValue: string }>('employee-management/designation/create', async ({ body }, { rejectWithValue }) => {
    try {
        const url = `/employee-designation/create`;
        const response = await appHttpService.createData(url, body);
        const data = response.data;
        notificationSuccess('success', 'Designation Successfully Created', '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const updateDesignation = createAsyncThunk<any, { body: designationBody; id: any }, { rejectValue: string }>(
    'employee-management/designation/update',
    async ({ body, id }, { rejectWithValue }) => {
        try {
            const url = `/employee-designation/update/${id}`;
            const response = await appHttpService.updateData(url, body);
            const data = response.data;
            notificationSuccess('success', 'Designation Successfully Update', '');
            return data;
        } catch (e: any) {
            notificationSuccess('error', e.response.data.message as string, '');
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const getDesignationPaginate = createAsyncThunk<any, { search: any; page: any; pageSize: any; sort: any }, { rejectValue: string }>(
    'employee-management/designation/view',
    async ({ search, page, pageSize, sort }, { rejectWithValue }) => {
        try {
            const searchParams = search === undefined ? '' : search;
            const sortParams = sort === undefined || sort === '' ? 'created_date~desc' : sort;
            const url = `/employee-designation/view-paging?search=${searchParams}&page=${page}&size=${pageSize}&sort=${sortParams}`;
            const response = await appHttpService.getPagination(url);
            const data = response.data;
            return data;
        } catch (e: any) {
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const deleteDesignation = createAsyncThunk<any, { id: any }, { rejectValue: string }>('employee-management/designation/delete', async ({ id }, { rejectWithValue }) => {
    try {
        const url = `/employee-designation/delete/${id}`;
        const response = await appHttpService.deleteData(url);
        const data = response.data;
        notificationSuccess('success', `Designation Successfully Deleted`, '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getDetailDesignation = createAsyncThunk<any, { id: any }, { rejectValue: string }>('employee-management/designation/detail', async (id, { rejectWithValue }) => {
    try {
        const url = `/employee-designation/view-detail/${id}`;
        const response = await appHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.message as string);
    }
});

const designationSlice = createSlice({
    name: 'designation',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            // Create Designation
            .addCase(createDesignation.pending, (state) => {
                state.loading = true;
            })
            .addCase(createDesignation.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDesignation = action.payload;
            })
            .addCase(createDesignation.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDesignation = action.payload;
            })

            // Update Designation
            .addCase(updateDesignation.pending, (state) => {
                state.loading = true;
            })
            .addCase(updateDesignation.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDesignation = action.payload;
            })
            .addCase(updateDesignation.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDesignation = action.payload;
            })

            // View Designation
            .addCase(getDesignationPaginate.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDesignationPaginate.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })
            .addCase(getDesignationPaginate.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })

            // Delete Designation
            .addCase(deleteDesignation.pending, (state) => {
                state.loading = true;
            })
            .addCase(deleteDesignation.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDesignation = action.payload;
            })
            .addCase(deleteDesignation.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDesignation = action.payload;
            })

            // Get Detail Designation
            .addCase(getDetailDesignation.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDetailDesignation.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            })
            .addCase(getDetailDesignation.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            });
    },
});

export default designationSlice.reducer;
