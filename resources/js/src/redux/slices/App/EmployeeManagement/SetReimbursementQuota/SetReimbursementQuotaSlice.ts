import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { notificationSuccess } from '../../../../../components/Notification';
import appHttpService from '../../../../services/AppService';

export interface setReimbursementQuotaBody {
    employee_id: number;
    reimbursement_type: string;
    amount: number;
    start_date: string;
    end_date: string;
}

interface setReimbursementQuotaState {
    loading: boolean;
    message: string;
    data: any | null;
    dataDetail: any | null;
    dataSetReimbursementQuota: any | null;
}

const initialState: setReimbursementQuotaState = {
    loading: false,
    message: '',
    data: null,
    dataDetail: null,
    dataSetReimbursementQuota: null,
};

export const createSetReimbursementQuota = createAsyncThunk<any, { body: any }, { rejectValue: string }>('employee-management/reimbursement/create', async ({ body }, { rejectWithValue }) => {
    try {
        const url = `/employee-reimbursement/create`;
        const response = await appHttpService.createData(url, body);
        const data = response.data;
        notificationSuccess('success', 'Set Reimbursement Quota Successfully Created', '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const updateSetReimbursementQuota = createAsyncThunk<any, { body: any; id: any }, { rejectValue: string }>(
    'employee-management/reimbursement/update',
    async ({ body, id }, { rejectWithValue }) => {
        try {
            const url = `/employee-reimbursement/update/${id}`;
            const response = await appHttpService.updateData(url, body);
            const data = response.data;
            notificationSuccess('success', 'Set Reimbursement Quota Successfully Update', '');
            return data;
        } catch (e: any) {
            notificationSuccess('error', e.response.data.message as string, '');
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const getSetReimbursementQuotaPaginate = createAsyncThunk<any, { search: any; page: any; pageSize: any; sort: any }, { rejectValue: string }>(
    'employee-management/reimbursement/view',
    async ({ search, page, pageSize, sort }, { rejectWithValue }) => {
        try {
            const searchParams = search === undefined ? '' : search;
            const sortParams = sort === undefined || sort === '' ? 'created_date~desc' : sort;
            const url = `/employee-reimbursement/view-paging?search=${searchParams}&page=${page}&size=${pageSize}&sort=${sortParams}`;
            const response = await appHttpService.getPagination(url);
            const data = response.data;
            return data;
        } catch (e: any) {
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const deleteSetReimbursementQuota = createAsyncThunk<any, { id: any }, { rejectValue: string }>('employee-management/reimbursement/delete', async ({ id }, { rejectWithValue }) => {
    try {
        const url = `/employee-reimbursement/delete/${id}`;
        const response = await appHttpService.deleteData(url);
        const data = response.data;
        notificationSuccess('success', `Set Reimbursement Quota Successfully Deleted`, '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getDetailSetReimbursementQuota = createAsyncThunk<any, { id: any }, { rejectValue: string }>('employee-management/reimbursement/detail', async (id, { rejectWithValue }) => {
    try {
        const url = `/employee-reimbursement/view-detail/${id}`;
        const response = await appHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.message as string);
    }
});

const setReimbursementSlice = createSlice({
    name: 'setReimbursementQuota',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            // Create Set Reimbursement Quota
            .addCase(createSetReimbursementQuota.pending, (state) => {
                state.loading = true;
            })
            .addCase(createSetReimbursementQuota.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSetReimbursementQuota = action.payload;
            })
            .addCase(createSetReimbursementQuota.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSetReimbursementQuota = action.payload;
            })

            // Update Set Reimbursement Quota
            .addCase(updateSetReimbursementQuota.pending, (state) => {
                state.loading = true;
            })
            .addCase(updateSetReimbursementQuota.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSetReimbursementQuota = action.payload;
            })
            .addCase(updateSetReimbursementQuota.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSetReimbursementQuota = action.payload;
            })

            // View Set Reimbursement Quota
            .addCase(getSetReimbursementQuotaPaginate.pending, (state) => {
                state.loading = true;
            })
            .addCase(getSetReimbursementQuotaPaginate.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })
            .addCase(getSetReimbursementQuotaPaginate.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })

            // Delete Set Reimbursement Quota
            .addCase(deleteSetReimbursementQuota.pending, (state) => {
                state.loading = true;
            })
            .addCase(deleteSetReimbursementQuota.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSetReimbursementQuota = action.payload;
            })
            .addCase(deleteSetReimbursementQuota.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSetReimbursementQuota = action.payload;
            })

            // Get Detail Set Reimbursement Quota
            .addCase(getDetailSetReimbursementQuota.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDetailSetReimbursementQuota.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            })
            .addCase(getDetailSetReimbursementQuota.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            });
    },
});

export default setReimbursementSlice.reducer;
