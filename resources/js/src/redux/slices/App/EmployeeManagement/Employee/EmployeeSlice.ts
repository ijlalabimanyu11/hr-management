import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { notificationSuccess, notificationSuccessWithBack } from '../../../../../components/Notification';
import appHttpService from '../../../../services/AppService';

// Employee
export interface employeeBody {
    employee_picture: File;
    employee_code: string;
    national_identifier: string;
    name: string;
    email: string;
    birth_date: string;
    gender: string;
    phone: string;
    address: string;
    salary_type: string;
    basic_salary: string;
    join_date: string;
    bank_name: string;
    branch_location: string;
    bank_account_name: string;
    bank_account_number: string;
    tax_payer_identifier: string;
    cv: File;
}

// Certificate
export interface certificateBody {
    employee_id: string;
    certificate: File;
    name: string;
    issuing_organization: string;
    issuing_date: string;
    expired_date: string;
    notes: string;
}

interface employeeState {
    loading: boolean;
    message: string;
    data: any;
    dataPagingCertificate: any;
    dataDetail: any;
    dataEmployee: any;
    dataCV: any;
    dataPicture: any;
    dataCertificate: any;
    dataDetailCertificate: any;
}

const initialState: employeeState = {
    loading: false,
    message: '',
    data: null,
    dataPagingCertificate: null,
    dataDetail: null,
    dataEmployee: null,
    dataCV: null,
    dataPicture: null,
    dataCertificate: null,
    dataDetailCertificate: null,
};

export const createEmployee = createAsyncThunk<any, { body: any }, { rejectValue: string }>('employee-management/employee/create', async ({ body }, { rejectWithValue }) => {
    try {
        const url = `/employee/create`;
        const response = await appHttpService.createData(url, body);
        const data = response.data;
        notificationSuccessWithBack('success', 'Employee Successfully Created', '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const createEmployeeCertificate = createAsyncThunk<any, { body: any }, { rejectValue: string }>('employee-management/employee/create-certificate', async ({ body }, { rejectWithValue }) => {
    try {
        const url = `/employee/store-certification`;
        const response = await appHttpService.createDataMulti(url, body);
        const data = response.data;
        // notificationSuccess('success', 'Employee Successfully Created', '');
        return data;
    } catch (e: any) {
        notificationSuccessWithBack('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const updateEmployee = createAsyncThunk<any, { body: any; id: any }, { rejectValue: string }>('employee-management/employee/update', async ({ body, id }, { rejectWithValue }) => {
    try {
        const url = `/employee/update/${id}`;
        const response = await appHttpService.createData(url, body);
        const data = response.data;
        notificationSuccessWithBack('success', 'Employee Successfully Update', '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const updateEmployeeCertificate = createAsyncThunk<any, { body: any; id: any }, { rejectValue: string }>(
    'employee-management/employee/update-certificate',
    async ({ body, id }, { rejectWithValue }) => {
        try {
            const url = `/employee/update-certification/${id}`;
            const response = await appHttpService.createDataMulti(url, body);
            const data = response.data;
            // notificationSuccess('success', 'Employee Successfully Update', '');
            return data;
        } catch (e: any) {
            notificationSuccess('error', e.response.data.message as string, '');
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const getEmployeePaginate = createAsyncThunk<any, { search: any; page: any; pageSize: any; sort: any }, { rejectValue: string }>(
    'employee-management/employee/view',
    async ({ search, page, pageSize, sort }, { rejectWithValue }) => {
        try {
            const searchParams = search === undefined ? '' : search;
            const sortParams = sort === undefined || sort === '' ? 'created_date~desc' : sort;
            const url = `/employee/view-paging?search=${searchParams}&page=${page}&size=${pageSize}&sort=${sortParams}`;
            const response = await appHttpService.getPagination(url);
            const data = response.data;
            return data;
        } catch (e: any) {
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const getEmployeeCertificatePaginate = createAsyncThunk<any, { id: any; search: any; page: any; pageSize: any; sort: any }, { rejectValue: string }>(
    'employee-management/employee/view-certificate',
    async ({ id, search, page, pageSize, sort }, { rejectWithValue }) => {
        try {
            const searchParams = search === undefined ? '' : search;
            const sortParams = sort === undefined || sort === '' ? 'created_date~desc' : sort;
            const url = `/employee/view-paging-certification/${id}?search=${searchParams}&page=${page}&size=${pageSize}&sort=${sortParams}`;
            const response = await appHttpService.getPagination(url);
            const data = response.data;
            return data;
        } catch (e: any) {
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const deleteEmployee = createAsyncThunk<any, { id: any }, { rejectValue: string }>('employee-management/employee/delete', async ({ id }, { rejectWithValue }) => {
    try {
        const url = `/employee/delete/${id}`;
        const response = await appHttpService.deleteData(url);
        const data = response.data;
        notificationSuccess('success', `Employee Successfully Deleted`, '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const deleteEmployeeCertification = createAsyncThunk<any, { id: any }, { rejectValue: string }>('employee-management/employee/delete-certification', async ({ id }, { rejectWithValue }) => {
    try {
        const url = `/employee/delete-certification/${id}`;
        const response = await appHttpService.deleteData(url);
        const data = response.data;
        notificationSuccess('success', `Employee Certification Successfully Deleted`, '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getDetailEmployee = createAsyncThunk<any, { id: any }, { rejectValue: string }>('employee-management/employee/detail', async (id, { rejectWithValue }) => {
    try {
        const url = `/employee/view-detail/${id}`;
        const response = await appHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.message as string);
    }
});

export const getDetailEmployeeCertificate = createAsyncThunk<any, { id: any }, { rejectValue: string }>('employee-management/employee/detail-certificate', async (id, { rejectWithValue }) => {
    try {
        const url = `/employee/view-detail-certification/${id}`;
        const response = await appHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.message as string);
    }
});

export const getDownloadPhoto = createAsyncThunk<any, { id: any; filename: any }, { rejectValue: string }>('employee-management/employee/photo', async ({ id, filename }, { rejectWithValue }) => {
    try {
        const url = `/employee/download/${id}/photo`;
        const response = await appHttpService.downloadData(url, filename);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.message as string);
    }
});

export const getDownloadCertificate = createAsyncThunk<any, { id: any; filename: any }, { rejectValue: string }>(
    'employee-management/employee/certificate',
    async ({ id, filename }, { rejectWithValue }) => {
        try {
            const url = `/employee/download-certification/${id}`;
            const response = await appHttpService.downloadData(url, filename);
            const data = response.data;
            return data;
        } catch (e: any) {
            return rejectWithValue(e.response.message as string);
        }
    }
);

export const getDownloadCV = createAsyncThunk<any, { id: any; filename: any }, { rejectValue: string }>('employee-management/employee/cv', async ({ id, filename }, { rejectWithValue }) => {
    try {
        const url = `/employee/download/${id}/cv`;
        const response = await appHttpService.downloadData(url, filename);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.message as string);
    }
});

export const activeOrInactiveEmployee = createAsyncThunk<any, { id: any; status: any }, { rejectValue: string }>('employee-management/employee/toggle', async ({ id, status }, { rejectWithValue }) => {
    try {
        const url = `/employee/toggle/${id}`;
        const response = await appHttpService.activeOrInactiveData(url);
        const data = response.data;
        notificationSuccess('success', `Employee Successfully ${status === 'ACTIVE' ? 'Deactivate' : 'Activate'}`, '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

const employeeSlice = createSlice({
    name: 'employee',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            // Create Employee
            .addCase(createEmployee.pending, (state) => {
                state.loading = true;
            })
            .addCase(createEmployee.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataEmployee = action.payload;
            })
            .addCase(createEmployee.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataEmployee = action.payload;
            })

            // Create Employee Certificate
            .addCase(createEmployeeCertificate.pending, (state) => {
                state.loading = true;
            })
            .addCase(createEmployeeCertificate.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataCertificate = action.payload;
            })
            .addCase(createEmployeeCertificate.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataCertificate = action.payload;
            })

            // Update Employee
            .addCase(updateEmployee.pending, (state) => {
                state.loading = true;
            })
            .addCase(updateEmployee.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataEmployee = action.payload;
            })
            .addCase(updateEmployee.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataEmployee = action.payload;
            })

            // Update Employee Certificate
            .addCase(updateEmployeeCertificate.pending, (state) => {
                state.loading = true;
            })
            .addCase(updateEmployeeCertificate.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataCertificate = action.payload;
            })
            .addCase(updateEmployeeCertificate.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataCertificate = action.payload;
            })

            // View Employee
            .addCase(getEmployeePaginate.pending, (state) => {
                state.loading = true;
            })
            .addCase(getEmployeePaginate.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })
            .addCase(getEmployeePaginate.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })

            // View Employee Certificate
            .addCase(getEmployeeCertificatePaginate.pending, (state) => {
                state.loading = true;
            })
            .addCase(getEmployeeCertificatePaginate.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataPagingCertificate = action.payload;
            })
            .addCase(getEmployeeCertificatePaginate.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataPagingCertificate = action.payload;
            })

            // Delete Employee
            .addCase(deleteEmployee.pending, (state) => {
                state.loading = true;
            })
            .addCase(deleteEmployee.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataEmployee = action.payload;
            })
            .addCase(deleteEmployee.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataEmployee = action.payload;
            })

            // Delete Employee Certificate
            .addCase(deleteEmployeeCertification.pending, (state) => {
                state.loading = true;
            })
            .addCase(deleteEmployeeCertification.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataCertificate = action.payload;
            })
            .addCase(deleteEmployeeCertification.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataCertificate = action.payload;
            })

            // Get Detail Employee
            .addCase(getDetailEmployee.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDetailEmployee.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            })
            .addCase(getDetailEmployee.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            })

            // Get Detail Employee Certificate
            .addCase(getDetailEmployeeCertificate.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDetailEmployeeCertificate.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetailCertificate = action.payload;
            })
            .addCase(getDetailEmployeeCertificate.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetailCertificate = action.payload;
            })

            // Get Download Photo Employee
            .addCase(getDownloadPhoto.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDownloadPhoto.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataPicture = action.payload;
            })
            .addCase(getDownloadPhoto.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataPicture = action.payload;
            })

            // Get Download Employee Certificate
            .addCase(getDownloadCertificate.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDownloadCertificate.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                // state.dataPicture = action.payload;
            })
            .addCase(getDownloadCertificate.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                // state.dataPicture = action.payload;
            })

            // Get Download CV Employee
            .addCase(getDownloadCV.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDownloadCV.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataCV = action.payload;
            })
            .addCase(getDownloadCV.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataCV = action.payload;
            })

            // Active/Inactive Employee
            .addCase(activeOrInactiveEmployee.pending, (state) => {
                state.loading = true;
            })
            .addCase(activeOrInactiveEmployee.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataEmployee = action.payload;
            })
            .addCase(activeOrInactiveEmployee.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataEmployee = action.payload;
            });
    },
});

export default employeeSlice.reducer;
