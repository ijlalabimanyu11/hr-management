import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { notificationSuccess } from '../../../../../components/Notification';
import appHttpService from '../../../../services/AppService';

export interface setLeaveQuotaBody {
    employee_id: number;
    leave_type: string;
    amount: number;
    start_date: string;
    end_date: string;
}

interface setLeaveQuotaState {
    loading: boolean;
    message: string;
    data: any | null;
    dataDetail: any | null;
    dataSetLeaveQuota: any | null;
}

const initialState: setLeaveQuotaState = {
    loading: false,
    message: '',
    data: null,
    dataDetail: null,
    dataSetLeaveQuota: null,
};

export const createSetLeaveQuota = createAsyncThunk<any, { body: any }, { rejectValue: string }>('employee-management/leave/create', async ({ body }, { rejectWithValue }) => {
    try {
        const url = `/employee-leave/create`;
        const response = await appHttpService.createData(url, body);
        const data = response.data;
        notificationSuccess('success', 'Set Leave Quota Successfully Created', '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const updateSetLeaveQuota = createAsyncThunk<any, { body: any; id: any }, { rejectValue: string }>('employee-management/leave/update', async ({ body, id }, { rejectWithValue }) => {
    try {
        const url = `/employee-leave/update/${id}`;
        const response = await appHttpService.updateData(url, body);
        const data = response.data;
        notificationSuccess('success', 'Set Leave Quota Successfully Update', '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getSetLeaveQuotaPaginate = createAsyncThunk<any, { search: any; page: any; pageSize: any; sort: any }, { rejectValue: string }>(
    'employee-management/leave/view',
    async ({ search, page, pageSize, sort }, { rejectWithValue }) => {
        try {
            const searchParams = search === undefined ? '' : search;
            const sortParams = sort === undefined || sort === '' ? 'created_date~desc' : sort;
            const url = `/employee-leave/view-paging?search=${searchParams}&page=${page}&size=${pageSize}&sort=${sortParams}`;
            const response = await appHttpService.getPagination(url);
            const data = response.data;
            return data;
        } catch (e: any) {
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const deleteSetLeaveQuota = createAsyncThunk<any, { id: any }, { rejectValue: string }>('employee-management/leave/delete', async ({ id }, { rejectWithValue }) => {
    try {
        const url = `/employee-leave/delete/${id}`;
        const response = await appHttpService.deleteData(url);
        const data = response.data;
        notificationSuccess('success', `Set Leave Quota Successfully Deleted`, '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getDetailSetLeaveQuota = createAsyncThunk<any, { id: any }, { rejectValue: string }>('employee-management/leave/detail', async (id, { rejectWithValue }) => {
    try {
        const url = `/employee-leave/view-detail/${id}`;
        const response = await appHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.message as string);
    }
});

const setLeaveQuotaSlice = createSlice({
    name: 'setLeaveQuota',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            // Create Set Leave Quota
            .addCase(createSetLeaveQuota.pending, (state) => {
                state.loading = true;
            })
            .addCase(createSetLeaveQuota.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSetLeaveQuota = action.payload;
            })
            .addCase(createSetLeaveQuota.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSetLeaveQuota = action.payload;
            })

            // Update Set Leave Quota
            .addCase(updateSetLeaveQuota.pending, (state) => {
                state.loading = true;
            })
            .addCase(updateSetLeaveQuota.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSetLeaveQuota = action.payload;
            })
            .addCase(updateSetLeaveQuota.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSetLeaveQuota = action.payload;
            })

            // View Set Leave Quota
            .addCase(getSetLeaveQuotaPaginate.pending, (state) => {
                state.loading = true;
            })
            .addCase(getSetLeaveQuotaPaginate.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })
            .addCase(getSetLeaveQuotaPaginate.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })

            // Delete Set Leave Quota
            .addCase(deleteSetLeaveQuota.pending, (state) => {
                state.loading = true;
            })
            .addCase(deleteSetLeaveQuota.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSetLeaveQuota = action.payload;
            })
            .addCase(deleteSetLeaveQuota.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataSetLeaveQuota = action.payload;
            })

            // Get Detail Set Leave Quota
            .addCase(getDetailSetLeaveQuota.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDetailSetLeaveQuota.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            })
            .addCase(getDetailSetLeaveQuota.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            });
    },
});

export default setLeaveQuotaSlice.reducer;
