import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { notificationSuccess } from '../../../../../components/Notification';
import appHttpService from '../../../../services/AppService';

export interface resignationBody {
    employee_id: number;
    attachment: File | null;
    submission_date: string;
    last_working_date: string;
    reason: string;
}

interface resignationState {
    loading: boolean;
    message: string;
    data: any | null;
    dataDetail: any | null;
    dataResignation: any | null;
    dataDownload: any | null;
    dataFile: any | null;
}

const initialState: resignationState = {
    loading: false,
    message: '',
    data: null,
    dataDetail: null,
    dataResignation: null,
    dataDownload: null,
    dataFile: null,
};

export const createResignation = createAsyncThunk<any, { body: any }, { rejectValue: string }>('employee-management/resignation/create', async ({ body }, { rejectWithValue }) => {
    try {
        const url = `/employee-resignation/create`;
        const response = await appHttpService.createData(url, body);
        const data = response.data;
        notificationSuccess('success', 'Resignation Successfully Created', '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const updateResignation = createAsyncThunk<any, { body: any; id: any }, { rejectValue: string }>('employee-management/resignation/update', async ({ body, id }, { rejectWithValue }) => {
    try {
        const url = `/employee-resignation/update/${id}`;
        const response = await appHttpService.createData(url, body);
        const data = response.data;
        notificationSuccess('success', 'Resignation Successfully Update', '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getResignationPaginate = createAsyncThunk<any, { search: any; page: any; pageSize: any; sort: any }, { rejectValue: string }>(
    'employee-management/resignation/view',
    async ({ search, page, pageSize, sort }, { rejectWithValue }) => {
        try {
            const searchParams = search === undefined ? '' : search;
            const sortParams = sort === undefined || sort === '' ? 'created_date~desc' : sort;
            const url = `/employee-resignation/view-paging?search=${searchParams}&page=${page}&size=${pageSize}&sort=${sortParams}`;
            const response = await appHttpService.getPagination(url);
            const data = response.data;
            return data;
        } catch (e: any) {
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const deleteResignation = createAsyncThunk<any, { id: any }, { rejectValue: string }>('employee-management/resignation/delete', async ({ id }, { rejectWithValue }) => {
    try {
        const url = `/employee-resignation/delete/${id}`;
        const response = await appHttpService.deleteData(url);
        const data = response.data;
        notificationSuccess('success', `Resignation Successfully Deleted`, '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getDetailResignation = createAsyncThunk<any, { id: any }, { rejectValue: string }>('employee-management/resignation/detail', async (id, { rejectWithValue }) => {
    try {
        const url = `/employee-resignation/view-detail/${id}`;
        const response = await appHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.message as string);
    }
});

export const getDownloadAttachment = createAsyncThunk<any, { id: any; filename: any }, { rejectValue: string }>(
    'employee-management/resignation/attachment',
    async ({ id, filename }, { rejectWithValue }) => {
        try {
            const url = `/employee-resignation/download-attachment/${id}`;
            const response = await appHttpService.downloadData(url, filename);
            const data = response.data;
            return data;
        } catch (e: any) {
            return rejectWithValue(e.response.message as string);
        }
    }
);

const resignationSlice = createSlice({
    name: 'resignation',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            // Create Resignation
            .addCase(createResignation.pending, (state) => {
                state.loading = true;
            })
            .addCase(createResignation.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataResignation = action.payload;
            })
            .addCase(createResignation.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataResignation = action.payload;
            })

            // Update Resignation
            .addCase(updateResignation.pending, (state) => {
                state.loading = true;
            })
            .addCase(updateResignation.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataResignation = action.payload;
            })
            .addCase(updateResignation.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataResignation = action.payload;
            })

            // View Resignation
            .addCase(getResignationPaginate.pending, (state) => {
                state.loading = true;
            })
            .addCase(getResignationPaginate.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })
            .addCase(getResignationPaginate.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })

            // Delete Resignation
            .addCase(deleteResignation.pending, (state) => {
                state.loading = true;
            })
            .addCase(deleteResignation.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataResignation = action.payload;
            })
            .addCase(deleteResignation.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataResignation = action.payload;
            })

            // Get Detail Resignation
            .addCase(getDetailResignation.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDetailResignation.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            })
            .addCase(getDetailResignation.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            })

            // Get Download Attachment Resignation
            .addCase(getDownloadAttachment.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDownloadAttachment.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDownload = action.payload;
            })
            .addCase(getDownloadAttachment.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDownload = action.payload;
            });
    },
});

export default resignationSlice.reducer;
