import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { notificationSuccess } from '../../../../../components/Notification';
import appHttpService from '../../../../services/AppService';

export interface terminationBody {
    employee_id: number;
    attachment: File | null;
    submission_date: string;
    last_working_date: string;
    reason: string;
}

interface terminationState {
    loading: boolean;
    message: string;
    data: any | null;
    dataDetail: any | null;
    dataTermination: any | null;
    dataDownload: any | null;
}

const initialState: terminationState = {
    loading: false,
    message: '',
    data: null,
    dataDetail: null,
    dataTermination: null,
    dataDownload: null,
};

export const createTermination = createAsyncThunk<any, { body: any }, { rejectValue: string }>('employee-management/termination/create', async ({ body }, { rejectWithValue }) => {
    try {
        const url = `/employee-termination/create`;
        const response = await appHttpService.createData(url, body);
        const data = response.data;
        notificationSuccess('success', 'Termination Successfully Created', '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const updateTermination = createAsyncThunk<any, { body: any; id: any }, { rejectValue: string }>('employee-management/termination/update', async ({ body, id }, { rejectWithValue }) => {
    try {
        const url = `/employee-termination/update/${id}`;
        const response = await appHttpService.createData(url, body);
        const data = response.data;
        notificationSuccess('success', 'Termination Successfully Update', '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getTerminationPaginate = createAsyncThunk<any, { search: any; page: any; pageSize: any; sort: any }, { rejectValue: string }>(
    'employee-management/termination/view',
    async ({ search, page, pageSize, sort }, { rejectWithValue }) => {
        try {
            const searchParams = search === undefined ? '' : search;
            const sortParams = sort === undefined || sort === '' ? 'created_date~desc' : sort;
            const url = `/employee-termination/view-paging?search=${searchParams}&page=${page}&size=${pageSize}&sort=${sortParams}`;
            const response = await appHttpService.getPagination(url);
            const data = response.data;
            return data;
        } catch (e: any) {
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const deleteTermination = createAsyncThunk<any, { id: any }, { rejectValue: string }>('employee-management/termination/delete', async ({ id }, { rejectWithValue }) => {
    try {
        const url = `/employee-termination/delete/${id}`;
        const response = await appHttpService.deleteData(url);
        const data = response.data;
        notificationSuccess('success', `Termination Successfully Deleted`, '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getDetailTermination = createAsyncThunk<any, { id: any }, { rejectValue: string }>('employee-management/termination/detail', async (id, { rejectWithValue }) => {
    try {
        const url = `/employee-termination/view-detail/${id}`;
        const response = await appHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.message as string);
    }
});

export const getDownloadAttachment = createAsyncThunk<any, { id: any; filename: any }, { rejectValue: string }>(
    'employee-management/termination/attachment',
    async ({ id, filename }, { rejectWithValue }) => {
        try {
            const url = `/employee-termination/download-attachment/${id}`;
            const response = await appHttpService.downloadData(url, filename);
            const data = response.data;
            return data;
        } catch (e: any) {
            return rejectWithValue(e.response.message as string);
        }
    }
);

const terminationSlice = createSlice({
    name: 'termination',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            // Create Termination
            .addCase(createTermination.pending, (state) => {
                state.loading = true;
            })
            .addCase(createTermination.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataTermination = action.payload;
            })
            .addCase(createTermination.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataTermination = action.payload;
            })

            // Update Termination
            .addCase(updateTermination.pending, (state) => {
                state.loading = true;
            })
            .addCase(updateTermination.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataTermination = action.payload;
            })
            .addCase(updateTermination.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataTermination = action.payload;
            })

            // View Termination
            .addCase(getTerminationPaginate.pending, (state) => {
                state.loading = true;
            })
            .addCase(getTerminationPaginate.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })
            .addCase(getTerminationPaginate.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })

            // Delete Termination
            .addCase(deleteTermination.pending, (state) => {
                state.loading = true;
            })
            .addCase(deleteTermination.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataTermination = action.payload;
            })
            .addCase(deleteTermination.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataTermination = action.payload;
            })

            // Get Detail Termination
            .addCase(getDetailTermination.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDetailTermination.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            })
            .addCase(getDetailTermination.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            })

            // Get Download Attachment Termination
            .addCase(getDownloadAttachment.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDownloadAttachment.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDownload = action.payload;
            })
            .addCase(getDownloadAttachment.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDownload = action.payload;
            });
    },
});

export default terminationSlice.reducer;
