import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { notificationSuccess } from '../../../../../components/Notification';
import appHttpService from '../../../../services/AppService';

export interface timeSheetBody {
    employee_id: number;
    correction_cause: string;
    date: string;
    start_time: string;
    end_time: string;
    activity: string;
    notes: string;
}

interface timeSheetState {
    loading: boolean;
    message: string;
    data: any | null;
    dataDetail: any | null;
    dataTimeSheet: any | null;
}

const initialState: timeSheetState = {
    loading: false,
    message: '',
    data: null,
    dataDetail: null,
    dataTimeSheet: null,
};

export const createTimeSheet = createAsyncThunk<any, { body: timeSheetBody }, { rejectValue: string }>('attendance/time-sheet/create', async ({ body }, { rejectWithValue }) => {
    try {
        const url = `/timesheet/create`;
        const response = await appHttpService.createData(url, body);
        const data = response.data;
        notificationSuccess('success', 'Timesheet Successfully Created', '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const updateTimeSheet = createAsyncThunk<any, { body: timeSheetBody; id: any }, { rejectValue: string }>('attendance/time-sheet/update', async ({ body, id }, { rejectWithValue }) => {
    try {
        const url = `/timesheet/update/${id}`;
        const response = await appHttpService.updateData(url, body);
        const data = response.data;
        notificationSuccess('success', 'Timesheet Successfully Update', '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getTimeSheetPaginate = createAsyncThunk<any, { search: any; page: any; pageSize: any; sort: any }, { rejectValue: string }>(
    'attendance/time-sheet/view',
    async ({ search, page, pageSize, sort }, { rejectWithValue }) => {
        try {
            const searchParams = search === undefined ? '' : search;
            const sortParams = sort === undefined || sort === '' ? 'created_date~desc' : sort;
            const url = `/timesheet/view-paging?search=${searchParams}&page=${page}&size=${pageSize}&sort=${sortParams}`;
            const response = await appHttpService.getPagination(url);
            const data = response.data;
            return data;
        } catch (e: any) {
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const deleteTimeSheet = createAsyncThunk<any, { id: any }, { rejectValue: string }>('attendance/time-sheet/delete', async ({ id }, { rejectWithValue }) => {
    try {
        const url = `/timesheet/delete/${id}`;
        const response = await appHttpService.deleteData(url);
        const data = response.data;
        notificationSuccess('success', `Timesheet Successfully Deleted`, '');
        return data;
    } catch (e: any) {
        notificationSuccess('error', e.response.data.message as string, '');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getDetailTimeSheet = createAsyncThunk<any, { id: any }, { rejectValue: string }>('attendance/time-sheet/detail', async (id, { rejectWithValue }) => {
    try {
        const url = `/timesheet/view-detail/${id}`;
        const response = await appHttpService.getAll(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.message as string);
    }
});

const timeSheetSlice = createSlice({
    name: 'timeSheet',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            // Create TimeSheet
            .addCase(createTimeSheet.pending, (state) => {
                state.loading = true;
            })
            .addCase(createTimeSheet.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataTimeSheet = action.payload;
            })
            .addCase(createTimeSheet.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataTimeSheet = action.payload;
            })

            // Update TimeSheet
            .addCase(updateTimeSheet.pending, (state) => {
                state.loading = true;
            })
            .addCase(updateTimeSheet.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataTimeSheet = action.payload;
            })
            .addCase(updateTimeSheet.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataTimeSheet = action.payload;
            })

            // View TimeSheet
            .addCase(getTimeSheetPaginate.pending, (state) => {
                state.loading = true;
            })
            .addCase(getTimeSheetPaginate.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })
            .addCase(getTimeSheetPaginate.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data = action.payload;
            })

            // Delete TimeSheet
            .addCase(deleteTimeSheet.pending, (state) => {
                state.loading = true;
            })
            .addCase(deleteTimeSheet.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataTimeSheet = action.payload;
            })
            .addCase(deleteTimeSheet.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataTimeSheet = action.payload;
            })

             // Get Detail TimeSheet
             .addCase(getDetailTimeSheet.pending, (state) => {
                state.loading = true;
            })
            .addCase(getDetailTimeSheet.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            })
            .addCase(getDetailTimeSheet.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.dataDetail = action.payload;
            });

    },
});

export default timeSheetSlice.reducer;
