import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import authHttpService from '../../services/AuthService';
import { setData } from '../DataSlice';
import { fetchCsrfToken } from '../../../constant/configApp';
import { showMessage, successMessage } from '../../../components/Notification';

export interface authenticationResponse {
    token: string | unknown;
    sidebar: string | unknown;
    role: string | unknown;
}

export interface authenticationBody {
    username: string;
    password: string;
}
export interface forgotPasswordBody {
    email: string;
}

interface AuthState {
    isLoggedIn: boolean;
    user: any | null;
    forgotPassword: any | null;
    loading: boolean;
    failedRequest: boolean;
    token: string | null;
    side_bar: string | null;
    remember: string | null;
    role: string | null;
    data_access: any | null;
}

const initialState: AuthState = {
    isLoggedIn: false,
    user: null,
    forgotPassword: null,
    loading: false,
    failedRequest: false,
    token: localStorage.getItem('token') || window.sessionStorage.getItem('token'),
    side_bar: localStorage.getItem('sidebar') || window.sessionStorage.getItem('sidebar'),
    remember: localStorage.getItem('remember') || window.sessionStorage.getItem('remember'),
    role: localStorage.getItem('role') || window.sessionStorage.getItem('role'),
    data_access: null,
};

export const login = createAsyncThunk<authenticationResponse, { body: authenticationBody; remember: any }, { rejectValue: string }>(
    'auth/login',
    async ({ body, remember }, { rejectWithValue, dispatch }) => {
        try {
            const url = '/login';
            fetchCsrfToken();
            const response = await authHttpService.login(url, body);
            const data = response.data;

            if (remember) {
                dispatch(setData({ key: 'token', data: data.token, storageType: 'local' }));
                dispatch(setData({ key: 'remember', data: remember.toString(), storageType: 'local' }));
                dispatch(setData({ key: 'sidebar', data: data.sidebar, storageType: 'local' }));
                dispatch(setData({ key: 'role', data: data.role, storageType: 'local' }));
            } else {
                dispatch(setData({ key: 'token', data: data.token, storageType: 'session' }));
                dispatch(setData({ key: 'remember', data: remember.toString(), storageType: 'session' }));
                dispatch(setData({ key: 'sidebar', data: data.sidebar, storageType: 'session' }));
                dispatch(setData({ key: 'role', data: data.role, storageType: 'session' }));
            }
            successMessage('success', response.message);
            return data;
        } catch (e: any) {
            successMessage('error', e.response.data.message as string);
            return rejectWithValue(e.response.data.message as string);
        }
    }
);

export const forgotPassword = createAsyncThunk<authenticationResponse, { body: forgotPasswordBody }, { rejectValue: string }>('auth/forgotPassword', async (body, { rejectWithValue }) => {
    try {
        const url = '/forgotPassword';
        fetchCsrfToken();
        const response = await authHttpService.login(url, body);
        const data: authenticationResponse = response.data;
        showMessage('success', response.message, 'success');
        return data;
    } catch (e: any) {
        showMessage('error', e.response.data.message as string, 'error');
        return rejectWithValue(e.response.data.message as string);
    }
});

export const getAccess = createAsyncThunk<any, { remember: any }, { rejectValue: string }>('auth/grant-access', async ({ remember }, { rejectWithValue, dispatch }) => {
    try {
        const url = '/grant-access';
        const response = await authHttpService.getAll(url);
        const data = response.data;
        if (remember) {
            dispatch(setData({ key: 'data_access', data: data, storageType: 'local' }));
        } else {
            dispatch(setData({ key: 'data_access', data: data, storageType: 'session' }));
        }
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.data.message as string);
    }
});

export const logOut = createAsyncThunk<any, void, { rejectValue: string }>('auth/logout', async (_, { rejectWithValue }) => {
    try {
        const url = '/logout';
        const response = await authHttpService.logout(url);
        const data = response.data;
        return data;
    } catch (e: any) {
        return rejectWithValue(e.response.data.message as string);
    }
});

const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            // Login
            .addCase(login.pending, (state) => {
                state.loading = true;
                state.isLoggedIn = false;
                state.failedRequest = false;
            })
            .addCase(login.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.isLoggedIn = true;
                state.failedRequest = false;
                state.user = action.payload;
                state.token = localStorage.getItem('token') || window.sessionStorage.getItem('token');
                state.side_bar = localStorage.getItem('sidebar') || window.sessionStorage.getItem('sidebar');
                state.remember = localStorage.getItem('remember') || window.sessionStorage.getItem('remember');
                state.role = localStorage.getItem('role') || window.sessionStorage.getItem('role');
            })
            .addCase(login.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.user = action.payload;
                state.failedRequest = true;
            })

            // Logout
            .addCase(logOut.pending, (state) => {
                state.loading = true;
                state.isLoggedIn = false;
                state.failedRequest = false;
            })
            .addCase(logOut.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.isLoggedIn = true;
                state.failedRequest = false;
                state.user = action.payload;
            })
            .addCase(logOut.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.user = action.payload;
                state.failedRequest = true;
            })

            // Forgot Password
            .addCase(forgotPassword.pending, (state) => {
                state.loading = true;
            })
            .addCase(forgotPassword.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.forgotPassword = action.payload;
            })
            .addCase(forgotPassword.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.forgotPassword = action.payload;
            })

            // Get Access
            .addCase(getAccess.pending, (state) => {
                state.loading = true;
            })
            .addCase(getAccess.fulfilled, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.role = localStorage.getItem('data_access') || window.sessionStorage.getItem('data_access');
                state.data_access = action.payload;
            })
            .addCase(getAccess.rejected, (state, action: PayloadAction<any>) => {
                state.loading = false;
                state.data_access = action.payload;
            });
    },
});

export default authSlice.reducer;
