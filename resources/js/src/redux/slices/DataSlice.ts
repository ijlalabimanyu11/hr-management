import { createSlice, PayloadAction } from "@reduxjs/toolkit";

// Define the types for the initial state
interface DataState {
  data: any | null;
}

const initialState: DataState = {
  data: null
};

// Define the types for the action payloads
interface StoragePayload {
  key: string;
  data: any;
  storageType: "local" | "session";
}

const dataSlice = createSlice({
  name: 'data',
  initialState,
  reducers: {
    setData: (state, action: PayloadAction<StoragePayload>) => {
      const { key, data, storageType } = action.payload;
      if (storageType === "local") {
        localStorage.setItem(key, JSON.stringify(data));
        state.data = data;
      } else {
        window.sessionStorage.setItem(key, JSON.stringify(data));
        state.data = data;
      }
    },
    removeData: (state, action: PayloadAction<StoragePayload>) => {
      const { key, storageType } = action.payload;
      if (storageType === "local") {
        localStorage.removeItem(key);
        state.data = null;
      } else {
        window.sessionStorage.removeItem(key);
        state.data = null;
      }
    }
  }
});

export const { setData, removeData } = dataSlice.actions;
export default dataSlice.reducer;
