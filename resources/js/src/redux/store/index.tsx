import { AnyAction, combineReducers, configureStore, ThunkAction } from '@reduxjs/toolkit';
import themeConfigSlice from './themeConfigSlice';
import EmployeeSlice from '../slices/App/EmployeeManagement/Employee/EmployeeSlice';
import GlobalSlice from '../slices/GlobalSlice';
import DataSlice from '../slices/DataSlice';
import AuthSlice from '../slices/Authentication/AuthSlice';
import UserSlice from '../slices/SystemSetup/User/UserSlice';
import RoleAccessSlice from '../slices/SystemSetup/RoleAccess/RoleAccessSlice';
import DataConfigurationSlice from '../slices/SystemSetup/DataConfiguration/DataConfigurationSlice';
import DepartmentSlice from '../slices/SystemSetup/Department/DepartmentSlice';
import PositionSlice from "../slices/SystemSetup/Position/PositionSlice"
import SectionSlice from '../slices/SystemSetup/Section/SectionSlice';
import SystemConfigurationSlice from '../slices/SystemSetup/SystemConfiguration/SystemConfigurationSlice';
import DesignationSlice from "../slices/App/EmployeeManagement/Designation/DesignationSlice"
import ResignationSlice from "../slices/App/EmployeeManagement/Resignation/ResignationSlice"
import TerminationSlice from "../slices/App/EmployeeManagement/Termination/TerminationSlice"
import SetLeaveQuotaSlice from "../slices/App/EmployeeManagement/SetLeaveQuota/SetLeaveQuotaSlice"
import SetReimbursementQuotaSlice from "../slices/App/EmployeeManagement/SetReimbursementQuota/SetReimbursementQuotaSlice"
import TimeSheetSlice from "../slices/App/Attendance/TImeSheet/TimeSheetSlice"
import OrganizationStructureSlice from "../slices/SystemSetup/OrganizationStructure/OrganizationStructureSlice"
import ApprovalConfigurationSlice from "../slices/SystemSetup/ApprovalConfiguration/ApprovalConfigurationSlice"

const rootReducer = combineReducers({
    global: GlobalSlice,
    themeConfig: themeConfigSlice,
    data_slice: DataSlice,

    // Auth
    auth: AuthSlice,

    // App
    employee: EmployeeSlice,
    designation: DesignationSlice,
    resignation: ResignationSlice,
    termination: TerminationSlice,
    set_leave_quota: SetLeaveQuotaSlice,
    set_reimbursement_quota: SetReimbursementQuotaSlice,
    time_sheet: TimeSheetSlice,

    // System Setup
    user: UserSlice,
    role: RoleAccessSlice,
    dataConfiguration: DataConfigurationSlice,
    department: DepartmentSlice,
    section: SectionSlice,
    systemConfiguration: SystemConfigurationSlice,
    position: PositionSlice,
    organizationStructure: OrganizationStructureSlice,
    approvalConfiguration: ApprovalConfigurationSlice

});

const store = configureStore({
    reducer: rootReducer,
    devTools: true,
});

export type RootState = ReturnType<typeof rootReducer>;
export type AppDispatch = typeof store.dispatch;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, AnyAction>;

export default store;
