export const SYSTEM_SETUP_ROUTES = {
    // System Configuration
    SYSTEM_CONFIGURATION_VIEW_ELEMENTS: '/system-configuration',

    // Data Configuration
    DATA_CONFIGURATION_VIEW_ELEMENTS: '/data-configuration',

    // User
    USER_VIEW_ELEMENTS: '/user',
    USER_CREATE_ELEMENTS: '/user/create',
    USER_UPDATE_ELEMENTS: '/user/update',

    // Role Access
    ROLE_ACCESS_VIEW_ELEMENTS: '/role-access',

    // Approval Configuration
    APPROVAL_CONFIGURATION_VIEW_ELEMENTS: '/approval-configuration',
    APPROVAL_CONFIGURATION_CREATE_ELEMENTS: '/approval-configuration/create',
    APPROVAL_CONFIGURATION_UPDATE_ELEMENTS: '/approval-configuration/update',
    APPROVAL_CONFIGURATION_DETAIL_ELEMENTS: '/approval-configuration/detail',
    APPROVAL_WORKFLOW_CREATE_ELEMENTS: '/approval-configuration/detail/approval-workflow/create',
    APPROVAL_WORKFLOW_UPDATE_ELEMENTS: '/approval-configuration/detail/approval-workflow/update',
    APPROVAL_WORKFLOW_DETAIL_ELEMENTS: '/approval-configuration/detail/approval-workflow/detail',

    // Department
    DEPARTMENT_VIEW_ELEMENTS: '/department',
    DEPARTMENT_CREATE_ELEMENTS: '/department/create',
    DEPARTMENT_UPDATE_ELEMENTS: '/department/update',

    // Section
    SECTION_VIEW_ELEMENTS: '/section',
    SECTION_CREATE_ELEMENTS: '/section/create',
    SECTION_UPDATE_ELEMENTS: '/section/update',

    // Position
    POSITION_VIEW_ELEMENTS: '/position',
    POSITION_CREATE_ELEMENTS: '/position/create',
    POSITION_UPDATE_ELEMENTS: '/position/update',

    // Organization Structure
    ORGANIZATION_STRUCTURE_VIEW_ELEMENTS: '/organization-structure',
    ORGANIZATION_STRUCTURE_CREATE_ELEMENTS: '/organization-structure/create',
    ORGANIZATION_STRUCTURE_UPDATE_ELEMENTS: '/organization-structure/update',
    ORGANIZATION_STRUCTURE_CHART_ELEMENTS: '/organization-structure/chart',

    COMPANY_PROFILE_VIEW_ELEMENTS: 'company/profile',
};
