import { CustomRouteObject } from '../../components/Layouts/CustomRoutes';
import { SYSTEM_SETUP_ELEMENTS } from './system_setup_elements';
import { SYSTEM_SETUP_ROUTES } from './system_setup_routes';

export const system_setup: CustomRouteObject[] = [
    // System Configuration
    {
        path: SYSTEM_SETUP_ROUTES.SYSTEM_CONFIGURATION_VIEW_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.SYSTEM_CONFIGURATION_ELEMENTS,
        layout: 'default',
    },

    // Data Configuration
    {
        path: SYSTEM_SETUP_ROUTES.DATA_CONFIGURATION_VIEW_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.DATA_CONFIGURATION_ELEMENTS,
        layout: 'default',
    },

    // User
    {
        path: SYSTEM_SETUP_ROUTES.USER_VIEW_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.USER_VIEW_ELEMENTS,
        layout: 'default',
    },
    {
        path: SYSTEM_SETUP_ROUTES.USER_CREATE_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.USER_CREATE_ELEMENTS,
        layout: 'default',
    },
    {
        path: SYSTEM_SETUP_ROUTES.USER_UPDATE_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.USER_UPDATE_ELEMENTS,
        layout: 'default',
    },

    // Role Access
    {
        path: SYSTEM_SETUP_ROUTES.ROLE_ACCESS_VIEW_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.ROLE_ACCESS_VIEW_ELEMENTS,
        layout: 'default',
    },

    // Approval Configuration
    {
        path: SYSTEM_SETUP_ROUTES.APPROVAL_CONFIGURATION_VIEW_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.APPROVAL_CONFIGURATION_VIEW_ELEMENTS,
        layout: 'default',
    },
    {
        path: SYSTEM_SETUP_ROUTES.APPROVAL_CONFIGURATION_CREATE_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.APPROVAL_CONFIGURATION_CREATE_ELEMENTS,
        layout: 'default',
    },
    {
        path: SYSTEM_SETUP_ROUTES.APPROVAL_CONFIGURATION_UPDATE_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.APPROVAL_CONFIGURATION_UPDATE_ELEMENTS,
        layout: 'default',
    },
    {
        path: SYSTEM_SETUP_ROUTES.APPROVAL_CONFIGURATION_DETAIL_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.APPROVAL_CONFIGURATION_DETAIL_ELEMENTS,
        layout: 'default',
    },
    {
        path: SYSTEM_SETUP_ROUTES.APPROVAL_WORKFLOW_CREATE_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.APPROVAL_WORKFLOW_CREATE_ELEMENTS,
        layout: 'default',
    },
    {
        path: SYSTEM_SETUP_ROUTES.APPROVAL_WORKFLOW_UPDATE_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.APPROVAL_WORKFLOW_UPDATE_ELEMENTS,
        layout: 'default',
    },
    {
        path: SYSTEM_SETUP_ROUTES.APPROVAL_WORKFLOW_DETAIL_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.APPROVAL_WORKFLOW_DETAIL_ELEMENTS,
        layout: 'default',
    },

    // Department
    {
        path: SYSTEM_SETUP_ROUTES.DEPARTMENT_VIEW_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.DEPARTMENT_VIEW_ELEMENTS,
        layout: 'default',
    },
    {
        path: SYSTEM_SETUP_ROUTES.DEPARTMENT_CREATE_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.DEPARTMENT_CREATE_ELEMENTS,
        layout: 'default',
    },
    {
        path: SYSTEM_SETUP_ROUTES.DEPARTMENT_UPDATE_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.DEPARTMENT_UPDATE_ELEMENTS,
        layout: 'default',
    },

    // Section
    {
        path: SYSTEM_SETUP_ROUTES.SECTION_VIEW_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.SECTION_VIEW_ELEMENTS,
        layout: 'default',
    },
    {
        path: SYSTEM_SETUP_ROUTES.SECTION_CREATE_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.SECTION_CREATE_ELEMENTS,
        layout: 'default',
    },
    {
        path: SYSTEM_SETUP_ROUTES.SECTION_UPDATE_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.SECTION_UPDATE_ELEMENTS,
        layout: 'default',
    },

    // Position
    {
        path: SYSTEM_SETUP_ROUTES.POSITION_VIEW_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.POSITION_VIEW_ELEMENTS,
        layout: 'default',
    },
    {
        path: SYSTEM_SETUP_ROUTES.POSITION_CREATE_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.POSITION_CREATE_ELEMENTS,
        layout: 'default',
    },
    {
        path: SYSTEM_SETUP_ROUTES.POSITION_UPDATE_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.POSITION_UPDATE_ELEMENTS,
        layout: 'default',
    },

    // Organization Structure
    {
        path: SYSTEM_SETUP_ROUTES.ORGANIZATION_STRUCTURE_VIEW_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.ORGANIZATION_STRUCTURE_VIEW_ELEMENTS,
        layout: 'default',
    },
    {
        path: SYSTEM_SETUP_ROUTES.ORGANIZATION_STRUCTURE_CREATE_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.ORGANIZATION_STRUCTURE_CREATE_ELEMENTS,
        layout: 'default',
    },
    {
        path: SYSTEM_SETUP_ROUTES.ORGANIZATION_STRUCTURE_UPDATE_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.ORGANIZATION_STRUCTURE_UPDATE_ELEMENTS,
        layout: 'default',
    },
    {
        path: SYSTEM_SETUP_ROUTES.ORGANIZATION_STRUCTURE_CHART_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.ORGANIZATION_STRUCTURE_CHART_ELEMENTS,
        layout: 'default',
    },


    // Company
    {
        path: SYSTEM_SETUP_ROUTES.COMPANY_PROFILE_VIEW_ELEMENTS,
        element: SYSTEM_SETUP_ELEMENTS.COMPANY_PROFILE_ELEMENTS,
        layout: 'default',
    },
];
