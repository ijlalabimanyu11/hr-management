import { lazy } from 'react';
import React from 'react';

// System Configuration
const SystemConfiguration = lazy(() => import('../../app/pages/SystemSetup/SystemConfiguration/SystemConfiguration'));

// Data Configuration
const DataConfiguration = lazy(() => import('../../app/pages/SystemSetup/DataConfiguration/DataConfiguration'));

// User
const UserList = lazy(() => import('../../app/pages/SystemSetup/User/UserList'));
const UserForm = lazy(() => import('../../app/pages/SystemSetup/User/UserForm'));

// Role Access
const RoleAccessList = lazy(() => import('../../app/pages/SystemSetup/RoleAccess/RoleAccessList'));

// Approval Configuration
const ApprovalConfigurationList = lazy(() => import('../../app/pages/SystemSetup/ApprovalConfiguration/ApprovalConfigurationList'));
const ApprovalConfigurationForm = lazy(() => import('../../app/pages/SystemSetup/ApprovalConfiguration/ApprovalConfigurationForm'));
const ApprovalConfigurationDetail = lazy(() => import('../../app/pages/SystemSetup/ApprovalConfiguration/ApprovalConfigurationDetail'));
const ApprovalWorkflowList = lazy(() => import('../../app/pages/SystemSetup/ApprovalConfiguration/ApprovalWorkflow/ApprovalWorkflowList'));
const ApprovalWorkflowForm = lazy(() => import('../../app/pages/SystemSetup/ApprovalConfiguration/ApprovalWorkflow/ApprovalWorkflowForm'));
const ApprovalWorkflowDetail = lazy(() => import('../../app/pages/SystemSetup/ApprovalConfiguration/ApprovalWorkflow/ApprovalWorkflowDetail'));

// Department
const DepartmentList = lazy(() => import('../../app/pages/SystemSetup/Department/DepartmentList'));
const DepartmentForm = lazy(() => import('../../app/pages/SystemSetup/Department/DepartmentForm'));

// Section
const SectionList = lazy(() => import('../../app/pages/SystemSetup/Section/SectionList'));
const SectionForm = lazy(() => import('../../app/pages/SystemSetup/Section/SectionForm'));

// Position
const PositionList = lazy(() => import('../../app/pages/SystemSetup/Position/PositionList'));
const PositionForm = lazy(() => import('../../app/pages/SystemSetup/Position/PositionForm'));

// Organization Structure
const OrganizationStructureList = lazy(() => import('../../app/pages/SystemSetup/OrganizationStructure/OrganizationStructureList'));
const OrganizationStructureForm = lazy(() => import('../../app/pages/SystemSetup/OrganizationStructure/OrganizationStructureForm'));
const OrganizationStructureChart = lazy(() => import('../../app/pages/SystemSetup/OrganizationStructure/OrganizationStructureChart'));

const CompanyProfile = lazy(() => import('../../app/pages/App/Profile'));

export const SYSTEM_SETUP_ELEMENTS = {
    // System Configuration
    SYSTEM_CONFIGURATION_ELEMENTS: <SystemConfiguration />,

    // Data Configuration
    DATA_CONFIGURATION_ELEMENTS: <DataConfiguration />,

    // User
    USER_VIEW_ELEMENTS: <UserList />,
    USER_CREATE_ELEMENTS: <UserForm type="Create" />,
    USER_UPDATE_ELEMENTS: <UserForm type="Update" />,

    // Role Access
    ROLE_ACCESS_VIEW_ELEMENTS: <RoleAccessList />,

    // Approval Configuration
    APPROVAL_CONFIGURATION_VIEW_ELEMENTS: <ApprovalConfigurationList />,
    APPROVAL_CONFIGURATION_CREATE_ELEMENTS: <ApprovalConfigurationForm type="Create" />,
    APPROVAL_CONFIGURATION_UPDATE_ELEMENTS: <ApprovalConfigurationForm type="Update" />,
    APPROVAL_CONFIGURATION_DETAIL_ELEMENTS: <ApprovalConfigurationDetail />,
    APPROVAL_WORKFLOW_CREATE_ELEMENTS: <ApprovalWorkflowForm type="Create" />,
    APPROVAL_WORKFLOW_UPDATE_ELEMENTS: <ApprovalWorkflowForm type="Update" />,
    APPROVAL_WORKFLOW_DETAIL_ELEMENTS: <ApprovalWorkflowDetail />,

    // Department
    DEPARTMENT_VIEW_ELEMENTS: <DepartmentList />,
    DEPARTMENT_CREATE_ELEMENTS: <DepartmentForm type="Create" />,
    DEPARTMENT_UPDATE_ELEMENTS: <DepartmentForm type="Update" />,

    // Section
    SECTION_VIEW_ELEMENTS: <SectionList />,
    SECTION_CREATE_ELEMENTS: <SectionForm type="Create" />,
    SECTION_UPDATE_ELEMENTS: <SectionForm type="Update" />,

    // Position
    POSITION_VIEW_ELEMENTS: <PositionList />,
    POSITION_CREATE_ELEMENTS: <PositionForm type="Create" />,
    POSITION_UPDATE_ELEMENTS: <PositionForm type="Update" />,

    // Organization Structure
    ORGANIZATION_STRUCTURE_VIEW_ELEMENTS: <OrganizationStructureList />,
    ORGANIZATION_STRUCTURE_CREATE_ELEMENTS: <OrganizationStructureForm type="Create" />,
    ORGANIZATION_STRUCTURE_UPDATE_ELEMENTS: <OrganizationStructureForm type="Update" />,
    ORGANIZATION_STRUCTURE_CHART_ELEMENTS: <OrganizationStructureChart />,


    COMPANY_PROFILE_ELEMENTS: <CompanyProfile />,
};
