export const APP_ROUTES = {
    // Employee Management

    /// Employment
    EMPLOYEE_VIEW_ELEMENTS: '/employee-management/employee',
    EMPLOYEE_DETAIL_ELEMENTS: '/employee-management/employee/detail',
    EMPLOYEE_CREATE_ELEMENTS: '/employee-management/employee/create',
    EMPLOYEE_UPDATE_ELEMENTS: '/employee-management/employee/update',

    /// Designation
    DESIGNATION_VIEW_ELEMENTS: '/employee-management/designation',
    DESIGNATION_CREATE_ELEMENTS: '/employee-management/designation/create',
    DESIGNATION_UPDATE_ELEMENTS: '/employee-management/designation/update',

    /// Resignation
    RESIGNATION_VIEW_ELEMENTS: '/employee-management/resignation',
    RESIGNATION_CREATE_ELEMENTS: '/employee-management/resignation/create',
    RESIGNATION_UPDATE_ELEMENTS: '/employee-management/resignation/update',

    /// Termination
    TERMINATION_VIEW_ELEMENTS: '/employee-management/termination',
    TERMINATION_CREATE_ELEMENTS: '/employee-management/termination/create',
    TERMINATION_UPDATE_ELEMENTS: '/employee-management/termination/update',

    /// Set Leave Quota
    SET_LEAVE_QUOTA_VIEW_ELEMENTS: '/employee-management/set-leave-quota',
    SET_LEAVE_QUOTA_CREATE_ELEMENTS: '/employee-management/set-leave-quota/create',
    SET_LEAVE_QUOTA_UPDATE_ELEMENTS: '/employee-management/set-leave-quota/update',

    /// Set Reimbursement Quota
    SET_REIMBURSEMENT_QUOTA_VIEW_ELEMENTS: '/employee-management/set-reimbursement-quota',
    SET_REIMBURSEMENT_QUOTA_CREATE_ELEMENTS: '/employee-management/set-reimbursement-quota/create',
    SET_REIMBURSEMENT_QUOTA_UPDATE_ELEMENTS: '/employee-management/set-reimbursement-quota/update',

    // Attendance

    /// TimeSheet
    TIME_SHEET_VIEW_ELEMENTS: 'attendance/timesheet',
    TIME_SHEET_CREATE_ELEMENTS: 'attendance/timesheet/create',
    TIME_SHEET_UPDATE_ELEMENTS: 'attendance/timesheet/update',

};
