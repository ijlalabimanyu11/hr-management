import { lazy } from 'react';
import React from 'react';

// Employee Management

/// Employee
const EmployeeList = lazy(() => import('../../app/pages/App/EmployeeManagement/Employee/EmployeeList'));
const EmployeeDetail = lazy(() => import('../../app/pages/App/EmployeeManagement/Employee/EmployeeDetail'));
const EmployeeForm = lazy(() => import('../../app/pages/App/EmployeeManagement/Employee/EmployeeForm'));

/// Designation
const DesignationList = lazy(() => import('../../app/pages/App/EmployeeManagement/Designation/DesignationList'));
const DesignationForm = lazy(() => import('../../app/pages/App/EmployeeManagement/Designation/DesignationForm'));

/// Resignation
const ResignationList = lazy(() => import('../../app/pages/App/EmployeeManagement/Resignation/ResignationList'));
const ResignationForm = lazy(() => import('../../app/pages/App/EmployeeManagement/Resignation/ResignationForm'));

/// Termination
const TerminationList = lazy(() => import('../../app/pages/App/EmployeeManagement/Termination/TerminationList'));
const TerminationForm = lazy(() => import('../../app/pages/App/EmployeeManagement/Termination/TerminationForm'));

/// Set Leave Quota
const SetLeaveQuotaList = lazy(() => import('../../app/pages/App/EmployeeManagement/SetLeaveQuota/SetLeaveQuotaList'));
const SetLeaveQuotaForm = lazy(() => import('../../app/pages/App/EmployeeManagement/SetLeaveQuota/SetLeaveQuotaForm'));

/// Set Reimbursement Quota
const SetReimbursementQuotaList = lazy(() => import('../../app/pages/App/EmployeeManagement/SetReimbursementQuota/SetReimbursementQuotaList'));
const SetReimbursementQuotaForm = lazy(() => import('../../app/pages/App/EmployeeManagement/SetReimbursementQuota/SetReimbursementQuotaForm'));

// Attendance

/// TimeSheet
const TimeSheetList = lazy(() => import('../../app/pages/App/Attendance/TimeSheet/TimeSheetList'));
const TimeSheetForm = lazy(() => import('../../app/pages/App/Attendance/TimeSheet/TimeSheetForm'));


export const APP_ELEMENTS = {
    // Employee Management

    // Employee
    EMPLOYEE_VIEW_ELEMENTS: <EmployeeList />,
    EMPLOYEE_DETAIL_ELEMENTS: <EmployeeDetail />,
    EMPLOYEE_CREATE_ELEMENTS: <EmployeeForm type="create" />,
    EMPLOYEE_UPDATE_ELEMENTS: <EmployeeForm type="update" />,

    // Designation
    DESIGNATION_VIEW_ELEMENTS: <DesignationList />,
    DESIGNATION_CREATE_ELEMENTS: <DesignationForm type={"create"} />,
    DESIGNATION_UPDATE_ELEMENTS: <DesignationForm type={"update"} />,

    // Resignation
    RESIGNATION_VIEW_ELEMENTS: <ResignationList />,
    RESIGNATION_CREATE_ELEMENTS: <ResignationForm type={"create"} />,
    RESIGNATION_UPDATE_ELEMENTS: <ResignationForm type={"update"} />,

    // Termination
    TERMINATION_VIEW_ELEMENTS: <TerminationList />,
    TERMINATION_CREATE_ELEMENTS: <TerminationForm type={"create"} />,
    TERMINATION_UPDATE_ELEMENTS: <TerminationForm type={"update"} />,

    // Set Leave Quota
    SET_LEAVE_QUOTA_VIEW_ELEMENTS: <SetLeaveQuotaList />,
    SET_LEAVE_QUOTA_CREATE_ELEMENTS: <SetLeaveQuotaForm type={"create"} />,
    SET_LEAVE_QUOTA_UPDATE_ELEMENTS: <SetLeaveQuotaForm type={"update"} />,

    // Set Reimbursement Quota
    SET_REIMBURSEMENT_QUOTA_VIEW_ELEMENTS: <SetReimbursementQuotaList />,
    SET_REIMBURSEMENT_QUOTA_CREATE_ELEMENTS: <SetReimbursementQuotaForm type={"create"} />,
    SET_REIMBURSEMENT_QUOTA_UPDATE_ELEMENTS: <SetReimbursementQuotaForm type={"update"} />,

    // Attendance

    // TimeSheet
    TIME_SHEET_VIEW_ELEMENTS: <TimeSheetList />,
    TIME_SHEET_CREATE_ELEMENTS: <TimeSheetForm type={"create"} />,
    TIME_SHEET_UPDATE_ELEMENTS: <TimeSheetForm type={"update"} />,


};
