import { CustomRouteObject } from '../../components/Layouts/CustomRoutes';
import { APP_ELEMENTS } from './app_elements';
import { APP_ROUTES } from './app_routes';

export const app: CustomRouteObject[] = [
    // Employee Management

    /// Employment
    {
        path: APP_ROUTES.EMPLOYEE_VIEW_ELEMENTS,
        element: APP_ELEMENTS.EMPLOYEE_VIEW_ELEMENTS,
        layout: 'default',
    },
    {
        path: APP_ROUTES.EMPLOYEE_DETAIL_ELEMENTS,
        element: APP_ELEMENTS.EMPLOYEE_DETAIL_ELEMENTS,
        layout: 'default',
    },
    {
        path: APP_ROUTES.EMPLOYEE_CREATE_ELEMENTS,
        element: APP_ELEMENTS.EMPLOYEE_CREATE_ELEMENTS,
        layout: 'default',
    },
    {
        path: APP_ROUTES.EMPLOYEE_UPDATE_ELEMENTS,
        element: APP_ELEMENTS.EMPLOYEE_UPDATE_ELEMENTS,
        layout: 'default',
    },

    /// Designation
    {
        path: APP_ROUTES.DESIGNATION_VIEW_ELEMENTS,
        element: APP_ELEMENTS.DESIGNATION_VIEW_ELEMENTS,
        layout: 'default',
    },
    {
        path: APP_ROUTES.DESIGNATION_CREATE_ELEMENTS,
        element: APP_ELEMENTS.DESIGNATION_CREATE_ELEMENTS,
        layout: 'default',
    },
    {
        path: APP_ROUTES.DESIGNATION_UPDATE_ELEMENTS,
        element: APP_ELEMENTS.DESIGNATION_UPDATE_ELEMENTS,
        layout: 'default',
    },

    /// Resignation
    {
        path: APP_ROUTES.RESIGNATION_VIEW_ELEMENTS,
        element: APP_ELEMENTS.RESIGNATION_VIEW_ELEMENTS,
        layout: 'default',
    },
    {
        path: APP_ROUTES.RESIGNATION_CREATE_ELEMENTS,
        element: APP_ELEMENTS.RESIGNATION_CREATE_ELEMENTS,
        layout: 'default',
    },
    {
        path: APP_ROUTES.RESIGNATION_UPDATE_ELEMENTS,
        element: APP_ELEMENTS.RESIGNATION_UPDATE_ELEMENTS,
        layout: 'default',
    },

    /// Termination
    {
        path: APP_ROUTES.TERMINATION_VIEW_ELEMENTS,
        element: APP_ELEMENTS.TERMINATION_VIEW_ELEMENTS,
        layout: 'default',
    },
    {
        path: APP_ROUTES.TERMINATION_CREATE_ELEMENTS,
        element: APP_ELEMENTS.TERMINATION_CREATE_ELEMENTS,
        layout: 'default',
    },
    {
        path: APP_ROUTES.TERMINATION_UPDATE_ELEMENTS,
        element: APP_ELEMENTS.TERMINATION_UPDATE_ELEMENTS,
        layout: 'default',
    },

     /// Set Leave Quota
     {
        path: APP_ROUTES.SET_LEAVE_QUOTA_VIEW_ELEMENTS,
        element: APP_ELEMENTS.SET_LEAVE_QUOTA_VIEW_ELEMENTS,
        layout: 'default',
    },
    {
        path: APP_ROUTES.SET_LEAVE_QUOTA_CREATE_ELEMENTS,
        element: APP_ELEMENTS.SET_LEAVE_QUOTA_CREATE_ELEMENTS,
        layout: 'default',
    },
    {
        path: APP_ROUTES.SET_LEAVE_QUOTA_UPDATE_ELEMENTS,
        element: APP_ELEMENTS.SET_LEAVE_QUOTA_UPDATE_ELEMENTS,
        layout: 'default',
    },

    /// Set Reimbursement Quota
    {
        path: APP_ROUTES.SET_REIMBURSEMENT_QUOTA_VIEW_ELEMENTS,
        element: APP_ELEMENTS.SET_REIMBURSEMENT_QUOTA_VIEW_ELEMENTS,
        layout: 'default',
    },
    {
        path: APP_ROUTES.SET_REIMBURSEMENT_QUOTA_CREATE_ELEMENTS,
        element: APP_ELEMENTS.SET_REIMBURSEMENT_QUOTA_CREATE_ELEMENTS,
        layout: 'default',
    },
    {
        path: APP_ROUTES.SET_REIMBURSEMENT_QUOTA_UPDATE_ELEMENTS,
        element: APP_ELEMENTS.SET_REIMBURSEMENT_QUOTA_UPDATE_ELEMENTS,
        layout: 'default',
    },

    // Attendance

    /// TimeSheet
    {
        path: APP_ROUTES.TIME_SHEET_VIEW_ELEMENTS,
        element: APP_ELEMENTS.TIME_SHEET_VIEW_ELEMENTS,
        layout: 'default',
    },
    {
        path: APP_ROUTES.TIME_SHEET_CREATE_ELEMENTS,
        element: APP_ELEMENTS.TIME_SHEET_CREATE_ELEMENTS,
        layout: 'default',
    },
    {
        path: APP_ROUTES.TIME_SHEET_UPDATE_ELEMENTS,
        element: APP_ELEMENTS.TIME_SHEET_UPDATE_ELEMENTS,
        layout: 'default',
    },

];
