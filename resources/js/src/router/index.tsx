import React from 'react';
import { createBrowserRouter, Navigate } from 'react-router-dom';
import BlankLayout from '../components/Layouts/BlankLayout';
import DefaultLayout from '../components/Layouts/DefaultLayout';
import PrivateRoutes from '../components/PrivateRoutes';
import { routes } from './routes';
import { CustomRouteObject } from '../components/Layouts/CustomRoutes';

// Add a new route object for the redirection
const redirectRoute = {
    path: '/',
    element: <Navigate to="/login" replace />,
};


// Map through routes and wrap elements in the appropriate layout and ProtectedRoute where necessary
const finalRoutes = routes.map((route: CustomRouteObject) => {
    const element = route.layout === 'blank' ? <BlankLayout>{route.element}</BlankLayout> : <DefaultLayout>{route.element}</DefaultLayout>;

    return {
        ...route,
        element: route.path === '/login' || route.path === '/forgot-password' || route.path === '*' ? element : <PrivateRoutes element={element} />,
    };
});

// Add the redirect route to the final routes array
finalRoutes.unshift(redirectRoute);

// Create the router with final routes
const router = createBrowserRouter(finalRoutes);

export default router;
