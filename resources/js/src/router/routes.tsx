import React, { lazy } from 'react';
import { CustomRouteObject } from '../components/Layouts/CustomRoutes';
import { app } from './app';
import { system_setup } from './systemSetup';

const DashboardAdmin = lazy(() => import('../app/pages/Main/DashboardAdmin'));
const Login = lazy(() => import('../app/pages/Authentication/Login'));
const ForgotPassword = lazy(() => import('../app/pages/Authentication/ForgotPassword'));
const Error = lazy(() => import('../app/Error404'));

export const publicRoutes: CustomRouteObject[] = [
    {
        path: '/login',
        element: <Login />,
        layout: 'blank',
    },
    {
        path: '/forgot-password',
        element: <ForgotPassword />,
        layout: 'blank',
    },
    {
        path: '*',
        element: <Error />,
        layout: 'blank',
    },
];

export const protectedRoutes: CustomRouteObject[] = [
    {
        path: '/dashboard',
        element: <DashboardAdmin />,
        layout: 'default',
    },
    ...app,
    ...system_setup
    // Add other protected routes here
];

export const routes = [...publicRoutes, ...protectedRoutes];
