export const dateFormatting = {
    dateTime: "DD MMM YYYY HH:mm:ss",
    date: "DD MMM YYYY",
    dateCapital: "DD MMM YYYY",
    dateFormal: "YYYY-MM-DD HH:mm:ss",
    datePeriod: "MMM YYYY",
    meas_date: "DD-MM-YYYY HH:mm:ss",
    hour_format: "HH:mm",
    f_date: "DD-MM-yyyy",
    year_only: "YYYY",
    dateForm: "DD-MM-YYYY",
    period: "YYYY-MM",
    month: "MMM",
  };
