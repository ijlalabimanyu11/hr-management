// Function for handling page changes
export const handleChangePage = (
    pageChange: number,
    pageSizeChange: number,
    currentPageSize: number,
    setPage: (page: number) => void,
    setPageSize: (pageSize: number) => void
) => {
    const tempPage = currentPageSize !== pageSizeChange ? 1 : pageChange;
    setPage(tempPage);
    setPageSize(pageSizeChange);
};

// Function for handling table search
export const handleSearch = (
    selectedKeys: string[],
    confirm: () => void,
    dataIndex: string,
    setSearchText: (callback: (prevState: Record<string, string>) => Record<string, string>) => void,
    setSearchedColumn: (callback: (prevState: string[]) => string[]) => void,
    setSearch: (callback: (prevState: Record<string, string>) => Record<string, string>) => void,
    setPage: (page: number) => void
) => {
    confirm();
    setSearchText((prevState) => ({ ...prevState, [dataIndex]: selectedKeys[0] }));
    setSearchedColumn((prevState) => [...new Set([...prevState, dataIndex])]);
    setSearch((prevState) => {
        const newSearch = { ...prevState, [dataIndex]: selectedKeys[0] };
        if (!selectedKeys[0]) {
            delete newSearch[dataIndex];
        }
        if (prevState[dataIndex] !== selectedKeys[0]) {
            setPage(1);
        }
        return newSearch;
    });
};

export interface SortParams {
    order?: 'ascend' | 'descend';
    field?: string;
}

// Function for handling sorting
export const handleSort = (sort: SortParams, setSort: (sortString: string) => void): void => {
    const dataSort = sort.order && sort.field
        ? `${sort.field}~${sort.order === 'ascend' ? 'asc' : 'desc'}`
        : '';
    setSort(dataSort);
};
