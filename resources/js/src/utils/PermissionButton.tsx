import React from 'react';
import { dataAccess } from './dataAccess';

// Define the props type for the wrapped component
type WithPermissionProps = {
    [key: string]: any; // Adjust as necessary based on the component's props
};

const PermissionButton = (Component: any, menuKey: string, privileges: string[]) => {
    return (props: WithPermissionProps) => {
        const permissions = dataAccess();

        const menu = permissions?.find((p: any) => p['menu-key'] === menuKey);

        const hasPermission = menu?.privilages ? privileges?.some((privilege) => menu?.privilages?.includes(privilege)) : false;

        if (!hasPermission) {
            return null; // Or some fallback UI
        }

        return <Component {...props} />;
    };
};

export default PermissionButton;
