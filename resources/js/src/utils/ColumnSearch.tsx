// ColumnSearch.tsx
import React, { useRef } from 'react';
import { Input, DatePicker, Button, Checkbox, Tooltip } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import Highlighter from 'react-highlight-words';
import moment from 'moment';

interface ColumnSearchProps {
    dataIndex: string;
    searchText: { [key: string]: string };
    searchedColumn: string[];
    handleSearch: (selectedKeys: string[], confirm: () => void, dataIndex: string) => void;
    excludeRender?: boolean;
    typeFilter?: string;
    tooltip?: boolean;
    renderData?: any;
}

const ColumnSearch: React.FC<ColumnSearchProps> = ({ dataIndex, searchText, searchedColumn, handleSearch, excludeRender, typeFilter, tooltip = false, renderData }) => {
    const searchInput = useRef<any>(null);

    const getColumnSearchProps = () => {
        let obj: any = {
            filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }: any) => {
                const onDataChange = (date: moment.Moment | null, dateString: string | string[]) => {
                    setSelectedKeys(dateString ? [dateString.toString()] : []);
                    handleSearch(dateString ? [dateString.toString()] : [], confirm, dataIndex);
                };

                return (
                    <div style={{ padding: 8 }} onKeyDown={(e: React.KeyboardEvent<HTMLDivElement>) => e.stopPropagation()}>
                        {typeFilter === 'date' && <DatePicker onChange={onDataChange} />}
                        {typeFilter === 'dateCapital' && <DatePicker onChange={onDataChange} format={'DD MMM YYYY'} />}
                        {typeFilter === 'datetime' && <DatePicker onChange={onDataChange} showTime />}
                        {typeFilter === 'datePeriod' && <DatePicker onChange={onDataChange} picker="month" />}
                        {typeFilter === 'input' && (
                            <Input
                                ref={searchInput}
                                placeholder={`Search`}
                                value={selectedKeys[0]}
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                                onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                                style={{ marginBottom: 8, display: 'block' }}
                            />
                        )}
                        {typeFilter === 'status' && (
                            <div>
                                <Checkbox
                                    checked={selectedKeys.includes('Active')}
                                    onChange={(e) => {
                                        const value = e.target.checked ? 'Active' : '';
                                        setSelectedKeys(value ? [value] : []);
                                        handleSearch(value ? [value] : [], confirm, dataIndex);
                                    }}
                                >
                                    Active
                                </Checkbox>
                                <Checkbox
                                    checked={selectedKeys.includes('Inactive')}
                                    onChange={(e) => {
                                        const value = e.target.checked ? 'Inactive' : '';
                                        setSelectedKeys(value ? [value] : []);
                                        handleSearch(value ? [value] : [], confirm, dataIndex);
                                    }}
                                >
                                    Inactive
                                </Checkbox>
                            </div>
                        )}
                    </div>
                );
            },
            filterIcon: (filtered: boolean) => <SearchOutlined style={{ color: filtered ? '#1890ff' : '#FEFEFE' }} />,
            onFilterDropdownOpenChange: (visible: boolean) => {
                if (visible) {
                    setTimeout(() => (searchInput.current as HTMLInputElement | null)?.select(), 100);
                }
            },
            // render: (text: string) => {
            //     const isSearched = searchedColumn.includes(dataIndex);

            //     const highlightText = (
            //         <Highlighter highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }} searchWords={[searchText[dataIndex] || '']} autoEscape textToHighlight={text ? text.toString() : ''} />
            //     );

            //     return tooltip ? (
            //         <Tooltip placement="topLeft" title={text}>
            //             {isSearched ? highlightText : text}
            //         </Tooltip>
            //     ) : isSearched ? (
            //         highlightText
            //     ) : (
            //         text
            //     );

            // },
            render: (text: string) => {
                const isSearched = searchedColumn.includes(dataIndex);

                const mappedValue = renderData?.find((item: any) => item.key === text)?.value || text;

                const highlightText = (
                    <Highlighter highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }} searchWords={[searchText[dataIndex] || '']} autoEscape textToHighlight={mappedValue?.toString() || ''} />
                );

                const content = isSearched ? highlightText : mappedValue;

                return tooltip ? (
                    <Tooltip placement="topLeft" title={mappedValue}>
                        {content}
                    </Tooltip>
                ) : (
                    <span>{content}</span>
                );
            },
        };

        if (excludeRender) {
            delete obj.render;
        }

        return obj;
    };

    return getColumnSearchProps();
};

export default ColumnSearch;
