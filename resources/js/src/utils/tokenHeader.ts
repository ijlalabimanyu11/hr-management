export const tokenHeader = (): { Authorization: string | undefined } => {
    const token = JSON.parse(localStorage.getItem('token') || window.sessionStorage.getItem('token') || 'null');
    return { Authorization: `Bearer ${token}` };
};
