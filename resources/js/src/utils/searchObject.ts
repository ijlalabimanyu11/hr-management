export const searchObject = (searchObj: Record<string, string>): string => {
    if (Object.keys(searchObj).length === 0) {
        return ''; // Default value for an empty search
    }
    return Object.entries(searchObj)
        .map(([key, value]) => `${key}~${encodeURIComponent(value)}`)
        .join('%26');
};
