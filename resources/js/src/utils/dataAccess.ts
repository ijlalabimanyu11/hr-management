export const dataAccess = (): any => {
    const item = JSON.parse(localStorage.getItem('data_access') || window.sessionStorage.getItem('data_access') || 'null');
    return item;
};
