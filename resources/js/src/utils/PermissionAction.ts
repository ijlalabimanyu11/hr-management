import { dataAccess } from './dataAccess';

export const PermissionAction = (menuKey: any, privilege: any) => {
    const permissions = dataAccess();
    const menu = permissions?.find((p: any) => p['menu-key'] === menuKey);
    return menu?.privilages?.includes(privilege);
};
