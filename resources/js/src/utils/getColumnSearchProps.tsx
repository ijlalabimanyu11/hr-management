import React from 'react';
import { Input, DatePicker } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import Highlighter from 'react-highlight-words';
import moment from 'moment';

export const getColumnSearchPropsPaging = (
    dataIndex: string,
    searchInput: any,
    searchedColumn: string,
    searchText: string,
    handleSearch: (selectedKeys: string[], confirm: () => void, dataIndex: string) => void,
    excludeRender?: boolean,
    typeFilter?: string
) => {
    let obj: any = {
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm }: any) => {
            const onDataChange = (date: moment.Moment | null, dateString: string | string[]) => {
                setSelectedKeys(dateString ? [dateString.toString()] : []);
                handleSearch(dateString ? [dateString.toString()] : [], confirm, dataIndex);
            };
            return (
                <div
                    style={{
                        padding: 8,
                    }}
                    onKeyDown={(e: React.KeyboardEvent<HTMLDivElement>) => e.stopPropagation()}
                >
                    {typeFilter === 'date' ? <DatePicker onChange={onDataChange} /> : null}
                    {typeFilter === 'dateCapital' ? <DatePicker onChange={onDataChange} format={'DD MMM YYYY'} /> : null}
                    {typeFilter === 'datetime' ? <DatePicker onChange={onDataChange} showTime={true} /> : null}
                    {typeFilter === 'datePeriod' ? <DatePicker onChange={onDataChange} picker="month" /> : null}
                    {typeFilter === 'input' ? (
                        <Input
                            ref={searchInput}
                            placeholder={`Search`}
                            value={selectedKeys[0]}
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                            onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                            style={{
                                marginBottom: 8,
                                display: 'block',
                            }}
                        />
                    ) : null}
                </div>
            );
        },
        filterIcon: (filtered: boolean) => (
            <SearchOutlined
                style={{
                    color: filtered ? '#1890ff' : "#FEFEFE",
                }}
            />
        ),
        onFilterDropdownOpenChange: (visible: boolean) => {
            if (visible) {
                setTimeout(() => (searchInput.current as HTMLInputElement | null)?.select(), 100);
            }
        },
        render: (text: string) =>
            searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{
                        backgroundColor: '#ffc069',
                        padding: 0,
                    }}
                    searchWords={[searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ''}
                />
            ) : (
                text || ''
            ),
    };
    if (excludeRender) {
        delete obj.render;
    }
    return obj;
};
