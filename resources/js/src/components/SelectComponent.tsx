import { Select, SelectProps } from 'antd';
import React from 'react';

// Define the prop types for the SelectComponent
interface SelectComponentProps {
    onChange?: SelectProps<any>['onChange'];
    value?: SelectProps<any>['value'];
    children?: React.ReactNode;
    options?: SelectProps<any>['options'];
    placeholder?: string;
    mode?: SelectProps<any>['mode'];
    tagRender?: SelectProps<any>['tagRender'];
    defaultValue?: SelectProps<any>['defaultValue'];
    allowClear?: boolean;
    disabled?: boolean;
    width?: string | number;
    labelInValue?: boolean;
    onSelect?: SelectProps<any>['onSelect'];
    onDeselect?: SelectProps<any>['onDeselect'];
    onClear?: () => void;
    onPopupScroll?: React.UIEventHandler<HTMLDivElement>;
}

const SelectComponent: React.FC<SelectComponentProps> = ({
    onChange,
    value,
    children,
    options,
    placeholder,
    mode,
    tagRender,
    defaultValue,
    allowClear = true,
    disabled,
    width,
    labelInValue = false,
    onSelect,
    onDeselect,
    onClear,
    onPopupScroll,
}) => {
    const wrapper = 'flex flex-col';
    const style: React.CSSProperties = {
        width: width || 'auto',
        borderRadius: '6px',
        boxShadow: '0 1px 2px 0 rgb(0 0 0 / 0.05)',
    };

    const filterOption = (input: string, option: any) => {
        if (options) {
            return option?.label?.toLowerCase()?.includes(input?.toLowerCase());
        }
        return option?.props?.children?.toLowerCase()?.includes(input?.toLowerCase());
    };

    return (
        <div className={wrapper}>
            <Select
                onPopupScroll={onPopupScroll}
                showSearch
                optionFilterProp="children"
                filterOption={filterOption}
                labelInValue={labelInValue}
                value={value ? value : undefined}
                style={style}
                placeholder={placeholder}
                onChange={onChange}
                mode={mode}
                tagRender={tagRender}
                disabled={disabled}
                allowClear={allowClear}
                defaultValue={defaultValue}
                options={options}
                maxTagCount={mode === 'multiple' ? 'responsive' : undefined}
                onSelect={onSelect}
                onDeselect={onDeselect}
                onClear={onClear}
            >
                {children}
            </Select>
        </div>
    );
};

export default SelectComponent;
