import React from 'react';

interface ButtonProps {
    icon?: any;
    buttonType?: string;
    text?: string;
    type: 'button' | 'submit' | 'reset' | undefined;
    onClick?: () => void;
    disabled?: boolean;
    form?: any
}

const Buttons: React.FC<ButtonProps> = ({ form, icon, type, text, buttonType, onClick, disabled }) => {
    return (
        <div>
            {buttonType === 'secondary' ? (
                <>
                    <button form={form} onClick={onClick} disabled={disabled} type={type} className="btn btn-outline-primary">
                        {icon}
                        {text}
                    </button>
                </>
            ) : buttonType === 'success' ? (
                <>
                    <button form={form} onClick={onClick} disabled={disabled} type={type} className="btn btn-success">
                        {icon}
                        {text}
                    </button>
                </>
            ) : buttonType === 'error' ? (
                <>
                    <button form={form} onClick={onClick} disabled={disabled} type={type} className="btn btn-outline-danger">
                        {icon}
                        {text}
                    </button>
                </>
            ) : buttonType === 'icon' ? (
                <>
                    <button form={form} type={type} onClick={onClick} disabled={disabled} className="btn-icon btn-primary">
                        {icon}
                    </button>
                </>
            ) : (
                <>
                    <button form={form} type={type} onClick={onClick} disabled={disabled} className="btn btn-primary">
                        {icon}
                        {text}
                    </button>
                </>
            )}
        </div>
    );
};

export default Buttons;
