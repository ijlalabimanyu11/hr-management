import React from 'react';
import { Navigate, useLocation } from 'react-router-dom';

interface ProtectedRouteProps {
    element: JSX.Element;
}

const PrivateRoutes: React.FC<ProtectedRouteProps> = ({ element }) => {
    const location = useLocation();
    const isAuthenticated = !!localStorage.getItem('token') || !!sessionStorage.getItem('token');

    if (!isAuthenticated) {
        return <Navigate to="/login" state={{ from: location }} replace />;
    }

    return element;
};

export default PrivateRoutes;
