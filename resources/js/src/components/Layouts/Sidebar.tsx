import React, { useState, useEffect, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { Link, NavLink, useLocation } from 'react-router-dom';
import { toggleSidebar } from '../../redux/store/themeConfigSlice';
import { RootState } from '../../redux/store';
import { Menu, theme } from 'antd';
import PerfectScrollbar from 'react-perfect-scrollbar';
import GlobalIcon from '../../../../../public/assets/images/icon/index';
import type { MenuProps } from 'antd';

const Sidebar = () => {
    // Selector
    const themeConfig = useSelector((state: RootState) => state.themeConfig);
    const semidark = useSelector((state: RootState) => state.themeConfig.semidark);

    // Declaration
    const location = useLocation();
    const dispatch = useDispatch();
    const { t } = useTranslation();
    const getLocation = location.pathname;

    const getSidebarData = (): [] => {
        const sidebarData = localStorage.getItem('sidebar') || window.sessionStorage.getItem('sidebar');
        return sidebarData ? JSON.parse(sidebarData) : null;
    };

    // Use State
    const [active, setActive] = useState<any>([]);
    const [temporaryKeys, setTemporaryKeys] = useState([]);

    // Use Effect
    // useEffect(() => {
    //     const selector = document.querySelector('.sidebar ul a[href="' + window.location.pathname + '"]');
    //     if (selector) {
    //         selector.classList.add('active');
    //         const ul: any = selector.closest('ul.sub-menu');
    //         if (ul) {
    //             let ele: any = ul.closest('li.menu').querySelectorAll('.nav-link') || [];
    //             if (ele.length) {
    //                 ele = ele[0];
    //                 setTimeout(() => {
    //                     ele.click();
    //                 });
    //             }
    //         }
    //     }
    // }, []);

    useEffect(() => {
        if (window.innerWidth < 1024 && themeConfig.sidebar) {
            dispatch(toggleSidebar());
        }
    }, [location]);

    // convert to flat map from tree data
    const extractPaths = useCallback((data: any) => data.flatMap((item: any) => [item, ...(item.children ? extractPaths(item.children) : [])]), []);

    // flat aray from tree data
    const extractPathsKey = useCallback(
        (data: any, parentKey = '') =>
            data.flatMap((item: any) => {
                const uniqueKey = parentKey ? `${parentKey}-${item.key}` : item.key;
                return [{ ...item, key: uniqueKey }, ...(item.children ? extractPathsKey(item.children, uniqueKey) : [])];
            }),
        []
    );

    // find specific parent
    const findMatchingPath = (data: any, currentPath: any) => {
        let bestMatch: any = null;
        const searchTree = (items: any, parentPath = '') => {
            for (const item of items) {
                const fullPath = `${parentPath}${item.path}`;
                if (currentPath.startsWith(fullPath)) {
                    if (!bestMatch || fullPath.length > bestMatch.path.length) {
                        bestMatch = { ...item, path: fullPath };
                    }
                    if (item.children) {
                        searchTree(item.children, fullPath);
                    }
                }
            }
        };

        searchTree(data);
        return bestMatch;
    };

    // filtering path
    const filterMenuByPath = (data: any, targetPath: any) => {
        const filterRecursive = (items: any) => {
            return items
                ?.map((item: any) => {
                    if (item?.path === targetPath) {
                        return item;
                    }
                    if (item.children) {
                        const filteredChildren = filterRecursive(item.children);
                        if (filteredChildren.length > 0) {
                            return { ...item, children: filteredChildren };
                        }
                    }
                    return null;
                })
                .filter((item: any) => item !== null);
        };

        return filterRecursive(data);
    };

    const generateMenuItems = (data: any) => {
        return data?.map((item: any) => {
            if (item.children) {
                return (
                    <Menu.SubMenu key={item?.key} icon={<GlobalIcon name={item?.key} size={20} />} title={<span>{item.label}</span>}>
                        {generateMenuItems(item.children)}
                    </Menu.SubMenu>
                );
            } else {
                return (
                    <Menu.Item key={item?.key} icon={<GlobalIcon name={item?.key} size={20} />}>
                        <Link to={item.path}>{item.label}</Link>
                    </Menu.Item>
                );
            }
        });
    };

    useEffect(() => {
        const activeKeys = filterMenuByPath(getSidebarData(), getLocation);

        // set temporary if user in the form page or detail
        if (activeKeys?.length === 0) {
            const keys = extractPathsKey(getSidebarData());
            const findMatching = findMatchingPath(keys, getLocation);
            const parentKey = filterMenuByPath(getSidebarData(), findMatching?.path);
            const extractingKeys = extractPaths(parentKey)?.map((item: any) => item?.key);
            setTemporaryKeys(extractingKeys);
        }
        setActive(extractPaths(activeKeys).map((item: any) => item.key));
    }, [getLocation]);

    // render props if collapse
    const renderProps = () => {
        if (themeConfig.sidebar) {
            return {
                defaultOpenKeys: active?.length === 0 ? temporaryKeys : active,
            };
        } else {
            return {
                openKeys: active?.length === 0 ? temporaryKeys : active,
                onOpenChange: (keys: any) => {
                    if (active?.length === 0) {
                        setTemporaryKeys(keys);
                    } else {
                        setActive(keys);
                    }
                },
            };
        }
    };

    return (
        <div className={semidark ? 'dark' : ''}>
            <nav
                className={`sidebar fixed min-h-screen h-full top-0 bottom-0 w-[260px] shadow-[5px_0_25px_0_rgba(94,92,154,0.1)] z-50 transition-all duration-300 ${semidark ? 'text-white-dark' : ''}`}
            >
                <div className="bg-white dark:bg-black h-full">
                    <div className="flex justify-between items-center px-4 py-3">
                        <NavLink to="/" className="main-logo flex items-center shrink-0">
                            <img className="w-8 ml-[5px] flex-none" src="/assets/images/logo.svg" alt="logo" />
                            <span className="text-2xl ltr:ml-1.5 rtl:mr-1.5 font-semibold align-middle lg:inline dark:text-white-light">{t('HRIS')}</span>
                        </NavLink>

                        <button
                            type="button"
                            className="collapse-icon w-8 h-8 rounded-full flex items-center hover:bg-gray-500/10 dark:hover:bg-dark-light/10 dark:text-white-light transition duration-300 rtl:rotate-180"
                            onClick={() => dispatch(toggleSidebar())}
                        >
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" className="w-5 h-5 m-auto">
                                <path d="M13 19L7 12L13 5" stroke="currentColor" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                                <path opacity="0.5" d="M16.9998 19L10.9998 12L16.9998 5" stroke="currentColor" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                            </svg>
                        </button>
                    </div>

                    <PerfectScrollbar className="h-[calc(100vh-80px)] relative">
                        <Menu theme="light" mode="inline" selectedKeys={active?.length === 0 ? temporaryKeys : active} className={'mb-6'} {...renderProps()}>
                            {getSidebarData().map((group: any) => (
                                <Menu.ItemGroup key={group.key} title={group.label}>
                                    {generateMenuItems(group.children)}
                                </Menu.ItemGroup>
                            ))}
                        </Menu>
                    </PerfectScrollbar>
                </div>
            </nav>
        </div>
    );
};

export default Sidebar;
