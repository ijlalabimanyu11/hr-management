import { RouteObject } from 'react-router-dom';

export type CustomRouteObject = RouteObject & {
    layout?: 'blank' | 'default';
};
