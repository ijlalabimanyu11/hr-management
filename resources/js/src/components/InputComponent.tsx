import { Input, InputNumber } from 'antd';
import React from 'react';

interface InputProps {
    type?: string;
    typeNumber?: any;
    maxLength?: number;
    value?: any;
    placeholder?: string;
    suffix?: any;
    prefix?: any;
    disabled?: boolean;
    rows?: number;
    styleGroup?: any;
    size?: any;
    iconRender?: any;
    onChange?: (e: any) => any;
    onClick?: (e: any) => any;
    onInput?: (e: any) => any;
}

const InputComponent: React.FC<InputProps> = ({ iconRender, size, type, typeNumber, maxLength, value, placeholder, suffix, prefix, disabled, rows, styleGroup, onChange, onClick, onInput }) => {
    const className = 'relative placeholder:text-white-dark';
    return (
        <div>
            {type === 'textarea' ? (
                <Input.TextArea
                size={size}
                    className={className}
                    style={styleGroup}
                    onChange={onChange}
                    value={value}
                    placeholder={placeholder}
                    prefix={prefix}
                    disabled={disabled}
                    onClick={onClick}
                    allowClear
                    maxLength={255}
                    onInput={onInput}
                    showCount
                    rows={rows ? rows : 5}
                />
            ) : type === 'Password' ? (
                <Input.Password size={size} prefix={prefix} placeholder={placeholder} className={className} allowClear maxLength={maxLength} iconRender={iconRender} />
            ) : type === 'confirm-password' ? (
                <Input.Password disabled={disabled} />
            ) : type === 'number' ? (
                <InputNumber
                    style={{ width: '100%' }}
                    size={size}
                    controls={false}
                    placeholder={placeholder}
                    disabled={disabled}
                    prefix={prefix}
                    suffix={suffix}
                    value={value}
                    onChange={onChange}
                    maxLength={maxLength}
                />
            ) : (
                <Input
                    size={size}
                    type={typeNumber}
                    className={className}
                    style={styleGroup}
                    onChange={onChange}
                    value={value}
                    placeholder={placeholder}
                    prefix={prefix}
                    suffix={suffix}
                    disabled={disabled}
                    onClick={onClick}
                    allowClear
                    maxLength={maxLength}
                    onInput={onInput}
                />
            )}
        </div>
    );
};

export default InputComponent;
