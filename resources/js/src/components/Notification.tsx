import { notification } from 'antd';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

type NotificationType = 'success' | 'info' | 'warning' | 'error';

const showMessage = (color: any, title: string, icon?: any): any => {
    const toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        // timer: 3000,
        // customClass: {
        //     popup: `color-${color}`,
        // },
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.onmouseenter = Swal.stopTimer;
            toast.onmouseleave = Swal.resumeTimer;
        },
    });
    toast.fire({
        title: title,
        icon: icon,
        width: 'auto',
    });
};

const notificationSuccess = (icon: any, title: string, text: string): any => {
    Swal.fire({
        icon: icon,
        title: title,
        text: text,
        padding: '2em',
        customClass: 'sweet-alerts',
    });
};

const notificationSuccessWithBack = (icon: any, title: string, text: string, navigate: (to: any) => void): any => {
    Swal.fire({
        icon: icon,
        title: title,
        text: text,
        padding: '2em',
        customClass: 'sweet-alerts',
    }).then((result) => {
        if (result.isConfirmed) {
            navigate(-1);
        }
    });
};

const successMessage = (type: NotificationType, message: string) => {
    notification[type]({
        message,
        style: {
            width: 'auto',
        },
    });
};

export { showMessage, notificationSuccess, notificationSuccessWithBack, successMessage };
