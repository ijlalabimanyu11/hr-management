import React, { useEffect } from 'react';
import Swal, { SweetAlertOptions } from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import 'sweetalert2/dist/sweetalert2.min.css';

const swalButtons = withReactContent(
    Swal.mixin({
        customClass: {
            confirmButton: 'btn-modal-confirm btn-primary-new',
            cancelButton: 'btn-modal-confirm btn-outline-danger',
        },
        buttonsStyling: false,
    })
);

interface ModalConfirmProps {
    title: string;
    icon: 'warning' | 'error' | 'success' | 'info' | 'question';
    showCancelButton?: boolean;
    confirmButtonText?: string;
    cancelButtonText?: string;
    reverseButtons?: boolean;
    onConfirm?: () => void;
    onCancel?: () => void;
}

const ModalConfirm: React.FC<ModalConfirmProps> = ({
    title,
    icon,
    showCancelButton = true,
    confirmButtonText = 'Yes, sure',
    cancelButtonText = 'No, cancel',
    reverseButtons = true,
    onConfirm,
    onCancel,
}) => {
    swalButtons
        .fire({
            title,
            icon,
            showCancelButton,
            confirmButtonText,
            cancelButtonText,
            reverseButtons,
        })
        .then((result) => {
            if (result.isConfirmed) {
                if (onConfirm) onConfirm();
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                if (onCancel) onCancel();
            }
        });

    return null;
};

export default ModalConfirm;
