import { Select, Table } from 'antd';
import React, { useEffect, useState } from 'react';

const { Option } = Select;

interface Column {
    title: string;
    // Add any other properties here as needed
}

interface RowSelection {
    // Define the row selection props if needed
}

interface TablePaginationProps {
    idTable?: string;
    dataSource: any[];
    columns: any[];
    pageSize: number;
    current?: number;
    loading?: boolean;
    onChange?: (pagination: any, filters: any, sorter: any, extra: any) => void;
    onSizeChanger?: (current: number, pageSize: number) => void;
    totalData: number;
    onDelete?: (index: number) => void;
    rowSelection?: RowSelection;
    onRowClicked?: () => void;
    tableScrolled?: any;
    expandable?: any;
    className?: string;
    useSelect?: boolean;
    usePagination?: boolean;
    onSort?: (_: any, __: any, sort: any) => void;
    type?: string;
    content?: any;
    filterData?: any;
}

const TablePagination: React.FC<TablePaginationProps> = ({
    idTable,
    dataSource,
    columns,
    pageSize,
    current,
    loading,
    onChange = () => {},
    totalData,
    rowSelection,
    tableScrolled,
    expandable,
    className,
    useSelect = true,
    usePagination = true,
    onSort = () => {},
    type = 'BE',
    content,
    filterData,
}) => {
    const [optionSelectedCol, setOptionSelectedCol] = useState<string[]>([]);
    const [totalDataFE, setTotalDataFE] = useState(0);
    const [isSmallScreen, setIsSmallScreen] = useState(false);

    useEffect(() => {
        const handleResize = () => {
            setIsSmallScreen(window.innerWidth < 1024);
        };

        // Initial check
        handleResize();

        // Add event listener
        window.addEventListener('resize', handleResize);

        // Clean up event listener on component unmount
        return () => window.removeEventListener('resize', handleResize);
    }, []);

    useEffect(() => {
        if (type !== 'BE') {
            setTotalDataFE(dataSource?.length || 0);
        }
    }, [type, dataSource]);

    const handleDisplayColumn = (value: string[]) => {
        setOptionSelectedCol(value);
    };

    const filterColumns = () => {
        return columns?.filter((col) => {
            return !optionSelectedCol.includes(col.title);
        });
    };

    const onChangeFE = (_: any, __: any, ___: any, extra: any) => {
        setTotalDataFE(extra?.currentDataSource?.length || 0);
    };

    return (
        <div
        >
            {useSelect && (
                <div className={`${(type === 'BE' ? totalData : totalDataFE) !== 0 ? 'mb-4' : 'my-4'} w-full flex justify-between items-center`}>
                    <div className={`${isSmallScreen ? 'w-1/2' : 'w-1/3'}`}>
                        <Select mode="multiple" placeholder="Show All Column" className={'w-full'} maxTagCount={'responsive'} onChange={handleDisplayColumn}>
                            {columns
                                ?.filter((col) => col.title !== 'No' && col.title !== 'Action')
                                ?.map((col) => (
                                    <Option key={col.title} value={col.title} disabled={optionSelectedCol.length > 3 ? (optionSelectedCol.includes(col.title) ? false : true) : false}>
                                        {col.title}
                                    </Option>
                                ))}
                        </Select>
                    </div>

                    <div>{content}</div>
                </div>
            )}

            {filterData}

            <div className={`${isSmallScreen ? 'overflow-x-auto' : ''}`}>
                <div className={`inline-block ${isSmallScreen ? 'min-w-[100px]' : 'w-full'}`}>
                    <Table
                        dataSource={dataSource}
                        columns={[...filterColumns()]}
                        scroll={tableScrolled}
                        // bordered
                        pagination={
                            !usePagination
                                ? false
                                : {
                                      current: current,
                                      pageSize: pageSize,
                                      total: type === 'BE' ? totalData : undefined,
                                      //   className: 'pr-1 w-full md:w-auto ml-auto mr-0',
                                      showSizeChanger: true,
                                      showTotal: (total) => `Total ${total} items`,
                                      onChange: onChange as (page: number, pageSize?: number | undefined) => void,
                                  }
                        }
                        // className={`w-full ${className || ''}`}
                        loading={loading}
                        tableLayout="fixed"
                        expandable={expandable}
                        // id={idTable}
                        onChange={type === 'BE' ? onSort : onChangeFE}
                        rowSelection={rowSelection}
                    />
                </div>
            </div>
        </div>
    );
};

export default TablePagination;
