import React from 'react';
import { NavLink } from 'react-router-dom';
import { Breadcrumb } from 'antd';

interface Route {
    href?: string;
    title: string;
}

interface BreadcrumbProps {
    routes: Route[];
}

const Breadcrumbs: React.FC<BreadcrumbProps> = ({ routes }) => {
    const itemRender = (route: Route) => {
        return route.href ? <NavLink to={route.href}>{route.title}</NavLink> : route.title;
    };

    return (
        <Breadcrumb separator={'/'} className='pb-5'>
            {routes.map((route, index) => (
                <Breadcrumb.Item key={index}>{itemRender(route)}</Breadcrumb.Item>
            ))}
        </Breadcrumb>
    );
};

export default Breadcrumbs;
