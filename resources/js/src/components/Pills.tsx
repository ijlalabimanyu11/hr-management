import React, { useEffect, useState } from 'react';

interface PillsProps {
    children?: any;
    color?: any;
    weight?: any;
    height?: any;
}

const Pills: React.FC<PillsProps> = ({ children, color, weight, height}) => {
    const [bgColor, setBgColor] = useState('');
    const [colorText, setColorText] = useState('');

    useEffect(() => {
        if (color) {
            switch (color.toLowerCase()) {
                case 'active':
                    setBgColor('bg-[#14A38B]');
                    setColorText('text[#FFFFFF]');
                    break;
                case 'inactive':
                    setBgColor('bg-[#FF4684]');
                    setColorText('text[#FFFFFF]');
                    break;

                default:
                    setBgColor('bg-[#557AF3]');
                    setColorText('text[#FFFFFF]');
                    break;
            }
        }
    }, [color]);
    return (
        <div>
            <p className={`text-white normal-case ${bgColor} ${weight ? `w-[${weight}]` : `w-[84px]`} ${height ? `h-[${height}]` : `h-[28px]`} my-0 py-1 px-2 rounded-[20px] text-center`}>
                {children}
            </p>
        </div>
    );
};

export default Pills;
