import { Tooltip } from 'antd';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom'; // Assuming you're using react-router for navigation

interface ActionButton {
    type: 'detail' | 'update' | 'delete' | 'active' | 'inactive' | 'reset password' | 'download';
    icon: React.ReactNode;
    onClick?: (record?: any) => void;
    link?: string;
    record?: any;
    state?: any;
}

interface ActionProps {
    actions: ActionButton[] | any;
}

const backgroundColor = (type: 'detail' | 'update' | 'delete' | 'active' | 'inactive' | 'reset password' | 'download'): string => {
    switch (type) {
        case 'detail':
        case 'update':
        case 'reset password':
        case 'download':
            return 'bg-[#557AF3]';
        case 'delete':
        case 'inactive':
            return 'bg-[#FF4684]';
        case 'active':
            return 'bg-[#14A38B]';
        default:
            return 'bg-gray-500';
    }
};

const handleText = (index: any) => {
    let text: any;
    switch (index) {
        case 'reset password':
            text = 'Reset Password';
            break;
        default:
            text = index ? index?.charAt(0)?.toUpperCase() + index?.slice(1)?.toLowerCase() : index;
            break;
    }
    return text;
};

const Action: React.FC<ActionProps> = ({ actions }) => {
    return (
        <div className="flex w-full justify-center">
            {actions.map((action: any) => {
                const buttonColor = backgroundColor(action.type);
                return (
                    <div key={action.type}>
                        {action.link ? (
                            <Tooltip title={handleText(action.type)}>
                                <Link to={action.link} state={action.state}>
                                    <div className="p-1">
                                        <button
                                            type="button"
                                            className={`flex items-center justify-center w-[29px] h-[29px] rounded-lg text-white ${buttonColor}`}
                                            onClick={() => action.onClick?.(action.record)}
                                        >
                                            {action.icon}
                                        </button>
                                    </div>
                                </Link>
                            </Tooltip>
                        ) : (
                            <Tooltip title={handleText(action.type)}>
                                <div className="p-1">
                                    <button
                                        type="button"
                                        className={`flex items-center justify-center w-[29px] h-[29px] rounded-lg text-white ${buttonColor}`}
                                        onClick={() => action.onClick?.(action.record)}
                                    >
                                        {action.icon}
                                    </button>
                                </div>
                            </Tooltip>
                        )}
                    </div>
                );
            })}
        </div>
    );
};

export default Action;
