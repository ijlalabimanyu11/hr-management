import React from 'react';
import { RightOutlined } from '@ant-design/icons';
import { Badge } from 'antd';

interface CardProps {
    title: string;
    isActive: boolean;
    onClick: () => void;
    errorCount?: number;
}

interface FrameProps {
    activeIndex: number;
    setActiveIndex: (index: number) => void;
    data: any;
    fieldErrors?: any;
}

const TitleList: React.FC<CardProps> = ({ title, isActive, onClick, errorCount = 0 }) => {
    return (
        <div
            className={`my-1 flex flex-col justify-center px-4 py-3 w-full cursor-pointer ${isActive ? 'bg-[#557AF3]' : 'bg-white'}
        rounded-lg ${isActive ? 'text-white' : 'text-[#343C6A]'} ${isActive ? '' : 'hover:bg-[#E7FFFF]'}`}
            onClick={onClick}
        >
            <div className="flex gap-5 justify-between py-1 relative">
                <div className="text-sm truncate">{title}</div>
                {errorCount > 0 && (
                    <Badge
                        count={errorCount}
                        style={{
                            backgroundColor: '#f5222d',
                            position: 'absolute',
                            right: '-10px',
                            top: '0',
                        }}
                    />
                )}
                <RightOutlined style={{ color: isActive ? '#FFFFFF' : '#343C6A', fontSize: '12px' }} />
            </div>
        </div>
    );
};

export const Frame: React.FC<FrameProps> = ({ activeIndex, setActiveIndex, data, fieldErrors }): JSX.Element => {
    const getErrorCount = (section: string) => {
        const sectionErrors = fieldErrors?.[section];
        if (!sectionErrors) return 0;
        return Object.values(sectionErrors)?.reduce((acc: any, count: any) => acc + count, 0);
    };
    return (
        <div className="flex flex-col text-lg font-semibold">
            {data?.map((value: any, index: any) => {
                let errorCounts: any = 0;
                if (value.key === 0) {
                    errorCounts = getErrorCount('basicInfo');
                } else if (value.key === 1) {
                    errorCounts = getErrorCount('bankAccountDetail');
                } else if (value.key === 2) {
                    errorCounts = getErrorCount('documentCV');
                }

                return errorCounts > 0 ? (
                    <TitleList key={index} title={value.value} isActive={value.key === activeIndex} onClick={() => setActiveIndex(value.key)} errorCount={errorCounts} />
                ) : (
                    <TitleList key={index} title={value.value} isActive={value.key === activeIndex} onClick={() => setActiveIndex(value.key)} />
                );
            })}
        </div>
    );
};

export default Frame;
