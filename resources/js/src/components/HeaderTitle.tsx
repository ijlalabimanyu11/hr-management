import React from 'react';

interface TitleProps {
    title: string;
    color?: boolean;
}

const HeaderTitle: React.FC<TitleProps> = ({ title, color }) => {
    return (
        <div>
            {color ? (
                <div className="relative">
                    <h5 className="relative font-semibold text-lg h5">{title}</h5>
                    <hr className='mb-6 mt-1'/>
                </div>
            ) : (
                <h5 className="font-semibold text-lg text-[#343c6a]">{title}</h5>
            )}
        </div>
    );
};

export default HeaderTitle;
