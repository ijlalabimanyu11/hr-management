import React, { ReactNode } from 'react';

interface CardsProps {
    children?: ReactNode;
    width?: any;
    type?: any;
    className?: any
}

const Cards: React.FC<CardsProps> = ({ children, width = 'w-full', type, className }) => {
    return (
        <div className={`my-5 flex items-start justify-start ${className}`}>
            <div className={`${width} bg-white shadow-[4px_6px_10px_-3px_#bfc9d4] rounded border border-white-light dark:border-[#1b2e4b] dark:bg-[#191e3a] dark:shadow-none`}>
                {type === 'card' ? <div className="py-7 px-2">{children}</div> : <div className="py-7 px-6">{children}</div>}
            </div>
        </div>
    );
};

export default Cards;
