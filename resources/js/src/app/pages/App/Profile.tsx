import React, { Fragment, useEffect, useState } from 'react';
import { SYSTEM_SETUP_ROUTES } from '../../../router/systemSetup/system_setup_routes';
import { setPageTitle } from '../../../redux/store/themeConfigSlice';
import { useDispatch } from 'react-redux';
import { AppDispatch } from '../../../redux/store';
import Breadcrumbs from '../../../components/Breadcrumbs';
import HeaderTitle from '../../../components/HeaderTitle';
import Cards from '../../../components/Cards';
import { Button, Form } from 'antd';
import InputComponent from '../../../components/InputComponent';

const Profile = () => {
    // Selector

    // Declaration
    const dispatch = useDispatch<AppDispatch>();
    const [form] = Form.useForm();

    // Use State
    const [flag, setFlag] = useState<boolean>(false);

    // Use Effect
    useEffect(() => {
        dispatch(setPageTitle('Company Profile'));
    });

    // Breadcrumbs
    const routes = [
        {
            title: 'System Setup',
            href: '',
        },
        {
            title: 'Company',
            href: '',
        },
        {
            title: 'Profile',
            href: '',
        },
    ];


    // Handle Cancel
    const handleCancel = () => {
        setFlag(false)
        form.resetFields()
    }

    const handleSubmit = (formValue?: any) => {
        console.log("in");

    };

    // handle Update Data
    const handleUpdateData = () => {
        if (!flag) {
          setFlag(true);
        } else {
          handleSubmit();
        }
      };
    return (
        <Fragment>
            <Breadcrumbs routes={routes} />
            <div className="flex w-full justify-between align-middle">
                <HeaderTitle title="Company Profile" />
            </div>

            <Cards>
                <Form id="form" layout="vertical" form={form} onFinish={handleSubmit} name="dynamic_rule">
                    <div className="w-full grid grid-cols-3 gap-3">
                        <Form.Item
                            label="Company Name"
                            name={'companyName'}
                            rules={[
                                {
                                    required: flag,
                                    message: 'Please input your Company Name!',
                                },
                            ]}
                        >
                            <InputComponent disabled={!flag} />
                        </Form.Item>

                        <Form.Item
                            label="Address"
                            name={'address'}
                            rules={[
                                {
                                    required: flag,
                                    message: 'Please input your Address!',
                                },
                            ]}
                        >
                            <InputComponent disabled={!flag}/>
                        </Form.Item>

                        <Form.Item
                            label="City"
                            name={'city'}
                            rules={[
                                {
                                    required: flag,
                                    message: 'Please input your City!',
                                },
                            ]}
                        >
                            <InputComponent disabled={!flag}/>
                        </Form.Item>

                        <Form.Item
                            label="Province"
                            name={'province'}
                            rules={[
                                {
                                    required: flag,
                                    message: 'Please input your Province!',
                                },
                            ]}
                        >
                            <InputComponent disabled={!flag}/>
                        </Form.Item>

                        <Form.Item
                            label="Zip/Post Code"
                            name={'postCode'}
                            rules={[
                                {
                                    required: flag,
                                    message: 'Please input your Zip/Post Code!',
                                },
                            ]}
                        >
                            <InputComponent disabled={!flag}/>
                        </Form.Item>

                        <Form.Item
                            label="Country"
                            name={'country'}
                            rules={[
                                {
                                    required: flag,
                                    message: 'Please input your Country!',
                                },
                            ]}
                        >
                            <InputComponent disabled={!flag}/>
                        </Form.Item>

                        <Form.Item
                            label="Phone"
                            name={'phone'}
                            rules={[
                                {
                                    required: flag,
                                    message: 'Please input your Phone!',
                                },
                            ]}
                        >
                            <InputComponent disabled={!flag}/>
                        </Form.Item>
                    </div>

                    <Form.Item>
                        <div className="w-full flex justify-end gap-4">
                            {flag ? (
                                <Button onClick={handleCancel} htmlType="button" danger className="btn btn-outline-danger">
                                    Cancel
                                </Button>
                            ) : null}
                            <Button htmlType="submit" type="primary" className="btn btn-primary-new" onClick={handleUpdateData}>
                               {flag ? "Save" : "Update"}
                            </Button>
                        </div>
                    </Form.Item>
                </Form>
            </Cards>
        </Fragment>
    );
};

export default Profile;
