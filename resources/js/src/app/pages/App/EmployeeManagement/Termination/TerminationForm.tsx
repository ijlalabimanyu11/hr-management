import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../../../redux/store';
import { Button, DatePicker, Form, Input, message, Modal, Select, Space, Upload } from 'antd';
import { getEmployee } from '../../../../../redux/slices/GlobalSlice';
import moment from 'moment';
import { createTermination, terminationBody, updateTermination } from '../../../../../redux/slices/App/EmployeeManagement/Termination/TerminationSlice';
import InputComponent from '../../../../../components/InputComponent';
import SelectComponent from '../../../../../components/SelectComponent';
import dayjs from 'dayjs';

interface TerminationFormProps {
    type: any;
    openModal?: any;
    cancelModal?: any;
    refreshPage?: any;
    dataUpdate?: any;
    dataFile?: any;
}

const TerminationForm: React.FC<TerminationFormProps> = ({ type, openModal, cancelModal, refreshPage, dataUpdate, dataFile }) => {
    // Selector
    const { dataEmployee } = useSelector((state: RootState) => state.global);

    // Declaration
    const dispatch = useDispatch<AppDispatch>();
    const [form] = Form.useForm();

    // Use State
    const [file, setFile] = useState<File | null>(null);

    // Use Effect
    useEffect(() => {
        dispatch(getEmployee());
    }, [dispatch]);

    useEffect(() => {
        if (type === 'Update' && dataUpdate) {
            form.setFieldsValue({
                id: dataUpdate?.id,
                employee_id: dataUpdate?.employee_id,
                reason: dataUpdate?.reason,
                submission_date: dayjs(dataUpdate?.submission_date),
                last_working_date: dayjs(dataUpdate?.last_working_date),
                attachment: dataFile
            });
            setFile(dataFile);
        }
    }, [type, form, dataUpdate, dataFile]);

    // Function to handle file change
    const handleFileChange = ({ file }) => {
        setFile(file);
    };

     // Props Upload
     const props = {
        beforeUpload: (file: any) => {
            const isLt10M = file.size / 1024 / 1024 < 10;
            if (!isLt10M) {
                message.error(`File must be smaller than 10 MB!`);
            }
            return isLt10M || Upload.LIST_IGNORE;
        },
    };

    // Helper function to convert data to FormData
    const createFormData = (data: any): FormData => {
        const formData = new FormData();
        formData.append('employee_id', data.employee_id);
        formData.append('attachment', data.attachment);
        formData.append('submission_date', data.submission_date);
        formData.append('last_working_date', data.last_working_date);
        formData.append('reason', data.reason);
        return formData;
    };

    const handleSubmit = (formValue: any) => {
        const data: terminationBody = {
            employee_id: formValue.employee_id,
            attachment: file,
            submission_date: dayjs(formValue.submission_date).format('YYYY-MM-DD'),
            last_working_date: dayjs(formValue.last_working_date).format('YYYY-MM-DD'),
            reason: formValue.reason,
        };

        const formDataBody = createFormData(data);

        if (type === 'Create') {
            dispatch(createTermination({ body: formDataBody }))
                .unwrap()
                .then(() => {
                    cancelModal();
                    refreshPage();
                    setFile(null);
                    form.resetFields();
                })
                .catch(() => {
                    refreshPage();
                });
        } else {
            dispatch(updateTermination({ body: formDataBody, id: dataUpdate?.id }))
                .unwrap()
                .then(() => {
                    cancelModal();
                    refreshPage();
                    setFile(null);
                    form.resetFields();
                })
                .catch(() => {
                    refreshPage();
                });
        }
    };
    return (
        <Fragment>
            <Modal
                maskClosable={false}
                centered
                title={`${type} Termination`}
                open={openModal}
                onOk={handleSubmit}
                onCancel={() => {
                    form.resetFields();
                    setFile(null);
                    cancelModal();
                }}
                width={600}
                footer={[
                    <Button
                        onClick={() => {
                            cancelModal();
                            setFile(null);
                        }}
                        form="form"
                        htmlType="reset"
                        danger
                        type="default"
                        className="btn btn-outline-danger"
                    >
                        Discard
                    </Button>,
                    <Button htmlType="submit" form="form" type="primary" className="btn btn-primary-new">
                        Save
                    </Button>,
                ]}
            >
                <div className="w-full flex flex-col p-2">
                    <Form id="form" layout="vertical" form={form} onFinish={handleSubmit} name="dynamic_rule">
                        <div className="w-full grid grid-cols-2 gap-2">
                            {type !== 'Create' ? (
                                <>
                                    <Form.Item label="ID" name={'id'}>
                                        <InputComponent disabled={true} />
                                    </Form.Item>
                                    <div />
                                </>
                            ) : null}

                            <Form.Item
                                label="Employee"
                                name={'employee_id'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please select your Employee!',
                                    },
                                ]}
                            >
                                <SelectComponent placeholder="Select Employee" disabled={type !== 'Create' ? true : false}>
                                    {dataEmployee &&
                                        dataEmployee?.map((data: any, index: any) => (
                                            <Select.Option value={data.key} key={index}>
                                                {data.value}
                                            </Select.Option>
                                        ))}
                                </SelectComponent>
                            </Form.Item>

                            <Form.Item
                                label="Reason"
                                name={'reason'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Reason!',
                                    },
                                ]}
                            >
                                <InputComponent placeholder="Input Reason" />
                            </Form.Item>

                            <Form.Item
                                label="Submission Date"
                                name={'submission_date'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Submission Date!',
                                    },
                                ]}
                            >
                                <DatePicker style={{ width: '100%' }} placeholder="Input Submission Date" format={'YYYY-MM-DD'} disabled={type !== 'Create' ? true : false} />
                            </Form.Item>

                            <Form.Item
                                label="Last Working Day"
                                name={'last_working_date'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Last Working Day!',
                                    },
                                ]}
                            >
                                <DatePicker style={{ width: '100%' }} placeholder="Input Last Working Day" format={'YYYY-MM-DD'} />
                            </Form.Item>

                            <div className="col-span-2">
                                <Form.Item
                                    label="Document"
                                    name="attachment"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Please upload your Attachment!',
                                        },
                                    ]}
                                >
                                    <Space.Compact>
                                        <Form.Item noStyle>
                                            <Input disabled={true} value={file?.name} />
                                        </Form.Item>
                                        <Form.Item noStyle>
                                            <Upload {...props} accept='application/pdf' showUploadList={false} maxCount={1} beforeUpload={() => false} onChange={handleFileChange}>
                                                <Button className="bg-blue-500 text-white px-4 py-2 hover:bg-blue-600 transition-colors duration-300">Browse</Button>
                                            </Upload>
                                        </Form.Item>
                                    </Space.Compact>
                                </Form.Item>
                            </div>
                        </div>
                    </Form>
                </div>
            </Modal>
        </Fragment>
    );
};

export default TerminationForm;
