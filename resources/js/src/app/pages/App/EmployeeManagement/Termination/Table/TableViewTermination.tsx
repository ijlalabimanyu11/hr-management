import React from 'react';
import ColumnSearch from '../../../../../../utils/ColumnSearch';
import Action from '../../../../../../components/Action';
import { TbPencil } from 'react-icons/tb';
import { RiDeleteBinLine } from 'react-icons/ri';
import { PermissionAction } from '../../../../../../utils/PermissionAction';
import { DownloadOutlined } from '@ant-design/icons';
import Pills from '../../../../../../components/Pills'

export const TableTermination = (
    page = 1,
    pageSize = 10,
    searchedColumn: any,
    searchText: any,
    handleSearch: (selectedKeys: string[], confirm: () => void, dataIndex: string) => void,
    handleUpdate: (value: any) => void,
    handleDelete: (value: any) => void,
    handleDownloadAttachment: (value: any) => void,
) => {
    const action = (record: any) => {
        const actions: any[] = [];

        if (PermissionAction('termination', 'READ')) {
            actions.push({
                type: 'download',
                icon: <DownloadOutlined size={16} />,
                onClick: () => handleDownloadAttachment(record),
            });
        }

        if (PermissionAction('termination', 'UPDATE')) {
            actions.push({
                type: 'update',
                icon: <TbPencil size={16} />,
                onClick: () => handleUpdate(record),
            });
        }

        if (PermissionAction('termination', 'DELETE')) {
            actions.push({
                type: 'delete',
                icon: <RiDeleteBinLine size={16} />,
                onClick: () => handleDelete(record),
            });
        }

        return actions.length > 0 ? <Action actions={actions} /> : null;
    };

    const columns: any = [
        {
            title: 'No',
            align: 'center',
            width: 60,
            render: (text: any, object: any, index: number) => (page - 1) * pageSize + index + 1,
        },
        {
            title: 'Employee Code',
            sorter: true,
            align: 'left',
            dataIndex: 'employee_code',
            key: 'employee_code',
            ...ColumnSearch({ dataIndex: 'employee_code', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Employee Name',
            sorter: true,
            align: 'left',
            dataIndex: 'name',
            key: 'name',
            ...ColumnSearch({ dataIndex: 'name', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Submission Date',
            sorter: true,
            align: 'center',
            dataIndex: 'submission_date',
            key: 'submission_date',
            ...ColumnSearch({ dataIndex: 'submission_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Approval Date',
            sorter: true,
            align: 'center',
            dataIndex: 'approved_date',
            key: 'approved_date',
            ...ColumnSearch({ dataIndex: 'approved_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Reason',
            sorter: true,
            align: 'left',
            dataIndex: 'reason',
            key: 'reason',
            ...ColumnSearch({ dataIndex: 'reason', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Document',
            sorter: true,
            align: 'left',
            dataIndex: 'attachment',
            key: 'attachment',
            ellipsis: {
                showTitle: false,
              },
            ...ColumnSearch({ dataIndex: 'attachment', searchedColumn, searchText, handleSearch, typeFilter: 'input', tooltip: true }),
        },
        {
            title: 'Created By',
            sorter: true,
            align: 'left',
            dataIndex: 'created_by',
            key: 'created_by',
            ...ColumnSearch({ dataIndex: 'created_by', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Created Date',
            sorter: true,
            dataIndex: 'created_date',
            key: 'created_date',
            align: 'center',
            ...ColumnSearch({ dataIndex: 'created_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Modified By',
            sorter: true,
            align: 'left',
            dataIndex: 'modified_by',
            key: 'modified_by',
            ...ColumnSearch({ dataIndex: 'modified_by', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Modified Date',
            sorter: true,
            dataIndex: 'modified_date',
            key: 'modified_date',
            align: 'center',
            ...ColumnSearch({ dataIndex: 'modified_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Status',
            sorter: true,
            dataIndex: 'status',
            key: 'status',
            ...ColumnSearch({ dataIndex: 'status', searchedColumn, searchText, handleSearch, typeFilter: 'status' }),
            render: (index: any) => {
                let text: any;
                switch (index) {
                    case 'WAITING APPROVAL':
                        text = 'Waiting Approval';
                        break;
                    default:
                        text = index ? index.charAt(0).toUpperCase() + index.slice(1).toLowerCase() : index;
                        break;
                }
                return text ? (
                    <div className={'flex justify-center'}>
                        <Pills color={text}>{text}</Pills>
                    </div>
                ) : (
                    text
                );
            },
        },
    ]

    if (PermissionAction('termination', 'UPDATE') || PermissionAction('termination', 'DELETE') || PermissionAction('termination', 'READ')) {
        columns.push({
            title: 'Action',
            align: 'center',
            fixed: 'right',
            render: (text: any, record: any) => action(record),
        });
    }

    return columns
}
