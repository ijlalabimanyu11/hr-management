import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../../../redux/store';
import { setPageTitle } from '../../../../../redux/store/themeConfigSlice';
import { searchObject } from '../../../../../utils/searchObject';
import {
    deleteResignation,
    getDetailResignation,
    getDownloadAttachment,
    getResignationPaginate,
} from '../../../../../redux/slices/App/EmployeeManagement/Resignation/ResignationSlice';
import { APP_ROUTES } from '../../../../../router/app/app_routes';
import PermissionButton from '../../../../../utils/PermissionButton';
import { Button, Spin, Tooltip } from 'antd';
import { DownloadOutlined, PlusOutlined } from '@ant-design/icons';
import Breadcrumbs from '../../../../../components/Breadcrumbs';
import HeaderTitle from '../../../../../components/HeaderTitle';
import Cards from '../../../../../components/Cards';
import TablePagination from '../../../../../components/TablePagination';
import ModalConfirm from '../../../../../components/ModalConfirm';
import { TableResignation } from './Table/TableViewResignation';
import ResignationForm from './ResignationForm';
import appHttpService from '../../../../../redux/services/AppService';

const ResignationList = () => {
    // Selector
    const { data, loading, dataDetail } = useSelector((state: RootState) => state.resignation);

    // Declaration
    const dispatch = useDispatch<AppDispatch>();
    const dataSource = data?.data;

    // Use State
    const [page, setPage] = useState<number>(1);
    const [pageSize, setPageSize] = useState<number>(10);
    const [sort, setSort] = useState<string>('');
    const [search, setSearch] = useState<Record<string, string>>({});
    const [searchText, setSearchText] = useState<{ [key: string]: string }>({});
    const [searchedColumn, setSearchedColumn] = useState<string[]>([]);

    const [typeModal, setTypeModal] = useState<string>('');
    const [idData, setIdData] = useState<any>();
    const [openModal, setOpenModal] = useState<boolean>(false);
    const [modalDelete, setModalDelete] = useState<boolean>(false);
    const [dataFile, setDataFile] = useState<any>();

    // Use Effect
    useEffect(() => {
        dispatch(setPageTitle('Resignation'));
    });

    useEffect(() => {
        dispatch(getResignationPaginate({ search: searchObject(search), page, pageSize, sort }));
    }, [dispatch, search, page, pageSize, sort]);

    // Refresh Data Table
    const refreshPage = () => {
        dispatch(getResignationPaginate({ search: searchObject(search), page, pageSize, sort }));
    };

    // Breadcrumbs
    const routes = [
        {
            title: 'App',
            href: '',
        },
        {
            title: 'Employee Management',
            href: '',
        },
        {
            title: 'Resignation',
            href: APP_ROUTES.RESIGNATION_VIEW_ELEMENTS,
        },
    ];

    // Handle Change Page Table
    const handleChangePage = (pageChange: number, pageSizeChange: number) => {
        const tempPage = pageSize !== pageSizeChange ? 1 : pageChange;
        setPage(tempPage);
        setPageSize(pageSizeChange);
    };

    // Sorting Table
    const onSort = (_: any, __: any, sort: any) => {
        const dataSort = sort.order !== undefined ? `${sort.field}~${sort.order === 'ascend' ? 'asc' : 'desc'}` : '';
        setSort(dataSort);
    };

    // Handle Search Table
    const handleSearch = (selectedKeys: string[], confirm: () => void, dataIndex: string) => {
        confirm();
        setSearchText((prevState) => ({ ...prevState, [dataIndex]: selectedKeys[0] }));
        setSearchedColumn((prevState) => [...new Set([...prevState, dataIndex])]);
        setSearch((prevState: Record<string, string>) => {
            const newSearch = { ...prevState, [dataIndex]: selectedKeys[0] };
            if (!selectedKeys[0]) {
                delete newSearch[dataIndex];
            }
            if (prevState[dataIndex] !== selectedKeys[0]) {
                setPage(1);
            }
            return newSearch;
        });
    };

    // Handle Create
    const handleCreate = () => {
        setTypeModal('Create');
        setOpenModal(true);
    };

    // Handle Cancel Modal
    const handleCancelModal = () => {
        setIdData('');
        setOpenModal(false);
        setTypeModal('');
        setDataFile(null);
    };

    // Handle Update
    const handleUpdate = (value: any) => {
        setTypeModal('Update');
        setOpenModal(true);
        dispatch(getDetailResignation(value?.id));
        const fetchDataFile = async () => {
            const url = `/employee-resignation/download-attachment/${value?.id}`;
            const file = await appHttpService.getFileData(url, value?.attachment);
            setDataFile(file);
        };

        fetchDataFile();
    };

    // Handle Delete
    const handleDelete = (value: any) => {
        setIdData(value.id);
        setModalDelete(true);
    };

    // Handle Download Attachment
    const handleDownloadAttachment = (value: any) => {
        dispatch(getDownloadAttachment({ id: value.id, filename: value.attachment }));
    };

    // Handle Submit Delete
    const submitDelete = () => {
        setModalDelete(false);
        dispatch(deleteResignation({ id: idData }))
            .unwrap()
            .then(() => {
                refreshPage();
            })
            .catch((r) => {
                refreshPage();
            });
    };

    // Handle Download
    const handleDownload = () => {};

    // Access Button
    const accessButton = PermissionButton(
        (props: any) => (
            <div className="flex align-middle gap-3">
                <Tooltip title="Download">
                    <Button onClick={props.handleDownload} className="btn-icon btn-primary" icon={<DownloadOutlined />} />
                </Tooltip>
                <Tooltip title="Create">
                    <Button onClick={props.handleCreate} className="btn-icon btn-primary" icon={<PlusOutlined />} />
                </Tooltip>
            </div>
        ),
        'resignation',
        ['CREATE', 'READ']
    );
    return (
        <Fragment>
            <Spin spinning={loading}>
                <Breadcrumbs routes={routes} />
                <div className="flex w-full justify-between align-middle">
                    <HeaderTitle title="Resignation" />
                </div>

                <Cards>
                    <TablePagination
                        dataSource={dataSource}
                        totalData={data?.totalData}
                        current={page}
                        pageSize={pageSize}
                        onChange={handleChangePage}
                        columns={TableResignation(page, pageSize, searchedColumn, searchText, handleSearch, handleUpdate, handleDelete, handleDownloadAttachment)}
                        onSort={onSort}
                        tableScrolled={{ x: 2800, y: 300 }}
                        content={accessButton({ handleDownload, handleCreate })}
                    />
                </Cards>

                {/* Modal Create/Update Resignation */}
                <ResignationForm dataFile={dataFile} type={typeModal} openModal={openModal} cancelModal={handleCancelModal} refreshPage={refreshPage} dataUpdate={dataDetail} />

                {/* Modal Delete */}
                {modalDelete ? <ModalConfirm title={`Are you sure want to delete the resignation?`} icon="warning" onConfirm={submitDelete} onCancel={() => setModalDelete(false)} /> : null}
            </Spin>
        </Fragment>
    );
};

export default ResignationList;
