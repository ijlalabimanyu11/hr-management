import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../../../redux/store';
import { Button, DatePicker, Form, Modal, Select } from 'antd';
import { getEmployee, getLeave } from '../../../../../redux/slices/GlobalSlice';
import dayjs from 'dayjs';
import { createSetLeaveQuota, setLeaveQuotaBody, updateSetLeaveQuota } from '../../../../../redux/slices/App/EmployeeManagement/SetLeaveQuota/SetLeaveQuotaSlice';
import InputComponent from '../../../../../components/InputComponent';
import SelectComponent from '../../../../../components/SelectComponent';

interface SetLeaveQuotaFormProps {
    type: any;
    openModal?: any;
    cancelModal?: any;
    refreshPage?: any;
    dataUpdate?: any;
}

const SetLeaveQuotaForm: React.FC<SetLeaveQuotaFormProps> = ({ type, openModal, cancelModal, refreshPage, dataUpdate }) => {
    // Selector
    const { dataEmployee, dataLeave } = useSelector((state: RootState) => state.global);

    // Declaration
    const dispatch = useDispatch<AppDispatch>();
    const [form] = Form.useForm();

    // Use Effect
    useEffect(() => {
        dispatch(getEmployee());
        dispatch(getLeave());
    }, [dispatch]);

    useEffect(() => {
        if (type === 'Update' && dataUpdate) {
            form.setFieldsValue({
                id: dataUpdate?.id,
                employee_id: dataUpdate?.employee_id,
                leave_type: dataUpdate?.leave_type,
                start_date: dayjs(dataUpdate?.start_date),
                end_date: dayjs(dataUpdate?.end_date),
                amount: dataUpdate?.amount,
            });
        }
    }, [type, form, dataUpdate]);

    const handleSubmit = (formValue: any) => {
        const data: setLeaveQuotaBody = {
            employee_id: formValue.employee_id,
            leave_type: formValue.leave_type,
            start_date: dayjs(formValue.start_date).format('YYYY-MM-DD'),
            end_date: dayjs(formValue.end_date).format('YYYY-MM-DD'),
            amount: formValue.amount,
        };

        if (type === 'Create') {
            dispatch(createSetLeaveQuota({ body: data }))
                .unwrap()
                .then(() => {
                    cancelModal();
                    refreshPage();
                    form.resetFields();
                })
                .catch(() => {
                    refreshPage();
                });
        } else {
            dispatch(updateSetLeaveQuota({ body: data, id: dataUpdate?.id }))
                .unwrap()
                .then(() => {
                    cancelModal();
                    refreshPage();
                    form.resetFields();
                })
                .catch(() => {
                    refreshPage();
                });
        }
    };
    return (
        <Fragment>
            <Modal
                maskClosable={false}
                centered
                title={`${type} Set Leave Quota`}
                open={openModal}
                onOk={handleSubmit}
                onCancel={() => {
                    form.resetFields();
                    cancelModal();
                }}
                width={600}
                footer={[
                    <Button
                        onClick={() => {
                            cancelModal();
                        }}
                        form="form"
                        htmlType="reset"
                        danger
                        type="default"
                        className="btn btn-outline-danger"
                    >
                        Discard
                    </Button>,
                    <Button htmlType="submit" form="form" type="primary" className="btn btn-primary-new">
                        Save
                    </Button>,
                ]}
            >
                <div className="w-full flex flex-col p-2">
                    <Form id="form" layout="vertical" form={form} onFinish={handleSubmit} name="dynamic_rule">
                        <div className="w-full grid grid-cols-2 gap-2">
                            {type !== 'Create' ? (
                                <>
                                    <Form.Item label="ID" name={'id'}>
                                        <InputComponent disabled={true} />
                                    </Form.Item>
                                    <div />
                                </>
                            ) : null}

                            <Form.Item
                                label="Employee"
                                name={'employee_id'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please select your Employee!',
                                    },
                                ]}
                            >
                                <SelectComponent placeholder="Select Employee" disabled={type !== 'Create' ? true : false}>
                                    {dataEmployee &&
                                        dataEmployee?.map((data: any, index: any) => (
                                            <Select.Option value={data.key} key={index}>
                                                {data.value}
                                            </Select.Option>
                                        ))}
                                </SelectComponent>
                            </Form.Item>

                            <Form.Item
                                label="Leave Type"
                                name={'leave_type'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please select your Leave Type!',
                                    },
                                ]}
                            >
                                <SelectComponent placeholder="Select Leave Type" disabled={type !== 'Create' ? true : false}>
                                    {dataLeave &&
                                        dataLeave?.map((data: any, index: any) => (
                                            <Select.Option value={data.key} key={index}>
                                                {data.value}
                                            </Select.Option>
                                        ))}
                                </SelectComponent>
                            </Form.Item>

                            <Form.Item
                                label="Start Date"
                                name={'start_date'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Start Date!',
                                    },
                                ]}
                            >
                                <DatePicker style={{ width: '100%' }} placeholder="Input Start Date" format={'YYYY-MM-DD'} />
                            </Form.Item>

                            <Form.Item
                                label="End Date"
                                name={'end_date'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your End Date!',
                                    },
                                ]}
                            >
                                <DatePicker style={{ width: '100%' }} placeholder="Input End Date" format={'YYYY-MM-DD'} />
                            </Form.Item>

                            <div className="col-span-2">
                                <Form.Item
                                    label="Amount"
                                    name={'amount'}
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Please input your Amount!',
                                        },
                                    ]}
                                >
                                    <InputComponent placeholder="Input Amount" />
                                </Form.Item>
                            </div>
                        </div>
                    </Form>
                </div>
            </Modal>
        </Fragment>
    );
};

export default SetLeaveQuotaForm;
