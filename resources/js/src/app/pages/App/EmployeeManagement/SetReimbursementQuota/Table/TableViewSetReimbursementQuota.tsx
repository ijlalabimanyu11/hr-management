import React from 'react';
import ColumnSearch from '../../../../../../utils/ColumnSearch';
import Action from '../../../../../../components/Action';
import { TbPencil } from 'react-icons/tb';
import { RiDeleteBinLine } from 'react-icons/ri';
import { PermissionAction } from '../../../../../../utils/PermissionAction';

export const TableViewSetReimbursementQuota = (
    page = 1,
    pageSize = 10,
    searchedColumn: any,
    searchText: any,
    handleSearch: (selectedKeys: string[], confirm: () => void, dataIndex: string) => void,
    handleUpdate: (value: any) => void,
    handleDelete: (value: any) => void,
) => {
    const action = (record: any) => {
        const actions: any[] = [];

        if (PermissionAction('set-reimbursement-quota', 'UPDATE')) {
            actions.push({
                type: 'update',
                icon: <TbPencil size={16} />,
                onClick: () => handleUpdate(record),
            });
        }

        if (PermissionAction('set-reimbursement-quota', 'DELETE')) {
            actions.push({
                type: 'delete',
                icon: <RiDeleteBinLine size={16} />,
                onClick: () => handleDelete(record),
            });
        }

        return actions.length > 0 ? <Action actions={actions} /> : null;
    };

    const columns: any = [
        {
            title: 'No',
            align: 'center',
            width: 60,
            render: (text: any, object: any, index: number) => (page - 1) * pageSize + index + 1,
        },
        {
            title: 'Employee Code',
            sorter: true,
            align: 'left',
            dataIndex: 'employee_code',
            key: 'employee_code',
            ...ColumnSearch({ dataIndex: 'employee_code', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Employee Name',
            sorter: true,
            align: 'left',
            dataIndex: 'name',
            key: 'name',
            ...ColumnSearch({ dataIndex: 'name', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Reimbursement Type',
            sorter: true,
            align: 'center',
            dataIndex: 'reimbursement_type',
            key: 'reimbursement_type',
            ...ColumnSearch({ dataIndex: 'reimbursement_type', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Amount',
            sorter: true,
            align: 'right',
            dataIndex: 'amount',
            key: 'amount',
            ...ColumnSearch({ dataIndex: 'amount', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Start Date',
            sorter: true,
            align: 'center',
            dataIndex: 'start_date',
            key: 'start_date',
            ...ColumnSearch({ dataIndex: 'start_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'End Date',
            sorter: true,
            align: 'center',
            dataIndex: 'end_date',
            key: 'end_date',
            ...ColumnSearch({ dataIndex: 'end_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Created By',
            sorter: true,
            align: 'left',
            dataIndex: 'created_by',
            key: 'created_by',
            ...ColumnSearch({ dataIndex: 'created_by', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Created Date',
            sorter: true,
            dataIndex: 'created_date',
            key: 'created_date',
            align: 'center',
            ...ColumnSearch({ dataIndex: 'created_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Modified By',
            sorter: true,
            align: 'left',
            dataIndex: 'modified_by',
            key: 'modified_by',
            ...ColumnSearch({ dataIndex: 'modified_by', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Modified Date',
            sorter: true,
            dataIndex: 'modified_date',
            key: 'modified_date',
            align: 'center',
            ...ColumnSearch({ dataIndex: 'modified_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
    ]

    if (PermissionAction('set-reimbursement-quota', 'UPDATE') || PermissionAction('set-reimbursement-quota', 'DELETE')) {
        columns.push({
            title: 'Action',
            align: 'center',
            fixed: 'right',
            render: (text: any, record: any) => action(record),
        });
    }

    return columns
}
