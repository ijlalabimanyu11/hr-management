import { Button, DatePicker, Form, Modal, Select } from 'antd';
import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../../../redux/store';
import { getDepartment, getEmployee, getPosition, getSection } from '../../../../../redux/slices/GlobalSlice';
import { createDesignation, designationBody, updateDesignation } from '../../../../../redux/slices/App/EmployeeManagement/Designation/DesignationSlice';
import SelectComponent from '../../../../../components/SelectComponent';
import InputComponent from '../../../../../components/InputComponent';
import dayjs from 'dayjs';

interface DesignationFormProps {
    type: any;
    openModal?: any;
    cancelModal?: any;
    refreshPage?: any;
    dataUpdate?: any;
}

const DesignationForm: React.FC<DesignationFormProps> = ({ type, openModal, cancelModal, refreshPage, dataUpdate }) => {
    // Selector
    const { dataEmployee, dataDepartment, dataSection, dataPosition } = useSelector((state: RootState) => state.global);

    // Declaration
    const dispatch = useDispatch<AppDispatch>();
    const [form] = Form.useForm();

    // Use State

    // Use Effect
    useEffect(() => {
        dispatch(getEmployee());
        dispatch(getDepartment());
        dispatch(getSection());
        dispatch(getPosition());
    }, [dispatch]);

    useEffect(() => {
        if (type === 'Update' && dataUpdate) {
            form.setFieldsValue({
                id: dataUpdate?.id,
                employee_id: dataUpdate?.employee_id,
                department: dataUpdate?.department_id,
                section: dataUpdate?.section_id,
                position: dataUpdate?.position_id,
                line_manager1: dataUpdate?.line_manager1,
                line_manager2: dataUpdate?.line_manager2,
                start_date: dataUpdate?.start_date ? dayjs(dataUpdate?.start_date, 'YYYY-MM-DD') : undefined,
                end_date: dataUpdate?.end_date ? dayjs(dataUpdate?.end_date, 'YYYY-MM-DD') : undefined,
                notes: dataUpdate?.notes,
            });
        }
    }, [type, form, dataUpdate]);

    const handleSubmit = (formValue: any) => {
        const data: designationBody = {
            employee_id: formValue.employee_id,
            department: formValue.department,
            section: formValue.section,
            position: formValue.position,
            line_manager1: formValue.line_manager1,
            line_manager2: formValue.line_manager2,
            start_date: dayjs(formValue.start_date).format('YYYY-MM-DD'),
            end_date: dayjs(formValue.end_date).format('YYYY-MM-DD'),
            notes: formValue.notes,
        };

        console.log(data);

        if (type === 'Create') {
            dispatch(createDesignation({ body: data }))
                .unwrap()
                .then(() => {
                    cancelModal();
                    refreshPage();
                    form.resetFields()
                })
                .catch(() => {
                    refreshPage();
                });
        } else {
            dispatch(updateDesignation({ body: data, id: dataUpdate?.id }))
                .unwrap()
                .then(() => {
                    cancelModal();
                    refreshPage();
                    form.resetFields()
                })
                .catch(() => {
                    refreshPage();
                });
        }
    };

    return (
        <Fragment>
            <Modal
                maskClosable={false}
                centered
                title={`${type} Designation`}
                open={openModal}
                onOk={handleSubmit}
                onCancel={() => {
                    form.resetFields();
                    cancelModal();
                }}
                width={600}
                footer={[
                    <Button onClick={cancelModal} form="form" htmlType="reset" danger type="default" className="btn btn-outline-danger">
                        Discard
                    </Button>,
                    <Button htmlType="submit" form="form" type="primary" className="btn btn-primary-new">
                        Save
                    </Button>,
                ]}
            >
                <div className="w-full flex flex-col p-2">
                    <Form id="form" layout="vertical" form={form} onFinish={handleSubmit}>
                        <div className="w-full grid grid-cols-2 gap-2">
                            {type !== 'Create' ? (
                                <>
                                    <Form.Item label="ID" name={'id'}>
                                        <InputComponent disabled={true} />
                                    </Form.Item>
                                    <div />
                                </>
                            ) : null}

                            <Form.Item
                                label="Employee"
                                name={'employee_id'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please select your Employee!',
                                    },
                                ]}
                            >
                                <SelectComponent placeholder="Select Employee" disabled={type !== 'Create' ? true : false}>
                                    {dataEmployee &&
                                        dataEmployee?.map((data: any, index: any) => (
                                            <Select.Option value={data.key} key={index}>
                                                {data.value}
                                            </Select.Option>
                                        ))}
                                </SelectComponent>
                            </Form.Item>
                            <Form.Item
                                label="Department"
                                name={'department'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please select your Department!',
                                    },
                                ]}
                            >
                                <SelectComponent placeholder="Select Department" disabled={type !== 'Create' ? true : false}>
                                    {dataDepartment &&
                                        dataDepartment?.map((data: any, index: any) => (
                                            <Select.Option value={data.key} key={index}>
                                                {data.value}
                                            </Select.Option>
                                        ))}
                                </SelectComponent>
                            </Form.Item>
                            <Form.Item
                                label="Section"
                                name={'section'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please select your Section!',
                                    },
                                ]}
                            >
                                <SelectComponent placeholder="Select Section" disabled={type !== 'Create' ? true : false}>
                                    {dataSection &&
                                        dataSection?.map((data: any, index: any) => (
                                            <Select.Option value={data.key} key={index}>
                                                {data.value}
                                            </Select.Option>
                                        ))}
                                </SelectComponent>
                            </Form.Item>
                            <Form.Item
                                label="Position"
                                name={'position'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please select your Position!',
                                    },
                                ]}
                            >
                                <SelectComponent placeholder="Select Position" disabled={type !== 'Create' ? true : false}>
                                    {dataPosition &&
                                        dataPosition?.map((data: any, index: any) => (
                                            <Select.Option value={data.key} key={index}>
                                                {data.value}
                                            </Select.Option>
                                        ))}
                                </SelectComponent>
                            </Form.Item>
                            <Form.Item
                                label="Line Manager 1"
                                name={'line_manager1'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please select your Line Manager 1!',
                                    },
                                ]}
                            >
                                <SelectComponent placeholder="Select Line Manager 1" disabled={type !== 'Create' && dataUpdate?.line_manager1_id !== null ? true : false}>
                                    {dataEmployee &&
                                        dataEmployee?.map((data: any, index: any) => (
                                            <Select.Option value={data.key} key={index}>
                                                {data.value}
                                            </Select.Option>
                                        ))}
                                </SelectComponent>
                            </Form.Item>
                            <Form.Item
                                label="Line Manager 2"
                                name={'line_manager2'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please select your Line Manager 2!',
                                    },
                                ]}
                            >
                                <SelectComponent placeholder="Select Line Manager 2" disabled={type !== 'Create' && dataUpdate?.line_manager2_id !== null ? true : false}>
                                    {dataEmployee &&
                                        dataEmployee?.map((data: any, index: any) => (
                                            <Select.Option value={data.key} key={index}>
                                                {data.value}
                                            </Select.Option>
                                        ))}
                                </SelectComponent>
                            </Form.Item>
                            <Form.Item
                                label="Start Date"
                                name={'start_date'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Start Date!',
                                    },
                                ]}
                            >
                                <DatePicker placement='bottomLeft' style={{ width: '100%' }} placeholder="Input Start Date" format={'YYYY-MM-DD'} value={dayjs(dataUpdate?.start_date, 'YYYY-MM-DD')} allowClear />
                            </Form.Item>
                            <Form.Item
                                label="End Date"
                                name={'end_date'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your End Date!',
                                    },
                                ]}
                            >
                                <DatePicker style={{ width: '100%' }} placeholder="Input End Date" format={'YYYY-MM-DD'} value={dayjs(dataUpdate?.end_date, 'YYYY-MM-DD')} allowClear />
                            </Form.Item>
                            <div className="col-span-2">
                                <Form.Item
                                    label="Notes"
                                    name={'notes'}
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Please input your Notes!',
                                        },
                                    ]}
                                >
                                    <InputComponent placeholder="Input Notes" />
                                </Form.Item>
                            </div>
                        </div>
                    </Form>
                </div>
            </Modal>
        </Fragment>
    );
};

export default DesignationForm;
