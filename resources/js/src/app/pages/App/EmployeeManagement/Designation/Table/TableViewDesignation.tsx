import React from 'react';
import ColumnSearch from '../../../../../../utils/ColumnSearch';
import Action from '../../../../../../components/Action';
import { TbPencil } from 'react-icons/tb';
import { RiDeleteBinLine } from 'react-icons/ri';
import { PermissionAction } from '../../../../../../utils/PermissionAction';

export const TableDesignation = (
    page = 1,
    pageSize = 10,
    searchedColumn: any,
    searchText: any,
    handleSearch: (selectedKeys: string[], confirm: () => void, dataIndex: string) => void,
    handleUpdate: (value: any) => void,
    handleDelete: (value: any) => void,
) => {
    const action = (record: any) => {
        const actions: any[] = [];

        if (PermissionAction('designation', 'UPDATE')) {
            actions.push({
                type: 'update',
                icon: <TbPencil size={16} />,
                onClick: () => handleUpdate(record),
            });
        }

        if (PermissionAction('designation', 'DELETE')) {
            actions.push({
                type: 'delete',
                icon: <RiDeleteBinLine size={16} />,
                onClick: () => handleDelete(record),
            });
        }

        return actions.length > 0 ? <Action actions={actions} /> : null;
    };

    const columns: any = [
        {
            title: 'No',
            align: 'center',
            width: 60,
            render: (text: any, object: any, index: number) => (page - 1) * pageSize + index + 1,
        },
        {
            title: 'Employee Code',
            sorter: true,
            align: 'left',
            dataIndex: 'employee_code',
            key: 'employee_code',
            ...ColumnSearch({ dataIndex: 'employee_code', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Employee Name',
            sorter: true,
            align: 'left',
            dataIndex: 'name',
            key: 'name',
            ...ColumnSearch({ dataIndex: 'name', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Department',
            sorter: true,
            align: 'left',
            dataIndex: 'department',
            key: 'department',
            ...ColumnSearch({ dataIndex: 'department', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Section',
            sorter: true,
            align: 'left',
            dataIndex: 'section',
            key: 'section',
            ...ColumnSearch({ dataIndex: 'section', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Position',
            sorter: true,
            align: 'left',
            dataIndex: 'position',
            key: 'position',
            ...ColumnSearch({ dataIndex: 'position', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Start Date',
            sorter: true,
            align: 'center',
            dataIndex: 'start_date',
            key: 'start_date',
            ...ColumnSearch({ dataIndex: 'start_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'End Date',
            sorter: true,
            align: 'center',
            dataIndex: 'end_date',
            key: 'end_date',
            ...ColumnSearch({ dataIndex: 'end_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Notes',
            sorter: true,
            align: 'left',
            dataIndex: 'notes',
            key: 'notes',
            ...ColumnSearch({ dataIndex: 'notes', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Created By',
            sorter: true,
            align: 'left',
            dataIndex: 'created_by',
            key: 'created_by',
            ...ColumnSearch({ dataIndex: 'created_by', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Created Date',
            sorter: true,
            dataIndex: 'created_date',
            key: 'created_date',
            align: 'center',
            ...ColumnSearch({ dataIndex: 'created_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Modified By',
            sorter: true,
            align: 'left',
            dataIndex: 'modified_by',
            key: 'modified_by',
            ...ColumnSearch({ dataIndex: 'modified_by', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Modified Date',
            sorter: true,
            dataIndex: 'modified_date',
            key: 'modified_date',
            align: 'center',
            ...ColumnSearch({ dataIndex: 'modified_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        // {
        //     title: 'Status',
        //     sorter: true,
        //     dataIndex: 'status',
        //     key: 'status',
        //     ...ColumnSearch({ dataIndex: 'status', searchedColumn, searchText, handleSearch, typeFilter: 'status' }),
        //     render: (index: any) => {
        //         let text: any;
        //         switch (index) {
        //             case 'WAITING APPROVAL':
        //                 text = 'Waiting Approval';
        //                 break;
        //             default:
        //                 text = index ? index.charAt(0).toUpperCase() + index.slice(1).toLowerCase() : index;
        //                 break;
        //         }
        //         return text ? (
        //             <div className={'flex justify-center'}>
        //                 <Pills color={text}>{text}</Pills>
        //             </div>
        //         ) : (
        //             text
        //         );
        //     },
        // },
    ];

    if (PermissionAction('designation', 'UPDATE') || PermissionAction('designation', 'DELETE') || PermissionAction('designation', 'TOGGLE')) {
        columns.push({
            title: 'Action',
            align: 'center',
            fixed: 'right',
            render: (text: any, record: any) => action(record),
        });
    }

    return columns;
};
