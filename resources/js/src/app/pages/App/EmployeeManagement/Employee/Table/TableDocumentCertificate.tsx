import React from 'react';
import ColumnSearch from '../../../../../../utils/ColumnSearch';
import Action from '../../../../../../components/Action';
import { RiDeleteBinLine } from 'react-icons/ri';
import { DownloadOutlined } from '@ant-design/icons';

export const TableDocumentCertificate = (
    page = 1,
    pageSize = 10,
    searchedColumn: any,
    searchText: any,
    mode: any,
    typeTable: any,
    handleSearch: (selectedKeys: string[], confirm: () => void, dataIndex: string) => void,
    handleDelete: (value: any) => void,
    handleUpdate: (value: any) => void,
    handleDownloadAttachment: (value: any) => void,
    onFilter: (dataIndex: any, value: any, record: any) => void,
    sorter: (fieldSort: any, a: any, b: any) => void
) => {
    const action = (record: any) => {
        const actions: any[] = [];

        if (mode === 'detail' || mode === 'update' || record.type === 'exist') {
            actions.push({
                type: 'download',
                icon: <DownloadOutlined size={16} />,
                onClick: () => handleDownloadAttachment(record),
            });
        }

        if (mode !== 'detail') {
            actions.push(
                // {
                //     type: 'update',
                //     icon: <TbPencil size={16} />,
                //     onClick: () => handleUpdate(record),
                // },
                {
                    type: 'delete',
                    icon: <RiDeleteBinLine size={16} />,
                    onClick: () => handleDelete(record),
                }
            );
        }

        return actions.length > 0 ? <Action actions={actions} /> : null;
    };

    const columns: any = [
        {
            title: 'No',
            align: 'center',
            width: 60,
            render: (text: any, object: any, index: number) => (page - 1) * pageSize + index + 1,
        },
        {
            title: 'Certification Name',
            dataIndex: 'name',
            align: 'left',
            key: 'name',
            onFilter: (value: any, record: any) => onFilter('name', value, record),
            sorter: (a: any, b: any) => sorter('name', a, b),
            ...ColumnSearch({ dataIndex: 'name', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Issuing Organization',
            dataIndex: 'issuing_organization',
            align: 'left',
            key: 'issuing_organization',
            onFilter: (value: any, record: any) => onFilter('issuing_organization', value, record),
            sorter: (a: any, b: any) => sorter('issuing_organization', a, b),
            ...ColumnSearch({ dataIndex: 'issuing_organization', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Issued Date',
            dataIndex: 'issuing_date',
            align: 'center',
            key: 'issuing_date',
            onFilter: (value: any, record: any) => onFilter('issuing_date', value, record),
            sorter: (a: any, b: any) => sorter('issuing_date', a, b),
            ...ColumnSearch({ dataIndex: 'issuing_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Expired Date',
            dataIndex: 'expired_date',
            align: 'center',
            key: 'expired_date',
            onFilter: (value: any, record: any) => onFilter('expired_date', value, record),
            sorter: (a: any, b: any) => sorter('expired_date', a, b),
            ...ColumnSearch({ dataIndex: 'expired_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Notes',
            dataIndex: 'notes',
            align: 'left',
            key: 'notes',
            onFilter: (value: any, record: any) => onFilter('notes', value, record),
            sorter: (a: any, b: any) => sorter('notes', a, b),
            ...ColumnSearch({ dataIndex: 'notes', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Attachment',
            dataIndex: 'file_name',
            align: 'left',
            key: 'file_name',
            ellipsis: {
                showTitle: false,
            },
            onFilter: (value: any, record: any) => onFilter('file_name', value, record),
            sorter: (a: any, b: any) => sorter('file_name', a, b),
            ...ColumnSearch({ dataIndex: 'file_name', searchedColumn, searchText, handleSearch, typeFilter: 'input', tooltip: true }),
        },
    ];

    const columnsAPI: any = [
        {
            title: 'No',
            align: 'center',
            width: 60,
            render: (text: any, object: any, index: number) => (page - 1) * pageSize + index + 1,
        },
        {
            title: 'Certification Name',
            sorter: true,
            align: 'left',
            dataIndex: 'name',
            key: 'name',
            ...ColumnSearch({ dataIndex: 'name', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Issuing Organization',
            sorter: true,
            align: 'left',
            dataIndex: 'issuing_organization',
            key: 'issuing_organization',
            ...ColumnSearch({ dataIndex: 'issuing_organization', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Issued Date',
            sorter: true,
            align: 'center',
            dataIndex: 'issuing_date',
            key: 'issuing_date',
            ...ColumnSearch({ dataIndex: 'issuing_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Expired Date',
            sorter: true,
            align: 'center',
            dataIndex: 'expired_date',
            key: 'expired_date',
            ...ColumnSearch({ dataIndex: 'expired_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Notes',
            sorter: true,
            align: 'left',
            dataIndex: 'notes',
            key: 'notes',
            ...ColumnSearch({ dataIndex: 'notes', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Attachment',
            sorter: true,
            align: 'left',
            dataIndex: 'file',
            key: 'file',
            ellipsis: {
                showTitle: false,
            },
            ...ColumnSearch({ dataIndex: 'file', searchedColumn, searchText, handleSearch, typeFilter: 'input', tooltip: true }),
        },
    ];

    columns.push({
        title: 'Action',
        align: 'center',
        fixed: 'right',
        render: (text: any, record: any) => action(record),
    });

    columnsAPI.push({
        title: 'Action',
        align: 'center',
        fixed: 'right',
        render: (text: any, record: any) => action(record),
    });

    return typeTable === 'API' ? columnsAPI : columns;
};
