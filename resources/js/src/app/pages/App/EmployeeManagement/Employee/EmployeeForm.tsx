import React, { Fragment, useEffect, useState } from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { APP_ROUTES } from '../../../../../router/app/app_routes';
import Breadcrumbs from '../../../../../components/Breadcrumbs';
import HeaderTitle from '../../../../../components/HeaderTitle';
import Cards from '../../../../../components/Cards';
import Frame from '../../../../../components/Frame';
import BasicInfoForm from './Form/BasicInfoForm';
import BankAccountDetailForm from './Form/BankAccountDetailForm';
import DocumentCVForm from './Form/DocumentCVForm';
import DocumentCertificateForm from './Form/DocumentCertificateForm';
import { Button, Form } from 'antd';
import { useLocation, useNavigate } from 'react-router-dom';
import {
    createEmployee,
    createEmployeeCertificate,
    employeeBody,
    getDetailEmployee,
    getEmployeeCertificatePaginate,
    updateEmployee,
    updateEmployeeCertificate,
} from '../../../../../redux/slices/App/EmployeeManagement/Employee/EmployeeSlice';
import dayjs from 'dayjs';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../../../redux/store';
import { setPageTitle } from '../../../../../redux/store/themeConfigSlice';
import appHttpService from '../../../../../redux/services/AppService';
import { searchObject } from '../../../../../utils/searchObject';

const EmployeeForm = ({ type }) => {
    // Selector
    const { dataEmployee, dataDetail, dataPagingCertificate } = useSelector((state: RootState) => state.employee);

    // Declaration
    const [form] = Form.useForm();
    const navigate = useNavigate();
    const dispatch = useDispatch<AppDispatch>();
    const location = useLocation();
    const { id } = location?.state || {};

    // Use State
    const [valuePage, setValuePage] = useState<any | null>(0);
    const [picture, setPicture] = useState<any>([]);
    const [fileCV, setFileCV] = useState<File | null>(null);
    const [dataListCertificate, setDataListCertificate] = useState<any[]>([]);
    const [fieldErrors, setFieldErrors] = useState<any>({});

    const [page, setPage] = useState<number>(1);
    const [pageSize, setPageSize] = useState<number>(10);
    const [sort, setSort] = useState<string>('');
    const [search, setSearch] = useState<Record<string, string>>({});

    // Use Effect
    useEffect(() => {
        dispatch(setPageTitle('Employee'));
    });

    useEffect(() => {
        if (id && type === 'update') {
            dispatch(getDetailEmployee(id));
            dispatch(getEmployeeCertificatePaginate({ id: id, search: searchObject(search), page, pageSize, sort }));
        }
    }, [dispatch, id, type, search, page, pageSize, sort]);

    useEffect(() => {
        const fetchData = async () => {
            if (id && dataDetail && type === 'update') {
                // Set initial form values
                form.setFieldsValue({
                    employee_picture: picture,
                    employee_code: dataDetail?.employee_code,
                    national_identifier: dataDetail?.national_identifier,
                    name: dataDetail?.name,
                    email: dataDetail?.email,
                    birth_date: dayjs(dataDetail?.birth_date),
                    gender: dataDetail?.gender,
                    phone: dataDetail?.phone,
                    address: dataDetail?.address,
                    salary_type: dataDetail?.salary_type,
                    basic_salary: dataDetail?.basic_salary,
                    join_date: dayjs(dataDetail?.join_date),
                    bank_name: dataDetail?.bank_name,
                    branch_location: dataDetail?.branch_location,
                    account_holder_name: dataDetail?.bank_account_name,
                    account_number: dataDetail?.bank_account_number,
                    tax_payer_identifier: dataDetail?.tax_payer_identifier,
                    cv: fileCV,
                });

                // Define async fetch functions for CV, Picture, and Certificates
                const fetchDataFileCV = async () => {
                    const url = `/employee/download/${id}/cv`;
                    return await appHttpService.getFileData(url, dataDetail?.cv);
                };

                const fetchDataFilePicture = async () => {
                    const url = `/employee/download/${id}/photo`;
                    return await appHttpService.getFileDataPicture(url, dataDetail?.employee_picture);
                };

                const fetchDataCertificates = async () => {
                    return await Promise.all(
                        (dataPagingCertificate?.data || []).map(async (item: any, index: any) => {
                            const url = `/employee/download-certification/${item.id}`;
                            const fileCertification = await appHttpService.getFileData(url, dataDetail?.employee_picture);

                            return {
                                key: index + 1,
                                id: item.id,
                                employee_id: item.employee_id,
                                name: item.name,
                                issuing_organization: item.issuing_organization,
                                issuing_date: item.issuing_date,
                                expired_date: item.expired_date,
                                notes: item.notes,
                                file_name: item.file,
                                certification: fileCertification,
                                type: 'exist',
                            };
                        })
                    );
                };

                // Execute all fetches in parallel and set state once all are complete
                const [cvFile, pictureFile, certificates] = await Promise.all([fetchDataFileCV(), fetchDataFilePicture(), fetchDataCertificates()]);

                // Update state with the fetched data
                setFileCV(cvFile);
                setPicture([pictureFile]);
                setDataListCertificate(certificates);
            }
        };

        fetchData();
    }, [id, dataDetail, type, dataPagingCertificate, form]);

    // Data Tab
    const dataTab = [
        {
            key: 0,
            value: 'Basic Info',
        },
        {
            key: 1,
            value: 'Bank Account Detail',
        },
        {
            key: 2,
            value: 'Document CV',
        },
        {
            key: 3,
            value: 'Document Certificate',
        },
    ];

    // Breadcrumbs
    const routes = [
        {
            title: 'App',
            href: '',
        },
        {
            title: 'Employee Management',
            href: '',
        },
        {
            title: 'Employee',
            href: APP_ROUTES.EMPLOYEE_VIEW_ELEMENTS,
        },
        {
            title: 'Create Employee',
            href: APP_ROUTES.EMPLOYEE_CREATE_ELEMENTS,
        },
    ];

    // Function Next
    const next = () => {
        setValuePage(valuePage + 1);
    };

    // Function Prev
    const prev = () => {
        setValuePage(valuePage - 1);
    };

    // Handle Cancel
    const handleCancel = () => {
        form.resetFields();
        setValuePage(0);
        navigate(-1);
    };

    // Function to handle file change Picture
    const handleFileChangePicture = ({ file }) => {
        setPicture(file);
    };

    // Function to handle file change CV
    const handleFileChangeCV = ({ file }) => {
        setFileCV(file);
    };

    // Helper function to convert data to FormData
    const createFormData = (data: any): FormData => {
        const formData = new FormData();
        formData.append('employee_picture', data.employee_picture);
        formData.append('employee_code', data.employee_code);
        formData.append('national_identifier', data.national_identifier);
        formData.append('name', data.name);
        formData.append('email', data.email);
        formData.append('birth_date', data.birth_date);
        formData.append('gender', data.gender);
        formData.append('phone', data.phone);
        formData.append('address', data.address);
        formData.append('salary_type', data.salary_type);
        formData.append('basic_salary', data.basic_salary);
        formData.append('join_date', data.join_date);
        formData.append('bank_name', data.bank_name);
        formData.append('branch_location', data.branch_location);
        formData.append('account_holder_name', data.account_holder_name);
        formData.append('account_number', data.account_number);
        formData.append('tax_payer_identifier', data.tax_payer_identifier);
        formData.append('cv', data.cv);
        return formData;
    };

    // Function to handle Certificate
    const handleCertificate = async (employeeId: string, certificates: any[]) => {
        for (const certificate of certificates) {
            const body = { ...certificate, employee_id: employeeId };
            await dispatch(createEmployeeCertificate({ body })).unwrap();
        }
    };

    // Reset Form
    const resetForm = () => {
        form.resetFields();
        setValuePage(0);
        setPicture(null);
        setFileCV(null);
        setDataListCertificate([]);
    };

    const groupedFieldErrors = {
        basicInfo: {
            employee_code: 0,
            national_identifier: 0,
            name: 0,
            email: 0,
            birth_date: 0,
            gender: 0,
            salary_type: 0,
            basic_salary: 0,
            join_date: 0,
            phone: 0,
            address: 0,
        },
        bankAccountDetail: {
            bank_name: 0,
            branch_location: 0,
            account_holder_name: 0,
            account_number: 0,
            tax_payer_identifier: 0,
        },
        documentCV: {
            cv: 0,
        }
    };

    const onFinishFailed = (errorInfo: any) => {
        const newFieldErrors = { ...groupedFieldErrors };

        errorInfo.errorFields.forEach((error: any) => {
            const fieldName = error.name[0];

            // Check which section the field belongs to and increment or initialize its error count
            if (newFieldErrors.basicInfo && newFieldErrors.basicInfo[fieldName] !== undefined) {
                newFieldErrors.basicInfo[fieldName] = (newFieldErrors.basicInfo[fieldName] || 0) + 1;
            } else if (newFieldErrors.bankAccountDetail && newFieldErrors.bankAccountDetail[fieldName] !== undefined) {
                newFieldErrors.bankAccountDetail[fieldName] = (newFieldErrors.bankAccountDetail[fieldName] || 0) + 1;
            } else if (newFieldErrors.documentCV && newFieldErrors.documentCV[fieldName] !== undefined) {
                newFieldErrors.documentCV[fieldName] = (newFieldErrors.documentCV[fieldName] || 0) + 1;
            }
        });
        setFieldErrors(newFieldErrors);
    };



    // Handle Submit
    const handleSubmit = (formValue: any) => {
        const data: employeeBody = {
            ...formValue,
            employee_picture: picture[0],
            birth_date: dayjs(formValue.birth_date).format('YYYY-MM-DD'),
            join_date: dayjs(formValue.join_date).format('YYYY-MM-DD'),
            cv: fileCV,
        };

        const formDataBody = createFormData(data);

        if (type === 'create') {
            dispatch(createEmployee({ body: formDataBody }))
                .unwrap()
                .then(async (dataEmployee: any) => {
                    const employeeId = dataEmployee.id;

                    const updatedCertificates = dataListCertificate.map((certificate: any) => ({
                        ...certificate,
                        employee_id: employeeId,
                        id: null,
                    }));

                    await handleCertificate(employeeId, updatedCertificates);
                    resetForm();
                });
        } else {
            dispatch(updateEmployee({ body: formDataBody, id }))
                .unwrap()
                .then(async (dataEmployee: any) => {
                    const employeeId = dataEmployee.id;

                    const updatedCertificates = dataListCertificate.map((certificate: any) => ({
                        ...certificate,
                        employee_id: employeeId,
                        id: certificate.id || null,
                    }));

                    await handleCertificate(employeeId, updatedCertificates);
                    resetForm();
                });
        }
    };

    return (
        <Fragment>
            <Breadcrumbs routes={routes} />
            <div className="flex w-full justify-between align-middle">
                <HeaderTitle title={`${type !== 'create' ? 'Update' : 'Create'} Employee`} />
            </div>

            <div className="flex justify-between">
                <div className="w-1/5 mr-2">
                    <Cards type={'card'}>
                        <Frame activeIndex={valuePage} setActiveIndex={setValuePage} data={dataTab} fieldErrors={fieldErrors} />
                    </Cards>
                </div>

                <div className="w-4/5 ml-2">
                    <Cards>
                        <Form layout="vertical" form={form} onFinish={handleSubmit} onFinishFailed={onFinishFailed}>
                            <div className={`${valuePage !== 0 ? 'hidden' : ''}`}>
                                <HeaderTitle title="Basic Info" />
                                <BasicInfoForm type={type} handleFileChange={handleFileChangePicture} setPicture={setPicture} picture={picture} />
                            </div>

                            <div className={`${valuePage !== 1 ? 'hidden' : ''}`}>
                                <HeaderTitle title="Bank Account Detail" />
                                <BankAccountDetailForm type={type} form={form} />
                            </div>

                            <div className={`${valuePage !== 2 ? 'hidden' : ''}`}>
                                <HeaderTitle title="Upload CV" />
                                <DocumentCVForm type={type} setFileCV={setFileCV} fileCV={fileCV} handleFileChange={handleFileChangeCV} />
                            </div>

                            <div className={`${valuePage !== 3 ? 'hidden' : ''}`}>
                                <HeaderTitle title="Upload Certificate" />
                                <DocumentCertificateForm id={id} type={type} typeTable="No API" dataListCertificate={dataListCertificate} setDataListCertificate={setDataListCertificate} />
                            </div>

                            <div className={'w-full flex justify-end gap-5'}>
                                <Button onClick={handleCancel} form="form" htmlType="reset" danger type="default" className="btn btn-outline-danger">
                                    Cancel
                                </Button>

                                {valuePage > 0 && (
                                    <Form.Item>
                                        <Button onClick={prev} type="primary" className="btn btn-primary-new">
                                            Previous
                                        </Button>
                                    </Form.Item>
                                )}

                                {valuePage < dataTab.length - 1 && (
                                    <Form.Item>
                                        <Button type="primary" onClick={next} className="btn btn-primary-new">
                                            Next
                                        </Button>
                                    </Form.Item>
                                )}

                                {valuePage === dataTab.length - 1 && (
                                    <Form.Item>
                                        <Button type="primary" htmlType="submit" className="btn btn-primary-new">
                                            Save
                                        </Button>
                                    </Form.Item>
                                )}
                            </div>
                        </Form>
                    </Cards>
                </div>
            </div>
        </Fragment>
    );
};

export default EmployeeForm;
