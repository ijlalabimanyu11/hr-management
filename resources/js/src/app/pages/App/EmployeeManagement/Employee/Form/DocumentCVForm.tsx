import { DownloadOutlined } from '@ant-design/icons';
import { Button, Form, Input, Space, Upload, message } from 'antd';
import React from 'react';
import { useDispatch } from 'react-redux';
import { AppDispatch } from '../../../../../../redux/store';
import { getDownloadCV } from '../../../../../../redux/slices/App/EmployeeManagement/Employee/EmployeeSlice';

interface DocumentCVFormProps {
    type: any;
    handleFileChange?: any;
    setFileCV?: any;
    fileCV?: any;
    id?: any;
}

const DocumentCVForm: React.FC<DocumentCVFormProps> = ({ type, fileCV, setFileCV, handleFileChange, id }) => {
    // Selector

    // Declaration
    const dispatch = useDispatch<AppDispatch>();

    // Use State

    // Use Effect

    const handleDownload = () => {
        dispatch(getDownloadCV({ id: id, filename: fileCV?.name }));
    };

    const props = {
        beforeUpload: (file: any) => {
            const isLt10M = file.size / 1024 / 1024 < 10;
            if (!isLt10M) {
                message.error(`File must be smaller than 10 MB!`);
            }
            return isLt10M || Upload.LIST_IGNORE;
        },
    };
    return (
        <div className="pt-5">
            <Form.Item
                label="Curriculum Vitae"
                name="cv"
                rules={[
                    {
                        required: true,
                        message: 'Please upload your Curriculum Vitae!',
                    },
                ]}
            >
                <Space.Compact>
                    <Form.Item noStyle>
                        <Input disabled={true} value={fileCV?.name} />
                    </Form.Item>
                    <Form.Item noStyle>
                        {type === 'detail' ? (
                            <Button icon={<DownloadOutlined />} onClick={handleDownload} className="bg-blue-500 text-white px-4 py-2 hover:bg-blue-600 transition-colors duration-300"></Button>
                        ) : (
                            <Upload {...props} showUploadList={false} maxCount={1} beforeUpload={() => false} onChange={handleFileChange} accept="application/pdf">
                                <Button disabled={type === 'detail'} className="bg-blue-500 text-white px-4 py-2 hover:bg-blue-600 transition-colors duration-300">
                                    Browse
                                </Button>
                            </Upload>
                        )}
                    </Form.Item>
                </Space.Compact>
            </Form.Item>
        </div>
    );
};

export default DocumentCVForm;
