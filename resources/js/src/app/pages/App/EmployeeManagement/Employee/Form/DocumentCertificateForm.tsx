import { PlusOutlined } from '@ant-design/icons';
import { Button, DatePicker, Form, Input, Modal, Space, Tooltip, Upload, message } from 'antd';
import React, { useCallback, useEffect, useState } from 'react';
import Cards from '../../../../../../components/Cards';
import TablePagination from '../../../../../../components/TablePagination';
import { TableDocumentCertificate } from '../Table/TableDocumentCertificate';
import InputComponent from '../../../../../../components/InputComponent';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../../../../redux/store';
import {
    deleteEmployeeCertification,
    getDetailEmployeeCertificate,
    getDownloadCertificate,
    getEmployeeCertificatePaginate,
} from '../../../../../../redux/slices/App/EmployeeManagement/Employee/EmployeeSlice';
import { searchObject } from '../../../../../../utils/searchObject';
import ModalConfirm from '../../../../../../components/ModalConfirm';
import dayjs from 'dayjs';

interface DocumentCertificateFormProps {
    type: any;
    id?: any;
    typeTable?: any;
    setDataListCertificate?: any;
    dataListCertificate?: any;
}

const DocumentCertificateForm: React.FC<DocumentCertificateFormProps> = ({ id, type, typeTable = 'No API', dataListCertificate, setDataListCertificate }) => {
    // Selector
    const { dataPagingCertificate } = useSelector((state: RootState) => state.employee);

    // Declaration
    const [formDetail] = Form.useForm();
    const dispatch = useDispatch<AppDispatch>();
    const dataSource = dataPagingCertificate?.data;

    // Use State
    const [page, setPage] = useState<number>(1);
    const [pageSize, setPageSize] = useState<number>(10);
    const [sort, setSort] = useState<string>('');
    const [search, setSearch] = useState<Record<string, string>>({});
    const [searchText, setSearchText] = useState<{ [key: string]: string }>({});
    const [searchedColumn, setSearchedColumn] = useState<string[]>([]);

    const [typeModal, setTypeModal] = useState<string>('');
    const [idData, setIdData] = useState<any>(0);
    const [openModal, setOpenModal] = useState<boolean>(false);
    const [modalDelete, setModalDelete] = useState<boolean>(false);
    const [file, setFile] = useState<File | null>(null);
    const [dataUpdate, setDataUpdate] = useState<any>({});
    const [keyTable, setKeyTable] = useState<any>();
    const [api, setApi] = useState<any>('');
    const [dataDelete, setDataDelete] = useState<any>({});

    // Use Effect
    useEffect(() => {
        if (typeTable === 'API') {
            dispatch(getEmployeeCertificatePaginate({ id: id, search: searchObject(search), page, pageSize, sort }));
        }
    }, [dispatch, typeTable, id, search, page, pageSize, sort]);

    useEffect(() => {
        if (typeModal === 'update' || api === 'no') {
            formDetail.setFieldsValue({
                name: dataUpdate.name,
                issuing_organization: dataUpdate.issuing_organization,
                issuing_date: dayjs(dataUpdate.issuing_date),
                expired_date: dayjs(dataUpdate.expired_date),
                notes: dataUpdate.notes,
                certification: dataUpdate.certification,
                file_name: dataUpdate.file_name,
            });
            setFile(dataUpdate.certification);
        }
    }, [typeModal, formDetail, api]);

    const refreshPage = () => {
        dispatch(getEmployeeCertificatePaginate({ id: id, search: searchObject(search), page, pageSize, sort }));
    };

    // Handle Change Page Table
    const handleChangePage = (pageChange: number, pageSizeChange: number) => {
        const tempPage = pageSize !== pageSizeChange ? 1 : pageChange;
        setPage(tempPage);
        setPageSize(pageSizeChange);
    };

    // Sorting Table
    const onSort = (_: any, __: any, sort: any) => {
        const dataSort = sort.order !== undefined ? `${sort.field}~${sort.order === 'ascend' ? 'asc' : 'desc'}` : '';
        setSort(dataSort);
    };

    // Handle Search Table
    const handleSearch = (selectedKeys: string[], confirm: () => void, dataIndex: string) => {
        confirm();
        setSearchText((prevState) => ({ ...prevState, [dataIndex]: selectedKeys[0] }));
        setSearchedColumn((prevState) => [...new Set([...prevState, dataIndex])]);
        setSearch((prevState: Record<string, string>) => {
            const newSearch = { ...prevState, [dataIndex]: selectedKeys[0] };
            if (!selectedKeys[0]) {
                delete newSearch[dataIndex];
            }
            if (prevState[dataIndex] !== selectedKeys[0]) {
                setPage(1);
            }
            return newSearch;
        });
    };

    // Filter Table
    const onFilter = (dataIndex: any, value: any, record: any) => {
        const fixSearchText = value?.toLowerCase();
        return record[dataIndex]?.toLowerCase().includes(fixSearchText);
    };

    // Sorting Table
    const sorter = (fieldSort: any, a: any, b: any) => {
        const handleDataSort = (obj: any) => {
            return obj[fieldSort]?.toLowerCase();
        };
        let fa = handleDataSort(a);
        let fb = handleDataSort(b);
        return fa.localeCompare(fb);
    };

    // Delete Row
    const handleDeleteFE = useCallback(
        (r: any) => {
            setDataListCertificate((prevState: any) => prevState.filter((e: any) => e.key !== r.key));
        },
        [dataListCertificate]
    );

    // Handle Create
    const handleCreate = () => {
        setTypeModal('Create');
        setOpenModal(true);
    };

    // Handle Cancel Modal
    const handleCancelModal = () => {
        formDetail.resetFields();
        setFile(null);
        setOpenModal(false);
        setTypeModal('');
        setApi('');
    };

    // Handle Update
    const handleUpdate = (value: any) => {
        if (value?.type === 'exist') {
            setApi('yes');
            dispatch(getDetailEmployeeCertificate(value?.id));
        } else {
            setKeyTable(value?.key);
            setApi('no');
            setDataUpdate(value);
        }
        setTypeModal('Update');
        setOpenModal(true);
    };

    // Handle Delete
    const handleDelete = (value: any) => {
        if (value?.type === 'exist') {
            setIdData(value.id);
            setModalDelete(true);
        } else {
            setDataDelete(value);
            setModalDelete(true);
        }
    };

    // Handle Download Attachment
    const handleDownloadAttachment = (value: any) => {
        dispatch(getDownloadCertificate({ id: value.id, filename: value.file }));
    };

    // Function to handle file change
    const handleFileChange = ({ file }) => {
        setFile(file);
    };

    // Props Upload
    const props = {
        beforeUpload: (file: any) => {
            const isLt10M = file.size / 1024 / 1024 < 10;
            if (!isLt10M) {
                message.error(`File must be smaller than 10 MB!`);
            }
            return isLt10M || Upload.LIST_IGNORE;
        },
    };

    // Handle Submit Delete
    const submitDelete = () => {
        if (idData === 0) {
            setModalDelete(false);
            handleDeleteFE(dataDelete);
            setDataDelete({});
        } else {
            setModalDelete(false);
            dispatch(deleteEmployeeCertification({ id: idData }))
                .unwrap()
                .then(() => {
                    refreshPage();
                    setIdData(0);
                })
                .catch((r) => {
                    refreshPage();
                });
        }
    };

    // Access Button
    const accessButton = (
        <Tooltip title="Create">
            <Button onClick={handleCreate} className="btn-icon btn-primary" icon={<PlusOutlined />} />
        </Tooltip>
    );

    // Handle Submit
    const handleSubmit = (formValue: any) => {
        if (typeModal === 'Create') {
            const newData = [...dataListCertificate];

            const dataValue = {
                key: dataListCertificate.length + 1,
                name: formValue.name,
                issuing_organization: formValue.issuing_organization,
                issuing_date: dayjs(formValue.issuing_date).format('YYYY-MM-DD'),
                expired_date: dayjs(formValue.expired_date).format('YYYY-MM-DD'),
                notes: formValue.notes,
                certification: file,
                file_name: file?.name,
            };

            newData.push(dataValue);
            setDataListCertificate(newData);
            setOpenModal(false);
            formDetail.resetFields();
            setFile(null);
            setTypeModal('');
        } else {
            const dataValue = {
                key: keyTable,
                name: formValue.name,
                issuing_organization: formValue.issuing_organization,
                issuing_date: dayjs(formValue.issuing_date).format('YYYY-MM-DD'),
                expired_date: dayjs(formValue.expired_date).format('YYYY-MM-DD'),
                notes: formValue.notes,
                certification: file,
                file_name: file?.name,
            };

            const findIndex = dataListCertificate.findIndex((e: any) => e.key === keyTable);
            const newData = [...dataListCertificate];
            const item = newData[findIndex];
            const updateRow = { ...item, ...dataValue };
            newData.splice(findIndex, 1, updateRow);
            setDataListCertificate(newData);
            setOpenModal(false);
            formDetail.resetFields();
            setFile(null);
            setTypeModal('');
        }
    };

    return (
        <div className="pt-5">
            <div className="pb-7">
                <TablePagination
                    type={typeTable === 'No API' ? 'FE' : 'BE'}
                    dataSource={typeTable === 'No API' ? dataListCertificate : dataSource}
                    totalData={typeTable === 'No API' ? dataListCertificate?.length : dataSource}
                    current={page}
                    pageSize={pageSize}
                    onChange={handleChangePage}
                    columns={TableDocumentCertificate(
                        page,
                        pageSize,
                        searchedColumn,
                        searchText,
                        type,
                        typeTable,
                        handleSearch,
                        handleDelete,
                        handleUpdate,
                        handleDownloadAttachment,
                        onFilter,
                        sorter
                    )}
                    onSort={onSort}
                    tableScrolled={{ x: 1800, y: 300 }}
                    content={type === 'detail' ? null : accessButton}
                />
            </div>

            <Modal
                maskClosable={false}
                centered
                title={`${typeModal} Certificate`}
                open={openModal}
                onOk={handleSubmit}
                onCancel={handleCancelModal}
                width={600}
                footer={[
                    <Button onClick={handleCancelModal} form="formDetail" htmlType="reset" danger type="default" className="btn btn-outline-danger">
                        Discard
                    </Button>,
                    <Button htmlType="submit" form="formDetail" type="primary" className="btn btn-primary-new">
                        Save
                    </Button>,
                ]}
            >
                <div className="w-full flex flex-col p-2">
                    <Form id="formDetail" layout="vertical" form={formDetail} onFinish={handleSubmit}>
                        <div className="w-full grid grid-cols-2 gap-2">
                            <Form.Item
                                label="Certification Name"
                                name={'name'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Certification Name!',
                                    },
                                ]}
                            >
                                <InputComponent placeholder="Input Certification Name" />
                            </Form.Item>

                            <Form.Item
                                label="Issuing Organization"
                                name={'issuing_organization'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Issuing Organization!',
                                    },
                                ]}
                            >
                                <InputComponent placeholder="Input Issuing Organization" />
                            </Form.Item>

                            <Form.Item
                                label="Issued Date"
                                name={'issuing_date'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Issued Date!',
                                    },
                                ]}
                            >
                                <DatePicker style={{ width: '100%' }} placeholder="Input Issued Date" format={'YYYY-MM-DD'} />
                            </Form.Item>

                            <Form.Item
                                label="Expired Date"
                                name={'expired_date'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Expired Date!',
                                    },
                                ]}
                            >
                                <DatePicker style={{ width: '100%' }} placeholder="Input Expired Date" format={'YYYY-MM-DD'} />
                            </Form.Item>

                            <Form.Item
                                label="Notes"
                                name={'notes'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Notes!',
                                    },
                                ]}
                            >
                                <InputComponent placeholder="Input Notes" />
                            </Form.Item>

                            <Form.Item
                                label="Attachment"
                                name="certification"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please upload your Attachment!',
                                    },
                                ]}
                            >
                                <Space.Compact>
                                    <Form.Item noStyle>
                                        <Input disabled={true} value={file?.name} />
                                    </Form.Item>
                                    <Form.Item noStyle>
                                        <Upload {...props} accept="application/pdf" showUploadList={false} maxCount={1} beforeUpload={() => false} onChange={handleFileChange}>
                                            <Button className="bg-blue-500 text-white px-4 py-2 hover:bg-blue-600 transition-colors duration-300">Browse</Button>
                                        </Upload>
                                    </Form.Item>
                                </Space.Compact>
                            </Form.Item>
                        </div>
                    </Form>
                </div>
            </Modal>

            {/* Modal Delete */}
            {modalDelete ? (
                <ModalConfirm
                    title={`Are you sure want to delete the employee certificate?`}
                    icon="warning"
                    onConfirm={submitDelete}
                    onCancel={() => {
                        setIdData(0);
                        setDataDelete({});
                        setModalDelete(false);
                    }}
                />
            ) : null}
        </div>
    );
};

export default DocumentCertificateForm;
