import React from 'react';
import ColumnSearch from '../../../../../../utils/ColumnSearch';
import Action from '../../../../../../components/Action';
import { TbCircleCheck, TbPencil } from 'react-icons/tb';
import { RiDeleteBinLine } from 'react-icons/ri';
import { PermissionAction } from '../../../../../../utils/PermissionAction';
import { BiMinusCircle } from 'react-icons/bi';
import { MdSearch } from "react-icons/md";
import Pills from '../../../../../../components/Pills';
import { APP_ROUTES } from '../../../../../../router/app/app_routes';

export const TableEmployee = (
    page = 1,
    pageSize = 10,
    searchedColumn: any,
    searchText: any,
    handleSearch: (selectedKeys: string[], confirm: () => void, dataIndex: string) => void,
    handleDelete: (value: any) => void,
    handleActiveOrInactive: (value: any) => void,
) => {
    const action = (record: any) => {
        const actions: any[] = [];

        if (PermissionAction('employee', 'READ')) {
            actions.push({
                link: APP_ROUTES.EMPLOYEE_DETAIL_ELEMENTS,
                state: record,
                type: 'detail',
                icon: <MdSearch size={16} />,
            });
        }

        if (PermissionAction('employee', 'TOGGLE')) {
            actions.push({
                type: record.status === 'ACTIVE' ? 'inactive' : 'active',
                icon: record.status === 'ACTIVE' ? <BiMinusCircle size={16} /> : <TbCircleCheck size={16} />,
                onClick: () => handleActiveOrInactive(record),
            });
        }

        if (PermissionAction('employee', 'UPDATE')) {
            actions.push({
                link: APP_ROUTES.EMPLOYEE_UPDATE_ELEMENTS,
                state: record,
                type: 'update',
                icon: <TbPencil size={16} />,
            });
        }

        if (PermissionAction('employee', 'DELETE')) {
            actions.push({
                type: 'delete',
                icon: <RiDeleteBinLine size={16} />,
                onClick: () => handleDelete(record),
            });
        }

        return actions.length > 0 ? <Action actions={actions} /> : null;
    }

    const columns: any = [
        {
            title: 'No',
            align: 'center',
            width: 60,
            render: (text: any, object: any, index: number) => (page - 1) * pageSize + index + 1,
        },
        {
            title: 'National Identifier',
            sorter: true,
            align: 'left',
            dataIndex: 'national_identifier',
            key: 'national_identifier',
            ...ColumnSearch({ dataIndex: 'national_identifier', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Name',
            sorter: true,
            align: 'left',
            dataIndex: 'name',
            key: 'name',
            ...ColumnSearch({ dataIndex: 'name', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Email',
            sorter: true,
            align: 'left',
            dataIndex: 'email',
            key: 'email',
            ...ColumnSearch({ dataIndex: 'email', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Date of Birth',
            sorter: true,
            dataIndex: 'birth_date',
            key: 'birth_date',
            ...ColumnSearch({ dataIndex: 'birth_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Gender',
            sorter: true,
            align: 'center',
            dataIndex: 'gender',
            key: 'gender',
            ...ColumnSearch({ dataIndex: 'gender', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Phone',
            sorter: true,
            align: "right",
            dataIndex: 'phone',
            key: 'phone',
            ...ColumnSearch({ dataIndex: 'phone', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Address',
            sorter: true,
            align: "left",
            dataIndex: 'address',
            key: 'address',
            ...ColumnSearch({ dataIndex: 'address', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Join Date',
            sorter: true,
            dataIndex: 'join_date',
            key: 'join_date',
            ...ColumnSearch({ dataIndex: 'join_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Resignation Date',
            sorter: true,
            dataIndex: 'resignation_date',
            key: 'resignation_date',
            ...ColumnSearch({ dataIndex: 'resignation_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Terminate Date',
            sorter: true,
            dataIndex: 'terminate_date',
            key: 'terminate_date',
            ...ColumnSearch({ dataIndex: 'terminate_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Created By',
            sorter: true,
            align: 'left',
            dataIndex: 'created_by',
            key: 'created_by',
            ...ColumnSearch({ dataIndex: 'created_by', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Created Date',
            sorter: true,
            dataIndex: 'created_date',
            key: 'created_date',
            align: 'center',
            ...ColumnSearch({ dataIndex: 'created_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Modified By',
            sorter: true,
            align: 'left',
            dataIndex: 'modified_by',
            key: 'modified_by',
            ...ColumnSearch({ dataIndex: 'modified_by', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Modified Date',
            sorter: true,
            dataIndex: 'modified_date',
            key: 'modified_date',
            align: 'center',
            ...ColumnSearch({ dataIndex: 'modified_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Status',
            sorter: true,
            dataIndex: 'status',
            key: 'status',
            ...ColumnSearch({ dataIndex: 'status', searchedColumn, searchText, handleSearch, typeFilter: 'status' }),
            render: (index: any) => {
                let text: any;
                switch (index) {
                    case 'WAITING APPROVAL':
                        text = 'Waiting Approval';
                        break;
                    default:
                        text = index ? index.charAt(0).toUpperCase() + index.slice(1).toLowerCase() : index;
                        break;
                }
                return text ? (
                    <div className={'flex justify-center'}>
                        <Pills color={text}>{text}</Pills>
                    </div>
                ) : (
                    text
                );
            },
        },
    ]

    if (PermissionAction('employee', 'UPDATE') || PermissionAction('employee', 'DELETE') || PermissionAction('employee', 'TOGGLE') || PermissionAction('employee', 'READ')) {
        columns.push({
            title: 'Action',
            align: 'center',
            fixed: 'right',
            render: (text: any, record: any) => action(record),
        });
    }

    return columns;

}
