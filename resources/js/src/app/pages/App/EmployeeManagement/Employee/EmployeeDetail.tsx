import React, { Fragment, useEffect, useState } from 'react';
import Breadcrumbs from '../../../../../components/Breadcrumbs';
import { useLocation, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../../../redux/store';
import { setPageTitle } from '../../../../../redux/store/themeConfigSlice';
import { APP_ROUTES } from '../../../../../router/app/app_routes';
import HeaderTitle from '../../../../../components/HeaderTitle';
import Cards from '../../../../../components/Cards';
import Frame from '../../../../../components/Frame';
import { Button, Form, Image } from 'antd';
import BasicInfoForm from './Form/BasicInfoForm';
import BankAccountDetailForm from './Form/BankAccountDetailForm';
import DocumentCVForm from './Form/DocumentCVForm';
import DocumentCertificateForm from './Form/DocumentCertificateForm';
import { getDetailEmployee } from '../../../../../redux/slices/App/EmployeeManagement/Employee/EmployeeSlice';
import dayjs from 'dayjs';
import appHttpService from '../../../../../redux/services/AppService';

const EmployeeDetail = () => {
    // Selector
    const { dataDetail } = useSelector((state: RootState) => state.employee);

    // Declaration
    const navigate = useNavigate();
    const dispatch = useDispatch<AppDispatch>();
    const [form] = Form.useForm();
    const location = useLocation();
    const { id } = location?.state || {};

    // Use State
    const [valuePage, setValuePage] = useState<any | null>(0);
    const [picture, setPicture] = useState<any>([]);
    const [fileCV, setFileCV] = useState<File | null>(null);

    // Use Effect
    useEffect(() => {
        dispatch(setPageTitle('Employee'));
    });

    useEffect(() => {
        dispatch(getDetailEmployee(id));
    }, [dispatch, id]);

    useEffect(() => {
        if (id && dataDetail) {
            form.setFieldsValue({
                employee_code: dataDetail?.employee_code,
                national_identifier: dataDetail?.national_identifier,
                name: dataDetail?.name,
                email: dataDetail?.email,
                birth_date: dayjs(dataDetail?.birth_date),
                gender: dataDetail?.gender,
                phone: dataDetail?.phone,
                address: dataDetail?.address,
                salary_type: dataDetail?.salary_type,
                basic_salary: dataDetail?.basic_salary,
                join_date: dayjs(dataDetail?.join_date),
                bank_name: dataDetail?.bank_name,
                branch_location: dataDetail?.branch_location,
                account_holder_name: dataDetail?.bank_account_name,
                account_number: dataDetail?.bank_account_number,
                tax_payer_identifier: dataDetail?.tax_payer_identifier,
            });

            const fetchDataFileCV = async () => {
                const url = `/employee/download/${id}/cv`;
                const file = await appHttpService.getFileData(url, dataDetail?.cv);
                setFileCV(file);
            };

            const fetchDataFilePicture = async () => {
                const url = `/employee/download/${id}/photo`;
                const filePicture = await appHttpService.getFileDataPicture(url, dataDetail?.employee_picture);
                setPicture([filePicture]);
            };

            fetchDataFileCV();
            fetchDataFilePicture();
        }
    }, [id, dataDetail]);

    // Data Tab
    const dataTab = [
        {
            key: 0,
            value: 'Basic Info',
        },
        {
            key: 1,
            value: 'Bank Account Detail',
        },
        {
            key: 2,
            value: 'Document CV',
        },
        {
            key: 3,
            value: 'Document Certificate',
        },
    ];

    // Breadcrumbs
    const routes = [
        {
            title: 'App',
            href: '',
        },
        {
            title: 'Employee Management',
            href: '',
        },
        {
            title: 'Employee',
            href: APP_ROUTES.EMPLOYEE_VIEW_ELEMENTS,
        },
        {
            title: 'Detail Employee',
            href: APP_ROUTES.EMPLOYEE_DETAIL_ELEMENTS,
        },
    ];

    // Function Next
    const next = () => {
        setValuePage(valuePage + 1);
    };

    // Function Prev
    const prev = () => {
        setValuePage(valuePage - 1);
    };

    // Handle Back
    const handleBack = () => {
        setValuePage(0);
        navigate(-1);
    };

    return (
        <Fragment>
            <Breadcrumbs routes={routes} />
            <div className="flex w-full justify-between align-middle">
                <HeaderTitle title={`Detail Employee`} />
            </div>

            <div className="flex justify-between">
                <div className="w-1/5 mr-2">
                    <Cards type={'card'}>
                        <Frame activeIndex={valuePage} setActiveIndex={setValuePage} data={dataTab} />
                    </Cards>
                </div>

                <div className="w-4/5 ml-2">
                    <Cards>
                        <Form id="form" layout="vertical" form={form} name="dynamic_rule">
                            <div className={`${valuePage !== 0 ? 'hidden' : ''}`}>
                                <HeaderTitle title="Basic Info" />
                                <BasicInfoForm type={'detail'} setPicture={setPicture} picture={picture} />
                            </div>

                            <div className={`${valuePage !== 1 ? 'hidden' : ''}`}>
                                <HeaderTitle title="Bank Account Detail" />
                                <BankAccountDetailForm type={'detail'} form={form} />
                            </div>

                            <div className={`${valuePage !== 2 ? 'hidden' : ''}`}>
                                <HeaderTitle title="Upload CV" />
                                <DocumentCVForm type={'detail'} fileCV={fileCV} id={id} />
                            </div>

                            <div className={`${valuePage !== 3 ? 'hidden' : ''}`}>
                                <HeaderTitle title="Upload Certificate" />
                                <DocumentCertificateForm id={id} type={'detail'} typeTable="API" />
                            </div>
                        </Form>
                        <div className={'w-full flex justify-end gap-5'}>
                            <Button onClick={handleBack} type="default" className="btn btn-primary-new">
                                Back
                            </Button>
                        </div>
                    </Cards>
                </div>
            </div>
        </Fragment>
    );
};

export default EmployeeDetail;
