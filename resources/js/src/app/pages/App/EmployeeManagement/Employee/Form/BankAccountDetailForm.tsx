import { Form, Select } from 'antd';
import React, { useEffect } from 'react';
import SelectComponent from '../../../../../../components/SelectComponent';
import InputComponent from '../../../../../../components/InputComponent';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../../../../redux/store';
import { getBank } from '../../../../../../redux/slices/GlobalSlice';

const BankAccountDetailForm = ({ type, form }) => {
    // Selector
    const { dataBank } = useSelector((state: RootState) => state.global);

    // Declaration
    const dispatch = useDispatch<AppDispatch>();

    // Use Effect
    useEffect(() => {
        dispatch(getBank());
    }, [dispatch]);
    return (
        <div className="pt-5">
            <div className="w-full grid grid-cols-3 gap-4">
                <Form.Item
                    label="Bank Name"
                    name={'bank_name'}
                    rules={[
                        {
                            required: true,
                            message: 'Please select your Bank Name!',
                        },
                    ]}
                >
                    <SelectComponent placeholder="Select Bank Name" disabled={type === 'detail'}>
                        {dataBank &&
                            dataBank?.map((data: any, index: any) => (
                                <Select.Option value={data.key} key={index}>
                                    {data.value}
                                </Select.Option>
                            ))}
                    </SelectComponent>
                </Form.Item>

                <Form.Item
                    label={'Branch Location'}
                    name={'branch_location'}
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Branch Location!',
                        },
                    ]}
                >
                    <InputComponent
                        placeholder="Input Branch Location"
                        disabled={type === 'detail'}
                        onChange={(e) => {
                            const { value } = e.target;
                            const alphabeticValue = value.replace(/[^a-zA-Z]/g, '').toUpperCase();
                            e.target.value = alphabeticValue;
                            form.setFieldsValue({
                                branch_location: alphabeticValue,
                            });
                        }}
                    />
                </Form.Item>

                <Form.Item
                    label={'Account Holder Name'}
                    name={'account_holder_name'}
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Account Holder Name!',
                        },
                    ]}
                >
                    <InputComponent
                        placeholder="Input Account Holder Name"
                        disabled={type === 'detail'}
                        onChange={(e) => {
                            const { value } = e.target;
                            const alphabeticValue = value.replace(/[^a-zA-Z]/g, '').toUpperCase();
                            e.target.value = alphabeticValue;
                            form.setFieldsValue({
                                account_holder_name: alphabeticValue,
                            });
                        }}
                    />
                </Form.Item>

                <Form.Item
                    label={'Account Number'}
                    name={'account_number'}
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Account Number!',
                        },
                    ]}
                >
                    <InputComponent
                        placeholder="Input Account Number"
                        disabled={type === 'detail'}
                        onChange={(e) => {
                            const { value } = e.target;
                            const numericValue = value.replace(/[^0-9]/g, '');
                            e.target.value = numericValue;
                            form.setFieldsValue({
                                account_number: numericValue,
                            });
                        }}
                    />
                </Form.Item>

                <Form.Item
                    label={'Tax Payer ID'}
                    name={'tax_payer_identifier'}
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Tax Payer ID!',
                        },
                        {
                            pattern: /^\d{16}$/,
                            message: 'National Identifier must be exactly 16 digits!',
                        },
                    ]}
                >
                    <InputComponent onInput={(e) => (e.target.value = e.target.value.replace(/\D/g, ''))} maxLength={16} placeholder="Input Tax Payer ID" disabled={type === 'detail'} />
                </Form.Item>
            </div>
        </div>
    );
};

export default BankAccountDetailForm;
