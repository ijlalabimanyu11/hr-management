import { Button, DatePicker, Form, Radio, Select, Upload, Image } from 'antd';
import React, { Fragment, useEffect, useState } from 'react';
import InputComponent from '../../../../../../components/InputComponent';
import SelectComponent from '../../../../../../components/SelectComponent';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../../../../redux/store';
import { setPageTitle } from '../../../../../../redux/store/themeConfigSlice';
import { getBasicSalary, getSalary } from '../../../../../../redux/slices/GlobalSlice';

interface BasicFormProps {
    type: any;
    handleFileChange?: any;
    setPicture?: any;
    picture?: any;
}

const BasicInfoForm: React.FC<BasicFormProps> = ({ type, handleFileChange, setPicture, picture }) => {
    // Selector
    const { dataSalary, dataBasicSalary } = useSelector((state: RootState) => state.global);

    // Declaration
    const dispatch = useDispatch<AppDispatch>();

    // Use State
    const [previewOpen, setPreviewOpen] = useState(false);
    const [previewImage, setPreviewImage] = useState('');
    const [address, setAddress] = useState('');

    // Use Effect
    useEffect(() => {
        dispatch(setPageTitle('Employee'));
    });
    useEffect(() => {
        dispatch(getSalary());
        dispatch(getBasicSalary());
    }, [dispatch]);

    // Upload Button
    const uploadButton = (
        <div style={{ position: 'relative', height: '200px' }}>
            <button
                style={{
                    position: 'absolute',
                    bottom: '50px', // Positioning at the bottom-right corner
                    left: '20px',
                    width: '40px', // Adjust size for the circle
                    height: '40px',
                    borderRadius: '50%', // Making it circular
                    backgroundImage: 'linear-gradient(to right, #5E88FC, #264E86)', // Linear gradient for the button
                    border: 'none', // Remove border since we want a solid gradient
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    cursor: 'pointer',
                }}
                type="button"
            >
                <PlusOutlined style={{ fontSize: '24px', color: '#FFFFFF' }} />
            </button>
        </div>
    );

    // Get Base 64
    const getBase64 = (file: any): Promise<string> =>
        new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result as string);
            reader.onerror = (error) => reject(error);
        });

    // Function Preview Image
    const handlePreview = async (file: any) => {
        if (!file.url && !file.preview) {
            file.preview = await getBase64(file.originFileObj);
        }
        setPreviewImage(file.url || file.preview);
        setPreviewOpen(true);
    };

    const handleChange = ({ fileList: newFileList }) => setPicture(newFileList);

    return (
        <div className="pt-5">
            <div className="flex justify-between items-start w-full">
                <div className="flex items-center w-1/6">
                    <Form.Item
                        name="employee_picture"
                        // rules={[
                        //     {
                        //         required: true,
                        //         message: 'Please upload your Picture!',
                        //     },
                        // ]}
                    >
                        <Form.Item noStyle>
                            <>
                                <Upload
                                    maxCount={1}
                                    beforeUpload={() => false}
                                    onChange={handleChange}
                                    listType="picture-circle"
                                    onPreview={handlePreview}
                                    fileList={picture}
                                >
                                    {picture === null || picture?.length === 0 || picture?.status === 'removed' ? uploadButton : null}
                                </Upload>
                                {previewImage && (
                                    <Image
                                        wrapperStyle={{
                                            display: 'none',
                                        }}
                                        preview={{
                                            visible: previewOpen,
                                            onVisibleChange: (visible) => setPreviewOpen(visible),
                                            afterOpenChange: (visible) => !visible && setPreviewImage(''),
                                        }}
                                        src={previewImage}
                                    />
                                )}
                            </>
                        </Form.Item>
                    </Form.Item>
                </div>

                <div className="w-5/6 grid grid-cols-3 gap-4">
                    <Form.Item
                        label={'Employee Code'}
                        name={'employee_code'}
                        rules={[
                            {
                                required: true,
                                message: 'Please input your Employee Code!',
                            },
                        ]}
                    >
                        <InputComponent placeholder="Input Employee Code" disabled={type === 'detail' || type === 'update'} />
                    </Form.Item>

                    <Form.Item
                        label={'National Identifier'}
                        name={'national_identifier'}
                        rules={[
                            {
                                required: true,
                                message: 'Please input your National Identifier!',
                            },
                            {
                                pattern: /^\d{16}$/,
                                message: 'National Identifier must be exactly 16 digits!',
                            },
                        ]}
                    >
                        <InputComponent onInput={(e) => (e.target.value = e.target.value.replace(/\D/g, ''))} placeholder="Input National Identifier" disabled={type === 'detail'} maxLength={16} />
                    </Form.Item>

                    <Form.Item
                        label={'Name'}
                        name={'name'}
                        rules={[
                            {
                                required: true,
                                message: 'Please input your Name!',
                            },
                        ]}
                    >
                        <InputComponent placeholder="Input Name" disabled={type === 'detail'} />
                    </Form.Item>

                    <Form.Item
                        label={'Email'}
                        name={'email'}
                        rules={[
                            {
                                required: true,
                                message: 'Please input your Email!',
                            },
                            {
                                type: 'email',
                                message: 'The input is not valid Email!',
                            },
                        ]}
                    >
                        <InputComponent placeholder="Input Email" disabled={type === 'detail'} />
                    </Form.Item>

                    <Form.Item
                        label={'Date of Birth'}
                        name={'birth_date'}
                        rules={[
                            {
                                required: true,
                                message: 'Please input your Date of Birth!',
                            },
                        ]}
                    >
                        <DatePicker style={{ width: '100%' }} placeholder="Input Date of Birth" format={'YYYY-MM-DD'} disabled={type === 'detail'} />
                    </Form.Item>

                    <div className="w-full">
                        <Form.Item
                            name="gender"
                            label="Gender"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please select your gender!',
                                },
                            ]}
                        >
                            <Radio.Group className="grid grid-cols-2 gap-2" disabled={type === 'detail'}>
                                <Radio value="Male"> Male </Radio>
                                <Radio value="Female"> Female </Radio>
                            </Radio.Group>
                        </Form.Item>
                    </div>

                    <Form.Item
                        label="Salary Type"
                        name={'salary_type'}
                        rules={[
                            {
                                required: true,
                                message: 'Please select your Salary Type!',
                            },
                        ]}
                    >
                        <SelectComponent placeholder="Select Salary Type" disabled={type === 'detail'}>
                            {dataSalary &&
                                dataSalary?.map((data: any, index: any) => (
                                    <Select.Option value={data.key} key={index}>
                                        {data.value}
                                    </Select.Option>
                                ))}
                        </SelectComponent>
                    </Form.Item>

                    <Form.Item
                        label="Basic Salary"
                        name={'basic_salary'}
                        rules={[
                            {
                                required: true,
                                message: 'Please select your Basic Salary!',
                            },
                        ]}
                    >
                        <SelectComponent placeholder="Select Basic Salary" disabled={type === 'detail'}>
                            {dataBasicSalary &&
                                dataBasicSalary?.map((data: any, index: any) => (
                                    <Select.Option value={data.key} key={index}>
                                        {data.value}
                                    </Select.Option>
                                ))}
                        </SelectComponent>
                    </Form.Item>

                    <Form.Item
                        label={'Join Date'}
                        name={'join_date'}
                        rules={[
                            {
                                required: true,
                                message: 'Please input your Join Date!',
                            },
                        ]}
                    >
                        <DatePicker style={{ width: '100%' }} placeholder="Input Join Date" format={'YYYY-MM-DD'} disabled={type === 'detail'} />
                    </Form.Item>

                    <Form.Item
                        label={'Phone'}
                        name={'phone'}
                        rules={[
                            {
                                required: true,
                                message: 'Please input your Phone!',
                            },
                        ]}
                    >
                        <InputComponent placeholder="Input Phone" disabled={type === 'detail'} />
                    </Form.Item>

                    <Form.Item
                        label={'Address'}
                        name={'address'}
                        rules={[
                            {
                                required: true,
                                message: 'Please input your Address!',
                            },
                        ]}
                    >
                        <InputComponent type="textarea" placeholder="Input Address" disabled={type === 'detail'} value={address} onChange={(e: any) => setAddress(e.target.value)} />
                    </Form.Item>
                </div>
            </div>
        </div>
    );
};

export default BasicInfoForm;
