import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../../../redux/store';
import { setPageTitle } from '../../../../../redux/store/themeConfigSlice';
import { deleteTimeSheet, getDetailTimeSheet, getTimeSheetPaginate } from '../../../../../redux/slices/App/Attendance/TImeSheet/TimeSheetSlice';
import { searchObject } from '../../../../../utils/searchObject';
import { APP_ROUTES } from '../../../../../router/app/app_routes';
import PermissionButton from '../../../../../utils/PermissionButton';
import { Button, Spin, Tooltip } from 'antd';
import { DownloadOutlined, PlusOutlined } from '@ant-design/icons';
import Breadcrumbs from '../../../../../components/Breadcrumbs';
import HeaderTitle from '../../../../../components/HeaderTitle';
import Cards from '../../../../../components/Cards';
import TablePagination from '../../../../../components/TablePagination';
import { TableTimeSheet } from './Table/TableViewTimeSheet';
import TimeSheetForm from './TimeSheetForm';
import ModalConfirm from '../../../../../components/ModalConfirm';

const TimeSheetList = () => {
    // Selector
    const { data, loading, dataDetail } = useSelector((state: RootState) => state.time_sheet);

    // Declaration
    const dispatch = useDispatch<AppDispatch>();
    const dataSource = data?.data;

    // Use State
    const [page, setPage] = useState<number>(1);
    const [pageSize, setPageSize] = useState<number>(10);
    const [sort, setSort] = useState<string>('');
    const [search, setSearch] = useState<Record<string, string>>({});
    const [searchText, setSearchText] = useState<{ [key: string]: string }>({});
    const [searchedColumn, setSearchedColumn] = useState<string[]>([]);

    const [typeModal, setTypeModal] = useState<string>('');
    const [idData, setIdData] = useState<any>();
    const [openModal, setOpenModal] = useState<boolean>(false);
    const [modalDelete, setModalDelete] = useState<boolean>(false);

    // Use Effect
    useEffect(() => {
        dispatch(setPageTitle('Timesheet'));
    });

    useEffect(() => {
        dispatch(getTimeSheetPaginate({ search: searchObject(search), page, pageSize, sort }));
    }, [dispatch, search, page, pageSize, sort]);

    // Refresh Data Table
    const refreshPage = () => {
        dispatch(getTimeSheetPaginate({ search: searchObject(search), page, pageSize, sort }));
    };

    // Breadcrumbs
    const routes = [
        {
            title: 'App',
            href: '',
        },
        {
            title: 'Attendance',
            href: '',
        },
        {
            title: 'Timesheet',
            href: APP_ROUTES.TIME_SHEET_VIEW_ELEMENTS,
        },
    ];

    // Handle Change Page Table
    const handleChangePage = (pageChange: number, pageSizeChange: number) => {
        const tempPage = pageSize !== pageSizeChange ? 1 : pageChange;
        setPage(tempPage);
        setPageSize(pageSizeChange);
    };

    // Sorting Table
    const onSort = (_: any, __: any, sort: any) => {
        const dataSort = sort.order !== undefined ? `${sort.field}~${sort.order === 'ascend' ? 'asc' : 'desc'}` : '';
        setSort(dataSort);
    };

    // Handle Search Table
    const handleSearch = (selectedKeys: string[], confirm: () => void, dataIndex: string) => {
        confirm();
        setSearchText((prevState) => ({ ...prevState, [dataIndex]: selectedKeys[0] }));
        setSearchedColumn((prevState) => [...new Set([...prevState, dataIndex])]);
        setSearch((prevState: Record<string, string>) => {
            const newSearch = { ...prevState, [dataIndex]: selectedKeys[0] };
            if (!selectedKeys[0]) {
                delete newSearch[dataIndex];
            }
            if (prevState[dataIndex] !== selectedKeys[0]) {
                setPage(1);
            }
            return newSearch;
        });
    };

    // Handle Create
    const handleCreate = () => {
        setTypeModal('Create');
        setOpenModal(true);
    };

    // Handle Cancel Modal
    const handleCancelModal = () => {
        setIdData('');
        setOpenModal(false);
        setTypeModal('');
    };

    // Handle Update
    const handleUpdate = (value: any) => {
        setTypeModal('Update');
        setOpenModal(true);
        dispatch(getDetailTimeSheet(value?.id));
    };

    // Handle Delete
    const handleDelete = (value: any) => {
        setIdData(value.id);
        setModalDelete(true);
    };

    // Handle Submit Delete
    const submitDelete = () => {
        setModalDelete(false);
        dispatch(deleteTimeSheet({ id: idData }))
            .unwrap()
            .then(() => {
                refreshPage();
            })
            .catch((r) => {
                refreshPage();
            });
    };

    // Handle Download
    const handleDownload = () => {};

    // Access Button
    const accessButton = PermissionButton(
        (props: any) => (
            <div className="flex align-middle gap-3">
                <Tooltip title="Download">
                    <Button onClick={props.handleDownload} className="btn-icon btn-primary" icon={<DownloadOutlined />} />
                </Tooltip>
                <Tooltip title="Create">
                    <Button onClick={props.handleCreate} className="btn-icon btn-primary" icon={<PlusOutlined />} />
                </Tooltip>
            </div>
        ),
        'timesheet',
        ['CREATE', 'READ']
    );
    return (
        <Fragment>
            <Spin spinning={loading}>
                <Breadcrumbs routes={routes} />
                <div className="flex w-full justify-between align-middle">
                    <HeaderTitle title="Timesheet" />
                </div>

                <Cards>
                    <TablePagination
                        dataSource={dataSource}
                        totalData={data?.totalData}
                        current={page}
                        pageSize={pageSize}
                        onChange={handleChangePage}
                        columns={TableTimeSheet(page, pageSize, searchedColumn, searchText, handleSearch, handleUpdate, handleDelete)}
                        onSort={onSort}
                        tableScrolled={{ x: 2800, y: 300 }}
                        content={accessButton({ handleDownload, handleCreate })}
                    />
                </Cards>

                {/* Modal Create/Update Timesheet */}
                <TimeSheetForm type={typeModal} openModal={openModal} cancelModal={handleCancelModal} refreshPage={refreshPage} dataUpdate={dataDetail} />

                {/* Modal Delete */}
                {modalDelete ? <ModalConfirm title={`Are you sure want to delete the timesheet?`} icon="warning" onConfirm={submitDelete} onCancel={() => setModalDelete(false)} /> : null}
            </Spin>
        </Fragment>
    );
};

export default TimeSheetList;
