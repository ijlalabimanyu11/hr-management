import React, { Fragment, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { AppDispatch } from '../../../../../redux/store';
import { Button, DatePicker, Form, Modal, TimePicker } from 'antd';
import dayjs from 'dayjs';
import { createTimeSheet, timeSheetBody, updateTimeSheet } from '../../../../../redux/slices/App/Attendance/TImeSheet/TimeSheetSlice';
import InputComponent from '../../../../../components/InputComponent';
import SelectComponent from '../../../../../components/SelectComponent';

interface TimeSheetFormProps {
    type: any;
    openModal?: any;
    cancelModal?: any;
    refreshPage?: any;
    dataUpdate?: any;
}

const TimeSheetForm: React.FC<TimeSheetFormProps> = ({ type, openModal, cancelModal, refreshPage, dataUpdate }) => {
    // Selector

    // Declaration
    const dispatch = useDispatch<AppDispatch>();
    const [form] = Form.useForm();

    // Use State

    // Use Effect
    useEffect(() => {
        if (type === 'Update' && dataUpdate) {
            form.setFieldsValue({
                id: dataUpdate?.id,
                employee_id: dataUpdate?.employee_id,
                date: dayjs(dataUpdate.date).format('YYYY-MM-DD'),
                correction_cause: dataUpdate?.correction_cause,
                start_time: dataUpdate?.start_time ? dayjs(dataUpdate?.start_time, 'HH:mm:ss') : undefined,
                end_time: dataUpdate?.end_time ? dayjs(dataUpdate?.end_time, 'HH:mm:ss') : undefined,
                activity: dataUpdate?.activity,
                notes: dataUpdate?.notes,
            });
        }
    }, [type, form, dataUpdate]);

    const handleSubmit = (formValue: any) => {
        const data: timeSheetBody = {
            employee_id: formValue.employee_id,
            date: dayjs(formValue.date).format('YYYY-MM-DD'),
            correction_cause: formValue.correction_cause,
            start_time: dayjs(formValue.start_time).format('HH:mm:ss'),
            end_time: dayjs(formValue.end_time).format('HH:mm:ss'),
            activity: formValue.activity,
            notes: formValue.notes,
        };

        console.log(data);

        // if (type === 'Create') {
        //     dispatch(createTimeSheet({ body: data }))
        //         .unwrap()
        //         .then(() => {
        //             cancelModal();
        //             refreshPage();
        //             form.resetFields()
        //         })
        //         .catch(() => {
        //             refreshPage();
        //         });
        // } else {
        //     dispatch(updateTimeSheet({ body: data, id: dataUpdate?.id }))
        //         .unwrap()
        //         .then(() => {
        //             cancelModal();
        //             refreshPage();
        //             form.resetFields()
        //         })
        //         .catch(() => {
        //             refreshPage();
        //         });
        // }
    };
    return (
        <Fragment>
            <Modal
                maskClosable={false}
                centered
                title={`${type} Timesheet`}
                open={openModal}
                onOk={handleSubmit}
                onCancel={() => {
                    form.resetFields();
                    cancelModal();
                }}
                width={600}
                footer={[
                    <Button onClick={cancelModal} form="form" htmlType="reset" danger type="default" className="btn btn-outline-danger">
                        Discard
                    </Button>,
                    <Button htmlType="submit" form="form" type="primary" className="btn btn-primary-new">
                        Save
                    </Button>,
                ]}
            >
                <div className="w-full flex flex-col p-2">
                    <Form id="form" layout="vertical" form={form} onFinish={handleSubmit}>
                        <div className="w-full grid grid-cols-2 gap-2">
                            {type !== 'Create' ? (
                                <>
                                    <Form.Item label="ID" name={'id'}>
                                        <InputComponent disabled={true} />
                                    </Form.Item>
                                    <div />
                                </>
                            ) : null}

                            <Form.Item
                                label="Date"
                                name={'date'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Date!',
                                    },
                                ]}
                            >
                                <DatePicker style={{ width: '100%' }} placeholder="Input Date" format={'YYYY-MM-DD'} value={dayjs(dataUpdate?.start_date, 'YYYY-MM-DD')} allowClear disabled={type !== 'Create' ? true : false} />
                            </Form.Item>

                            <Form.Item
                                label="Correction Cause"
                                name={'correction_cause'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please select your Correction Cause!',
                                    },
                                ]}
                            >
                                <SelectComponent placeholder="Select Correction Cause">
                                    {/* {dataEmployee &&
                                        dataEmployee?.map((data: any, index: any) => (
                                            <Select.Option value={data.key} key={index}>
                                                {data.value}
                                            </Select.Option>
                                        ))} */}
                                </SelectComponent>
                            </Form.Item>

                            <Form.Item
                                label="Start Time"
                                name={'start_time'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Start Time!',
                                    },
                                ]}
                            >
                                <TimePicker showNow={false} needConfirm={false} hideDisabledOptions style={{ width: '100%' }} value={dayjs(dataUpdate?.start_date, 'HH:mm:ss')} placeholder="Input Start Time" allowClear />
                            </Form.Item>

                            <Form.Item
                                label="End Time"
                                name={'end_time'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your End Time!',
                                    },
                                ]}
                            >
                                <TimePicker showNow={false} needConfirm={false} hideDisabledOptions style={{ width: '100%' }} value={dayjs(dataUpdate?.start_date, 'HH:mm:ss')} placeholder="Input End Time" allowClear />
                            </Form.Item>

                            <Form.Item
                                label="Activity"
                                name={'activity'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Activity!',
                                    },
                                ]}
                            >
                                <InputComponent placeholder="Input Activity" />
                            </Form.Item>

                            <Form.Item
                                label="Notes"
                                name={'notes'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Notes!',
                                    },
                                ]}
                            >
                                <InputComponent placeholder="Input Notes" />
                            </Form.Item>
                        </div>
                    </Form>
                </div>
            </Modal>
        </Fragment>
    );
};

export default TimeSheetForm;
