import React, { Fragment, useCallback, useEffect, useState } from 'react';
import Cards from '../../../../../../components/Cards';
import HeaderTitle from '../../../../../../components/HeaderTitle';
import TablePagination from '../../../../../../components/TablePagination';
import { Button, Form, Modal, Tooltip } from 'antd';
import { useDispatch } from 'react-redux';
import { AppDispatch } from '../../../../../../redux/store';
import { handleChangePage, handleSearch, handleSort, SortParams } from '../../../../../../utils/UtilsTable';
import { TableApprovalWorkflowCriteria } from '../Table/TableApprovalWorkflowCriteria';
import { PlusOutlined } from '@ant-design/icons';
import InputComponent from '../../../../../../components/InputComponent';
import SelectComponent from '../../../../../../components/SelectComponent';
import ModalConfirm from '../../../../../../components/ModalConfirm';

interface ApprovalCriteriaFormProps {
    type: any;
    id?: any;
    dataCriteria?: any;
    setDataCriteria?: any;
}

const ApprovalCriteriaForm: React.FC<ApprovalCriteriaFormProps> = ({ type, id, dataCriteria, setDataCriteria }) => {
    // Selector

    // Declaration
    const [formCriteria] = Form.useForm();
    const dispatch = useDispatch<AppDispatch>();

    // Use State
    const [page, setPage] = useState<number>(1);
    const [pageSize, setPageSize] = useState<number>(10);
    const [sort, setSort] = useState<string>('');
    const [search, setSearch] = useState<Record<string, string>>({});
    const [searchText, setSearchText] = useState<{ [key: string]: string }>({});
    const [searchedColumn, setSearchedColumn] = useState<string[]>([]);

    const [typeModal, setTypeModal] = useState<string>('');
    const [openModal, setOpenModal] = useState<boolean>(false);
    const [modalDelete, setModalDelete] = useState<boolean>(false);
    const [dataUpdate, setDataUpdate] = useState<any>({});
    const [keyTable, setKeyTable] = useState<any>();
    const [dataDelete, setDataDelete] = useState<any>({});

    // Use Effect
    useEffect(() => {
        if (typeModal === 'update') {
            formCriteria.setFieldsValue({
                criteria: dataUpdate.criteria,
                operator: dataUpdate.operator,
                value: dataUpdate.value,
            });
        }
    }, [typeModal, formCriteria]);

    // Change page for table
    const handleGeneralPageChange = (page: number, pageSize: number) => handleChangePage(page, pageSize, pageSize, setPage, setPageSize);

    // Search for general table
    const handleGeneralSearch = (selectedKeys: string[], confirm: () => void, dataIndex: string) =>
        handleSearch(selectedKeys, confirm, dataIndex, setSearchText, setSearchedColumn, setSearch, setPage);

    // Sort for general table
    const handleGeneralSort = (_: any, __: any, sorter: SortParams) => {
        handleSort(sorter, setSort);
    };

    // Filter Table
    const onFilter = (dataIndex: any, value: any, record: any) => {
        const fixSearchText = value?.toLowerCase();
        return record[dataIndex]?.toLowerCase().includes(fixSearchText);
    };

    // Sorting Table
    const sorter = (fieldSort: any, a: any, b: any) => {
        const handleDataSort = (obj: any) => {
            return obj[fieldSort]?.toLowerCase();
        };
        let fa = handleDataSort(a);
        let fb = handleDataSort(b);
        return fa.localeCompare(fb);
    };

    // Handle Create
    const handleCreate = () => {
        setTypeModal('Create');
        setOpenModal(true);
    };

    // Handle Cancel Modal
    const handleCancelModal = () => {
        formCriteria.resetFields();
        setOpenModal(false);
        setTypeModal('');
    };

    // Handle Update
    const handleUpdate = (value: any) => {
        setKeyTable(value?.key);
        setDataUpdate(value);
        setTypeModal('Update');
        setOpenModal(true);
    };

    // Delete Row
    const handleDeleteFE = useCallback(
        (r: any) => {
            setDataCriteria((prevState: any) => prevState.filter((e: any) => e.key !== r.key));
        },
        [dataCriteria]
    );

    // Handle Delete
    const handleDelete = (value: any) => {
        setDataDelete(value);
        setModalDelete(true);
    };

    // Handle Submit Delete
    const submitDelete = () => {
        setModalDelete(false);
        handleDeleteFE(dataDelete);
        setDataDelete({});
    };

    // Access Button
    const accessButton = (
        <Tooltip title="Create">
            <Button onClick={handleCreate} className="btn-icon btn-primary" icon={<PlusOutlined />} />
        </Tooltip>
    );

    // Handle Submit
    const handleSubmit = (formValue: any) => {
        if (typeModal === 'Create') {
            const newData = [...dataCriteria];

            const dataValue = {
                key: dataCriteria.length + 1,
                criteria: formValue?.criteria,
                operator: formValue?.operator,
                value: formValue?.value,
            };

            newData.push(dataValue);
            setDataCriteria(newData);
            setOpenModal(false);
            formCriteria.resetFields();
            setTypeModal('');
        } else {
            const dataValue = {
                key: keyTable,
                criteria: formValue?.criteria,
                operator: formValue?.operator,
                value: formValue?.value,
            };

            const findIndex = dataCriteria.findIndex((e: any) => e.key === keyTable);
            const newData = [...dataCriteria];
            const item = newData[findIndex];
            const updateRow = { ...item, ...dataValue };
            newData.splice(findIndex, 1, updateRow);
            setDataCriteria(newData);
            setOpenModal(false);
            formCriteria.resetFields();
            setTypeModal('');
        }
    };
    return (
        <Fragment>
            <Cards>
                <HeaderTitle title="Approval Workflow Criteria" />
                <TablePagination
                    type={'FE'}
                    dataSource={dataCriteria}
                    totalData={dataCriteria?.length}
                    current={page}
                    pageSize={pageSize}
                    onChange={handleGeneralPageChange}
                    columns={TableApprovalWorkflowCriteria(page, pageSize, searchedColumn, searchText, type, handleGeneralSearch, handleDelete, handleUpdate, onFilter, sorter)}
                    onSort={handleGeneralSort}
                    tableScrolled={{ x: 1000, y: 300 }}
                    content={type === 'detail' ? null : accessButton}
                />
            </Cards>

            <Modal
                maskClosable={false}
                centered
                title={`${typeModal} Approval Workflow Criteria`}
                open={openModal}
                onOk={handleSubmit}
                onCancel={handleCancelModal}
                width={600}
                footer={[
                    <Button onClick={handleCancelModal} form="formCriteria" htmlType="reset" danger type="default" className="btn btn-outline-danger">
                        Discard
                    </Button>,
                    <Button htmlType="submit" form="formCriteria" type="primary" className="btn btn-primary-new">
                        Save
                    </Button>,
                ]}
            >
                <div className="w-full flex flex-col p-2">
                    <Form id="formCriteria" layout="vertical" form={formCriteria} onFinish={handleSubmit}>
                        <div className="w-full grid grid-cols-1 gap-2">
                            <Form.Item
                                label="Criteria"
                                name={'criteria'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Criteria!',
                                    },
                                ]}
                            >
                                <InputComponent placeholder="Input Criteria" />
                            </Form.Item>

                            <Form.Item
                                label="Operator"
                                name={'operator'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please select your Operator!',
                                    },
                                ]}
                            >
                                <SelectComponent placeholder="Select Operator">
                                    {/* {dataSalary &&
                                        dataSalary?.map((data: any, index: any) => (
                                            <Select.Option value={data.key} key={index}>
                                                {data.value}
                                            </Select.Option>
                                        ))} */}
                                </SelectComponent>
                            </Form.Item>

                            <Form.Item
                                label="Value"
                                name={'value'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Value!',
                                    },
                                ]}
                            >
                                <InputComponent placeholder="Input Value" />
                            </Form.Item>
                        </div>
                    </Form>
                </div>
            </Modal>

            {/* Modal Delete */}
            {modalDelete ? (
                <ModalConfirm
                    title={`Are you sure want to delete the employee certificate?`}
                    icon="warning"
                    onConfirm={submitDelete}
                    onCancel={() => {
                        setDataDelete({});
                        setModalDelete(false);
                    }}
                />
            ) : null}
        </Fragment>
    );
};

export default ApprovalCriteriaForm;
