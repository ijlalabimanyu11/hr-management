import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../../../redux/store';
import { Button, Form } from 'antd';
import { useLocation, useNavigate } from 'react-router-dom';
import { getDepartment, getPosition, getSection } from '../../../../../redux/slices/GlobalSlice';
import { SYSTEM_SETUP_ROUTES } from '../../../../../router/systemSetup/system_setup_routes';
import Breadcrumbs from '../../../../../components/Breadcrumbs';
import HeaderTitle from '../../../../../components/HeaderTitle';
import Cards from '../../../../../components/Cards';
import InputComponent from '../../../../../components/InputComponent';
import SelectComponent from '../../../../../components/SelectComponent';
import ApprovalCriteriaForm from './Form/ApprovalCriteriaForm';
import AdditionalApprovalWorkflow from './Form/AdditionalApprovalWorkflow';

const ApprovalWorkflowForm = ({ type }) => {
    // Selector
    const { dataSection, dataDepartment, dataPosition } = useSelector((state: RootState) => state.global);

    // Declaration
    const [form] = Form.useForm();
    const [formAdditional] = Form.useForm();
    const navigate = useNavigate();
    const dispatch = useDispatch<AppDispatch>();
    const location = useLocation();
    const { id } = location?.state || {};

    // Use State
    const [dataCriteria, setDataCriteria] = useState<any>([]);
    const [description, setDescription] = useState<string>('');

    const [dataAdditional, setDataAdditional] = useState<any>([]);
    const [typeModalAdditional, setTypeModalAdditional] = useState<string>('');
    const [descriptionAdditional, setDescriptionAdditional] = useState<string>('');
    const [openModalAdditional, setOpenModalAdditional] = useState<boolean>(false);
    const [modalDeleteAdditional, setModalDeleteAdditional] = useState<boolean>(false);
    const [dataUpdateAdditional, setDataUpdateAdditional] = useState<any>({});
    const [keyTableAdditional, setKeyTableAdditional] = useState<any>();
    const [dataDeleteAdditional, setDataDeleteAdditional] = useState<any>({});

    const [pageAdditional, setPageAdditional] = useState<number>(1);
    const [pageSizeAdditional, setPageSizeAdditional] = useState<number>(10);
    const [sortAdditional, setSortAdditional] = useState<string>('');
    const [searchAdditional, setSearchAdditional] = useState<Record<string, string>>({});
    const [searchTextAdditional, setSearchTextAdditional] = useState<{ [key: string]: string }>({});
    const [searchedColumnAdditional, setSearchedColumnAdditional] = useState<string[]>([]);

    // Use Effect
    useEffect(() => {
        dispatch(getSection());
        dispatch(getDepartment());
        dispatch(getPosition());
    }, [dispatch]);

    useEffect(() => {
        if (typeModalAdditional === 'update') {
            formAdditional.setFieldsValue({
                step: dataUpdateAdditional.step,
                type: dataUpdateAdditional.type,
                lineManager: dataUpdateAdditional.lineManager,
                position: dataUpdateAdditional.position,
                section: dataUpdateAdditional.section,
                department: dataUpdateAdditional.department,
            });
        }
    }, [typeModalAdditional, formAdditional]);

    // Breadcrumbs
    const routes = [
        {
            title: 'System Setup',
            href: '',
        },
        {
            title: 'Approval Configuration',
            href: SYSTEM_SETUP_ROUTES.APPROVAL_CONFIGURATION_VIEW_ELEMENTS,
        },
        {
            title: 'Approval Configuration Detail',
            href: SYSTEM_SETUP_ROUTES.APPROVAL_CONFIGURATION_DETAIL_ELEMENTS,
        },
        {
            title: 'Approval Workflow',
            href: SYSTEM_SETUP_ROUTES.APPROVAL_CONFIGURATION_DETAIL_ELEMENTS,
        },
        {
            title: 'Create Approval Workflow',
            href: SYSTEM_SETUP_ROUTES.APPROVAL_WORKFLOW_CREATE_ELEMENTS,
        },
    ];

    // Handle Cancel
    const handleCancel = () => {
        form.resetFields();
        setDataAdditional([]);
        setDataCriteria([]);
        setDescription('');
        setDescriptionAdditional('');
        navigate(-1);
    };
    return (
        <Fragment>
            <Breadcrumbs routes={routes} />
            <div className="flex w-full justify-between align-middle">
                <HeaderTitle title={`${type !== 'Create' ? 'Update' : 'Create'} Approval Workflow`} />
            </div>

            <Form
                layout="vertical"
                form={form}
                // onFinish={handleSubmit}
            >
                <Cards>
                    <HeaderTitle title="Approval Configuration" />

                    <div className="pt-5">
                        <div className="w-full grid grid-cols-2 gap-4">
                            <Form.Item
                                label="Code"
                                name={'code'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please select your Code!',
                                    },
                                ]}
                            >
                                <SelectComponent disabled={true}>
                                    {/* {dataSalary &&
                                        dataSalary?.map((data: any, index: any) => (
                                            <Select.Option value={data.key} key={index}>
                                                {data.value}
                                            </Select.Option>
                                        ))} */}
                                </SelectComponent>
                            </Form.Item>

                            <Form.Item
                                label={'Description'}
                                name={'description'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Description!',
                                    },
                                ]}
                            >
                                <InputComponent rows={1} type="textarea" value={description} disabled={true} />
                            </Form.Item>
                        </div>
                    </div>

                    <HeaderTitle title="Approval Workflow" />

                    <div className="pt-5">
                        <div className="w-full grid grid-cols-2 gap-4">
                            <Form.Item
                                label="Name"
                                name={'name'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please select your Name!',
                                    },
                                ]}
                            >
                                <InputComponent placeholder="Input Name" />
                            </Form.Item>

                            <Form.Item
                                label={'Description'}
                                name={'description'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Description!',
                                    },
                                ]}
                            >
                                <InputComponent
                                    rows={1}
                                    type="textarea"
                                    placeholder="Input Description"
                                    value={descriptionAdditional}
                                    onChange={(e: any) => setDescriptionAdditional(e.target.value)}
                                />
                            </Form.Item>
                        </div>
                    </div>
                </Cards>

                <ApprovalCriteriaForm type={type} id={id} dataCriteria={dataCriteria} setDataCriteria={setDataCriteria} />
                <Cards>
                    <HeaderTitle title="Additional Approval Workflow" />
                    <AdditionalApprovalWorkflow />
                </Cards>

                <div className={'w-full flex justify-end gap-5'}>
                    <Button onClick={handleCancel} form="form" htmlType="reset" danger type="default" className="btn btn-outline-danger">
                        Cancel
                    </Button>

                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="btn btn-primary-new">
                            Save
                        </Button>
                    </Form.Item>
                </div>
            </Form>
        </Fragment>
    );
};

export default ApprovalWorkflowForm;
