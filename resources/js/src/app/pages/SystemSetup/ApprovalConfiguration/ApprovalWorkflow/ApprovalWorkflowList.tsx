import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../../../redux/store';
import { setPageTitle } from '../../../../../redux/store/themeConfigSlice';
import { handleChangePage, handleSearch, handleSort, SortParams } from '../../../../../utils/UtilsTable';
import { Button, Tooltip } from 'antd';
import { NavLink } from 'react-router-dom';
import { SYSTEM_SETUP_ROUTES } from '../../../../../router/systemSetup/system_setup_routes';
import { PlusOutlined } from '@ant-design/icons';
import Cards from '../../../../../components/Cards';
import HeaderTitle from '../../../../../components/HeaderTitle';
import TablePagination from '../../../../../components/TablePagination';
import { TableViewApprovalWorkflow } from './Table/TableApprovalWorkflow';
import ModalConfirm from '../../../../../components/ModalConfirm';

interface ApprovalWorkflowListProps {
    id?: any;
}

const ApprovalWorkflowList: React.FC<ApprovalWorkflowListProps> = ({ id }) => {
    // Selector
    const { dataDetailHeader } = useSelector((state: RootState) => state.approvalConfiguration);

    // Declaration
    const dispatch = useDispatch<AppDispatch>();
    const dataSource = [];

    // Use State
    const [page, setPage] = useState<number>(1);
    const [pageSize, setPageSize] = useState<number>(10);
    const [sort, setSort] = useState<string>('');
    const [search, setSearch] = useState<Record<string, string>>({});
    const [searchText, setSearchText] = useState<{ [key: string]: string }>({});
    const [searchedColumn, setSearchedColumn] = useState<string[]>([]);

    const [idData, setIdData] = useState<any>();
    const [modalDelete, setModalDelete] = useState<boolean>(false);

    // Use Effect
    useEffect(() => {
        dispatch(setPageTitle('Approval Configuration'));
    });

    // useEffect(() => {
    //     dispatch(getApprovalConfigurationHeaderPaginate({ search: searchObject(search), page, pageSize, sort }));
    // }, [dispatch, search, page, pageSize, sort]);

    // // Refresh Data Table
    // const refreshPage = () => {
    //     dispatch(getApprovalConfigurationHeaderPaginate({ search: searchObject(search), page, pageSize, sort }));
    // };

    // Change page for table
    const handleGeneralPageChange = (page: number, pageSize: number) => handleChangePage(page, pageSize, pageSize, setPage, setPageSize);

    // Search for general table
    const handleGeneralSearch = (selectedKeys: string[], confirm: () => void, dataIndex: string) =>
        handleSearch(selectedKeys, confirm, dataIndex, setSearchText, setSearchedColumn, setSearch, setPage);

    // Sort for general table
    const handleGeneralSort = (_: any, __: any, sorter: SortParams) => {
        handleSort(sorter, setSort);
    };

    // Access Button
    const accessButton = (
        <Tooltip title="Create">
            <NavLink to={SYSTEM_SETUP_ROUTES.APPROVAL_WORKFLOW_CREATE_ELEMENTS}>
                <Button className="btn-icon btn-primary" icon={<PlusOutlined />} />
            </NavLink>
        </Tooltip>
    );

    // Handle Delete
    const handleDelete = (value: any) => {
        setIdData(value.id);
        setModalDelete(true);
    };

    // Handle Submit Delete
    const submitDelete = () => {
        setModalDelete(false);
        // dispatch(deleteDepartment({ id: idData }))
        //     .unwrap()
        //     .then(() => {
        //         refreshPage();
        //     })
        //     .catch((r) => {
        //         refreshPage();
        //     });
    };

    return (
        <Fragment>
            <Cards>
                <HeaderTitle title="Approval Workflow" />

                <TablePagination
                    dataSource={dataSource}
                    totalData={0}
                    current={page}
                    pageSize={pageSize}
                    onChange={handleGeneralPageChange}
                    columns={TableViewApprovalWorkflow(page, pageSize, searchedColumn, searchText, handleGeneralSearch, handleDelete)}
                    onSort={handleGeneralSort}
                    tableScrolled={{ x: 1600, y: 300 }}
                    content={accessButton}
                />
            </Cards>

            {/* Modal Delete */}
            {modalDelete ? <ModalConfirm title={`Are you sure want to delete the approval workflow?`} icon="warning" onConfirm={submitDelete} onCancel={() => setModalDelete(false)} /> : null}
        </Fragment>
    );
};

export default ApprovalWorkflowList;
