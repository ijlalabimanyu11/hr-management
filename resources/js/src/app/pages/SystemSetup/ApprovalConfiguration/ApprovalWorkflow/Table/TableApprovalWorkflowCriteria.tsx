import React from 'react';
import ColumnSearch from '../../../../../../utils/ColumnSearch';
import Action from '../../../../../../components/Action';
import { RiDeleteBinLine } from 'react-icons/ri';
import { TbPencil } from 'react-icons/tb';

export const TableApprovalWorkflowCriteria = (
    page = 1,
    pageSize = 10,
    searchedColumn: any,
    searchText: any,
    mode: any,
    handleSearch: (selectedKeys: string[], confirm: () => void, dataIndex: string) => void,
    handleDelete: (value: any) => void,
    handleUpdate: (value: any) => void,
    onFilter: (dataIndex: any, value: any, record: any) => void,
    sorter: (fieldSort: any, a: any, b: any) => void
) => {
    const action = (record: any) => {
        const actions: any[] = [];

        if (mode !== 'detail') {
            actions.push(
                {
                    type: 'update',
                    icon: <TbPencil size={16} />,
                    onClick: () => handleUpdate(record),
                },
                {
                    type: 'delete',
                    icon: <RiDeleteBinLine size={16} />,
                    onClick: () => handleDelete(record),
                }
            );
        }

        return actions.length > 0 ? <Action actions={actions} /> : null;
    };

    const columns: any = [
        {
            title: 'No',
            align: 'center',
            width: 60,
            render: (text: any, object: any, index: number) => (page - 1) * pageSize + index + 1,
        },
        {
            title: 'Criteria',
            dataIndex: 'criteria',
            align: 'left',
            key: 'criteria',
            onFilter: (value: any, record: any) => onFilter('criteria', value, record),
            sorter: (a: any, b: any) => sorter('criteria', a, b),
            ...ColumnSearch({ dataIndex: 'criteria', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Operator',
            dataIndex: 'operator',
            align: 'left',
            key: 'operator',
            onFilter: (value: any, record: any) => onFilter('operator', value, record),
            sorter: (a: any, b: any) => sorter('operator', a, b),
            ...ColumnSearch({ dataIndex: 'operator', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Value',
            dataIndex: 'value',
            align: 'left',
            key: 'value',
            onFilter: (value: any, record: any) => onFilter('value', value, record),
            sorter: (a: any, b: any) => sorter('value', a, b),
            ...ColumnSearch({ dataIndex: 'value', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
    ];

    columns.push({
        title: 'Action',
        align: 'center',
        fixed: 'right',
        render: (text: any, record: any) => action(record),
    });

    return columns;
};
