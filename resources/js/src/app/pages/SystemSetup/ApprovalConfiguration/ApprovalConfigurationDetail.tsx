import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../../redux/store';
import { useLocation, useNavigate } from 'react-router-dom';
import { Button, Form } from 'antd';
import { setPageTitle } from '../../../../redux/store/themeConfigSlice';
import { getDetailApprovalConfigurationHeader } from '../../../../redux/slices/SystemSetup/ApprovalConfiguration/ApprovalConfigurationSlice';
import { SYSTEM_SETUP_ROUTES } from '../../../../router/systemSetup/system_setup_routes';
import Breadcrumbs from '../../../../components/Breadcrumbs';
import HeaderTitle from '../../../../components/HeaderTitle';
import ApprovalWorkflowList from './ApprovalWorkflow/ApprovalWorkflowList';
import ApprovalConfiguration from './Form/ApprovalConfiguration';
import DefaultApprovalWorkflow from './Form/DefaultApprovalWorkflow';

const ApprovalConfigurationDetail = () => {
    // Selector
    const { dataDetailHeader } = useSelector((state: RootState) => state.approvalConfiguration);

    // Declaration
    const navigate = useNavigate();
    const dispatch = useDispatch<AppDispatch>();
    const [form] = Form.useForm();
    const location = useLocation();
    const { id } = location?.state || {};

    // Use State
    const [description, setDescription] = useState<string>('');

    // Use Effect
    useEffect(() => {
        dispatch(setPageTitle('Approval Configuration'));
    });

    useEffect(() => {
        dispatch(getDetailApprovalConfigurationHeader(id));
    }, [dispatch, id]);

    useEffect(() => {
        if (id && dataDetailHeader) {
            form.setFieldsValue({
                code: dataDetailHeader?.approval?.code,
                description: dataDetailHeader?.approval?.description,
                status: dataDetailHeader?.approval?.status,
                created_date: dataDetailHeader?.approval?.created_date,
                created_by: dataDetailHeader?.approval?.created_by,
                modified_date: dataDetailHeader?.approval?.modified_date,
                modified_by: dataDetailHeader?.approval?.modified_by,
            });

            setDescription(dataDetailHeader?.description);
        }
    }, [id, dataDetailHeader]);

    // Breadcrumbs
    const routes = [
        {
            title: 'System Setup',
            href: '',
        },
        {
            title: 'Approval Configuration',
            href: SYSTEM_SETUP_ROUTES.APPROVAL_CONFIGURATION_VIEW_ELEMENTS,
        },
        {
            title: 'Approval Configuration Detail',
            href: SYSTEM_SETUP_ROUTES.APPROVAL_CONFIGURATION_DETAIL_ELEMENTS,
        },
    ];


    // Handle Back
    const handleBack = () => {
        navigate(-1);
    };

    return (
        <Fragment>
            <Breadcrumbs routes={routes} />
            <div className="flex w-full justify-between align-middle">
                <HeaderTitle title={`Approval Configuration Detail`} />
            </div>

            <Form layout="vertical" form={form}>
                <div>
                    <ApprovalConfiguration type={'detail'} description={description} setDescription={setDescription} />
                </div>

                <div>
                    <DefaultApprovalWorkflow type={'detail'} dataDefaultWorkflow={dataDetailHeader?.defaultWorkflowStep?.data} />
                </div>
            </Form>

            <div>
                <ApprovalWorkflowList id={id} />
            </div>

            <div className={'w-full flex justify-end gap-5'}>
                <Button onClick={handleBack} type="default" className="btn btn-primary-new">
                    Back
                </Button>
            </div>
        </Fragment>
    );
};

export default ApprovalConfigurationDetail;
