import React from 'react';
import ColumnSearch from '../../../../../utils/ColumnSearch';
import Action from '../../../../../components/Action';
import { TbCircleCheck, TbPencil } from 'react-icons/tb';
import { RiDeleteBinLine } from 'react-icons/ri';
import { BiMinusCircle } from 'react-icons/bi';
import { dataLineManager, dataType } from '../Form/UtilsData';
import Pills from '../../../../../components/Pills';

export const TableDefaultWorkflow = (
    page: number = 1,
    pageSize: number = 10,
    searchedColumn: any,
    searchText: any,
    handleSearch: (selectedKeys: string[], confirm: () => void, dataIndex: string) => void,
    onFilter: (dataIndex: any, value: any, record: any) => void,
    sorter: (fieldSort: any, a: any, b: any) => void,
    mode?: any,
    handleDelete?: (value: any) => void,
    handleUpdate?: (value: any) => void,
    dataPosition?: any,
    dataSection?: any,
    dataDepartment?: any
) => {
    const safeHandleDelete = handleDelete ?? (() => {});
    const safeHandleUpdate = handleUpdate ?? (() => {});

    const action = (record: any) => {
        const actions: any[] = [];

        if (mode !== 'detail') {
            actions.push(
                {
                    type: 'update',
                    icon: <TbPencil size={16} />,
                    onClick: () => safeHandleUpdate(record),
                },
                {
                    type: 'delete',
                    icon: <RiDeleteBinLine size={16} />,
                    onClick: () => safeHandleDelete(record),
                }
            );
        }

        return actions.length > 0 ? <Action actions={actions} /> : null;
    };

    const columns: any = [
        {
            title: 'No',
            align: 'center',
            width: 60,
            render: (text: any, object: any, index: number) => (page - 1) * pageSize + index + 1,
        },
        {
            title: 'Step',
            dataIndex: 'step',
            align: 'center',
            key: 'step',
            onFilter: (value: any, record: any) => onFilter('step', value, record),
            sorter: (a: any, b: any) => sorter('step', a, b),
            ...ColumnSearch({ dataIndex: 'step', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
            render: (index: any) => {
                const text = index
                    ? index >= 1 && index <= 10 // Handle cases 1 to 10
                        ? index.toString()
                        : index.charAt(0).toUpperCase() + index.slice(1).toLowerCase() // Handle other transformations
                    : index;

                return text ? (
                    <div className="flex justify-center">
                        <Pills weight={"12px"} height={"24px"} color={text}>{text}</Pills>
                    </div>
                ) : (
                    text
                );
            },
        },
        {
            title: 'Type',
            dataIndex: 'type',
            align: 'left',
            key: 'type',
            onFilter: (value: any, record: any) => onFilter('type', value, record),
            sorter: (a: any, b: any) => sorter('type', a, b),
            ...ColumnSearch({ dataIndex: 'type', searchedColumn, searchText, handleSearch, typeFilter: 'input', renderData: dataType }),
        },
        {
            title: 'Line Manager',
            dataIndex: 'lineManager',
            align: 'left',
            key: 'lineManager',
            onFilter: (value: any, record: any) => onFilter('lineManager', value, record),
            sorter: (a: any, b: any) => sorter('lineManager', a, b),
            ...ColumnSearch({ dataIndex: 'lineManager', searchedColumn, searchText, handleSearch, typeFilter: 'input', renderData: dataLineManager }),
        },
        {
            title: 'Position',
            dataIndex: 'position',
            align: 'left',
            key: 'position',
            onFilter: (value: any, record: any) => onFilter('position', value, record),
            sorter: (a: any, b: any) => sorter('position', a, b),
            ...ColumnSearch({ dataIndex: 'position', searchedColumn, searchText, handleSearch, typeFilter: 'input', renderData: dataPosition }),
        },
        {
            title: 'Section',
            dataIndex: 'section',
            align: 'left',
            key: 'section',
            onFilter: (value: any, record: any) => onFilter('section', value, record),
            sorter: (a: any, b: any) => sorter('section', a, b),
            ...ColumnSearch({ dataIndex: 'section', searchedColumn, searchText, handleSearch, typeFilter: 'input', renderData: dataSection }),
        },
        {
            title: 'Department',
            dataIndex: 'department',
            align: 'left',
            key: 'department',
            onFilter: (value: any, record: any) => onFilter('department', value, record),
            sorter: (a: any, b: any) => sorter('department', a, b),
            ...ColumnSearch({ dataIndex: 'department', searchedColumn, searchText, handleSearch, typeFilter: 'input', renderData: dataDepartment }),
        },
    ];

    if (mode !== 'detail') {
        columns.push({
            title: 'Action',
            align: 'center',
            fixed: 'right',
            render: (text: any, record: any) => action(record),
        });
    }

    return columns;
};
