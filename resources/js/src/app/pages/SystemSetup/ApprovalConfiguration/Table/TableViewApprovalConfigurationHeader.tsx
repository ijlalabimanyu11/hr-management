import React from 'react';
import ColumnSearch from '../../../../../utils/ColumnSearch';
import Action from '../../../../../components/Action';
import { TbCircleCheck, TbPencil } from 'react-icons/tb';
import { RiDeleteBinLine } from 'react-icons/ri';
import { PermissionAction } from '../../../../../utils/PermissionAction';
import { BiMinusCircle } from 'react-icons/bi';
import { MdSearch } from 'react-icons/md';
import Pills from '../../../../../components/Pills';
import { SYSTEM_SETUP_ROUTES } from '../../../../../router/systemSetup/system_setup_routes';

export const TableViewApprovalConfigurationHeader = (
    page = 1,
    pageSize = 10,
    searchedColumn: any,
    searchText: any,
    handleSearch: (selectedKeys: string[], confirm: () => void, dataIndex: string) => void,
    handleActiveOrInactive: (value: any) => void,
    handleDelete: (value: any) => void
) => {
    const action = (record: any) => {
        const actions: any[] = [];

        if (PermissionAction('approval-configuration', 'READ')) {
            actions.push({
                link: SYSTEM_SETUP_ROUTES.APPROVAL_CONFIGURATION_DETAIL_ELEMENTS,
                state: record,
                type: 'detail',
                icon: <MdSearch size={16} />,
            });
        }

        if (PermissionAction('approval-configuration', 'TOGGLE')) {
            actions.push({
                type: record.status === 'ACTIVE' ? 'inactive' : 'active',
                icon: record.status === 'ACTIVE' ? <BiMinusCircle size={16} /> : <TbCircleCheck size={16} />,
                onClick: () => handleActiveOrInactive(record),
            });
        }

        if (PermissionAction('approval-configuration', 'UPDATE')) {
            actions.push({
                link: SYSTEM_SETUP_ROUTES.APPROVAL_CONFIGURATION_UPDATE_ELEMENTS,
                state: record,
                type: 'update',
                icon: <TbPencil size={16} />,
            });
        }

        if (PermissionAction('approval-configuration', 'DELETE')) {
            actions.push({
                type: 'delete',
                icon: <RiDeleteBinLine size={16} />,
                onClick: () => handleDelete(record),
            });
        }

        return actions.length > 0 ? <Action actions={actions} /> : null;
    };

    const columns: any = [
        {
            title: 'No',
            align: 'center',
            width: 60,
            render: (text: any, object: any, index: number) => (page - 1) * pageSize + index + 1,
        },
        {
            title: 'Code',
            sorter: true,
            align: 'left',
            dataIndex: 'code',
            key: 'code',
            ...ColumnSearch({ dataIndex: 'code', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Description',
            sorter: true,
            align: 'left',
            dataIndex: 'description',
            key: 'description',
            ellipsis: {
                showTitle: false,
            },
            ...ColumnSearch({ dataIndex: 'description', searchedColumn, searchText, handleSearch, typeFilter: 'input', tooltip: true }),
        },
        {
            title: 'Created By',
            sorter: true,
            align: 'left',
            dataIndex: 'created_by',
            key: 'created_by',
            ...ColumnSearch({ dataIndex: 'created_by', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Created Date',
            sorter: true,
            dataIndex: 'created_date',
            key: 'created_date',
            align: 'center',
            ...ColumnSearch({ dataIndex: 'created_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Modified By',
            sorter: true,
            align: 'left',
            dataIndex: 'modified_by',
            key: 'modified_by',
            ...ColumnSearch({ dataIndex: 'modified_by', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Modified Date',
            sorter: true,
            dataIndex: 'modified_date',
            key: 'modified_date',
            align: 'center',
            ...ColumnSearch({ dataIndex: 'modified_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Status',
            sorter: true,
            dataIndex: 'status',
            key: 'status',
            ...ColumnSearch({ dataIndex: 'status', searchedColumn, searchText, handleSearch, typeFilter: 'status' }),
            render: (index: any) => {
                let text: any;
                switch (index) {
                    case 'WAITING APPROVAL':
                        text = 'Waiting Approval';
                        break;
                    default:
                        text = index ? index.charAt(0).toUpperCase() + index.slice(1).toLowerCase() : index;
                        break;
                }
                return text ? (
                    <div className={'flex justify-center'}>
                        <Pills color={text}>{text}</Pills>
                    </div>
                ) : (
                    text
                );
            },
        },
    ];

    if (
        PermissionAction('approval-configuration', 'UPDATE') ||
        PermissionAction('approval-configuration', 'DELETE') ||
        PermissionAction('approval-configuration', 'TOGGLE') ||
        PermissionAction('approval-configuration', 'READ')
    ) {
        columns.push({
            title: 'Action',
            align: 'center',
            fixed: 'right',
            render: (text: any, record: any) => action(record),
        });
    }

    return columns;
};
