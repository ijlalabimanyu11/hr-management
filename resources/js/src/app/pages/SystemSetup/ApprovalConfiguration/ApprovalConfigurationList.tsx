import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../../redux/store';
import { setPageTitle } from '../../../../redux/store/themeConfigSlice';
import { activeOrInactiveApprovalConfigurationHeader, deleteApprovalConfiguration, getApprovalConfigurationHeaderPaginate } from '../../../../redux/slices/SystemSetup/ApprovalConfiguration/ApprovalConfigurationSlice';
import { searchObject } from '../../../../utils/searchObject';
import { SYSTEM_SETUP_ROUTES } from '../../../../router/systemSetup/system_setup_routes';
import PermissionButton from '../../../../utils/PermissionButton';
import { Button, Spin, Tooltip } from 'antd';
import { DownloadOutlined, PlusOutlined } from '@ant-design/icons';
import { NavLink } from 'react-router-dom';
import Breadcrumbs from '../../../../components/Breadcrumbs';
import HeaderTitle from '../../../../components/HeaderTitle';
import Cards from '../../../../components/Cards';
import TablePagination from '../../../../components/TablePagination';
import ModalConfirm from '../../../../components/ModalConfirm';
import { TableViewApprovalConfigurationHeader } from './Table/TableViewApprovalConfigurationHeader';
import { handleChangePage, handleSearch, handleSort, SortParams } from '../../../../utils/UtilsTable';

const ApprovalConfigurationList = () => {
    // Selector
    const { dataPagingHeader, loading } = useSelector((state: RootState) => state.approvalConfiguration);

    // Declaration
    const dispatch = useDispatch<AppDispatch>();
    const dataSource = dataPagingHeader?.data;

    // Use State
    const [page, setPage] = useState<number>(1);
    const [pageSize, setPageSize] = useState<number>(10);
    const [sort, setSort] = useState<string>('');
    const [search, setSearch] = useState<Record<string, string>>({});
    const [searchText, setSearchText] = useState<{ [key: string]: string }>({});
    const [searchedColumn, setSearchedColumn] = useState<string[]>([]);

    const [idData, setIdData] = useState<any>();
    const [status, setStatus] = useState<string>('');
    const [modalDelete, setModalDelete] = useState<boolean>(false);
    const [modalActiveOrInactive, setModalActiveOrInactive] = useState<boolean>(false);

    // Use Effect
    useEffect(() => {
        dispatch(setPageTitle('Approval Configuration'));
    });

    useEffect(() => {
        dispatch(getApprovalConfigurationHeaderPaginate({ search: searchObject(search), page, pageSize, sort }));
    }, [dispatch, search, page, pageSize, sort]);

    // Refresh Data Table
    const refreshPage = () => {
        dispatch(getApprovalConfigurationHeaderPaginate({ search: searchObject(search), page, pageSize, sort }));
    };

    // Breadcrumbs
    const routes = [
        {
            title: 'System Setup',
            href: '',
        },
        {
            title: 'Approval Configuration',
            href: SYSTEM_SETUP_ROUTES.APPROVAL_CONFIGURATION_VIEW_ELEMENTS,
        },
    ];

    // Change page for table
    const handleGeneralPageChange = (page: number, pageSize: number) => handleChangePage(page, pageSize, pageSize, setPage, setPageSize);

    // Search for general table
    const handleGeneralSearch = (selectedKeys: string[], confirm: () => void, dataIndex: string) =>
        handleSearch(selectedKeys, confirm, dataIndex, setSearchText, setSearchedColumn, setSearch, setPage);

    // Sort for general table
    const handleGeneralSort = (_: any, __: any, sorter: SortParams) => {
        handleSort(sorter, setSort);
    };

    // Handle ActiveOrInactive
    const handleActiveOrInactive = (value: any) => {
        setStatus(value.status);
        setIdData(value.id);
        setModalActiveOrInactive(true);
    };

    // Handle Delete
    const handleDelete = (value: any) => {
        setIdData(value.id);
        setModalDelete(true);
    };

    // Handle Submit ActiveOrInactive
    const submitActiveOrInactive = () => {
        setModalActiveOrInactive(false);
        dispatch(activeOrInactiveApprovalConfigurationHeader({ id: idData, status: status }))
            .unwrap()
            .then(() => {
                refreshPage();
            })
            .catch((r) => {
                refreshPage();
            });
    };

    // Handle Submit Delete
    const submitDelete = () => {
        setModalDelete(false);
        dispatch(deleteApprovalConfiguration({ id: idData }))
            .unwrap()
            .then(() => {
                refreshPage();
            })
            .catch((r) => {
                refreshPage();
            });
    };

    // Handle Download
    const handleDownload = () => {};

    // Access Button
    const accessButton = PermissionButton(
        (props: any) => (
            <div className="flex align-middle gap-3">
                <Tooltip title="Download">
                    <Button onClick={props.handleDownload} className="btn-icon btn-primary" icon={<DownloadOutlined />} />
                </Tooltip>
                <Tooltip title="Create">
                    <NavLink to={SYSTEM_SETUP_ROUTES.APPROVAL_CONFIGURATION_CREATE_ELEMENTS}>
                        <Button className="btn-icon btn-primary" icon={<PlusOutlined />} />
                    </NavLink>
                </Tooltip>
            </div>
        ),
        'approval-configuration',
        ['CREATE', 'READ']
    );

    return (
        <Fragment>
            <Spin spinning={loading}>
                <Breadcrumbs routes={routes} />
                <div className="flex w-full justify-between align-middle">
                    <HeaderTitle title="Approval Configuration" />
                </div>

                <Cards>
                    <TablePagination
                        dataSource={dataSource}
                        totalData={dataPagingHeader?.totalData}
                        current={page}
                        pageSize={pageSize}
                        onChange={handleGeneralPageChange}
                        columns={TableViewApprovalConfigurationHeader(page, pageSize, searchedColumn, searchText, handleGeneralSearch, handleActiveOrInactive, handleDelete)}
                        onSort={handleGeneralSort}
                        tableScrolled={{ x: 1600, y: 300 }}
                        content={accessButton({ handleDownload })}
                    />
                </Cards>

                {/* Modal Delete */}
                {modalDelete ? <ModalConfirm title={`Are you sure want to delete the approval configuration?`} icon="warning" onConfirm={submitDelete} onCancel={() => setModalDelete(false)} /> : null}

                {/* Modal Active/Inactive */}
                {modalActiveOrInactive ? (
                    <ModalConfirm
                        title={`Are you sure want to ${status === 'ACTIVE' ? 'deactivate' : 'activate'} the approval configuration?`}
                        icon="warning"
                        onConfirm={submitActiveOrInactive}
                        onCancel={() => setModalActiveOrInactive(false)}
                    />
                ) : null}
            </Spin>
        </Fragment>
    );
};

export default ApprovalConfigurationList;
