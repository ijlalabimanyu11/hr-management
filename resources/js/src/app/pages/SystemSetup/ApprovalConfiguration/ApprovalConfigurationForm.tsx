import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { AppDispatch } from '../../../../redux/store';
import { Button, Form } from 'antd';
import { getDepartment, getPosition, getSection } from '../../../../redux/slices/GlobalSlice';
import Breadcrumbs from '../../../../components/Breadcrumbs';
import { SYSTEM_SETUP_ROUTES } from '../../../../router/systemSetup/system_setup_routes';
import HeaderTitle from '../../../../components/HeaderTitle';
import { useLocation, useNavigate } from 'react-router-dom';
import {
    approvalConfigurationHeaderBody,
    createApprovalConfigurationHeader,
    updateApprovalConfigurationHeader,
} from '../../../../redux/slices/SystemSetup/ApprovalConfiguration/ApprovalConfigurationSlice';
import ApprovalConfiguration from './Form/ApprovalConfiguration';
import DefaultApprovalWorkflow from './Form/DefaultApprovalWorkflow';

const ApprovalConfigurationForm = ({ type }) => {
    // Selector

    // Declaration
    const [form] = Form.useForm();
    const navigate = useNavigate();
    const dispatch = useDispatch<AppDispatch>();
    const location = useLocation();
    const { id } = location?.state || {};

    // Use State
    const [dataDefaultWorkflow, setDataDefaultWorkflow] = useState<any>([]);
    const [description, setDescription] = useState<string>('');

    // Use Effect
    useEffect(() => {
        dispatch(getSection());
        dispatch(getDepartment());
        dispatch(getPosition());
    }, [dispatch]);

    // Breadcrumbs
    const routes = [
        {
            title: 'System Setup',
            href: '',
        },
        {
            title: 'Approval Configuration',
            href: SYSTEM_SETUP_ROUTES.APPROVAL_CONFIGURATION_VIEW_ELEMENTS,
        },
        {
            title: 'Create Approval Configuration',
            href: SYSTEM_SETUP_ROUTES.APPROVAL_CONFIGURATION_CREATE_ELEMENTS,
        },
    ];

    // Handle Cancel
    const handleCancel = () => {
        form.resetFields();
        setDataDefaultWorkflow([]);
        setDescription('');
        navigate(-1);
    };

    // Reset Form
    const resetForm = () => {
        form.resetFields();
        setDataDefaultWorkflow([]);
    };

    // Handle Submit
    const handleSubmit = (formValue: any) => {
        const data: approvalConfigurationHeaderBody = {
            code: formValue.code,
            description: formValue.description,
            defaultWorkflowStep: dataDefaultWorkflow,
        };

        if (type === 'Create') {
            dispatch(createApprovalConfigurationHeader({ body: data, navigate: navigate }))
                .unwrap()
                .then(() => {
                    resetForm();
                })
                .catch(() => {});
        } else {
            dispatch(updateApprovalConfigurationHeader({ body: data, id: id, navigate: navigate }))
                .unwrap()
                .then(() => {
                    resetForm();
                })
                .catch(() => {});
        }
    };
    return (
        <Fragment>
            <Breadcrumbs routes={routes} />
            <div className="flex w-full justify-between align-middle">
                <HeaderTitle title={`${type !== 'Create' ? 'Update' : 'Create'} Approval Configuration`} />
            </div>

            <Form layout="vertical" form={form} onFinish={handleSubmit}>
                <div>
                    <ApprovalConfiguration type={type} description={description} setDescription={setDescription} />
                </div>

                <div>
                    <DefaultApprovalWorkflow type={type} dataDefaultWorkflow={dataDefaultWorkflow} setDataDefaultWorkflow={setDataDefaultWorkflow} />
                </div>

                <div className={'w-full flex justify-end gap-5'}>
                    <Button onClick={handleCancel} form="form" htmlType="reset" danger type="default" className="btn btn-outline-danger">
                        Cancel
                    </Button>

                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="btn btn-primary-new">
                            Save
                        </Button>
                    </Form.Item>
                </div>
            </Form>
        </Fragment>
    );
};

export default ApprovalConfigurationForm;
