import React, { Fragment } from 'react';
import Cards from '../../../../../components/Cards';
import HeaderTitle from '../../../../../components/HeaderTitle';
import { Form } from 'antd';
import InputComponent from '../../../../../components/InputComponent';

interface approvalConfigurationProps {
    type?: string;
    description?: string;
    setDescription?: any;
}

const ApprovalConfiguration: React.FC<approvalConfigurationProps> = ({ type, description, setDescription }) => {
    return (
        <Fragment>
            <Cards>
                <HeaderTitle title="Approval Configuration" />
                <div className="pt-5">
                    {type !== 'detail' ? (
                        <div className="w-full grid grid-cols-2 gap-4">
                            <Form.Item
                                label="Code"
                                name={'code'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Input your Code!',
                                    },
                                ]}
                            >
                                <InputComponent placeholder="Input Code" />
                            </Form.Item>

                            <Form.Item
                                label={'Description'}
                                name={'description'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Description!',
                                    },
                                ]}
                            >
                                <InputComponent rows={1} type="textarea" placeholder="Input Description" value={description} onChange={(e: any) => setDescription(e.target.value)} />
                            </Form.Item>
                        </div>
                    ) : (
                        <div className="w-full grid grid-cols-3 gap-4">
                            <Form.Item label={'Code'} name={'code'}>
                                <InputComponent disabled={true} />
                            </Form.Item>
                            <Form.Item label={'Description'} name={'description'}>
                                <InputComponent rows={1} type="textarea" disabled={true} value={description} />
                            </Form.Item>
                            <Form.Item label={'Status'} name={'status'}>
                                <InputComponent disabled={true} />
                            </Form.Item>
                            <Form.Item label={'Created By'} name={'created_by'}>
                                <InputComponent disabled={true} />
                            </Form.Item>
                            <Form.Item label={'Created Date'} name={'created_date'}>
                                <InputComponent disabled={true} />
                            </Form.Item>
                            <Form.Item label={'Modified Date'} name={'modified_date'}>
                                <InputComponent disabled={true} />
                            </Form.Item>
                            <Form.Item label={'Modified Date'} name={'modified_date'}>
                                <InputComponent disabled={true} />
                            </Form.Item>
                        </div>
                    )}
                </div>
            </Cards>
        </Fragment>
    );
};

export default ApprovalConfiguration;
