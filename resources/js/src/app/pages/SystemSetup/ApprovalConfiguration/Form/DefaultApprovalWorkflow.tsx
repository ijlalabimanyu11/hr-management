import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../../../redux/store';
import { Button, Form, Modal, Select, Tooltip } from 'antd';
import { getDepartment, getPosition, getSection } from '../../../../../redux/slices/GlobalSlice';
import { handleChangePage, handleSearch, handleSort, SortParams } from '../../../../../utils/UtilsTable';
import { PlusOutlined } from '@ant-design/icons';
import Cards from '../../../../../components/Cards';
import HeaderTitle from '../../../../../components/HeaderTitle';
import TablePagination from '../../../../../components/TablePagination';
import { TableDefaultWorkflow } from '../Table/TableDefaultWorkflow';
import ModalConfirm from '../../../../../components/ModalConfirm';
import SelectComponent from '../../../../../components/SelectComponent';
import { dataLineManager, dataStep, dataType } from './UtilsData';

interface defaultApprovalWorkflowProps {
    type?: string;
    dataDefaultWorkflow?: any;
    setDataDefaultWorkflow?: any;
}

const DefaultApprovalWorkflow: React.FC<defaultApprovalWorkflowProps> = ({ type, dataDefaultWorkflow, setDataDefaultWorkflow }) => {
    // Selector
    const { dataSection, dataDepartment, dataPosition } = useSelector((state: RootState) => state.global);

    // Declaration
    const [formDetail] = Form.useForm();
    const dispatch = useDispatch<AppDispatch>();

    // Use State
    const [page, setPage] = useState<number>(1);
    const [pageSize, setPageSize] = useState<number>(10);
    const [sort, setSort] = useState<string>('');
    const [search, setSearch] = useState<Record<string, string>>({});
    const [searchText, setSearchText] = useState<{ [key: string]: string }>({});
    const [searchedColumn, setSearchedColumn] = useState<string[]>([]);

    const [typeModal, setTypeModal] = useState<string>('');
    const [openModal, setOpenModal] = useState<boolean>(false);
    const [modalDelete, setModalDelete] = useState<boolean>(false);
    const [dataUpdate, setDataUpdate] = useState<any>({});
    const [keyTable, setKeyTable] = useState<any>();
    const [dataDelete, setDataDelete] = useState<any>({});

    const [dataTypeSelect, setDataTypeSelect] = useState<number>(0);

    // Use Effect
    useEffect(() => {
        dispatch(getSection());
        dispatch(getDepartment());
        dispatch(getPosition());
    }, [dispatch]);

    useEffect(() => {
        if (typeModal === 'Update') {
            formDetail.setFieldsValue({
                step: dataUpdate.step,
                type: dataUpdate.type,
                lineManager: dataUpdate.lineManager,
                position: dataUpdate.position,
                section: dataUpdate.section,
                department: dataUpdate.department,
            });
        }
    }, [typeModal, formDetail]);

    // Change page for table
    const handleGeneralPageChange = (page: number, pageSize: number) => handleChangePage(page, pageSize, pageSize, setPage, setPageSize);

    // Search for general table
    const handleGeneralSearch = (selectedKeys: string[], confirm: () => void, dataIndex: string) =>
        handleSearch(selectedKeys, confirm, dataIndex, setSearchText, setSearchedColumn, setSearch, setPage);

    // Sort for general table
    const handleGeneralSort = (_: any, __: any, sorter: SortParams) => {
        handleSort(sorter, setSort);
    };

    // Filter Table
    const onFilter = (dataIndex: any, value: any, record: any) => {
        const fixSearchText = value?.toLowerCase();
        switch (dataIndex) {
            case 'type': {
                const tempValueType = record[dataIndex] || 0;
                const typeName = dataType.find((item: any) => item?.key === tempValueType)?.value || '';
                return typeName.toLowerCase().includes(fixSearchText);
            }
            case 'lineManager': {
                const tempValueLineManager = record[dataIndex] || 0;
                const typeLineManager = dataLineManager.find((item: any) => item?.key === tempValueLineManager)?.value || '';
                return typeLineManager.toLowerCase().includes(fixSearchText);
            }
            case 'position': {
                const tempValuePosition = record[dataIndex] || 0;
                const typePosition = dataPosition?.find((item: any) => item?.key === tempValuePosition)?.value || '';
                return typePosition.toLowerCase().includes(fixSearchText);
            }
            case 'section': {
                const tempValueSection = record[dataIndex] || 0;
                const typeSection = dataSection?.find((item: any) => item?.key === tempValueSection)?.value || '';
                return typeSection.toLowerCase().includes(fixSearchText);
            }
            case 'department': {
                const tempValueDepartment = record[dataIndex] || 0;
                const typeDepartment = dataDepartment?.find((item: any) => item?.key === tempValueDepartment)?.value || '';
                return typeDepartment.toLowerCase().includes(fixSearchText);
            }
            default:
                return record[dataIndex]?.toString().toLowerCase().includes(fixSearchText);
        }
    };

    // Sorting Table
    const sorter = (fieldSort: any, a: any, b: any) => {
        const handleDataSort = (obj: any) => {
            return obj[fieldSort]?.toString()?.toLowerCase();
        };
        let fa = handleDataSort(a);
        let fb = handleDataSort(b);
        return fa.localeCompare(fb);
    };

    // Function Filter Columns
    const filterColumns = (data: any) => {
        return type === 'detail' ? data.filter((item: any) => item.title !== 'ACTION') : data;
    };

    // Delete Row
    const handleDeleteFE = useCallback(
        (r: any) => {
            setDataDefaultWorkflow((prevState: any) => prevState.filter((e: any) => e.key !== r.key));
        },
        [dataDefaultWorkflow]
    );

    // Handle Create
    const handleCreate = () => {
        setTypeModal('Create');
        setOpenModal(true);
    };

    // Handle Cancel Modal
    const handleCancelModal = () => {
        formDetail.resetFields();
        setOpenModal(false);
        setTypeModal('');
    };

    // Handle Update
    const handleUpdate = (value: any) => {
        setKeyTable(value?.key);
        setDataUpdate(value);
        setTypeModal('Update');
        setOpenModal(true);
    };

    // Handle Delete
    const handleDelete = (value: any) => {
        setDataDelete(value);
        setModalDelete(true);
    };

    // Handle Submit Delete
    const submitDelete = () => {
        setModalDelete(false);
        handleDeleteFE(dataDelete);
        setDataDelete({});
    };

    // Handle Change Type
    const onChangeType = (value: any) => {
        setDataTypeSelect(value);
    };

    // Filter Data Step
    const dataStepLength = dataDefaultWorkflow?.length > 0 ? dataDefaultWorkflow?.map((item: any) => item?.step) : [];

    const filterDataStep = () => {
        return dataStepLength?.length > 0 ? dataStep?.filter((item: any) => !dataStepLength?.includes(item?.key)) : dataStep;
    };

    // Access Button
    const accessButton = (
        <Tooltip title="Create">
            <Button onClick={handleCreate} className="btn-icon btn-primary" icon={<PlusOutlined />} />
        </Tooltip>
    );

    // Handle Submit Default Workflow
    const handleSubmitDefaultWorkflow = (formValue: any) => {
        if (typeModal === 'Create') {
            const newData = [...dataDefaultWorkflow];

            const dataValue = {
                key: dataDefaultWorkflow.length + 1,
                step: formValue.step,
                type: formValue.type,
                lineManager: formValue.lineManager,
                position: formValue.position,
                section: formValue.section,
                department: formValue.department,
            };

            newData.push(dataValue);
            setDataDefaultWorkflow(newData);
            setOpenModal(false);
            formDetail.resetFields();
            setTypeModal('');
        } else {
            const dataValue = {
                key: keyTable,
                step: formValue.step,
                type: formValue.type,
                lineManager: formValue.lineManager,
                position: formValue.position,
                section: formValue.section,
                department: formValue.department,
            };

            const findIndex = dataDefaultWorkflow.findIndex((e: any) => e.key === keyTable);
            const newData = [...dataDefaultWorkflow];
            const item = newData[findIndex];
            const updateRow = { ...item, ...dataValue };
            newData.splice(findIndex, 1, updateRow);
            setDataDefaultWorkflow(newData);
            setOpenModal(false);
            formDetail.resetFields();
            setTypeModal('');
        }
    };

    return (
        <Fragment>
            <Cards>
                <HeaderTitle title="Default Approval Workflow Step" />
                <div className="pt-5">
                    <TablePagination
                        type={'FE'}
                        dataSource={dataDefaultWorkflow}
                        totalData={dataDefaultWorkflow?.length}
                        current={page}
                        pageSize={pageSize}
                        onChange={handleGeneralPageChange}
                        columns={filterColumns(
                            TableDefaultWorkflow(
                                page,
                                pageSize,
                                searchedColumn,
                                searchText,
                                handleGeneralSearch,
                                onFilter,
                                sorter,
                                type,
                                handleDelete,
                                handleUpdate,
                                dataPosition,
                                dataSection,
                                dataDepartment
                            )
                        )}
                        onSort={handleGeneralSort}
                        tableScrolled={{ x: 1800, y: 300 }}
                        content={type === 'detail' ? null : accessButton}
                    />

                    <Modal
                        maskClosable={false}
                        centered
                        title={`${typeModal} Default Workflow`}
                        open={openModal}
                        onOk={handleSubmitDefaultWorkflow}
                        onCancel={handleCancelModal}
                        width={600}
                        footer={[
                            <Button onClick={handleCancelModal} form="formDetail" htmlType="reset" danger type="default" className="btn btn-outline-danger">
                                Discard
                            </Button>,
                            <Button htmlType="submit" form="formDetail" type="primary" className="btn btn-primary-new">
                                Save
                            </Button>,
                        ]}
                    >
                        <div className="w-full flex flex-col p-2">
                            <Form id="formDetail" layout="vertical" form={formDetail} onFinish={handleSubmitDefaultWorkflow}>
                                <div className="w-full grid grid-cols-1 gap-2">
                                    <Form.Item
                                        label="Step"
                                        name={'step'}
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Please input your Step!',
                                            },
                                        ]}
                                    >
                                        <SelectComponent placeholder="Select Step">
                                            {dataStep &&
                                                filterDataStep()?.map((data: any, index: any) => (
                                                    <Select.Option value={data.key} key={index}>
                                                        {data.value}
                                                    </Select.Option>
                                                ))}
                                        </SelectComponent>
                                    </Form.Item>

                                    <Form.Item
                                        label="Type"
                                        name={'type'}
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Please input your Type!',
                                            },
                                        ]}
                                    >
                                        <SelectComponent placeholder="Select Type" onChange={(e: any) => onChangeType(e)}>
                                            {dataType &&
                                                dataType?.map((data: any, index: any) => (
                                                    <Select.Option value={data.key} key={index}>
                                                        {data.value}
                                                    </Select.Option>
                                                ))}
                                        </SelectComponent>
                                    </Form.Item>

                                    <Form.Item
                                        label="Line Manager"
                                        name={'lineManager'}
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Please input your Line Manager!',
                                            },
                                        ]}
                                    >
                                        <SelectComponent placeholder="Select Line Manager">
                                            {dataLineManager &&
                                                dataLineManager?.map((data: any, index: any) => (
                                                    <Select.Option value={data.key} key={index}>
                                                        {data.value}
                                                    </Select.Option>
                                                ))}
                                        </SelectComponent>
                                    </Form.Item>

                                    <Form.Item
                                        label="Position"
                                        name={'position'}
                                        rules={[
                                            {
                                                required: dataTypeSelect === 2 ? true : false,
                                                message: 'Please input your Position!',
                                            },
                                        ]}
                                    >
                                        <SelectComponent placeholder="Select Position">
                                            {dataPosition &&
                                                dataPosition?.map((data: any, index: any) => (
                                                    <Select.Option value={data?.key} key={index}>
                                                        {data?.value}
                                                    </Select.Option>
                                                ))}
                                        </SelectComponent>
                                    </Form.Item>

                                    <Form.Item
                                        label="Section"
                                        name={'section'}
                                        rules={[
                                            {
                                                required: dataTypeSelect === 2 ? true : false,
                                                message: 'Please input your Section!',
                                            },
                                        ]}
                                    >
                                        <SelectComponent placeholder="Select Section">
                                            {dataSection &&
                                                dataSection?.map((data: any, index: any) => (
                                                    <Select.Option value={data?.key} key={index}>
                                                        {data?.value}
                                                    </Select.Option>
                                                ))}
                                        </SelectComponent>
                                    </Form.Item>

                                    <Form.Item
                                        label="Department"
                                        name={'department'}
                                        rules={[
                                            {
                                                required: dataTypeSelect === 2 ? true : false,
                                                message: 'Please input your Department!',
                                            },
                                        ]}
                                    >
                                        <SelectComponent placeholder="Select Department">
                                            {dataDepartment &&
                                                dataDepartment?.map((data: any, index: any) => (
                                                    <Select.Option value={data?.key} key={index}>
                                                        {data?.value}
                                                    </Select.Option>
                                                ))}
                                        </SelectComponent>
                                    </Form.Item>
                                </div>
                            </Form>
                        </div>
                    </Modal>

                    {/* Modal Delete */}
                    {modalDelete ? (
                        <ModalConfirm
                            title={`Are you sure want to delete the default approval workflow?`}
                            icon="warning"
                            onConfirm={submitDelete}
                            onCancel={() => {
                                setDataDelete({});
                                setModalDelete(false);
                            }}
                        />
                    ) : null}
                </div>
            </Cards>
        </Fragment>
    );
};

export default DefaultApprovalWorkflow;
