import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../../redux/store';
import OrganizationChart from '@dabeng/react-orgchart';
import { getChartOrganizationStructure } from '../../../../redux/slices/SystemSetup/OrganizationStructure/OrganizationStructureSlice';
import { useLocation, useNavigate } from 'react-router-dom';
import { Button, Spin } from 'antd';
import Breadcrumbs from '../../../../components/Breadcrumbs';
import { SYSTEM_SETUP_ROUTES } from '../../../../router/systemSetup/system_setup_routes';
import HeaderTitle from '../../../../components/HeaderTitle';
import Cards from '../../../../components/Cards';
import { Tree, TreeNode } from 'react-organizational-chart';

const OrganizationStructureChart = () => {
    // Selector
    const { dataChart, loading } = useSelector((state: RootState) => state.organizationStructure);

    // Declaration
    const dispatch = useDispatch<AppDispatch>();
    const navigate = useNavigate();

    // Use Effect
    useEffect(() => {
        dispatch(getChartOrganizationStructure());
    }, [dispatch]);

    // Breadcrumbs
    const routes = [
        {
            title: 'System Setup',
            href: '',
        },
        {
            title: 'Organization Structure',
            href: SYSTEM_SETUP_ROUTES.ORGANIZATION_STRUCTURE_VIEW_ELEMENTS,
        },
        {
            title: 'View Chart',
            href: SYSTEM_SETUP_ROUTES.ORGANIZATION_STRUCTURE_CHART_ELEMENTS,
        },
    ];

    const handleBack = () => {
        navigate(-1);
    };

    const renderTreeNodes = (data: any) =>
        data?.map((item: any) => (
            <TreeNode
                key={item?.label}
                label={
                    <div className="tree-label">
                        {item?.department && <p className="employee-department">{item?.department}</p>}
                        {item?.position && <p className="employee-position">{item?.position}</p>}
                        <p className="employee-name">{item?.label}</p>
                    </div>
                }
            >
                {item?.children && renderTreeNodes(item?.children)}
            </TreeNode>
        ));

    return (
        <Fragment>
            <Spin spinning={loading}>
                <Breadcrumbs routes={routes} />
                <div className="flex w-full justify-between align-middle">
                    <HeaderTitle title="View Chart" />
                </div>

                <Cards>
                    <div className="cards-container">
                        <Tree label={<div className="company-label">{dataChart?.label}</div>}>{renderTreeNodes(dataChart?.children)}</Tree>
                    </div>
                </Cards>

                <div className={'w-full flex justify-end gap-5'}>
                    <Button onClick={handleBack} type="default" className="btn btn-primary-new">
                        Back
                    </Button>
                </div>
            </Spin>
        </Fragment>
    );
};

export default OrganizationStructureChart;
