import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../../redux/store';
import { setPageTitle } from '../../../../redux/store/themeConfigSlice';
import Breadcrumbs from '../../../../components/Breadcrumbs';
import HeaderTitle from '../../../../components/HeaderTitle';
import Cards from '../../../../components/Cards';
import { SYSTEM_SETUP_ROUTES } from '../../../../router/systemSetup/system_setup_routes';
import { deleteOrganizationStructure, getDetailOrganizationStructure, getOrganizationStructurePaginate } from '../../../../redux/slices/SystemSetup/OrganizationStructure/OrganizationStructureSlice';
import { searchObject } from '../../../../utils/searchObject';
import PermissionButton from '../../../../utils/PermissionButton';
import { Button, Spin, Tooltip } from 'antd';
import { ApartmentOutlined, DownloadOutlined, PlusOutlined } from '@ant-design/icons';
import TablePagination from '../../../../components/TablePagination';
import { TableOrganizationStructure } from './Table/TableViewOrganizationStructure';
import { NavLink } from 'react-router-dom';
import ModalConfirm from '../../../../components/ModalConfirm';
import OrganizationStructureForm from './OrganizationStructureForm';
import { handleChangePage, handleSearch, handleSort, SortParams } from '../../../../utils/UtilsTable';

const OrganizationStructureList = () => {
    // Selector
    const { data, loading, dataDetail } = useSelector((state: RootState) => state.organizationStructure);

    // Declaration
    const dispatch = useDispatch<AppDispatch>();
    const dataSource = data?.data;

    // Use State
    const [page, setPage] = useState<number>(1);
    const [pageSize, setPageSize] = useState<number>(10);
    const [sort, setSort] = useState<string>('');
    const [search, setSearch] = useState<Record<string, string>>({});
    const [searchText, setSearchText] = useState<{ [key: string]: string }>({});
    const [searchedColumn, setSearchedColumn] = useState<string[]>([]);

    const [typeModal, setTypeModal] = useState<string>('');
    const [idData, setIdData] = useState<any>();
    const [status, setStatus] = useState<string>('');
    const [openModal, setOpenModal] = useState<boolean>(false);
    const [modalDelete, setModalDelete] = useState<boolean>(false);

    // Use Effect
    useEffect(() => {
        dispatch(setPageTitle('Organization Structure'));
    });

    useEffect(() => {
        dispatch(getOrganizationStructurePaginate({ search: searchObject(search), page, pageSize, sort }));
    }, [dispatch, search, page, pageSize, sort]);

    // Refresh Data Table
    const refreshPage = () => {
        dispatch(getOrganizationStructurePaginate({ search: searchObject(search), page, pageSize, sort }));
    };

    // Breadcrumbs
    const routes = [
        {
            title: 'System Setup',
            href: '',
        },
        {
            title: 'Organization Structure',
            href: SYSTEM_SETUP_ROUTES.ORGANIZATION_STRUCTURE_VIEW_ELEMENTS,
        },
    ];

    // Handle Change Page Table
    const handleGeneralPageChange = (page: number, pageSize: number) => handleChangePage(page, pageSize, pageSize, setPage, setPageSize);

    // Sorting Table
    const handleGeneralSort = (_: any, __: any, sorter: SortParams) => {
        handleSort(sorter, setSort);
    };

    // Handle Search Table
    const handleGeneralSearch = (selectedKeys: string[], confirm: () => void, dataIndex: string) =>
        handleSearch(selectedKeys, confirm, dataIndex, setSearchText, setSearchedColumn, setSearch, setPage);

    // Handle Create
    const handleCreate = () => {
        setTypeModal('Create');
        setOpenModal(true);
    };

    // Handle Cancel Modal
    const handleCancelModal = () => {
        setIdData('');
        setStatus('');
        setOpenModal(false);
        setTypeModal('');
    };

    // Handle Detail
    const handleDetail = (value: any) => {
        dispatch(getDetailOrganizationStructure(value?.id));
    };

    // Handle Update
    const handleUpdate = (value: any) => {
        setTypeModal('Update');
        setOpenModal(true);
        dispatch(getDetailOrganizationStructure(value?.id));
    };

    // Handle Delete
    const handleDelete = (value: any) => {
        setIdData(value.id);
        setModalDelete(true);
    };

    // Handle Submit Delete
    const submitDelete = () => {
        setModalDelete(false);
        dispatch(deleteOrganizationStructure({ id: idData }))
            .unwrap()
            .then(() => {
                refreshPage();
            })
            .catch((r) => {
                refreshPage();
            });
    };

    // Handle Download
    const handleDownload = () => {};

    // Handle Download
    const handleChart = () => {};

    // Access Button
    const accessButton = PermissionButton(
        (props: any) => (
            <div className="flex align-middle gap-3">
                <Tooltip title="View Chart">
                    <NavLink to={SYSTEM_SETUP_ROUTES.ORGANIZATION_STRUCTURE_CHART_ELEMENTS}>
                        <Button className="btn-icon btn-primary" icon={<ApartmentOutlined />} />
                    </NavLink>
                </Tooltip>
                <Tooltip title="Download">
                    <Button onClick={props.handleDownload} className="btn-icon btn-primary" icon={<DownloadOutlined />} />
                </Tooltip>
                <Tooltip title="Create">
                    <Button onClick={props.handleCreate} className="btn-icon btn-primary" icon={<PlusOutlined />} />
                </Tooltip>
            </div>
        ),
        'organization-structure',
        ['CREATE', 'READ']
    );

    return (
        <Fragment>
            <Spin spinning={loading}>
                <Breadcrumbs routes={routes} />
                <div className="flex w-full justify-between align-middle">
                    <HeaderTitle title="Organization Structure" />
                </div>

                <Cards>
                    <TablePagination
                        dataSource={dataSource}
                        totalData={data?.totalData}
                        current={page}
                        pageSize={pageSize}
                        onChange={handleGeneralPageChange}
                        columns={TableOrganizationStructure(page, pageSize, searchedColumn, searchText, handleGeneralSearch, handleUpdate, handleDelete)}
                        onSort={handleGeneralSort}
                        tableScrolled={{ x: 2400, y: 300 }}
                        content={accessButton({ handleDownload, handleCreate })}
                    />
                </Cards>

                {/* Modal Create/Update Position */}
                <OrganizationStructureForm type={typeModal} openModal={openModal} cancelModal={handleCancelModal} refreshPage={refreshPage} dataUpdate={dataDetail} />

                {/* Modal Delete */}
                {modalDelete ? <ModalConfirm title={`Are you sure want to delete the organization structure?`} icon="warning" onConfirm={submitDelete} onCancel={() => setModalDelete(false)} /> : null}
            </Spin>
        </Fragment>
    );
};

export default OrganizationStructureList;
