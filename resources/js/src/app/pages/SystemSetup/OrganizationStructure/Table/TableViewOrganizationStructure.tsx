import React from 'react';
import ColumnSearch from '../../../../../utils/ColumnSearch';
import Pills from '../../../../../components/Pills';
import Action from '../../../../../components/Action';
import { TbCircleCheck, TbPencil } from 'react-icons/tb';
import { RiDeleteBinLine } from 'react-icons/ri';
import { BiMinusCircle } from 'react-icons/bi';
import { PermissionAction } from '../../../../../utils/PermissionAction';
import { MdSearch } from 'react-icons/md';

export const TableOrganizationStructure = (
    page = 1,
    pageSize = 10,
    searchedColumn: any,
    searchText: any,
    handleSearch: (selectedKeys: string[], confirm: () => void, dataIndex: string) => void,
    handleUpdate: (value: any) => void,
    handleDelete: (value: any) => void,
) => {
    const action = (record: any) => {
        const actions: any[] = [];

        if (PermissionAction('organization-structure', 'UPDATE')) {
            actions.push({
                type: 'update',
                icon: <TbPencil size={16} />,
                onClick: () => handleUpdate(record),
            });
        }

        if (PermissionAction('organization-structure', 'DELETE')) {
            actions.push({
                type: 'delete',
                icon: <RiDeleteBinLine size={16} />,
                onClick: () => handleDelete(record),
            });
        }

        return actions.length > 0 ? <Action actions={actions} /> : null;
    };

    const columns: any = [
        {
            title: 'No',
            align: 'center',
            width: 60,
            render: (text: any, object: any, index: number) => (page - 1) * pageSize + index + 1,
        },
        {
            title: 'Employee Name',
            sorter: true,
            align: 'left',
            dataIndex: 'employee_name',
            key: 'employee_name',
            ...ColumnSearch({ dataIndex: 'employee_name', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Position',
            sorter: true,
            align: 'left',
            dataIndex: 'position',
            key: 'position',
            ...ColumnSearch({ dataIndex: 'position', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Parent Position',
            sorter: true,
            align: 'left',
            dataIndex: 'parent_position',
            key: 'parent_position',
            ...ColumnSearch({ dataIndex: 'parent_position', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Section',
            sorter: true,
            align: 'left',
            dataIndex: 'section_name',
            key: 'section_name',
            ...ColumnSearch({ dataIndex: 'section_name', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Department',
            sorter: true,
            align: 'left',
            dataIndex: 'department_name',
            key: 'department_name',
            ...ColumnSearch({ dataIndex: 'department_name', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Created By',
            sorter: true,
            align: 'left',
            dataIndex: 'created_by',
            key: 'created_by',
            ...ColumnSearch({ dataIndex: 'created_by', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Created Date',
            sorter: true,
            dataIndex: 'created_date',
            key: 'created_date',
            align: 'center',
            ...ColumnSearch({ dataIndex: 'created_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Modified By',
            sorter: true,
            align: 'left',
            dataIndex: 'modified_by',
            key: 'modified_by',
            ...ColumnSearch({ dataIndex: 'modified_by', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Modified Date',
            sorter: true,
            dataIndex: 'modified_date',
            key: 'modified_date',
            align: 'center',
            ...ColumnSearch({ dataIndex: 'modified_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
    ]

    if (PermissionAction('organization-structure', 'UPDATE') || PermissionAction('organization-structure', 'DELETE')  || PermissionAction('organization-structure', 'READ')) {
        columns.push({
            title: 'Action',
            align: 'center',
            fixed: 'right',
            render: (text: any, record: any) => action(record),
        });
    }

    return columns
};
