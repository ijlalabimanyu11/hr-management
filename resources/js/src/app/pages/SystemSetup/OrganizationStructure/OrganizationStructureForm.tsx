import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../../redux/store';
import { Button, Form, Modal, Select } from 'antd';
import { createOrganizationStructure, organizationStructureBody, updateOrganizationStructure } from '../../../../redux/slices/SystemSetup/OrganizationStructure/OrganizationStructureSlice';
import InputComponent from '../../../../components/InputComponent';
import SelectComponent from '../../../../components/SelectComponent';
import { getDepartment, getPosition, getSection } from '../../../../redux/slices/GlobalSlice';

interface OrganizationStructureFormProps {
    type: any;
    openModal?: any;
    cancelModal?: any;
    refreshPage?: any;
    dataUpdate?: any;
}

const OrganizationStructureForm: React.FC<OrganizationStructureFormProps> = ({ type, openModal, cancelModal, refreshPage, dataUpdate }) => {
    // Selector
    const { dataPosition } = useSelector((state: RootState) => state.global);

    // Declaration
    const dispatch = useDispatch<AppDispatch>();
    const [form] = Form.useForm();

    // Use State
    const [dataPositionParent, setDataPositionParent] = useState<any>([]);

    // Use Effect
    useEffect(() => {
        dispatch(getPosition());
    }, [dispatch]);

    useEffect(() => {
        if (dataPosition) {
            const adjustData = [
                ...dataPosition.map((data: any) => ({
                    key: data.key,
                    value: data.value,
                })),
                { key: null, value: "Company" }
            ];

            setDataPositionParent(adjustData);
        }
    }, [dataPosition]);

    useEffect(() => {
        if (type === 'Update' && dataUpdate) {
            form.setFieldsValue({
                id: dataUpdate?.id,
                position: dataUpdate?.position,
                parentPosition: dataUpdate?.parentPosition,
            });
        }
    }, [type, form, dataUpdate]);

    const handleSubmit = (formValue: any) => {
        const data: organizationStructureBody = {
            position: formValue.position,
            parentPosition: formValue.parentPosition,
        };

        if (type === 'Create') {
            dispatch(createOrganizationStructure({ body: data }))
                .unwrap()
                .then(() => {
                    cancelModal();
                    refreshPage();
                    form.resetFields();
                })
                .catch(() => {
                    refreshPage();
                });
        } else {
            dispatch(updateOrganizationStructure({ body: data, id: dataUpdate?.id }))
                .unwrap()
                .then(() => {
                    cancelModal();
                    refreshPage();
                    form.resetFields();
                })
                .catch(() => {
                    refreshPage();
                });
        }
    };

    return (
        <Fragment>
            <Modal
                maskClosable={false}
                centered
                title={`${type} Organization Structure`}
                open={openModal}
                onOk={handleSubmit}
                onCancel={() => {
                    form.resetFields();
                    cancelModal();
                }}
                width={600}
                footer={[
                    <Button onClick={cancelModal} form="form" htmlType="reset" danger type="default" className="btn btn-outline-danger">
                        Discard
                    </Button>,
                    <Button htmlType="submit" form="form" type="primary" className="btn btn-primary-new">
                        Save
                    </Button>,
                ]}
            >
                <div className="w-full flex flex-col">
                    <Form id="form" layout="vertical" form={form} onFinish={handleSubmit} name="dynamic_rule">
                        <div className="w-full grid grid-cols-1 gap-2">
                            {type !== 'Create' ? (
                                <Form.Item label="ID" name={'id'}>
                                    <InputComponent disabled={true} />
                                </Form.Item>
                            ) : null}

                            <Form.Item
                                label="Position"
                                name={'position'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please select your Position!',
                                    },
                                ]}
                            >
                                <SelectComponent placeholder="Select Position">
                                    {dataPosition &&
                                        dataPosition?.map((data: any, index: any) => (
                                            <Select.Option value={data.key} key={index}>
                                                {data.value}
                                            </Select.Option>
                                        ))}
                                </SelectComponent>
                            </Form.Item>

                            <Form.Item
                                label="Parent Position"
                                name={'parentPosition'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please select your Parent Position!',
                                    },
                                ]}
                            >
                                <SelectComponent placeholder="Select Parent Position">
                                    {dataPositionParent &&
                                        dataPositionParent?.map((data: any, index: any) => (
                                            <Select.Option value={data.key} key={index}>
                                                {data.value}
                                            </Select.Option>
                                        ))}
                                </SelectComponent>
                            </Form.Item>
                        </div>
                    </Form>
                </div>
            </Modal>
        </Fragment>
    );
};

export default OrganizationStructureForm;
