import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { AppDispatch } from '../../../../redux/store';
import { Button, Form, Modal } from 'antd';
import { createPosition, positionBody, updatePosition } from '../../../../redux/slices/SystemSetup/Position/PositionSlice';
import InputComponent from '../../../../components/InputComponent';

interface PositionFormProps {
    type: any;
    openModal?: any;
    cancelModal?: any;
    refreshPage?: any;
    dataUpdate?: any;
}

const PositionForm: React.FC<PositionFormProps> = ({ type, openModal, cancelModal, refreshPage, dataUpdate }) => {
    // Selector

    // Declaration
    const dispatch = useDispatch<AppDispatch>();
    const [form] = Form.useForm();

    // Use State
    const [description, setDescription] = useState<string>('');

    // Use Effect
    useEffect(() => {
        if (type === 'Update' && dataUpdate) {
            form.setFieldsValue({
                id: dataUpdate?.id,
                positionCode: dataUpdate?.positionCode,
                positionName: dataUpdate?.positionName,
                description: dataUpdate?.description,
            });
        }
    }, [type, form, dataUpdate]);

    const handleSubmit = (formValue: any) => {
        const data: positionBody = {
            positionCode: formValue.positionCode,
            positionName: formValue.positionName,
            description: formValue.description,
        };

        if (type === 'Create') {
            dispatch(createPosition({ body: data }))
                .unwrap()
                .then(() => {
                    cancelModal();
                    refreshPage();
                    form.resetFields();
                })
                .catch(() => {
                    refreshPage();
                });
        } else {
            dispatch(updatePosition({ body: data, id: dataUpdate?.id }))
                .unwrap()
                .then(() => {
                    cancelModal();
                    refreshPage();
                    form.resetFields();
                })
                .catch(() => {
                    refreshPage();
                });
        }
    };

    return (
        <Fragment>
            <Modal
                maskClosable={false}
                centered
                title={`${type} Position`}
                open={openModal}
                onOk={handleSubmit}
                onCancel={() => {
                    form.resetFields();
                    cancelModal();
                }}
                width={600}
                footer={[
                    <Button onClick={cancelModal} form="form" htmlType="reset" danger type="default" className="btn btn-outline-danger">
                        Discard
                    </Button>,
                    <Button htmlType="submit" form="form" type="primary" className="btn btn-primary-new">
                        Save
                    </Button>,
                ]}
            >
                <div className="w-full flex flex-col">
                    <Form id="form" layout="vertical" form={form} onFinish={handleSubmit} name="dynamic_rule">
                        <div className="w-full grid grid-cols-1 gap-2">
                            {type !== 'Create' ? (
                                <Form.Item label="ID" name={'id'}>
                                    <InputComponent disabled={true} />
                                </Form.Item>
                            ) : null}

                            <Form.Item
                                label="Position Code"
                                name={'positionCode'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Position Code!',
                                    },
                                ]}
                            >
                                <InputComponent placeholder="Input Position Code" />
                            </Form.Item>

                            <Form.Item
                                label="Position Name"
                                name={'positionName'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Position Name!',
                                    },
                                ]}
                            >
                                <InputComponent placeholder="Input Position Name" />
                            </Form.Item>

                            <Form.Item
                                label={'Description'}
                                name={'description'}
                                className={'w-full'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Description!',
                                    },
                                ]}
                            >
                                <InputComponent placeholder="Input Description" type="textarea" value={description} onChange={(e: any) => setDescription(e.target.value)} />
                            </Form.Item>
                        </div>
                    </Form>
                </div>
            </Modal>
        </Fragment>
    );
};

export default PositionForm;
