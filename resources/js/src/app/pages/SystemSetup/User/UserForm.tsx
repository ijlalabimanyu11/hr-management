import { Button, Checkbox, Form, Input, Modal, Select } from 'antd';
import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import InputComponent from '../../../../components/InputComponent';
import SelectComponent from '../../../../components/SelectComponent';
import { createUser, updateUser, userBody } from '../../../../redux/slices/SystemSetup/User/UserSlice';
import { AppDispatch, RootState } from '../../../../redux/store';
import { getDefaultPassword, getEmployee, getRoleType } from '../../../../redux/slices/GlobalSlice';

interface UserFormProps {
    type: any;
    openModal?: any;
    cancelModal?: any;
    refreshPage?: any;
    dataUpdate?: any;
}

const UserForm: React.FC<UserFormProps> = ({ type, openModal, cancelModal, refreshPage, dataUpdate }) => {
    // Selector
    const { dataRole, dataDefaultPassword, dataEmployee } = useSelector((state: RootState) => state.global);

    // Declaration
    const dispatch = useDispatch<AppDispatch>();
    const [form] = Form.useForm();

    // Use State
    const [defaultPassword, setDefaultPassword] = useState(false);

    // Use Effect
    useEffect(() => {
        dispatch(getRoleType());
        dispatch(getEmployee());
        dispatch(getDefaultPassword());
    }, [dispatch]);

    useEffect(() => {
        if (type === 'Update' && dataUpdate) {
            form.setFieldsValue({
                id: dataUpdate?.user_id,
                employeeId: dataUpdate?.employee_id,
                username: dataUpdate?.username,
                email: dataUpdate?.email,
                role: dataUpdate?.role_id,
            });
        }
    }, [type, form, dataUpdate]);

    useEffect(() => {
        if (defaultPassword) {
            form.resetFields(['password', 'confirm']);
            form.setFieldsValue({
                password: dataDefaultPassword?.map((a: any) => a.value)[0],
                confirm: dataDefaultPassword?.map((a: any) => a.value)[0],
            });
        } else {
            form.resetFields(['password', 'confirm']);
        }
    }, [defaultPassword, form]);

    const handleSubmit = (formValue: any) => {
        const data: userBody = {
            employeeId: formValue.employeeId,
            email: formValue.email,
            username: formValue.username,
            role: formValue.role,
            isDefault: type === "Create" ? defaultPassword : undefined,
            password: type === "Create" ?  defaultPassword ? null : formValue.password : undefined,
        };

        if (type === 'Create') {
            dispatch(createUser({ body: data }))
                .unwrap()
                .then(() => {
                    cancelModal();
                    refreshPage();
                    form.resetFields();
                    setDefaultPassword(false);
                })
                .catch(() => {
                    refreshPage();
                });
        } else {
            dispatch(updateUser({ body: data, id: dataUpdate?.id }))
            .unwrap()
            .then(() => {
                cancelModal();
                refreshPage();
                form.resetFields();
                setDefaultPassword(false);
            })
            .catch(() => {
                refreshPage();
            });
        }
    };

    return (
        <Fragment>
            <Modal
                maskClosable={false}
                centered
                title={`${type} User`}
                open={openModal}
                onCancel={() => {
                    form.resetFields();
                    setDefaultPassword(false);
                    cancelModal();
                }}
                onOk={handleSubmit}
                width={600}
                footer={[
                    <Button onClick={cancelModal} form="form" htmlType="reset" danger type="default" className="btn btn-outline-danger">
                        Discard
                    </Button>,
                    <Button htmlType="submit" form="form" type="primary" className="btn btn-primary-new">
                        Save
                    </Button>,
                ]}
            >
                <div className="w-full flex flex-col p-2">
                    <Form id="form" layout="vertical" form={form} onFinish={handleSubmit} name="dynamic_rule">
                        <div className="w-full grid grid-cols-2 gap-2">
                            {type !== 'Create' ? (
                                <Form.Item label="ID" name={'id'}>
                                    <InputComponent disabled={true} />
                                </Form.Item>
                            ) : null}

                            <Form.Item
                                label="Employee"
                                name={'employeeId'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please select your Employee!',
                                    },
                                ]}
                            >
                                <SelectComponent placeholder='Select Employee' disabled={type !== 'Create' ? true : false}>
                                    {dataEmployee &&
                                        dataEmployee?.map((data: any, index: any) => (
                                            <Select.Option value={data.key} key={index}>
                                                {data.value}
                                            </Select.Option>
                                        ))}
                                </SelectComponent>
                            </Form.Item>

                            <Form.Item
                                label="Username"
                                name={'username'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Username!',
                                    },
                                ]}
                            >
                                <InputComponent placeholder='Input Username'/>
                            </Form.Item>

                            <Form.Item
                                label="Email"
                                name={'email'}
                                rules={[
                                    {
                                        type: 'email',
                                        message: 'The input is not valid Email!',
                                    },
                                    {
                                        required: true,
                                        message: 'Please input your Email!',
                                    },
                                ]}
                            >
                                <InputComponent placeholder='Input Email'/>
                            </Form.Item>

                            <Form.Item
                                label="Role"
                                name={'role'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please select your Role!',
                                    },
                                ]}
                            >
                                <SelectComponent placeholder='Select Role'>
                                    {dataRole &&
                                        dataRole?.map((data: any, index: any) => (
                                            <Select.Option value={data.key} key={index}>
                                                {data.value}
                                            </Select.Option>
                                        ))}
                                </SelectComponent>
                            </Form.Item>

                            {type !== 'Create' ? null : (
                                <>
                                    <Form.Item name={'defaultPassword'}>
                                        <Checkbox checked={defaultPassword} onChange={(e) => setDefaultPassword(e.target.checked)}>
                                            Use Default Password
                                        </Checkbox>
                                    </Form.Item>

                                    <div></div>

                                    <Form.Item
                                        name="password"
                                        label="New Password"
                                        rules={[
                                            {
                                                required: !defaultPassword,
                                                message: 'Please input your password!',
                                            },
                                        ]}
                                        hasFeedback
                                    >
                                        <Input.Password disabled={defaultPassword} allowClear placeholder='Input Password'/>
                                    </Form.Item>
                                    <Form.Item
                                        name={'confirm'}
                                        label="Confirm Password"
                                        dependencies={['password']}
                                        hasFeedback
                                        rules={[
                                            {
                                                required: !defaultPassword,
                                                message: 'Please confirm your password!',
                                            },
                                            ({ getFieldValue }) => ({
                                                validator(_, value) {
                                                    if (!value || getFieldValue('password') === value) {
                                                        return Promise.resolve();
                                                    }
                                                    return Promise.reject(new Error('The new password that you entered do not match!'));
                                                },
                                            }),
                                        ]}
                                    >
                                        <Input.Password disabled={defaultPassword} allowClear placeholder='Input Confirm Password'/>
                                    </Form.Item>
                                </>
                            )}
                        </div>
                    </Form>
                </div>
            </Modal>
        </Fragment>
    );
};

export default UserForm;
