import { Button, Checkbox, Form, Input, Modal } from 'antd';
import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../../redux/store';
import {resetPasswordUser, userBody } from '../../../../redux/slices/SystemSetup/User/UserSlice';
import { getDefaultPassword } from '../../../../redux/slices/GlobalSlice';

const ResetPassword = ({ openModal, cancelModal, refreshPage, idData }) => {
    // Selector
    const { dataDefaultPassword } = useSelector((state: RootState) => state.global);

    // Declaration
    const dispatch = useDispatch<AppDispatch>();
    const [form] = Form.useForm();

    // Use State
    const [defaultPassword, setDefaultPassword] = useState(false);

    // Use Effect
    useEffect(() => {
        dispatch(getDefaultPassword());
    }, [dispatch]);

    useEffect(() => {
        if (defaultPassword) {
            form.resetFields(['password', 'confirm']);
            form.setFieldsValue({
                password: dataDefaultPassword?.map((a: any) => a.value)[0],
                confirm: dataDefaultPassword?.map((a: any) => a.value)[0],
            });
        } else {
            form.resetFields(['password', 'confirm']);
        }
    }, [defaultPassword, form]);

    const handleSubmit = (formValue: any) => {
        const data: userBody = {
            isDefault: defaultPassword,
            password: defaultPassword ? null : formValue.password,
        };

        dispatch(resetPasswordUser({ body: data, id: idData }))
            .unwrap()
            .then(() => {
                cancelModal();
                refreshPage();
                form.resetFields();
                setDefaultPassword(false);
            })
            .catch(() => {
                cancelModal();
                refreshPage();
            });
    };
    return (
        <Fragment>
            <Modal
                maskClosable={false}
                centered
                title={`Update Password`}
                open={openModal}
                onCancel={() => {
                    form.resetFields();
                    setDefaultPassword(false);
                    cancelModal();
                }}
                onOk={handleSubmit}
                width={600}
                footer={[
                    <Button onClick={cancelModal} form="form" htmlType="reset" danger type="default" className="btn btn-outline-danger">
                        Discard
                    </Button>,
                    <Button htmlType="submit" form="form" type="primary" className="btn btn-primary-new">
                        Save
                    </Button>,
                ]}
            >
                <div className="w-full flex flex-col p-2">
                    <Form id="form" layout="vertical" form={form} onFinish={handleSubmit} name="dynamic_rule">
                        <div className="w-full grid grid-cols-2 gap-2">
                            <Form.Item name={'defaultPassword'}>
                                <Checkbox checked={defaultPassword} onChange={(e) => setDefaultPassword(e.target.checked)}>
                                    Use Default Password
                                </Checkbox>
                            </Form.Item>

                            <div></div>

                            <Form.Item
                                name="password"
                                label="New Password"
                                rules={[
                                    {
                                        required: !defaultPassword,
                                        message: 'Please input your password!',
                                    },
                                ]}
                                hasFeedback
                            >
                                <Input.Password disabled={defaultPassword} allowClear />
                            </Form.Item>
                            <Form.Item
                                name={'confirm'}
                                label="Confirm Password"
                                dependencies={['password']}
                                hasFeedback
                                rules={[
                                    {
                                        required: !defaultPassword,
                                        message: 'Please confirm your password!',
                                    },
                                    ({ getFieldValue }) => ({
                                        validator(_, value) {
                                            if (!value || getFieldValue('password') === value) {
                                                return Promise.resolve();
                                            }
                                            return Promise.reject(new Error('The new password that you entered do not match!'));
                                        },
                                    }),
                                ]}
                            >
                                <Input.Password disabled={defaultPassword} allowClear />
                            </Form.Item>
                        </div>
                    </Form>
                </div>
            </Modal>
        </Fragment>
    );
};

export default ResetPassword;
