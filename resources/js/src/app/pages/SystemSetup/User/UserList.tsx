import React, { Fragment, useEffect, useRef, useState } from 'react';
import Breadcrumbs from '../../../../components/Breadcrumbs';
import HeaderTitle from '../../../../components/HeaderTitle';
import { SYSTEM_SETUP_ROUTES } from '../../../../router/systemSetup/system_setup_routes';
import Cards from '../../../../components/Cards';
import TablePagination from '../../../../components/TablePagination';
import { useDispatch, useSelector } from 'react-redux';
import { setPageTitle } from '../../../../redux/store/themeConfigSlice';
import { DownloadOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, InputRef, Spin, Tooltip } from 'antd';
import UserForm from './UserForm';
import { AppDispatch, RootState } from '../../../../redux/store';
import { activeOrInactiveUser, deleteUser, getDetailUser, getUserPaginate } from '../../../../redux/slices/SystemSetup/User/UserSlice';
import { columnsUsers } from './Table/TableViewUser';
import ModalConfirm from '../../../../components/ModalConfirm';
import ResetPassword from './ResetPassword';
import PermissionButton from '../../../../utils/PermissionButton';
import { searchObject } from '../../../../utils/searchObject';
import { handleChangePage, handleSearch, handleSort, SortParams } from '../../../../utils/UtilsTable';

const UserList = () => {
    // Selector
    const { data, loading, dataDetail } = useSelector((state: RootState) => state.user);

    // Declaration
    const searchInput = useRef<InputRef>(null);
    const dispatch = useDispatch<AppDispatch>();
    const dataSource = data?.data;

    // Use State
    const [page, setPage] = useState<number>(1);
    const [pageSize, setPageSize] = useState<number>(10);
    const [sort, setSort] = useState<string>('');
    const [search, setSearch] = useState<Record<string, string>>({});
    const [searchText, setSearchText] = useState<{ [key: string]: string }>({});
    const [searchedColumn, setSearchedColumn] = useState<string[]>([]);

    const [typeModal, setTypeModal] = useState<string>('');
    const [idData, setIdData] = useState<any>();
    const [status, setStatus] = useState<string>('');
    const [openModal, setOpenModal] = useState<boolean>(false);
    const [modalActiveOrInactive, setModalActiveOrInactive] = useState<boolean>(false);
    const [modalDelete, setModalDelete] = useState<boolean>(false);
    const [modalResetPassword, setModalResetPassword] = useState<boolean>(false);

    // Use Effect
    useEffect(() => {
        dispatch(setPageTitle('User'));
    });

    useEffect(() => {
        dispatch(getUserPaginate({ search: searchObject(search), page, pageSize, sort }));
    }, [dispatch, search, page, pageSize, sort]);

    // Refresh Data Table
    const refreshPage = () => {
        dispatch(getUserPaginate({ search: searchObject(search), page, pageSize, sort }));
    };

    // Breadcrumbs
    const routes = [
        {
            title: 'System Setup',
            href: '',
        },
        {
            title: 'User',
            href: SYSTEM_SETUP_ROUTES.USER_VIEW_ELEMENTS,
        },
    ];

    // Handle Change Page Table
    const handleGeneralPageChange = (page: number, pageSize: number) => handleChangePage(page, pageSize, pageSize, setPage, setPageSize);

    // Sorting Table
    const handleGeneralSort = (_: any, __: any, sorter: SortParams) => {
        handleSort(sorter, setSort);
    };


    // Handle Search Table
    const handleGeneralSearch = (selectedKeys: string[], confirm: () => void, dataIndex: string) =>
        handleSearch(selectedKeys, confirm, dataIndex, setSearchText, setSearchedColumn, setSearch, setPage);

    // Handle Create
    const handleCreate = () => {
        setTypeModal('Create');
        setOpenModal(true);
    };

    // Handle Cancel Modal
    const handleCancelModal = () => {
        setIdData('');
        setStatus('');
        setOpenModal(false);
        setTypeModal('');
    };

    // Handle Update
    const handleUpdate = (value: any) => {
        setTypeModal('Update');
        setOpenModal(true);
        dispatch(getDetailUser(value?.user_id));
    };

    // Handle Delete
    const handleDelete = (value: any) => {
        setIdData(value.user_id);
        setModalDelete(true);
    };

    // Handle Reset Password
    const handleResetPassword = (value: any) => {
        setIdData(value.user_id);
        setModalResetPassword(true);
    };

    // Handle ActiveOrInactive
    const handleActiveOrInactive = (value: any) => {
        setStatus(value.status);
        setIdData(value.user_id);
        setModalActiveOrInactive(true);
    };

    // Handle Submit ActiveOrInactive
    const submitActiveOrInactive = () => {
        setModalActiveOrInactive(false);
        dispatch(activeOrInactiveUser({ id: idData, status: status }))
            .unwrap()
            .then(() => {
                setModalActiveOrInactive(false)
                refreshPage();
            })
            .catch((r) => {
                setModalActiveOrInactive(false)
                refreshPage();
            });
    };

    // Handle Submit Delete
    const submitDelete = () => {
        setModalDelete(false);
        dispatch(deleteUser({ id: idData }))
            .unwrap()
            .then(() => {
                setModalDelete(false)
                refreshPage();
            })
            .catch((r) => {
                setModalDelete(false)
                refreshPage();
            });
    };

    // Handle Download
    const handleDownload = () => {};

    // Access Button
    const accessButton = PermissionButton(
        (props: any) => (
            <div className="flex align-middle gap-3">
                <Tooltip title="Download">
                    <Button onClick={props.handleDownload} className="btn-icon btn-primary" icon={<DownloadOutlined />} />
                </Tooltip>
                <Tooltip title="Create">
                    <Button onClick={props.handleCreate} className="btn-icon btn-primary" icon={<PlusOutlined />}/>
                </Tooltip>
            </div>
        ),
        'user',
        ['CREATE', 'READ']
    );

    return (
        <Fragment>
            <Spin spinning={loading}>
                <Breadcrumbs routes={routes} />
                <div className="flex w-full justify-between align-middle">
                    <HeaderTitle title="User" />
                </div>

                <Cards>
                    <TablePagination
                        dataSource={dataSource}
                        totalData={data?.totalData || 0}
                        current={page}
                        pageSize={pageSize}
                        onChange={handleGeneralPageChange}
                        columns={columnsUsers(page, pageSize, searchedColumn, searchText, handleGeneralSearch, handleUpdate, handleDelete, handleResetPassword, handleActiveOrInactive)}
                        onSort={handleGeneralSort}
                        tableScrolled={{ x: 2000, y: 300 }}
                        content={accessButton({ handleDownload, handleCreate })}
                    />
                </Cards>

                {/* Modal Create/Update User */}
                <UserForm type={typeModal} openModal={openModal} cancelModal={handleCancelModal} refreshPage={refreshPage} dataUpdate={dataDetail} />

                {/* Modal Delete */}
                {modalDelete ? <ModalConfirm title={`Are you sure want to delete the user account?`} icon="warning" onConfirm={submitDelete} onCancel={() => setModalDelete(false)} /> : null}

                {/* Modal Reset Password */}
                <ResetPassword openModal={modalResetPassword} cancelModal={() => setModalResetPassword(false)} refreshPage={refreshPage} idData={idData} />

                {/* Modal Active/Inactive */}
                {modalActiveOrInactive ? (
                    <ModalConfirm
                        title={`Are you sure want to ${status === 'ACTIVE' ? 'deactivate' : 'activate'} the user account?`}
                        icon="warning"
                        onConfirm={submitActiveOrInactive}
                        onCancel={() => setModalActiveOrInactive(false)}
                    />
                ) : null}
            </Spin>
        </Fragment>
    );
};

export default UserList;
