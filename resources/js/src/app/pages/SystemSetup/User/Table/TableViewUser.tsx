import React from 'react';
import { TbPencil } from 'react-icons/tb';
import { RiDeleteBinLine } from 'react-icons/ri';
import { PiKey } from 'react-icons/pi';
import { TbCircleCheck } from 'react-icons/tb';
import { BiMinusCircle } from 'react-icons/bi';
import Pills from '../../../../../components/Pills';
import Action from '../../../../../components/Action';
import ColumnSearch from '../../../../../utils/ColumnSearch';
import { PermissionAction } from '../../../../../utils/PermissionAction';

export const columnsUsers = (
    page = 1,
    pageSize = 10,
    searchedColumn: any,
    searchText: any,
    handleSearch: (selectedKeys: string[], confirm: () => void, dataIndex: string) => void,
    handleUpdate: (value: any) => void,
    handleDelete: (value: any) => void,
    handleResetPassword: (value: any) => void,
    handleActiveOrInactive: (value: any) => void
) => {
    const action = (record: any) => {
        const actions: any[] = [];

        if (PermissionAction('user', 'UPDATE')) {
            actions.push({
                type: 'reset password',
                icon: <PiKey size={16} />,
                onClick: () => handleResetPassword(record),
            });
        }

        if (PermissionAction('user', 'TOGGLE')) {
            actions.push({
                type: record.status === 'ACTIVE' ? 'inactive' : 'active',
                icon: record.status === 'ACTIVE' ? <BiMinusCircle size={16} /> : <TbCircleCheck size={16} />,
                onClick: () => handleActiveOrInactive(record),
            });
        }

        if (PermissionAction('user', 'UPDATE')) {
            actions.push({
                type: 'update',
                icon: <TbPencil size={16} />,
                onClick: () => handleUpdate(record),
            });
        }

        if (PermissionAction('user', 'DELETE')) {
            actions.push({
                type: 'delete',
                icon: <RiDeleteBinLine size={16} />,
                onClick: () => handleDelete(record),
            });
        }

        return actions.length > 0 ? <Action actions={actions} /> : null;
    };

    const columns: any = [
        {
            title: 'No',
            align: 'center',
            width: 60,
            render: (text: any, object: any, index: number) => (page - 1) * pageSize + index + 1,
        },
        {
            title: 'Employee',
            sorter: true,
            align: 'left',
            dataIndex: 'username',
            key: 'username',
            ...ColumnSearch({ dataIndex: 'username', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Username',
            sorter: true,
            align: 'left',
            dataIndex: 'username',
            key: 'username',
            ...ColumnSearch({ dataIndex: 'username', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Email',
            sorter: true,
            align: 'left',
            dataIndex: 'email',
            key: 'email',
            ...ColumnSearch({ dataIndex: 'email', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Role',
            sorter: true,
            dataIndex: 'role',
            align: 'center',
            key: 'role',
            ...ColumnSearch({ dataIndex: 'role', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Created By',
            sorter: true,
            align: 'left',
            dataIndex: 'created_by',
            key: 'created_by',
            ...ColumnSearch({ dataIndex: 'created_by', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Created Date',
            sorter: true,
            dataIndex: 'created_date',
            key: 'created_date',
            align: 'center',
            ...ColumnSearch({ dataIndex: 'created_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Modified By',
            sorter: true,
            align: 'left',
            dataIndex: 'modified_by',
            key: 'modified_by',
            ...ColumnSearch({ dataIndex: 'modified_by', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Modified Date',
            sorter: true,
            dataIndex: 'modified_date',
            key: 'modified_date',
            align: 'center',
            ...ColumnSearch({ dataIndex: 'modified_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Status',
            sorter: true,
            dataIndex: 'status',
            key: 'status',
            ...ColumnSearch({ dataIndex: 'status', searchedColumn, searchText, handleSearch, typeFilter: 'status' }),
            render: (index: any) => {
                let text: any;
                switch (index) {
                    case 'WAITING APPROVAL':
                        text = 'Waiting Approval';
                        break;
                    default:
                        text = index ? index.charAt(0).toUpperCase() + index.slice(1).toLowerCase() : index;
                        break;
                }
                return text ? (
                    <div className={'flex justify-center'}>
                        <Pills color={text}>{text}</Pills>
                    </div>
                ) : (
                    text
                );
            },
        },
    ];

    if (PermissionAction('user', 'TOGGLE') || PermissionAction('user', 'UPDATE') || PermissionAction('user', 'DELETE')) {
        columns.push({
            title: 'Action',
            align: 'center',
            fixed: 'right',
            render: (text: any, record: any) => action(record),
        });
    }

    return columns;
};
