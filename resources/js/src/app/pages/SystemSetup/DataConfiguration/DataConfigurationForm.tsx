import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../../redux/store';
import { Button, Form, Modal, Select } from 'antd';
import InputComponent from '../../../../components/InputComponent';
import { createDataConfiguration, dataConfigurationBody, updateDataConfiguration } from '../../../../redux/slices/SystemSetup/DataConfiguration/DataConfigurationSlice';

interface DataConfigurationFormProps {
    type: any;
    openModal?: any;
    cancelModal?: any;
    refreshPage?: any;
    dataUpdate?: any;
    valuePage?: any;
    header?: any;
}

const DataConfigurationForm: React.FC<DataConfigurationFormProps> = ({ type, openModal, cancelModal, dataUpdate, refreshPage, valuePage, header }) => {
    // Selector

    // Declaration
    const dispatch = useDispatch<AppDispatch>();
    const [form] = Form.useForm();

    // Use State

    // Use Effect
    useEffect(() => {
        if (type === 'Update' && dataUpdate) {
            form.setFieldsValue({
                id: dataUpdate?.id,
                key: dataUpdate?.key,
                value: dataUpdate?.value,
            });
        }
    }, [type, form, dataUpdate]);

    const handleSubmit = (formValue: any) => {
        const data: dataConfigurationBody = {
            name_slug: valuePage,
            key: formValue.key,
            value: formValue.value,
            is_props: false,
        };

        if (type === 'Create') {
            dispatch(createDataConfiguration({ body: data }))
                .unwrap()
                .then(() => {
                    cancelModal();
                    refreshPage();
                    form.resetFields();
                })
                .catch(() => {
                    refreshPage();
                });
        } else {
            dispatch(updateDataConfiguration({ body: data, id: dataUpdate?.id }))
                .unwrap()
                .then(() => {
                    cancelModal();
                    refreshPage();
                    form.resetFields();
                })
                .catch(() => {
                    refreshPage();
                });
        }
    };
    return (
        <Fragment>
            <Modal
                maskClosable={false}
                centered
                title={`${type} ${header}`}
                open={openModal}
                onOk={handleSubmit}
                onCancel={() => {
                    form.resetFields();
                    cancelModal();
                }}
                width={400}
                footer={[
                    <Button onClick={cancelModal} form="form" htmlType="reset" danger type="default" className="btn btn-outline-danger">
                        Discard
                    </Button>,
                    <Button htmlType="submit" form="form" type="primary" className="btn btn-primary-new">
                        Save
                    </Button>,
                ]}
            >
                <div className="w-full flex flex-col p-2">
                    <Form id="form" layout="vertical" form={form} onFinish={handleSubmit} name="dynamic_rule">
                        <div className="w-full grid grid-cols-1 gap-2">
                            {type !== 'Create' ? (
                                <Form.Item label="ID" name={'id'}>
                                    <InputComponent disabled={true} />
                                </Form.Item>
                            ) : null}

                            <Form.Item
                                label="Key"
                                name={'key'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Key!',
                                    },
                                ]}
                            >
                                <InputComponent placeholder='Input Key'/>
                            </Form.Item>

                            <Form.Item
                                label="Value"
                                name={'value'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Value!',
                                    },
                                ]}
                            >
                                <InputComponent placeholder='Input Value'/>
                            </Form.Item>
                        </div>
                    </Form>
                </div>
            </Modal>
        </Fragment>
    );
};

export default DataConfigurationForm;
