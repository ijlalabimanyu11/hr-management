import React, { Fragment, useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Breadcrumbs from '../../../../components/Breadcrumbs';
import HeaderTitle from '../../../../components/HeaderTitle';
import { AppDispatch, RootState } from '../../../../redux/store';
import { setPageTitle } from '../../../../redux/store/themeConfigSlice';
import Cards from '../../../../components/Cards';
import PerfectScrollbar from 'react-perfect-scrollbar';
import Frame from '../../../../components/Frame';
import {
    activeOrInactiveDataConfiguration,
    deleteDataConfiguration,
    getDataConfigurationHeader,
    getDataConfigurationPaginate,
    getDetailDataConfiguration,
} from '../../../../redux/slices/SystemSetup/DataConfiguration/DataConfigurationSlice';
import TablePagination from '../../../../components/TablePagination';
import { Button, InputRef, Tooltip } from 'antd';
import PermissionButton from '../../../../utils/PermissionButton';
import { columnsDataConfiguration } from './Table/TableViewDataConfiguration';
import ModalConfirm from '../../../../components/ModalConfirm';
import { DownloadOutlined, PlusOutlined } from '@ant-design/icons';
import { searchObject } from '../../../../utils/searchObject';
import DataConfigurationForm from './DataConfigurationForm';
import { handleChangePage, handleSearch, handleSort, SortParams } from '../../../../utils/UtilsTable';

const DataConfiguration = () => {
    // Selector
    const { loading, dataHeader, dataPaging, dataDetail } = useSelector((state: RootState) => state.dataConfiguration);

    // Declaration
    const searchInput = useRef<InputRef>(null);
    const dispatch = useDispatch<AppDispatch>();
    const dataSource = dataPaging?.data;

    // Use State
    const [page, setPage] = useState<number>(1);
    const [pageSize, setPageSize] = useState<number>(10);
    const [sort, setSort] = useState<string>('');
    const [search, setSearch] = useState<Record<string, string>>({});
    const [searchText, setSearchText] = useState<{ [key: string]: string }>({});
    const [searchedColumn, setSearchedColumn] = useState<string[]>([]);

    const [valuePage, setValuePage] = useState<any | null>(null);
    const [typeModal, setTypeModal] = useState<string>('');
    const [idData, setIdData] = useState<any>();
    const [status, setStatus] = useState<string>('');
    const [openModal, setOpenModal] = useState<boolean>(false);
    const [modalActiveOrInactive, setModalActiveOrInactive] = useState<boolean>(false);
    const [modalDelete, setModalDelete] = useState<boolean>(false);

    // Use Effect
    useEffect(() => {
        dispatch(setPageTitle('Data Configuration'));
    });

    useEffect(() => {
        if (valuePage !== '') {
            dispatch(getDataConfigurationPaginate({ type: valuePage, search: searchObject(search), page, pageSize, sort }));
        }
    }, [dispatch, search, page, pageSize, sort, valuePage]);

    useEffect(() => {
        dispatch(getDataConfigurationHeader());
    }, [dispatch]);

    // Refresh Data Table
    const refreshPage = () => {
        dispatch(getDataConfigurationPaginate({ type: valuePage, search: searchObject(search), page, pageSize, sort }));
    };

    // Breadcrumbs
    const routes = [
        {
            title: 'System Setup',
            href: '',
        },
        {
            title: 'Data Configuration',
            href: '',
        },
    ];

    // Handle Change Page Table
    const handleGeneralPageChange = (page: number, pageSize: number) => handleChangePage(page, pageSize, pageSize, setPage, setPageSize);

    // Sorting Table
    const handleGeneralSort = (_: any, __: any, sorter: SortParams) => {
        handleSort(sorter, setSort);
    };

    // Handle Search Table
    const handleGeneralSearch = (selectedKeys: string[], confirm: () => void, dataIndex: string) =>
        handleSearch(selectedKeys, confirm, dataIndex, setSearchText, setSearchedColumn, setSearch, setPage);

    // Handle Create
    const handleCreate = () => {
        setTypeModal('Create');
        setOpenModal(true);
    };

    // Handle Cancel Modal
    const handleCancelModal = () => {
        setIdData('');
        setStatus('');
        setOpenModal(false);
        setTypeModal('');
    };

    // Handle Update
    const handleUpdate = (value: any) => {
        setTypeModal('Update');
        setOpenModal(true);
        dispatch(getDetailDataConfiguration(value?.id));
    };

    // Handle Delete
    const handleDelete = (value: any) => {
        setIdData(value.id);
        setModalDelete(true);
    };

    // Handle ActiveOrInactive
    const handleActiveOrInactive = (value: any) => {
        setStatus(value.status);
        setIdData(value.id);
        setModalActiveOrInactive(true);
    };

    // Handle Submit ActiveOrInactive
    const submitActiveOrInactive = () => {
        setModalActiveOrInactive(false);
        dispatch(activeOrInactiveDataConfiguration({ id: idData, status: status }))
            .unwrap()
            .then(() => {
                setModalActiveOrInactive(false);
                refreshPage();
            })
            .catch((r) => {
                setModalActiveOrInactive(false);
                refreshPage();
            });
    };

    // Handle Submit Delete
    const submitDelete = () => {
        setModalDelete(false);
        dispatch(deleteDataConfiguration({ id: idData }))
            .unwrap()
            .then(() => {
                setModalDelete(false);
                refreshPage();
            })
            .catch((r) => {
                setModalDelete(false);
                refreshPage();
            });
    };

    // Handle Download
    const handleDownload = () => {};

    // Access Button
    const accessButton = PermissionButton(
        (props: any) => (
            <div className="flex align-middle gap-3">
                <Tooltip title="Download">
                    <Button onClick={props.handleDownload} className="btn-icon btn-primary" icon={<DownloadOutlined />} />
                </Tooltip>
                <Tooltip title="Create">
                    <Button onClick={props.handleCreate} className="btn-icon btn-primary" icon={<PlusOutlined />} />
                </Tooltip>
            </div>
        ),
        'data-configuration',
        ['CREATE', 'READ']
    );

    return (
        <Fragment>
            <Breadcrumbs routes={routes} />

            <div className="flex w-full justify-between align-middle">
                <HeaderTitle title="Data Configuration" />
            </div>

            <div className="flex justify-between">
                <div className="w-1/5 mr-2">
                    <Cards type={'card'}>
                        <PerfectScrollbar className="h-[calc(100vh-80px)] relative">
                            <Frame activeIndex={valuePage} setActiveIndex={setValuePage} data={dataHeader?.filter((item: any) => item.is_props === 0)} />
                        </PerfectScrollbar>
                    </Cards>
                </div>

                <div className="w-4/5 ml-2">
                    <Cards>
                        <TablePagination
                            dataSource={valuePage === '' ? [] : dataSource}
                            totalData={valuePage === '' ? 0 : dataPaging?.totalData}
                            current={page}
                            pageSize={pageSize}
                            onChange={handleGeneralPageChange}
                            columns={columnsDataConfiguration(page, pageSize, searchedColumn, searchText, handleGeneralSearch, handleUpdate, handleDelete, handleActiveOrInactive)}
                            onSort={handleGeneralSort}
                            tableScrolled={{ x: 2000, y: 300 }}
                            content={accessButton({ handleDownload, handleCreate })}
                        />
                    </Cards>
                </div>

                {/* Modal Create/Update Data Configuration */}
                <DataConfigurationForm
                    type={typeModal}
                    openModal={openModal}
                    cancelModal={handleCancelModal}
                    refreshPage={refreshPage}
                    dataUpdate={dataDetail}
                    valuePage={valuePage}
                    header={dataHeader?.filter((a: any) => a.key === valuePage)[0]?.value}
                />

                {/* Modal Delete */}
                {modalDelete ? <ModalConfirm title={`Are you sure want to delete the data?`} icon="warning" onConfirm={submitDelete} onCancel={() => setModalDelete(false)} /> : null}

                {/* Modal Active/Inactive */}
                {modalActiveOrInactive ? (
                    <ModalConfirm
                        title={`Are you sure want to ${status === 'ACTIVE' ? 'deactivate' : 'activate'} the data?`}
                        icon="warning"
                        onConfirm={submitActiveOrInactive}
                        onCancel={() => setModalActiveOrInactive(false)}
                    />
                ) : null}
            </div>
        </Fragment>
    );
};

export default DataConfiguration;
