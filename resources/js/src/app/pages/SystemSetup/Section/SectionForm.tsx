import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../../redux/store';
import { Button, Form, Modal, Select } from 'antd';
import InputComponent from '../../../../components/InputComponent';
import SelectComponent from '../../../../components/SelectComponent';
import { getEmployee } from '../../../../redux/slices/GlobalSlice';
import { createSection, updateSection } from '../../../../redux/slices/SystemSetup/Section/SectionSlice';

interface SectionFormProps {
    type: any;
    openModal?: any;
    cancelModal?: any;
    refreshPage?: any;
    dataUpdate?: any;
}

const SectionForm: React.FC<SectionFormProps> = ({type, openModal, cancelModal, dataUpdate, refreshPage}) => {
    // Selector
    const { dataEmployee } = useSelector((state: RootState) => state.global);

    // Declaration
    const dispatch = useDispatch<AppDispatch>();
    const [form] = Form.useForm();

    // Use State
    const [description, setDescription] = useState<string>('');

    // Use Effect
    useEffect(() => {
        dispatch(getEmployee())
    }, [dispatch]);

    useEffect(() => {
        if (type === 'Update' && dataUpdate) {
            form.setFieldsValue({
                id: dataUpdate?.id,
                sectionCode: dataUpdate?.sectionCode,
                sectionName: dataUpdate?.sectionName,
                sectionHead: dataUpdate?.employee_id,
                description: dataUpdate?.description,
            });
        }
    }, [type, form, dataUpdate]);

    // Function Submit Form
    const handleSubmit = (formValue: any) => {
        const data = {
            sectionCode: formValue.sectionCode,
            sectionName: formValue.sectionName,
            sectionHead: formValue.sectionHead,
            description: formValue.description,
        };

        if (type === 'Create') {
            dispatch(createSection({ body: data }))
                .unwrap()
                .then(() => {
                    cancelModal();
                    refreshPage();
                    form.resetFields();
                    setDescription('');
                })
                .catch(() => {
                    refreshPage();
                });
        } else {
            dispatch(updateSection({ body: data, id: dataUpdate?.id }))
            .unwrap()
            .then(() => {
                cancelModal();
                refreshPage();
                form.resetFields();
                setDescription('');
            })
            .catch(() => {
                refreshPage();
            });
        }
    };
  return (
    <Fragment>
        <Modal
                maskClosable={false}
                centered
                title={`${type} Section`}
                open={openModal}
                onOk={handleSubmit}
                onCancel={() => {
                    form.resetFields();
                    cancelModal();
                }}
                width={600}
                footer={[
                    <Button onClick={cancelModal} form="form" htmlType="reset" danger type="default" className="btn btn-outline-danger">
                        Discard
                    </Button>,
                    <Button htmlType="submit" form="form" type="primary" className="btn btn-primary-new">
                        Save
                    </Button>,
                ]}
            >
                <div className="w-full flex flex-col p-2">
                    <Form id="form" layout="vertical" form={form} onFinish={handleSubmit} name="dynamic_rule">
                        <div className="w-full grid grid-cols-1 gap-2">
                            {type !== 'Create' ? (
                                <Form.Item label="ID" name={'id'}>
                                    <InputComponent disabled={true} />
                                </Form.Item>
                            ) : null}

                            <Form.Item
                                label="Section Code"
                                name={'sectionCode'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Section Code!',
                                    },
                                ]}
                            >
                                <InputComponent placeholder='Input Section Code'/>
                            </Form.Item>

                            <Form.Item
                                label="Section Name"
                                name={'sectionName'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Section Name!',
                                    },
                                ]}
                            >
                                <InputComponent placeholder='Input Section Name'/>
                            </Form.Item>

                            <Form.Item
                                label="Head of Section"
                                name={'sectionHead'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please select your Section Head!',
                                    },
                                ]}
                            >
                                <SelectComponent placeholder='Select Head of Section'>
                                    {dataEmployee &&
                                        dataEmployee?.map((data: any, index: any) => (
                                            <Select.Option value={data.key} key={index}>
                                                {data.value}
                                            </Select.Option>
                                        ))}
                                </SelectComponent>
                            </Form.Item>

                            <Form.Item
                                label={'Description'}
                                name={'description'}
                                className={'w-full'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Description!',
                                    },
                                ]}
                            >
                                <InputComponent placeholder='Input Description' type="textarea" value={description} onChange={(e: any) => setDescription(e.target.value)} />
                            </Form.Item>
                        </div>
                    </Form>
                </div>
            </Modal>
    </Fragment>
  )
}

export default SectionForm
