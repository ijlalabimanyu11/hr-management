import React from 'react';
import ColumnSearch from '../../../../../utils/ColumnSearch';
import Pills from '../../../../../components/Pills';
import Action from '../../../../../components/Action';
import { TbCircleCheck, TbPencil } from 'react-icons/tb';
import { RiDeleteBinLine } from 'react-icons/ri';
import { BiMinusCircle } from 'react-icons/bi';
import { PermissionAction } from '../../../../../utils/PermissionAction';

export const TableSection = (
    page = 1,
    pageSize = 10,
    searchedColumn: any,
    searchText: any,
    handleSearch: (selectedKeys: string[], confirm: () => void, dataIndex: string) => void,
    handleUpdate: (value: any) => void,
    handleDelete: (value: any) => void,
    handleActiveOrInactive: (value: any) => void
) => {
    const action = (record: any) => {
        const actions: any[] = [];

        if (PermissionAction('section', 'TOGGLE')) {
            actions.push({
                type: record.status === 'ACTIVE' ? 'inactive' : 'active',
                icon: record.status === 'ACTIVE' ? <BiMinusCircle size={16} /> : <TbCircleCheck size={16} />,
                onClick: () => handleActiveOrInactive(record),
            });
        }

        if (PermissionAction('section', 'UPDATE')) {
            actions.push({
                type: 'update',
                icon: <TbPencil size={16} />,
                onClick: () => handleUpdate(record),
            });
        }

        if (PermissionAction('section', 'DELETE')) {
            actions.push({
                type: 'delete',
                icon: <RiDeleteBinLine size={16} />,
                onClick: () => handleDelete(record),
            });
        }

        return actions.length > 0 ? <Action actions={actions} /> : null;
    };

    const actionColumn =
        action.length > 0
            ? {
                  title: 'Action',
                  align: 'center',
                  fixed: 'right',
                  render: (id: any, record: any) => action(record),
              }
            : null;

    return [
        {
            title: 'No',
            align: 'center',
            width: 60,
            render: (text: any, object: any, index: number) => (page - 1) * pageSize + index + 1,
        },
        {
            title: 'Section Code',
            sorter: true,
            align: 'left',
            dataIndex: 'section_code',
            key: 'section_code',
            ...ColumnSearch({ dataIndex: 'section_code', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Head of Section',
            sorter: true,
            align: 'left',
            dataIndex: 'section_head',
            key: 'section_head',
            ...ColumnSearch({ dataIndex: 'section_head', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Section Name',
            sorter: true,
            align: 'left',
            dataIndex: 'section_name',
            key: 'section_name',
            ...ColumnSearch({ dataIndex: 'section_name', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Description',
            sorter: true,
            align: 'left',
            dataIndex: 'description',
            key: 'description',
            ...ColumnSearch({ dataIndex: 'description', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Created By',
            sorter: true,
            align: 'left',
            dataIndex: 'created_by',
            key: 'created_by',
            ...ColumnSearch({ dataIndex: 'created_by', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Created Date',
            sorter: true,
            dataIndex: 'created_date',
            key: 'created_date',
            align: 'center',
            ...ColumnSearch({ dataIndex: 'created_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Modified By',
            sorter: true,
            align: 'left',
            dataIndex: 'modified_by',
            key: 'modified_by',
            ...ColumnSearch({ dataIndex: 'modified_by', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Modified Date',
            sorter: true,
            dataIndex: 'modified_date',
            key: 'modified_date',
            align: 'center',
            ...ColumnSearch({ dataIndex: 'modified_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Status',
            sorter: true,
            dataIndex: 'status',
            key: 'status',
            ...ColumnSearch({ dataIndex: 'status', searchedColumn, searchText, handleSearch, typeFilter: 'status' }),
            render: (index: any) => {
                let text: any;
                switch (index) {
                    case 'WAITING APPROVAL':
                        text = 'Waiting Approval';
                        break;
                    default:
                        text = index ? index.charAt(0).toUpperCase() + index.slice(1).toLowerCase() : index;
                        break;
                }
                return text ? (
                    <div className={'flex justify-center'}>
                        <Pills color={text}>{text}</Pills>
                    </div>
                ) : (
                    text
                );
            },
        },
        actionColumn,
    ];
};
