import React from 'react';
import ColumnSearch from '../../../../../utils/ColumnSearch';
import Pills from '../../../../../components/Pills';
import Action from '../../../../../components/Action';
import { TbCircleCheck, TbPencil } from 'react-icons/tb';
import { RiDeleteBinLine } from 'react-icons/ri';
import { BiMinusCircle } from 'react-icons/bi';
import { PermissionAction } from '../../../../../utils/PermissionAction';

export const TableRoleAccess = (
    page = 1,
    pageSize = 10,
    searchedColumn: any,
    searchText: any,
    handleSearch: (selectedKeys: string[], confirm: () => void, dataIndex: string) => void,
    handleUpdate: (value: any) => void,
    handleDelete: (value: any) => void,
    handleActiveOrInactive: (value: any) => void
) => {
    const action = (record: any) => {
        const actions: any[] = [];

        if (PermissionAction('role-access', 'TOGGLE')) {
            actions.push({
                type: record.status === 'ACTIVE' ? 'inactive' : 'active',
                icon: record.status === 'ACTIVE' ? <BiMinusCircle size={16} /> : <TbCircleCheck size={16} />,
                onClick: () => handleActiveOrInactive(record),
            });
        }

        if (PermissionAction('role-access', 'UPDATE')) {
            actions.push({
                type: 'update',
                icon: <TbPencil size={16} />,
                onClick: () => handleUpdate(record),
            });
        }

        if (PermissionAction('role-access', 'DELETE')) {
            actions.push({
                type: 'delete',
                icon: <RiDeleteBinLine size={16} />,
                onClick: () => handleDelete(record),
            });
        }

        return actions.length > 0 ? <Action actions={actions} /> : null;
    };

    const columns: any = [
        {
            title: 'No',
            align: 'center',
            width: 60,
            render: (text: any, object: any, index: number) => (page - 1) * pageSize + index + 1,
        },
        {
            title: 'Menu',
            sorter: true,
            align: 'left',
            dataIndex: 'menu_name',
            key: 'menu_name',
            ...ColumnSearch({ dataIndex: 'menu_name', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Privileges',
            sorter: true,
            dataIndex: 'privilages_json',
            key: 'privilages_json',
            width: 600,
            ...ColumnSearch({ dataIndex: 'privilages_json', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
            render: (text: any) => {
                return (
                    <div className={'w-full flex justify-center gap-2'}>
                        {text.map((item: any, index: any) => (
                            <Pills key={index} color="privileges">
                                {item.charAt(0).toUpperCase() + item.slice(1).toLowerCase()}
                            </Pills>
                        ))}
                    </div>
                );
            },
        },
        {
            title: 'Created By',
            sorter: true,
            align: 'left',
            dataIndex: 'created_by',
            key: 'created_by',
            ...ColumnSearch({ dataIndex: 'created_by', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Created Date',
            sorter: true,
            dataIndex: 'created_date',
            key: 'created_date',
            align: 'center',
            ...ColumnSearch({ dataIndex: 'created_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Modified By',
            sorter: true,
            align: 'left',
            dataIndex: 'modified_by',
            key: 'modified_by',
            ...ColumnSearch({ dataIndex: 'modified_by', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Modified Date',
            sorter: true,
            dataIndex: 'modified_date',
            key: 'modified_date',
            align: 'center',
            ...ColumnSearch({ dataIndex: 'modified_date', searchedColumn, searchText, handleSearch, typeFilter: 'input' }),
        },
        {
            title: 'Status',
            sorter: true,
            dataIndex: 'status',
            key: 'status',
            ...ColumnSearch({ dataIndex: 'status', searchedColumn, searchText, handleSearch, typeFilter: 'status' }),
            render: (index: any) => {
                let text: any;
                switch (index) {
                    case 'WAITING APPROVAL':
                        text = 'Waiting Approval';
                        break;
                    default:
                        text = index ? index.charAt(0).toUpperCase() + index.slice(1).toLowerCase() : index;
                        break;
                }
                return text ? (
                    <div className={'flex justify-center'}>
                        <Pills color={text}>{text}</Pills>
                    </div>
                ) : (
                    text
                );
            },
        },
    ];

    // Conditionally add the action column if actions exist
    if (PermissionAction('role-access', 'TOGGLE') || PermissionAction('role-access', 'UPDATE') || PermissionAction('role-access', 'DELETE')) {
        columns.push({
            title: 'Action',
            align: 'center',
            fixed: 'right',
            render: (id: any, record: any) => action(record),
        });
    }

    return columns;
};

export default TableRoleAccess;
