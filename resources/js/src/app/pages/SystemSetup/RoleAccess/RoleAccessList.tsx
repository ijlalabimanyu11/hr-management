import { Button, InputRef, Select, Spin, Tooltip } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import React, { Fragment, useEffect, useRef, useState } from 'react';
import { DownloadOutlined, PlusOutlined } from '@ant-design/icons';
import Breadcrumbs from '../../../../components/Breadcrumbs';
import { SYSTEM_SETUP_ROUTES } from '../../../../router/systemSetup/system_setup_routes';
import HeaderTitle from '../../../../components/HeaderTitle';
import Cards from '../../../../components/Cards';
import SelectComponent from '../../../../components/SelectComponent';
import { AppDispatch, RootState } from '../../../../redux/store';
import { setPageTitle } from '../../../../redux/store/themeConfigSlice';
import TablePagination from '../../../../components/TablePagination';
import TableRoleAccess from './Table/TableRoleAccess';
import PermissionButton from '../../../../utils/PermissionButton';
import RoleAccessForm from './RoleAccessForm';
import { getRoleType } from '../../../../redux/slices/GlobalSlice';
import { activeOrInactiveRoleAccess, deleteRoleAccess, getDetailRoleAccess, getRoleAccessPaginate } from '../../../../redux/slices/SystemSetup/RoleAccess/RoleAccessSlice';
import { searchObject } from '../../../../utils/searchObject';
import ModalConfirm from '../../../../components/ModalConfirm';
import { handleChangePage, handleSearch, handleSort, SortParams } from '../../../../utils/UtilsTable';

const RoleAccessList = () => {
    // Selector
    const { dataRole } = useSelector((state: RootState) => state.global);
    const { data, loading, dataDetail } = useSelector((state: RootState) => state.role);

    // Declaration
    const dispatch = useDispatch<AppDispatch>();
    const searchInput = useRef<InputRef>(null);
    const dataSource = data?.data;

    // Use State
    const [page, setPage] = useState<number>(1);
    const [pageSize, setPageSize] = useState<number>(10);
    const [sort, setSort] = useState<string>('');
    const [search, setSearch] = useState<Record<string, string>>({});
    const [searchText, setSearchText] = useState<{ [key: string]: string }>({});
    const [searchedColumn, setSearchedColumn] = useState<string[]>([]);

    const [typeModal, setTypeModal] = useState<string>('');
    const [idData, setIdData] = useState<any>();
    const [status, setStatus] = useState<string>('');
    const [openModal, setOpenModal] = useState<boolean>(false);
    const [modalActiveOrInactive, setModalActiveOrInactive] = useState<boolean>(false);
    const [modalDelete, setModalDelete] = useState<boolean>(false);
    const [filterRole, setFilterRole] = useState<string>('');

    // Use Effect
    useEffect(() => {
        dispatch(setPageTitle('Role Access'));
    });

    useEffect(() => {
        dispatch(getRoleType());
    }, [dispatch]);

    useEffect(() => {
        if (filterRole !== '') {
            dispatch(getRoleAccessPaginate({ type: filterRole, search: searchObject(search), page, pageSize, sort }));
        }
    }, [dispatch, search, page, pageSize, sort, filterRole]);

    // Refresh Data Table
    const refreshPage = () => {
        dispatch(getRoleAccessPaginate({ type: filterRole, search: searchObject(search), page, pageSize, sort }));
    };

    // Breadcrumbs
    const routes = [
        {
            title: 'System Setup',
            href: '',
        },
        {
            title: 'Role Access',
            href: SYSTEM_SETUP_ROUTES.ROLE_ACCESS_VIEW_ELEMENTS,
        },
    ];

    // Handle Change Page Table
    const handleGeneralPageChange = (page: number, pageSize: number) => handleChangePage(page, pageSize, pageSize, setPage, setPageSize);

    // Sorting Table
    const handleGeneralSort = (_: any, __: any, sorter: SortParams) => {
        handleSort(sorter, setSort);
    };

    // Handle Search Table
    const handleGeneralSearch = (selectedKeys: string[], confirm: () => void, dataIndex: string) =>
        handleSearch(selectedKeys, confirm, dataIndex, setSearchText, setSearchedColumn, setSearch, setPage);

    // Handle Create
    const handleCreate = () => {
        setTypeModal('Create');
        setOpenModal(true);
    };

    // Handle Cancel Modal
    const handleCancelModal = () => {
        setIdData('');
        setStatus('');
        setOpenModal(false);
        setTypeModal('');
    };

    // Handle Update
    const handleUpdate = (value: any) => {
        setTypeModal('Update');
        setOpenModal(true);
        dispatch(getDetailRoleAccess(value?.id));
    };

    // Handle Delete
    const handleDelete = (value: any) => {
        setIdData(value.id);
        setModalDelete(true);
    };

    // Handle ActiveOrInactive
    const handleActiveOrInactive = (value: any) => {
        setStatus(value.status);
        setIdData(value.id);
        setModalActiveOrInactive(true);
    };

    // Handle Submit ActiveOrInactive
    const submitActiveOrInactive = () => {
        setModalActiveOrInactive(false);
        dispatch(activeOrInactiveRoleAccess({ id: idData, status: status }))
            .unwrap()
            .then(() => {
                refreshPage();
            })
            .catch((r) => {
                refreshPage();
            });
    };

    // Handle Submit Delete
    const submitDelete = () => {
        setModalDelete(false);
        dispatch(deleteRoleAccess({ id: idData }))
            .unwrap()
            .then(() => {
                refreshPage();
            })
            .catch((r) => {
                refreshPage();
            });
    };

    // Handle Download
    const handleDownload = () => {};

    // Adjust data filterRole
    const adjustFilterRole = (value: any) => {
        let text: string;
        switch (value) {
            case 'super-admin':
                text = 'Super Admin';
                break;
            default:
                text = value ? value.charAt(0).toUpperCase() + value.slice(1).toLowerCase() : value;
                break;
        }
        return text;
    };

    // Access Button
    const accessButton = PermissionButton(
        (props: any) => (
            <div className="flex align-middle gap-3">
                <Tooltip title="Download">
                    <Button onClick={props.handleDownload} className="btn-icon btn-primary" icon={<DownloadOutlined />} disabled={filterRole === '' || filterRole === undefined ? true : false} />
                </Tooltip>
                <Tooltip title="Create">
                    <Button onClick={props.handleCreate} className="btn-icon btn-primary" icon={<PlusOutlined />} disabled={filterRole === '' || filterRole === undefined ? true : false} />
                </Tooltip>
            </div>
        ),
        'role-access',
        ['CREATE', 'READ']
    );

    return (
        <Fragment>
            <Spin spinning={loading}>
                <Breadcrumbs routes={routes} />
                <div className="flex w-full justify-between align-middle">
                    <HeaderTitle title="Role Access" />
                </div>

                <Cards>
                    <TablePagination
                        dataSource={filterRole === '' ? [] : dataSource}
                        totalData={filterRole === '' ? 0 : data?.totalData}
                        current={page}
                        pageSize={pageSize}
                        onChange={handleGeneralPageChange}
                        columns={TableRoleAccess(page, pageSize, searchedColumn, searchText, handleGeneralSearch, handleUpdate, handleDelete, handleActiveOrInactive)}
                        onSort={handleGeneralSort}
                        useSelect={false}
                        tableScrolled={{ x: 1900, y: 300 }}
                        filterData={
                            <>
                                <div className={`mb-4  w-full flex justify-between items-center`}>
                                    <div className="w-1/3">
                                        <SelectComponent placeholder="Select Role" onChange={(e) => setFilterRole(e)} value={filterRole}>
                                            {dataRole &&
                                                dataRole?.map((data: any, index: any) => (
                                                    <Select.Option value={data.key} key={index}>
                                                        {data.value}
                                                    </Select.Option>
                                                ))}
                                        </SelectComponent>
                                    </div>

                                    <div>{accessButton({ handleDownload, handleCreate })}</div>
                                </div>
                            </>
                        }
                    />
                </Cards>

                {/* Modal Create/Update Role Access */}
                <RoleAccessForm
                    type={typeModal}
                    openModal={openModal}
                    cancelModal={handleCancelModal}
                    refreshPage={refreshPage}
                    dataUpdate={dataDetail}
                    role={filterRole}
                    showRole={adjustFilterRole(filterRole)}
                />

                {/* Modal Delete */}
                {modalDelete ? <ModalConfirm title={`Are you sure want to delete the role access?`} icon="warning" onConfirm={submitDelete} onCancel={() => setModalDelete(false)} /> : null}

                {/* Modal Active/Inactive */}
                {modalActiveOrInactive ? (
                    <ModalConfirm
                        title={`Are you sure want to ${status === 'ACTIVE' ? 'deactivate' : 'activate'} the role access?`}
                        icon="warning"
                        onConfirm={submitActiveOrInactive}
                        onCancel={() => setModalActiveOrInactive(false)}
                    />
                ) : null}
            </Spin>
        </Fragment>
    );
};

export default RoleAccessList;
