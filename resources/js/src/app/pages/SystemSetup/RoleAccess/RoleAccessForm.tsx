import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../../redux/store';
import { Button, Form, Modal, Select } from 'antd';
import InputComponent from '../../../../components/InputComponent';
import SelectComponent from '../../../../components/SelectComponent';
import { getMenu, getPrivileges } from '../../../../redux/slices/GlobalSlice';
import { createRoleAccess, roleAccessBody, updateRoleAccess } from '../../../../redux/slices/SystemSetup/RoleAccess/RoleAccessSlice';

interface RoleAccessFormProps {
    type: any;
    openModal?: any;
    cancelModal?: any;
    refreshPage?: any;
    dataUpdate?: any;
    role: any
    showRole: any
}

const RoleAccessForm: React.FC<RoleAccessFormProps> = ({ type, openModal, cancelModal, dataUpdate, refreshPage, role, showRole }) => {
    // Selector
    const { dataMenu, dataPrivileges } = useSelector((state: RootState) => state.global);


    // Declaration
    const dispatch = useDispatch<AppDispatch>();
    const [form] = Form.useForm();

    // Use State

    // Use Effect
    useEffect(() => {
        dispatch(getMenu())
        dispatch(getPrivileges())
    }, [dispatch]);

    useEffect(() => {
        if (type === 'Update' && dataUpdate) {
            form.setFieldsValue({
                id: dataUpdate?.id,
                menu_key: dataUpdate?.menu_key,
                privilages_json: dataUpdate?.privilages_json,
            });
        }
    }, [type, form, dataUpdate]);

    const handleSubmit = (formValue: any) => {
        const data: roleAccessBody = {
            menu_key: formValue.menu_key,
            privilages_json: formValue.privilages_json
        }


        if (type === 'Create') {
            dispatch(createRoleAccess({ body: data, role: role }))
                .unwrap()
                .then(() => {
                    cancelModal();
                    refreshPage();
                    form.resetFields();
                })
                .catch(() => {
                    refreshPage();
                });
        } else {
            dispatch(updateRoleAccess({ body: data, id: dataUpdate?.id }))
            .unwrap()
            .then(() => {
                cancelModal();
                refreshPage();
                form.resetFields();
            })
            .catch(() => {
                refreshPage();
            });
        }
    };

    return (
        <Fragment>
            <Modal
                maskClosable={false}
                centered
                title={`${type} Role User`}
                open={openModal}
                onOk={handleSubmit}
                onCancel={() => {
                    form.resetFields();
                    cancelModal();
                }}
                width={400}
                footer={[
                    <Button onClick={cancelModal} form="form" htmlType="reset" danger type="default" className="btn btn-outline-danger">
                        Discard
                    </Button>,
                    <Button htmlType="submit" form="form" type="primary" className="btn btn-primary-new">
                        Save
                    </Button>,
                ]}
            >
                <div className="w-full flex flex-col p-2">
                    <Form id="form" layout="vertical" form={form} onFinish={handleSubmit} name="dynamic_rule">
                        <div className="w-full grid grid-cols-1 gap-2">
                            {type !== 'Create' ? (
                                <Form.Item label="ID" name={'id'}>
                                    <InputComponent disabled={true} />
                                </Form.Item>
                            ) : null}

                            <Form.Item label="Role" name={'role'} valuePropName={showRole}>
                                <InputComponent disabled={true} value={showRole}/>
                            </Form.Item>

                            <Form.Item
                                label="Menu"
                                name={'menu_key'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please select your Menu!',
                                    },
                                ]}
                            >
                                <SelectComponent placeholder='Select Menu' disabled={type !== 'Create' ? true : false}>
                                    {dataMenu &&
                                        dataMenu?.map((data: any, index: any) => (
                                            <Select.Option value={data.key} key={index}>
                                                {data.value}
                                            </Select.Option>
                                        ))}
                                </SelectComponent>
                            </Form.Item>

                            <Form.Item
                                label="Privileges"
                                name={'privilages_json'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please select your Privileges!',
                                    },
                                ]}
                            >
                                <SelectComponent placeholder='Select Privileges' mode='multiple'>
                                    {dataPrivileges &&
                                        dataPrivileges?.map((data: any, index: any) => (
                                            <Select.Option value={data.key} key={index}>
                                                {data.value}
                                            </Select.Option>
                                        ))}
                                </SelectComponent>
                            </Form.Item>
                        </div>
                    </Form>
                </div>
            </Modal>
        </Fragment>
    );
};

export default RoleAccessForm;
