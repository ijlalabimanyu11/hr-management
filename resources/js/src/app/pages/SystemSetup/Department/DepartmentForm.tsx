import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../../redux/store';
import { Button, Form, Modal, Select } from 'antd';
import InputComponent from '../../../../components/InputComponent';
import SelectComponent from '../../../../components/SelectComponent';
import { getEmployee } from '../../../../redux/slices/GlobalSlice';
import { createDepartment, departmentBody, updateDepartment } from '../../../../redux/slices/SystemSetup/Department/DepartmentSlice';

interface DepartmentFormProps {
    type: any;
    openModal?: any;
    cancelModal?: any;
    refreshPage?: any;
    dataUpdate?: any;
}

const DepartmentForm: React.FC<DepartmentFormProps> = ({ type, openModal, cancelModal, dataUpdate, refreshPage }) => {
    // Selector
    const { dataEmployee } = useSelector((state: RootState) => state.global);

    // Declaration
    const dispatch = useDispatch<AppDispatch>();
    const [form] = Form.useForm();

    // Use State
    const [description, setDescription] = useState<string>('');

    // Use Effect
    useEffect(() => {
        dispatch(getEmployee());
    }, [dispatch]);

    useEffect(() => {
        if (type === 'Update' && dataUpdate) {
            form.setFieldsValue({
                id: dataUpdate?.id,
                departmentCode: dataUpdate?.departmentCode,
                departmentName: dataUpdate?.departmentName,
                departmentHead: dataUpdate?.employee_id,
                description: dataUpdate?.description,
            });
        }
    }, [type, form, dataUpdate]);

    const handleSubmit = (formValue: any) => {
        const data: departmentBody = {
            departmentCode: formValue.departmentCode,
            departmentName: formValue.departmentName,
            departmentHead: formValue.departmentHead,
            description: formValue.description,
        };

        if (type === 'Create') {
            dispatch(createDepartment({ body: data }))
                .unwrap()
                .then(() => {
                    cancelModal();
                    refreshPage();
                    form.resetFields();
                })
                .catch(() => {
                    refreshPage();
                });
        } else {
            dispatch(updateDepartment({ body: data, id: dataUpdate?.id }))
                .unwrap()
                .then(() => {
                    cancelModal();
                    refreshPage();
                    form.resetFields();
                })
                .catch(() => {
                    refreshPage();
                });
        }
    };

    return (
        <Fragment>
            <Modal
                maskClosable={false}
                centered
                title={`${type} Department`}
                open={openModal}
                onOk={handleSubmit}
                onCancel={() => {
                    form.resetFields();
                    cancelModal();
                }}
                width={600}
                footer={[
                    <Button onClick={cancelModal} form="form" htmlType="reset" danger type="default" className="btn btn-outline-danger">
                        Discard
                    </Button>,
                    <Button htmlType="submit" form="form" type="primary" className="btn btn-primary-new">
                        Save
                    </Button>,
                ]}
            >
                <div className="w-full flex flex-col p-2">
                    <Form id="form" layout="vertical" form={form} onFinish={handleSubmit} name="dynamic_rule">
                        <div className="w-full grid grid-cols-1 gap-2">
                            {type !== 'Create' ? (
                                <Form.Item label="ID" name={'id'}>
                                    <InputComponent disabled={true} />
                                </Form.Item>
                            ) : null}

                            <Form.Item
                                label="Department Code"
                                name={'departmentCode'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Department Code!',
                                    },
                                ]}
                            >
                                <InputComponent placeholder="Input Department Code" />
                            </Form.Item>

                            <Form.Item
                                label="Department Name"
                                name={'departmentName'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Department Name!',
                                    },
                                ]}
                            >
                                <InputComponent placeholder="Input Department Name" />
                            </Form.Item>

                            <Form.Item
                                label="Head of Department"
                                name={'departmentHead'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please select your Head of Department!',
                                    },
                                ]}
                            >
                                <SelectComponent placeholder="Select Head of Department">
                                    {dataEmployee &&
                                        dataEmployee?.map((data: any, index: any) => (
                                            <Select.Option value={data.key} key={index}>
                                                {data.value}
                                            </Select.Option>
                                        ))}
                                </SelectComponent>
                            </Form.Item>

                            <Form.Item
                                label={'Description'}
                                name={'description'}
                                className={'w-full'}
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Description!',
                                    },
                                ]}
                            >
                                <InputComponent placeholder="Input Description" type="textarea" value={description} onChange={(e: any) => setDescription(e.target.value)} />
                            </Form.Item>
                        </div>
                    </Form>
                </div>
            </Modal>
        </Fragment>
    );
};

export default DepartmentForm;
