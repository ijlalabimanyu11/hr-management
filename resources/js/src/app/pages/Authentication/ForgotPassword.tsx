import React, { useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { setPageTitle } from '../../../redux/store/themeConfigSlice';
import { AppDispatch, RootState } from '../../../redux/store';
import { Form, FormInstance, Input } from 'antd';
import { MailOutlined } from '@ant-design/icons';
import { forgotPassword } from '../../../redux/slices/Authentication/AuthSlice';
import Swal from 'sweetalert2';

interface FormValue {
    email: string;
}

const ForgotPassword = () => {
    // Selector
    // const { forgotPassword } = useSelector((state: RootState) => state.auth);

    // Declaration
    const dispatch = useDispatch<AppDispatch>();
    const navigate = useNavigate();
    const [form] = Form.useForm<FormInstance>();

    // State

    // Use Effect
    useEffect(() => {
        dispatch(setPageTitle('Forgot Password'));
    });

    const handlePassword = (formValue: FormValue) => {
        const data = {
            email: formValue.email,
        };

        dispatch(forgotPassword({ body: data }))
            .unwrap()
            .then(() => {})
            .catch(() => {});
    };

    const submitForm = (values: any) => {
        handlePassword(values);
    };

    return (
        <div>
            <div className="absolute inset-0">
                <img src="/assets/images/auth/bg-gradient.png" alt="image" className="h-full w-full object-cover" />
            </div>

            <div className="relative flex min-h-screen items-center justify-center bg-cover bg-center bg-no-repeat px-6 py-10 dark:bg-[#060818] sm:px-16">
                <div className="relative w-full max-w-[600px]">
                    <div className="relative flex flex-col justify-center rounded-md bg-white/20 backdrop-blur-lg dark:bg-black/50 px-6 lg:min-h-[200px] py-20">
                        <div className="mx-auto w-full max-w-[440px]">
                            <div className="mb-5">
                                <h1 className="text-3xl font-extrabold !leading-snug text-white text-center md:text-4xl">Logo</h1>
                            </div>
                            <div className="mb-5">
                                <h1 className="text-3xl font-extrabold !leading-snug text-white text-center md:text-2xl">Forgot Password</h1>
                            </div>

                            <Form
                                className="space-y-5 dark:text-white"
                                form={form}
                                onFinish={submitForm}
                                initialValues={{
                                    remember: true,
                                }}
                                layout="vertical"
                            >
                                <Form.Item
                                    name="email"
                                    rules={[
                                        {
                                            type: 'email',
                                            message: 'The input is not valid Email!',
                                          },
                                          {
                                            required: true,
                                            message: 'Please input your Email!',
                                          },
                                    ]}
                                    label={<p className="text-white">Email</p>}
                                >
                                    <Input prefix={<MailOutlined />} size="large" placeholder="Username" className="relative text-white-dark form-input placeholder:text-white-dark" />
                                </Form.Item>

                                <Form.Item htmlFor="submit">
                                    <button type="submit" className="btn btn-gradient !mt-2 w-full border-0 uppercase shadow-[0_10px_20px_-10px_rgba(67,97,238,0.44)]">
                                        Submit
                                    </button>
                                </Form.Item>

                                <Form.Item>
                                    <Link to="/login" className="text-white relative flex items-center justify-center">
                                        Back to Login
                                    </Link>
                                </Form.Item>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ForgotPassword;
