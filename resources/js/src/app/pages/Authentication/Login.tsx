import React, { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { setPageTitle } from '../../../redux/store/themeConfigSlice';
import { Checkbox, Form, FormInstance, Input } from 'antd';
import { AppDispatch } from '../../../redux/store';
import { authenticationBody, getAccess, login } from '../../../redux/slices/Authentication/AuthSlice';
import { EyeInvisibleOutlined, EyeTwoTone, LockOutlined, UserOutlined } from '@ant-design/icons';
import InputComponent from '../../../components/InputComponent';

interface FormValue {
    username: string;
    password: string;
    remember: boolean;
}

const Login = () => {
    // Selector

    // Declaration
    const dispatch = useDispatch<AppDispatch>();
    const navigate = useNavigate();
    const [form] = Form.useForm<FormInstance>();

    // State
    const [rememberMe, setRememberMe] = useState<boolean>(false);

    // Use Effect
    useEffect(() => {
        dispatch(setPageTitle('Login'));
    });

    const handleLogin = (formValue: FormValue) => {
        const data: authenticationBody = {
            username: formValue.username,
            password: formValue.password,
        };

        dispatch(login({ body: data, remember: rememberMe }))
            .unwrap()
            .then(() => {
                navigate('/dashboard');
                dispatch(getAccess({ remember: rememberMe }));
            })
            .catch(() => {});
    };

    const onFinish = (values: any) => {
        handleLogin(values);
    };

    return (
        <div>
            <div className="absolute inset-0">
                <img src="/assets/images/auth/bg-gradient.png" alt="image" className="h-full w-full object-cover" />
            </div>

            <div className="relative flex min-h-screen items-center justify-center bg-cover bg-center bg-no-repeat px-6 py-10 dark:bg-[#5882C1] sm:px-16">
                <div className="relative w-full max-w-[600px]">
                    <div className="relative flex flex-col justify-center rounded-md bg-white/20 backdrop-blur-lg dark:bg-[#5882C1]/50 px-6 lg:min-h-[350px] py-20">
                        <div className="mx-auto w-full max-w-[440px]">
                            <div className="mb-5">
                                <h1 className="text-3xl font-extrabold !leading-snug text-white text-center md:text-4xl">Logo</h1>
                            </div>
                            <div className="mb-5">
                                <h1 className="text-3xl font-extrabold !leading-snug text-white text-center md:text-2xl">Sign In</h1>
                            </div>

                            <Form
                                className="space-y-5 dark:text-white"
                                form={form}
                                onFinish={onFinish}
                                initialValues={{
                                    remember: true,
                                }}
                                layout="vertical"
                            >
                                <Form.Item
                                    name="username"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Please input your username!',
                                        },
                                    ]}
                                    label={<p className="text-white">Username</p>}
                                >
                                    <InputComponent prefix={<UserOutlined />} size="large" placeholder="Username" />
                                </Form.Item>

                                <Form.Item
                                    name="password"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Please input your password!',
                                        },
                                    ]}
                                    label={<p className="text-white">Password</p>}
                                >
                                    <Input.Password
                                        prefix={<LockOutlined />}
                                        size="large"
                                        placeholder="Password"
                                        className="relative placeholder:text-white-dark"
                                        iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                                    />
                                </Form.Item>

                                <Form.Item name={'remember'} noStyle>
                                    <Checkbox onChange={(e) => setRememberMe(e.target.checked)} className="text-white">
                                        Remember me
                                    </Checkbox>
                                    <Link to="/forgot-password" className="text-white float-right">
                                        Forgot Password?
                                    </Link>
                                </Form.Item>

                                <Form.Item htmlFor="submit">
                                    <button type="submit" className="btn btn-gradient !mt-2 w-full border-0 uppercase shadow-[0_10px_20px_-10px_rgba(67,97,238,0.44)]">
                                        Sign in
                                    </button>
                                </Form.Item>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Login;
