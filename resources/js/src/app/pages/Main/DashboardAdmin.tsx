import React from 'react';
import Breadcrumb from '../../../components/Breadcrumbs';

const DashboardAdmin = () => {
    // Breadcrumbs
    const routes = [
        {
            path: '',
            breadcrumbName: 'Dashboard',
        },
    ];
    return (
        <div>
            <h5 className="font-semibold text-lg">Dashboard</h5>
            <div className="pt-5"></div>
        </div>
    );
};

export default DashboardAdmin;
