<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('employee_code')->unique();
            $table->string('national_identifier');
            $table->string('name');
            $table->string('email');
            $table->date('birth_date');
            $table->string('gender');
            $table->string('phone');
            $table->string('address');
            $table->string('salary_type'); //GT
            $table->string('basic_salary'); //GT

            $table->string('bank_name'); //GT
            $table->string('branch_location');
            $table->string('bank_account_name');
            $table->string('bank_account_number');
            $table->string('tax_payer_identifier');

            $table->string('employee_picture');
            $table->string('cv');

            $table->date('join_date');
            $table->string('status');
            $table->timestamp('created_date')->nullable()->useCurrent();
            $table->string('created_by');
            $table->timestamp('modified_date')->nullable()->useCurrentOnUpdate();
            $table->string('modified_by');
        });
        Schema::create('employee_certification', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('employee_id');
            $table->string('name');
            $table->string('file');
            $table->string('issuing_organization');
            $table->date('issuing_date');
            $table->date('expired_date');
            $table->string('notes');
            $table->string('status');
            $table->timestamp('created_date')->nullable()->useCurrent();
            $table->string('created_by');
            $table->timestamp('modified_date')->nullable()->useCurrentOnUpdate();
            $table->string('modified_by');
        });
        Schema::create('employee_designation', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('employee_id');
            $table->bigInteger('department');
            $table->bigInteger('section');
            $table->bigInteger('position');
            $table->bigInteger('line_manager1');
            $table->bigInteger('line_manager2');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('notes');
            $table->string('status');
            $table->timestamp('created_date')->nullable()->useCurrent();
            $table->string('created_by');
            $table->timestamp('modified_date')->nullable()->useCurrentOnUpdate();
            $table->string('modified_by');
        });
        Schema::create('employee_resignation', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('employee_id');
            $table->date('submission_date');
            $table->date('approved_date');
            $table->date('last_working_date');
            $table->string('reason');
            $table->string('attachment');
            $table->string('status');
            $table->timestamp('created_date')->nullable()->useCurrent();
            $table->string('created_by');
            $table->timestamp('modified_date')->nullable()->useCurrentOnUpdate();
            $table->string('modified_by');
        });
        Schema::create('employee_termination', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('employee_id');
            $table->date('submission_date');
            $table->date('approved_date');
            $table->date('last_working_date');
            $table->string('reason');
            $table->string('attachment');
            $table->string('status');
            $table->timestamp('created_date')->nullable()->useCurrent();
            $table->string('created_by');
            $table->timestamp('modified_date')->nullable()->useCurrentOnUpdate();
            $table->string('modified_by');
        });
        Schema::create('employee_leave', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('employee_id');
            $table->string('leave_type');
            $table->int('amount');
            $table->int('remaining');
            $table->date('start_date');
            $table->date('end_date');
            $table->timestamp('created_date')->nullable()->useCurrent();
            $table->string('created_by');
            $table->timestamp('modified_date')->nullable()->useCurrentOnUpdate();
            $table->string('modified_by');
        });
        Schema::create('employee_reimbursement', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('employee_id');
            $table->string('reimbursement_type');
            $table->int('amount');
            $table->date('start_date');
            $table->date('end_date');
            $table->timestamp('created_date')->nullable()->useCurrent();
            $table->string('created_by');
            $table->timestamp('modified_date')->nullable()->useCurrentOnUpdate();
            $table->string('modified_by');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('employees');
        Schema::dropIfExists('employee_certification');
        Schema::dropIfExists('employee_designation');
        Schema::dropIfExists('employee_resignation');
        Schema::dropIfExists('employee_termination');
        Schema::dropIfExists('employee_leave');
        Schema::dropIfExists('employee_reimbursement');
    }
};
