<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('global_type', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('is_properties');
            $table->string('status');
            $table->timestamp('created_by')->nullable()->useCurrent();
            $table->string('created_by');
            $table->timestamp('modified_by')->nullable()->useCurrentOnUpdate();
            $table->string('modified_by');
        });
        Schema::create('global_type_detail', function (Blueprint $table) {
            $table->id();
            $table->integer('global_type_id');
            $table->string('key');
            $table->string('value');
            $table->string('status');
            $table->timestamp('created_by')->nullable()->useCurrent();
            $table->string('created_by');
            $table->timestamp('modified_by')->nullable()->useCurrentOnUpdate();
            $table->string('modified_by');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('global_type');
        Schema::dropIfExists('global_type_detail');
    }
};
