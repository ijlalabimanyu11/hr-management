<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('recruitment_jobs', function (Blueprint $table) {
            $table->id();
            $table->string('job_title');
            $table->int('number_of_position');
            $table->string('skill_box');
            $table->string('location');
            $table->string('work_type');
            $table->bigInteger('position');
            $table->string('job_type');
            $table->string('salary_range');
            $table->string('salary_type');
            $table->string('job_description');
            $table->string('job_requirement');
            $table->tinyint('publish_on_career');
            $table->date('publish_start_date');
            $table->date('publish_end_date');
            $table->string('status');
            $table->timestamp('created_date')->nullable()->useCurrent();
            $table->string('created_by');
            $table->timestamp('modified_date')->nullable()->useCurrentOnUpdate();
            $table->string('modified_by');
        });
        Schema::create('job_applications', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('job_id');
            $table->string('full_name');
            $table->date('date_of_birth');
            $table->string('email');
            $table->string('phone');
            $table->string('gender');
            $table->string('cv');
            $table->string('linkedin_url');
            $table->string('reason_to_apply');
            $table->int('rating');
            $table->string('status');
            $table->timestamp('created_date')->nullable()->useCurrent();
            $table->string('created_by');
            $table->timestamp('modified_date')->nullable()->useCurrentOnUpdate();
            $table->string('modified_by');
        });
        Schema::create('interview_schedule', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('job_id');
            $table->bigInteger('application_id');
            $table->string('appllier_name');
            $table->bigInteger('pic');
            $table->bigInteger('department');
            $table->bigInteger('position');
            $table->bigInteger('employee');
            $table->timestamp('start_date');
            $table->timestamp('end_date');
            $table->string('platform');
            $table->string('link');
            $table->string('description');
            $table->string('feedback');
            $table->string('status');
            $table->timestamp('created_date')->nullable()->useCurrent();
            $table->string('created_by');
            $table->timestamp('modified_date')->nullable()->useCurrentOnUpdate();
            $table->string('modified_by');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('recruitment_jobs');
        Schema::dropIfExists('job_applications');
        Schema::dropIfExists('interview_schedule');
    }
};
