<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('attendance', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('employee_id');
            $table->date('date');
            $table->timestamp('start_time');
            $table->timestamp('end_time');
            $table->string('activity');
            $table->string('notes');
            $table->string('correction_cause');
            $table->string('status');
            $table->timestamp('created_date')->nullable()->useCurrent();
            $table->string('created_by');
            $table->timestamp('modified_date')->nullable()->useCurrentOnUpdate();
            $table->string('modified_by');
        });
        Schema::create('attendance_leave', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('employee_id');
            $table->string('leave_type');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('reason');
            $table->tinyint('is_debt');
            $table->string('status');
            $table->timestamp('created_date')->nullable()->useCurrent();
            $table->string('created_by');
            $table->timestamp('modified_date')->nullable()->useCurrentOnUpdate();
            $table->string('modified_by');
        });
        Schema::create('attendance_overtime', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('employee_id');
            $table->string('overtime_type');
            $table->string('cause');
            $table->timestamp('start_date');
            $table->timestamp('end_date');
            $table->string('notes');
            $table->string('status');
            $table->timestamp('created_date')->nullable()->useCurrent();
            $table->string('created_by');
            $table->timestamp('modified_date')->nullable()->useCurrentOnUpdate();
            $table->string('modified_by');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('attendance');
        Schema::dropIfExists('attendance_leave');
        Schema::dropIfExists('attendance_overtime');
    }
};
