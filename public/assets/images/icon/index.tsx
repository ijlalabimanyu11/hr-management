import React from 'react';
import { FiHome, FiLock, FiMenu, FiUser, FiUsers } from 'react-icons/fi';
import { RiCalendarEventLine, RiUserSearchLine, RiMapPinUserLine  } from 'react-icons/ri';
import { PiMoney } from 'react-icons/pi';
import { BiDollarCircle } from 'react-icons/bi';
import { TbChartAreaLine, TbDatabase, TbBuildingSkyscraper } from 'react-icons/tb';
import { MdChecklist, MdOutlineSettings, MdApproval } from 'react-icons/md';
import { VscTypeHierarchySub } from 'react-icons/vsc';
import { GrGroup } from "react-icons/gr";
import { LuUserCircle } from "react-icons/lu";

interface IconProps {
    name: string;
    color?: string;
    size: number;
}

const GlobalIcon: React.FC<IconProps> = ({ name, color, size }) => {
    switch (name) {
        case 'profile':
            return <LuUserCircle color={color} size={size} />;
        case 'dashboard':
            return <FiHome color={color} size={size} />;
        case 'employee-management':
            return <FiUsers color={color} size={size} />;
        case 'attendance':
            return <RiCalendarEventLine color={color} size={size} />;
        case 'finance':
            return <PiMoney color={color} size={size} />;
        case 'payroll':
            return <BiDollarCircle color={color} size={size} />;
        case 'recruitment':
            return <RiUserSearchLine color={color} size={size} />;
        case 'performance':
            return <TbChartAreaLine color={color} size={size} />;
        case 'user':
            return <FiUser color={color} size={size} />;
        case 'menu':
            return <FiMenu color={color} size={size} />;
        case 'role-access':
            return <FiLock color={color} size={size} />;
        case 'data-configuration':
            return <TbDatabase color={color} size={size} />;
        case 'approval-configuration':
            return <MdApproval color={color} size={size} />;
        case 'system-configuration':
            return <MdOutlineSettings color={color} size={size} />;
        case 'approval':
            return <MdChecklist color={color} size={size} />;
        case 'department':
            return <TbBuildingSkyscraper color={color} size={size} />;
        case 'section':
            return <GrGroup color={color} size={size} />;
        case 'organization-structure':
            return <VscTypeHierarchySub color={color} size={size} />;
        case 'position':
            return <RiMapPinUserLine color={color} size={size} />;
        default:
            return null;
    }
};

export default GlobalIcon;
